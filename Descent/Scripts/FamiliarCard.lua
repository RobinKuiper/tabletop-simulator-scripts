--[[LUAStart
Stats = {
  hp = 1,
  attackDice = nil,
  defenseDice = nil,
  color = "Green"
}
diceRoller = nil

ScriptAdder = nil
StorageFinder = nil

function onload(saved_data)
    loadStats(saved_data)
    loadScript()

    for _, obj in pairs(getObjects()) do
        if obj:getName() == "ScriptAdder" then ScriptAdder = obj end
        if obj.getName() == "StorageFinder" then StorageFinder = obj end
    end
end

function onSave()
  return JSON.encode(Stats)
end

function loadStats(saved_data)
  saved_stats = JSON.decode(saved_data)

  if type(saved_stats) == "table" then
    for stat, value in pairs(saved_stats) do Stats[stat] = value end
  end
end

function loadScript()
  local script = self.getLuaScript()
  local xml = script:sub(script:find("StartXML") + 8,
                          script:find("StopXML") - 1)
  self.UI.setXml(xml)
end

function doActionEx(params) doAction(params.player, params.value, params.id) end

function doAction(player, value, id)
    if id == "attack" then
        if Stats.attackDice then roll(Stats.attackDice) end
    elseif id == "defend" then
        if Stats.defenseDice then roll(Stats.defenseDice) end
    end
end

function addDie(player, value, id)
    getDiceRoller()
    diceRoller.call("click_roll", {dieIndex = tonumber(id)})
end

function roll(dieIndexes)
    getDiceRoller()
    for k, i in pairs(dieIndexes) do
        diceRoller.call("click_roll", {dieIndex = i})
    end
    function coroutine_rollDice()
        wait(0.5)
        diceRoller.call("rollDice")
        return 1
    end
    startLuaCoroutine(self, "coroutine_rollDice")
end

function getDiceRoller()
    diceRoller = Global.call("getClosestObjectWithTag",
                             {tag = "DiceRoller", fromObj = self})
end

function spawn(player, value, id)
    local pos = self.getPosition()
    local obj = StorageFinder.call("getFamiliarFigure", {
        name = self.getName(),
        pos = {x = pos.x + 2, y = pos.y, z = pos.z}
    })

    -- obj.setScale({1.55, 1.55, 1.55})
    obj.setRotation({0, 270, 0})
    obj.addTag("FamiliarFigure")

    if Stats and Stats.hp and #Stats.hp > 0 then
        local hp = Stats.hp
        local color = Stats.color and Color.fromString(Stats.color) or self.getColorTint()
        obj.setColorTint(color)
        local stats = {hp = hp}
        ScriptAdder.call("addHpBarScript", {object = obj, data = stats})
    end
end

-- Coroutine delay, in seconds
function wait(time)
    local start = os.time()
    repeat coroutine.yield(0) until os.time() > start + time
end

function onCollisionEnter(info)
    local object = info.collision_object
    if object.hasTag("PlayerMat") then
        local color = object.getDescription()
        self.setColorTint(Color.fromString(color))
        Stats.color = color
    end
end

LUAStop--lua]] --[[XMLStart
<Button id="minion" position="121 -95 -15" value="true" color="white" width="25" height="25" onClick="spawn" />

<Button id="attack" active="false" position="50 -175 -15" value="true" color="#FFFAB8" text="Attack" width="100" height="50" onClick="doAction" rotation="0 0 180" />
<Button id="defend" active="false" position="-50 -175 -15" value="true" color="#FFFAB8" text="Defend" width="100" height="50" onClick="doAction" rotation="0 0 180" />

<Button id="1" active="true" position="-121 -95 -15" value="true" color="red" width="25" height="25" onClick="addDie" />
<Button id="2" active="true" position="-121 -70 -15" value="true" color="yellow" width="25" height="25" onClick="addDie" />
<Button id="3" active="true" position="-121 -45 -15" value="true" color="green" width="25" height="25" onClick="addDie" />
<Button id="4" active="true" position="-121 -20 -15" value="true" color="gray" width="25" height="25" onClick="addDie" />
<Button id="5" active="true" position="-121 5 -15" value="true" color="black" width="25" height="25" onClick="addDie" />
<Button id="6" active="true" position="-121 30 -15" value="true" color="brown" width="25" height="25" onClick="addDie" />
XMLStop--xml]] 
