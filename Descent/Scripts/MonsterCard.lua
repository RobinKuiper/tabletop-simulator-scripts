--[[LUAStart
monsterCard = true
attackDice = {1}
defenseDice = {1}
attackDice2 = {1}
defenseDice2 = {1}
diceRoller = nil

lieutenant = false
act = 1

Data = nil
ScriptAdder = nil
StorageFinder = nil

function onload()
    local script = self.getLuaScript()
    local xml = script:sub(script:find("StartXML")+8, script:find("StopXML")-1)
    self.UI.setXml(xml)

    for _, obj in pairs(getObjects()) do
        if obj:getName() == "Data" then Data = obj end
        if obj:getName() == "ScriptAdder" then ScriptAdder = obj end
        if obj.getName() == "StorageFinder" then StorageFinder = obj end
    end
end

function doActionEx(params) doAction(params.player, params.value, params.id) end

function doAction(player, value, id)
    if id == "attack" then
        roll(attackDice)
    elseif id == "defend" then
        roll(defenseDice)
    elseif id == "attack2" then
        roll(attackDice2)
    elseif id == "defend2" then
        roll(defenseDice2)
    end
end

function addDie(player, value, id)
    getDiceRoller()
    diceRoller.call("click_roll", {dieIndex = tonumber(id)})
end

function roll(dieIndexes)
    getDiceRoller()
    for k, i in pairs(dieIndexes) do
        diceRoller.call("click_roll", {dieIndex = i})
    end
    function coroutine_rollDice()
        wait(0.5)
        diceRoller.call("rollDice")
        return 1
    end
    startLuaCoroutine(self, "coroutine_rollDice")
end

function spawn(player, value, id)
    local objFunc = lieutenant and "getLieutenantFigure" or id == "master" and
                        "getMasterFigure" or "getMinionFigure"
    local pos = self.getPosition()
    local obj = StorageFinder.call(objFunc, {
        name = self.getName(),
        pos = {x = pos.x + 2, y = pos.y, z = pos.z}
    })

    obj.setScale({1.55, 1.55, 1.55})
    obj.setRotation({0, 270, 0})

    if lieutenant then obj.setColorTint({221 / 255, 22 / 255, 224 / 255}) end

    local statFunc = lieutenant and "getLieutenant" or "getMonster"
    local act = act == 1 and "act1" or "act2"
    local stats = Data.call(statFunc, {name = self.getName(), act = act})
    local type = lieutenant and "four" or id
    ScriptAdder.call("addHpBarScript", {object = obj, data = stats[type]})
end

function getDiceRoller() diceRoller = Global.call("getClosestObjectWithTag", { tag = "DiceRoller", fromObj = self }) end

-- Coroutine delay, in seconds
function wait(time)
    local start = os.time()
    repeat coroutine.yield(0) until os.time() > start + time
end

LUAStop--lua]] --[[XMLStart
<Button id="minion" position="121 -95 -15" value="true" color="#FFFAB8" width="25" height="25" onClick="spawn" />
<Button id="master" position="121 -70 -15" value="true" color="red" width="25" height="25" onClick="spawn" />

<Button id="attack" position="50 -175 -15" value="true" color="#FFFAB8" text="Attack" width="100" height="50" onClick="doAction" rotation="0 0 180" />
<Button id="defend" position="-50 -175 -15" value="true" color="#FFFAB8" text="Defend" width="100" height="50" onClick="doAction" rotation="0 0 180" />

<Button id="attack2" position="50 175 -15" value="true" color="#FF1919" text="Attack" width="100" height="50" onClick="doAction" rotation="0 0 180" />
<Button id="defend2" position="-50 175 -15" value="true" color="#FF1919" text="Defend" width="100" height="50" onClick="doAction" rotation="0 0 180" />

<Button id="1" position="-121 -95 -15" value="true" color="red" width="25" height="25" onClick="addDie" />
<Button id="2" position="-121 -70 -15" value="true" color="yellow" width="25" height="25" onClick="addDie" />
<Button id="3" position="-121 -45 -15" value="true" color="green" width="25" height="25" onClick="addDie" />
<Button id="4" position="-121 -20 -15" value="true" color="gray" width="25" height="25" onClick="addDie" />
<Button id="5" position="-121 5 -15" value="true" color="black" width="25" height="25" onClick="addDie" />
<Button id="6" position="-121 30 -15" value="true" color="brown" width="25" height="25" onClick="addDie" />
XMLStop--xml]] 
