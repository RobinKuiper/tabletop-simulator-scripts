--[[LUAStart
Stats = {
    hp = 1,
    stamina = 1,
    speed = 1,
    willpower = 1,
    might = 1,
    knowledge = 1,
    awareness = 1,
    defenseDice = {1},
    color = "Green"
}
Add = true

function onload(saved_data)
  loadStats(saved_data)
  loadScript()
  Wait.frames(setUIStats, 20)
end

function onSave()
  return JSON.encode(Stats)
end

function onCollisionEnter(info)
    local object = info.collision_object
    if object.hasTag("PlayerMat") then
        local color = object.getDescription()
        self.setColorTint(Color.fromString(color))
        Stats.color = color
    end
end

function loadStats(saved_data)
  saved_stats = JSON.decode(saved_data)

  if type(saved_stats) == "table" then
    for stat, value in pairs(saved_stats) do Stats[stat] = value end
  end
end

function loadScript()
  local script = self.getLuaScript()
  local xml = script:sub(script:find("StartXML")+8, script:find("StopXML")-1)
  self.UI.setXml(xml)
end

function setUIStats()
  for stat, value in pairs(Stats) do
        if stat ~= "defenseDice" then
            self.UI.setAttribute(stat, "text", value)
            self.UI.setAttribute(stat, "textColor", "White")
        end
    end
end

function changeStat(object, value, id)
    if Add then
        Stats[id] = Stats[id] + 1
    else
        Stats[id] = Stats[id] - 1
    end

    self.UI.setAttribute(id, "text", Stats[id])
    self.UI.setAttribute(id, "textColor", "White")
end

function toggle(object, value, id)
    if Add then
        Add = false
        self.UI.setAttribute("toggle", "text", "-")
    else
        Add = true
        self.UI.setAttribute("toggle", "text", "+")
    end
end

function defend(player, value, id)
    local diceRoller = Global.call("getClosestObjectWithTag",
                                   {tag = "DiceRoller", fromObj = self})

    for i, die in ipairs(Stats.defenseDice) do
        diceRoller.call("click_roll", {dieIndex = die, color = player.color})
    end

    function coroutine_rollDice()
        wait(0.5)
        diceRoller.call("rollDice")
        return 1
    end
    startLuaCoroutine(self, "coroutine_rollDice")
end

-- Coroutine delay, in seconds
function wait(time)
    local start = os.time()
    repeat coroutine.yield(0) until os.time() > start + time
end

-- function onObjectRotate(object, spin, flip, color, old_spin, old_flip)
--     if object ~= self then return end

--     local normal = -21
--     local flipped = 20

--     if flip ~= 0 then
--         for _, v in pairs(StatIndexes) do
--             local currentPosition = self.UI.getAttribute(v, "position")
--             local newPosition = string.gsub(currentPosition, flipped, normal)
--             self.UI.setAttribute(v, "position", newPosition)
--         end
--     else
--         for _, v in pairs(StatIndexes) do
--             local currentPosition = self.UI.getAttribute(v, "position")
--             local newPosition = string.gsub(currentPosition, normal, flipped)
--             self.UI.setAttribute(v, "position", newPosition)
--         end
--     end
-- end
LUAStop--lua]] --[[XMLStart
<Defaults>
  <Button width="20" height="20" onClick="changeStat" rotation="0 0 180" textColor="white" fontSize="8"  />
</Defaults>

<Button id="speed" position="-1 -55 -21" color="#436044" text="4" textColor="white" />
<Button id="hp" position="-11.5 -19 -21" color="#764C45" text="10" textColor="white" />
<Button id="stamina" position="-11.5 18 -21" color="#68543C" text="4" textColor="white" />

<Button id="willpower" position="95 88 -21" width="15" height="15" color="#603622" text="2" textColor="white" />
<Button id="might" position="86 68.5 -21" width="15" height="15" color="#441C18" text="3" textColor="white" />
<Button id="knowledge" position="49 68.5 -21" width="15" height="15" color="#48324F" text="2" textColor="white" />
<Button id="awareness" position="40 88 -21" width="15" height="15" color="#4C4D26" text="4" textColor="white" />

<Button id="toggle" position="115 90 -21" color="white" text="+" onClick="toggle" textColor="black" />

<Button id="defend" position="-100 90 -21" width="50" height="20" color="white" text="Defend" onClick="defend" textColor="black" />
XMLStop--xml]] 
