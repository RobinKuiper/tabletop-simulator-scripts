--[[LUAStart
dice = {1}
attack = true
defend = false
useCardDefenseDie = true

diceRoller = nil
heroCard = nil

function onload()
    local script = self.getLuaScript()
    local xml = script:sub(script:find("StartXML")+8, script:find("StopXML")-1)
    -- if attack then xml = string.gsub(xml, "ATTACK", "true") end
    -- if defend then xml = string.gsub(xml, "DEFENSE", "true") end
    self.UI.setXml(xml)
end

function doAction(player, value, id)
    getDiceRoller()
    if id == "action" then roll(player.color) end
end

function doActionEx(params)
    local player = params.player
    doAction(player, "", "action")
end

function addDie(player, value, id)
    getDiceRoller()
    diceRoller.call("click_roll",
                    {dieIndex = tonumber(id), color = player.color})
end

function toggleuseCardDefenseDie(player, value, id)
  if useCardDefenseDie then 
    useCardDefenseDie = false
    self.UI.setAttribute("useCardDefenseDie", "color", "red")
  else
    useCardDefenseDie = true
    self.UI.setAttribute("useCardDefenseDie", "color", "green")
  end
end

function roll(color)
    local heroCard = Global.call("getClosestObjectWithTag", { tag = "HeroCard", fromObj = self})
    local heroStats = heroCard.call("getStats")
    if defend then
      for i, die in ipairs(heroStats.defenseDice) do
        diceRoller.call("click_roll", {dieIndex = die, color = color})
      end
    end
    if attack or (defend and useCardDefenseDie) then
      for k, i in pairs(dice) do
          diceRoller.call("click_roll", {dieIndex = i, color = color})
      end
    end

    function coroutine_rollDice()
        wait(0.5)
        diceRoller.call("rollDice")
        return 1
    end
    startLuaCoroutine(self, "coroutine_rollDice")
end

function getDiceRoller() diceRoller = Global.call("getClosestObjectWithTag", { tag = "DiceRoller", fromObj = self }) end

-- Coroutine delay, in seconds
function wait(time)
    local start = os.time()
    repeat coroutine.yield(0) until os.time() > start + time
end
LUAStop--lua]] --[[XMLStart
<Button id="action" text="Attack" position="45.5 -175 -15" value="true" color="#FFFAB8" width="100" height="50" onClick="doAction" rotation="0 0 180" />

<!-- -187.4 -->
<Button id="2" active="ATTACK" position="-20.5 -175 -15" width="25" height="50" onClick="addDie(GUID)" color="red" />
<Button id="3" active="ATTACK" position="-67.5 -175 -15" width="25" height="50" onClick="addDie(GUID)" color="yellow" />
<Button id="4" active="ATTACK" position="-44 -175 -15" width="25" height="50" onClick="addDie(GUID)" color="green" />

<Button id="5" active="DEFENSE" position="-20.5 -175 -15" width="25" height="50" onClick="addDie(GUID)" color="gray" />
<Button id="6" active="DEFENSE" position="-44 -175 -15" width="25" height="50" onClick="addDie(GUID)" color="black" />
<Button id="7" active="DEFENSE" position="-67.5 -175 -15" width="25" height="50" onClick="addDie(GUID)" color="brown" />
<Button id="useCardDefenseDie" active="DEFENSE" position="-91 -175 -15" width="25" height="50" onClick="toggleuseCardDefenseDie(GUID)" color="green" />
XMLStop--xml]] 
