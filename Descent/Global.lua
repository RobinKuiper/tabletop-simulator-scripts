require("Descent/UI/DiceRoller")
require("Descent/UI/Monsters")
require("Descent/UI/Players")
require("Descent/UI/QuickSlots")

figures = {}
monsters = {}
lock = false
xml = ""

playerTableEntry =
    '<TableLayout id="GUID_table" minHeight="80" active="true" columnWidths="25 25 25 25 25 25 25 25"><Row><Cell padding="8 8 8 8"><Image id="GUID_koImg" image="KnockedOut" active="false" /></Cell><Cell columnSpan="7"><Text id="GUID_name" clickable="true" onClick="UIButton" alignment="MiddleLeft" fontSize="16" text="PLAYERNAME" color="PLAYERCOLOR" /></Cell></Row><Row><Cell><button id="sub" text="-" onClick="modifyHP(GUID)" /></Cell><Cell columnSpan="6"><ProgressBar id="GUID_hpBar" percentage="HPPERC" showPercentageText="false" fillImageColor="#B22A28" color="#000000E0"><Text id="GUID_hpText" clickable="true" onClick="UIButton" class="HPtext" text="HPTEXT" /></ProgressBar></Cell><Cell><button id="add" text="+" onClick="modifyHP(GUID)" /></Cell></Row><Row active="STAMINAACTIVE"><Cell><button id="sub" text="-" onClick="modifyStamina(GUID)" /></Cell><Cell columnSpan="6"><ProgressBar id="GUID_staminaBar" percentage="STAMINAPERC" showPercentageText="false" fillImageColor="#2E397E" color="#000000E0"><Text id="GUID_staminaText" clickable="true" onClick="UIButton" class="HPtext" text="STAMINATEXT" /></ProgressBar></Cell><Cell><button id="add" text="+" onClick="modifyStamina(GUID)" /></Cell></Row><!-- <Row><Cell /><Cell /><Cell><Text id="GUID_willpower" color="orange" clickable="true" onClick="UIButton" alignment="MiddleCenter" fontSize="13" text="PLAYERWILLPOWER" /></Cell><Cell><Text id="GUID_might" color="red" clickable="true" onClick="UIButton" alignment="MiddleCenter" fontSize="13" text="PLAYERMIGHT" /></Cell><Cell><Text id="GUID_knowledge" color="purple" clickable="true" onClick="UIButton" alignment="MiddleCenter" fontSize="13" text="PLAYERKNOWLEDGE" /></Cell><Cell><Text id="GUID_awareness" color="green" clickable="true" onClick="UIButton" alignment="MiddleCenter" fontSize="13" text="PLAYERAWARENESS" /></Cell><Cell /><Cell /></Row> --></TableLayout><PlayerPlaceholder />'

monsterTableEntry =
    '<HorizontalLayout spacing="0"><VerticalLayout><TableLayout id="GUID_mTable" minHeight="50" active="true" columnWidths="25 25 25 25"><Row><Cell columnSpan="5"><Text id="GUID_mName" clickable="true" onClick="UIButton" alignment="MiddleLeft" fontSize="16" text="MONSTERNAME" color="White"></Text></Cell></Row><Row><Cell><Button id="attack" iconColor="black" icon="Attack-W" onClick="attack(GUID)" color="white" /></Cell><Cell columnSpan="1"><Button id="defend" icon="Shield-W" onClick="defend(GUID)" color="white" iconColor="black" /></Cell><Cell><Button active="false" id="attack2" icon="Attack-W" onClick="attack(GUID)" color="Red" /></Cell><Cell><Button active="false" id="defend2" icon="Shield-W" onClick="defend(GUID)" color="Red" /></Cell></Row></TableLayout></VerticalLayout></HorizontalLayout><MonsterPlaceholder />'

function onLoad(saved_data)
    xml = self.UI.getXml()

    Wait.frames(sync, 10)
end

function onObjectDestroy(dying_object)
    if dying_object.hasTag("MonsterCard") or dying_object.hasTag("HeroFigure") or
        dying_object.hasTag("FamiliarFigure") then sync() end
end

-- Coroutine delay, in seconds
function wait(time)
    local start = os.time()
    repeat coroutine.yield(0) until os.time() > start + time
end

function getUnusedDiceRoller(player)
    for i, object in ipairs(getObjectsWithTag("DiceRoller")) do
        if not object.call("getInUse") or object.call("getInUse") ==
            player.color then diceRoller = object end
    end
end

function sync(player, value, id)
    if lock then return end

    figures = {}
    monsters = {}
    for k, obj in pairs(getObjects()) do
        -- TODO: Filter Familiars (Maybe getObjectsWithTag)
        local script = obj.getLuaScript()
        if obj.hasTag("HeroFigure") or obj.hasTag("FamiliarFigure") then
            table.insert(figures, {
                guid = obj.getGUID(),
                name = obj.getName(),
                hpText = obj.UI.getAttribute("hpText", "Text"),
                staminaText = obj.UI.getAttribute("extraText", "Text"),
                colorTint = obj.getColorTint(),
                familiar = obj.UI.getAttribute("extraText", "Text") == "0/0"
            })
        elseif obj.hasTag("MonsterCard") then
            table.insert(monsters, {
                guid = obj.getGUID(),
                name = obj.getName(),
                secondAttack = not string.match(script, "attackDice2 = {0}"),
                secondDefense = not string.match(script, "defenseDice2 = {0}")
            })
        end
    end

    table.sort(figures, function(a, b)
        local hexColora = string.format("%02x%02x%02x", a.colorTint.r * 255,
                                        a.colorTint.g * 255, a.colorTint.b * 255)
        local hexColorb = string.format("%02x%02x%02x", b.colorTint.r * 255,
                                        b.colorTint.g * 255, b.colorTint.b * 255)
        local familiara = a.familiar and 1 or 0
        local familiarb = b.familiar and 1 or 0

        if hexColora > hexColorb then
            return true
        elseif hexColora < hexColorb then
            return false
        else
            return familiara < familiarb
        end
    end)

    table.sort(monsters, function(a, b) return a.name < b.name end)

    if value then broadcastToAll("Syncing complete", "Green") end

    updateTable()
    updateQuickSlots()
end

function updateTable()
    local newXml = xml
    for k, v in pairs(figures) do
        local name = v.name
        local guid = v.guid
        local colorTint = v.colorTint
        local hpText = v.hpText
        local staminaText = v.staminaText

        local hpMin, hpMax = hpText:match("([^/]+)/([^/]+)")
        local hpPerc = (hpMin / hpMax) * 100

        local staminaMin, staminaMax = staminaText:match("([^/]+)/([^/]+)")
        local staminaPerc = (staminaMin / staminaMax) * 100

        local newEntry = playerTableEntry
        newEntry = string.gsub(newEntry, "GUID", guid)

        newEntry = string.gsub(newEntry, "PLAYERNAME", name)
        newEntry = string.gsub(newEntry, "PLAYERCOLOR",
                               "rgba(" .. colorTint.r .. ", " .. colorTint.g ..
                                   ", " .. colorTint.b .. ", " .. colorTint.a ..
                                   ")")
        newEntry = string.gsub(newEntry, "HPTEXT", "♥ " .. hpText)
        newEntry = string.gsub(newEntry, "HPPERC", hpPerc)
        newEntry = string.gsub(newEntry, "STAMINATEXT", "❣ " .. staminaText)
        newEntry = string.gsub(newEntry, "STAMINAPERC", staminaPerc)

        if staminaText and string.len(staminaText) > 0 and staminaText ~= "0/0" then
            newEntry = string.gsub(newEntry, "STAMINAACTIVE", "true")
        else
            newEntry = string.gsub(newEntry, "STAMINAACTIVE", "false")
        end

        if hpPerc == 0 then
            newEntry = string.gsub(newEntry,
                                   'image="KnockedOut" active="false"',
                                   'image="KnockedOut" active="true"')
        else
            newEntry = string.gsub(newEntry, 'image="KnockedOut" active="true"',
                                   'image="KnockedOut" active="false"')
        end

        newXml = string.gsub(newXml, "<PlayerPlaceholder />", newEntry)
    end

    for k, v in pairs(monsters) do
        local name = v.name
        local guid = v.guid
        local secondAttack = v.secondAttack
        local secondDefense = v.secondDefense

        local newEntry = monsterTableEntry
        newEntry = string.gsub(newEntry, "GUID", guid)
        newEntry = string.gsub(newEntry, "MONSTERNAME", name)

        if secondAttack then
            newEntry = string.gsub(newEntry, 'active="false"', 'active="true"')
        else
            newEntry = string.gsub(newEntry, 'white', 'purple')
        end

        if secondDefense then
            newEntry = string.gsub(newEntry, 'active="false"', 'active="true"')
        else
            newEntry = string.gsub(newEntry, "white", "purple")
        end

        newXml = string.gsub(newXml, "<MonsterPlaceholder />", newEntry)
    end
    self.UI.setXml(newXml)
end

function getClosestObjectWithName(params)
    local name = params.name
    local objects = getObjects()
    local closestObject = nil
    local closestDistance = nil
    local currentPosition = params.fromObj.getPosition()

    for i, object in ipairs(objects) do
        if object.getName() == name then
            local distance =
                (object.getPosition() - currentPosition):sqrMagnitude()
            if closestDistance == nil or distance < closestDistance then
                closestObject = object
                closestDistance = distance
            end
        end
    end

    return closestObject
end

function getClosestObjectWithTag(params)
    local tag = params.tag
    local objects = getObjectsWithTag(tag)
    local closestObject = nil
    local closestDistance = nil
    local currentPosition = params.fromObj ~= nil and
                                params.fromObj.getPosition() or
                                params.fromPosition

    for i, object in ipairs(objects) do
        local distance = (object.getPosition() - currentPosition):sqrMagnitude()
        if closestDistance == nil or distance < closestDistance then
            closestObject = object
            closestDistance = distance
        end
    end

    return closestObject
end

function getObjectWithName(name)
    for k, v in pairs(getObjects()) do
        if v.getName() == name then return v end
    end
end
