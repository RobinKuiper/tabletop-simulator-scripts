inputValue = ""
findOnlyOne = false

options = {
    act1 = true,
    act2 = false,
    master = true,
    minion = true,
    cards = true,
    lieutenant = false
}

ScriptAdder = nil
StorageFinder = nil
Data = nil
CardHolder = nil

Act = "act1"

function onLoad(saved_data)
    local data = JSON.decode(saved_data)
    if data then
        for k, v in pairs(options) do options[k] = data[k] end

        if options.act1 then
            setCheckboxOn("act1")
            setCheckboxOff("act2")
        else
            setCheckboxOn("act2")
            setCheckboxOff("act1")
        end

        if options.master or options.minion then
            setCheckboxOff("lieutenant")
            if options.master then setCheckboxOn("master") end

            if options.minion then setCheckboxOn("minion") end
        elseif options.lieutenant then
            setCheckboxOn("lieutenant")
            setCheckboxOff("master")
            setCheckboxOff("minion")
        end

        if options.cards then setCheckboxOn("cards") end
    end
    for _, v in pairs(getObjects()) do
        if v.getName() == "ScriptAdder" then ScriptAdder = v end
        if v.getName() == "StorageFinder" then StorageFinder = v end
        if v.getName() == "Data" then Data = v end
    end

    self.createButton({
        click_function = "search",
        function_owner = self,
        label = "Search",
        position = {0, 0.1, 0.35},
        scale = {0.3, 0.3, 0.3},
        width = 3600,
        height = 700,
        font_size = 600,
        color = {0.7573, 0.7573, 0.7573, 0},
        font_color = {0, 0, 0, 0}
    })

    self.createInput({
        input_function = "input",
        function_owner = self,
        label = "Monster Name",
        position = {0, 0.1, -0.25},
        scale = {0.5, 0.5, 0.5},
        width = 4000,
        height = 400,
        font_size = 200,
        alignment = 3,
        font_color = {0, 0, 0, 95},
        color = {0, 0, 0, 0}
    })
end

function onSave() return JSON.encode(options) end

function isempty(s) return s == nil or s == '' end

function input(obj, player_clicker_color, input_value, selected)
    if not selected then inputValue = input_value end
    if input_value:sub(-1) == "\n" then
        inputValue = input_value:sub(1, -2)
        Wait.time(function()
            self.editInput({index = 0, value = inputValue})
        end, 0.2)
        search()
    end
end

function search()
    if isempty(inputValue) then return end

    getCardHolder()

    local obj = nil
    local pos = self.getPosition()
    local cardHolder = nil
    pos = {x = pos.x, y = pos.y - 3, z = pos.z}
    local positions = {
        card1 = {x = pos.x - 20, y = pos.y, z = pos.z + 1.7},
        card2 = {x = pos.x - 20, y = pos.y, z = pos.z - 1.7},
        minion = {x = pos.x - 16, y = pos.y, z = pos.z - 1},
        master = {x = pos.x - 16, y = pos.y, z = pos.z + 1},
        lieutenant = {x = pos.x - 16, y = pos.y, z = pos.z}
    }
    if CardHolder ~= nil then
        cardHolder = CardHolder.call("GetFreePosition")
        if cardHolder then
            local chPositions = cardHolder.positions
            positions = {
                card1 = {
                    x = chPositions.first[1],
                    y = chPositions.first[2],
                    z = chPositions.first[3]
                },
                card2 = {
                    x = chPositions.second[1],
                    y = chPositions.second[2],
                    z = chPositions.second[3]
                },
                minion = {
                    x = chPositions.first[1] + 2,
                    y = chPositions.first[2],
                    z = chPositions.first[3] - 1
                },
                master = {
                    x = chPositions.first[1] + 2,
                    y = chPositions.first[2],
                    z = chPositions.first[3] + 1
                },
                lieutenant = {
                    x = chPositions.first[1] + 2,
                    y = chPositions.first[2],
                    z = chPositions.first[3]
                }
            }
        end
    end
    Act = options.act1 and "act1" or "act2"

    local getObjFunc = "getMonsterCard"
    local getStatFunc = "getMonster"

    if options.lieutenant then
        getObjFunc = "getLieutenantCard"
        getStatFunc = "getLieutenant"
    end

    if options.cards then
        obj = StorageFinder.call(getObjFunc, {name = inputValue, act = Act})
        obj.setPosition(positions.card1)
        obj.use_grid = false

        if CardHolder ~= nil then
            CardHolder.call("ClaimFreePosition",
                            {guid = obj.getGUID(), index = cardHolder.index})
        end

        if options.lieutenant then
            obj.setRotation({0, 270, 0})
            obj.setScale({1.7, 1, 1.7})
        end

        local clone = obj.clone({position = clonePos})
        clone.setPosition(positions.card2)
        clone.flip()

        obj.addTag("MonsterCard")

        local monster =
            Data.call(getStatFunc, {name = obj.getName(), act = Act})

        if not monster then
            broadcastToAll("Couldn't find monster stats for monster: " ..
                               obj.getName())
        end

        local stats = {}
        if options.lieutenant then
            stats.lieutenant = true
            stats.attackDice = table.concat(monster["four"].attackDice, ",")
            stats.defenseDice = table.concat(monster["four"].defenseDice, ",")
        else
            stats.attackDice = table.concat(monster["minion"].attackDice, ",")
            stats.defenseDice = table.concat(monster["minion"].defenseDice, ",")
            stats.attackDice2 = table.concat(monster["master"].attackDice, ",")
            stats.defenseDice2 =
                table.concat(monster["master"].defenseDice, ",")
        end
        -- addCardScript(newObj)
        ScriptAdder.call("addMonsterCardScript",
                         {object = obj, data = stats, act = "act1" and 1 or 2})
    end

    if options.minion then
        obj = StorageFinder.call("getMinionFigure", {name = inputValue})
        obj.setPosition(positions.minion)
        obj.setScale({1.55, 1.55, 1.55})
        obj.setRotation({0, 270, 0})

        addScript(obj, getStatFunc, "minion")
    end

    if options.master then
        obj = StorageFinder.call("getMasterFigure", {name = inputValue})
        obj.setPosition(positions.master)
        obj.setScale({1.55, 1.55, 1.55})
        obj.setRotation({0, 270, 0})

        addScript(obj, getStatFunc, "master")
    end

    if options.lieutenant then
        obj = StorageFinder.call("getLieutenantFigure", {name = inputValue})
        obj.setPosition(positions.lieutenant)
        obj.setColorTint({221 / 255, 22 / 255, 224 / 255})
        obj.setScale({1.55, 1.55, 1.55})
        obj.setRotation({0, 270, 0})

        addScript(obj, getStatFunc, "four")
    end

    Wait.time(function() self.editInput({index = 0, value = ""}) end, 1)
    Wait.frames(function() Global.call("sync") end, 10)
end

function addScript(obj, func, type)
    local monster = Data.call(func, {name = obj.getName(), act = Act})
    local stats = {hp = 10}
    if (monster[type]) then
        stats.hp = monster[type].hp
    else
        broadcastToAll(obj.getName() ..
                           " not in monster list, defaulting to 10 hp.")
    end

    ScriptAdder.call("addHpBarScript", {object = obj, data = stats})
end

function getCardHolder()
    for _, v in pairs(getObjects()) do
        if v.getName() == "CardHolder" then CardHolder = v end
    end
end

function toggleCheckBox(player, value, id)
    if self.UI.getAttribute(id, "value") == "false" then
        setCheckboxOn(id)

        if id == "act1" then
            setCheckboxOff("act2")
        elseif id == "act2" then
            setCheckboxOff("act1")
        end

        if id == "lieutenant" then
            setCheckboxOff("master")
            setCheckboxOff("minion")
        elseif id == "master" or id == "minion" then
            setCheckboxOff("lieutenant")
        end
    else
        setCheckboxOff(id)

        if id == "act1" then
            setCheckboxOn("act2")
        elseif id == "act2" then
            setCheckboxOn("act1")
        end
    end
end

function setCheckboxOn(id)
    self.UI.setAttribute(id, "value", "true")
    self.UI.setAttribute(id, "color", "#154c79")
    options[id] = true
end

function setCheckboxOff(id)
    self.UI.setAttribute(id, "value", "false")
    self.UI.setAttribute(id, "color", "white")
    options[id] = false
end
