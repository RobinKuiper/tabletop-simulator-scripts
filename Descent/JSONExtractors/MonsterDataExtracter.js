// https://github.com/any2cards/d2e/blob/master/data/monsters.js
const monsters = [
  {
    "name": "Barghest",
    "points": 0,
    "act": "I",
    "attack": "Melee",
    "characteristics": [
      "Minion: Speed: 4, Health: 4, Defense: 5, Attack: 1 2, Abilities: Action: Howl, Surge: +1 Heart",
      "Master: Speed: 4, Health: 6, Defense: 5, Attack: 1 2, Abilities: Night Stalker, Action: Howl, Surge: +2 Hearts"
    ],
    "expansion": "Base Game",
    "image": "monsters/barghest-bg-act1-front.png",
    "xws": "barghest"
  },
  {
    "name": "Barghest",
    "points": 1,
    "act": "I",
    "traits": [
      "Wilderness",
      "Dark"
    ],
    "ability rules": [
      "Night Stalker: If attacked by any non-adjacent hero, this monster may add 1 7 die to its defense roll.",
      "Howl: Each hero within 3 spaces of this monster must test Willpower. Each hero that fails suffers 1 Fatigue."
    ],
    "group size": [
      "2 Heroes: 1,1",
      "3 Heroes: 2,1",
      "4 Heroes: 3,1"
    ],
    "expansion": "Base Game",
    "image": "monsters/barghest-bg-act1-back.png",
    "xws": "barghest"
  },
  {
    "name": "Barghest",
    "points": 2,
    "act": "II",
    "attack": "Melee",
    "characteristics": [
      "Minion: Speed: 4, Health: 6, Defense: 6, Attack: 1 2, Abilities: Action: Howl, Surge: +2 Hearts",
      "Master: Speed: 4, Health: 8, Defense: 6, Attack: 1 2 3, Abilities: Night Stalker, Action: Howl, Surge: +2 Hearts"
    ],
    "expansion": "Base Game",
    "image": "monsters/barghest-bg-act2-front.png",
    "xws": "barghest"
  },
  {
    "name": "Barghest",
    "points": 3,
    "act": "II",
    "traits": [
      "Wilderness",
      "Dark"
    ],
    "ability rules": [
      "Night Stalker: If attacked by any non-adjacent hero, this monster may add 1 7 die to its defense roll.",
      "Howl: Each hero within 3 spaces of this monster must test Willpower. Each hero that fails suffers 1 Fatigue."
    ],
    "group size": [
      "2 Heroes: 1,1",
      "3 Heroes: 2,1",
      "4 Heroes: 3,1"
    ],
    "expansion": "Base Game",
    "image": "monsters/barghest-bg-act2-back.png",
    "xws": "barghest"
  },
  {
    "name": "Cave Spider",
    "points": 4,
    "act": "I",
    "attack": "Melee",
    "characteristics": [
      "Minion: Speed: 4, Health: 3, Defense: 5, Attack: 1 3, Abilities: Surge: Poison, Surge: +1 Heart",
      "Master: Speed: 4, Health: 5, Defense: 5, Attack: 1 3, Abilities: Web, Surge: Poison, Surge: +2 Hearts"
    ],
    "expansion": "Base Game",
    "image": "monsters/cave-spider-bg-act1-front.png",
    "xws": "cavespider"
  },
  {
    "name": "Cave Spider",
    "points": 5,
    "act": "I",
    "traits": [
      "Wilderness",
      "Cave"
    ],
    "ability rules": [
      "Web: Each hero adjacent to this monster must suffer 1 Fatigue to move out of his current space; this is in addition to any other Fatigue suffe2 to move.",
      "Poison: If this attack deals at least 1 Heart (after the defense roll), the target is Poisoned."
    ],
    "group size": [
      "2 Heroes: 2,1",
      "3 Heroes: 3,1",
      "4 Heroes: 4,1"
    ],
    "expansion": "Base Game",
    "image": "monsters/cave-spider-bg-act1-back.png",
    "xws": "cavespider"
  },
  {
    "name": "Cave Spider",
    "points": 6,
    "act": "II",
    "attack": "Melee",
    "characteristics": [
      "Minion: Speed: 4, Health: 5, Defense: 5, Attack: 1 3 3, Abilities: Surge: Poison, Surge: +2 Hearts",
      "Master: Speed: 4, Health: 7, Defense: 5, Attack: 1 3 3, Abilities: Web, Surge: Poison, Surge: +2 Hearts, Surge: +1 Heart"
    ],
    "expansion": "Base Game",
    "image": "monsters/cave-spider-bg-act2-front.png",
    "xws": "cavespider"
  },
  {
    "name": "Cave Spider",
    "points": 7,
    "act": "II",
    "traits": [
      "Wilderness",
      "Cave"
    ],
    "ability rules": [
      "Web: Each hero adjacent to this monster must suffer 1 Fatigue to move out of his current space; this is in addition to any other Fatigue suffe2 to move.",
      "Poison: If this attack deals at least 1 Heart (after the defense roll), the target is Poisoned."
    ],
    "group size": [
      "2 Heroes: 2,1",
      "3 Heroes: 3,1",
      "4 Heroes: 4,1"
    ],
    "expansion": "Base Game",
    "image": "monsters/cave-spider-bg-act2-back.png",
    "xws": "cavespider"
  },
  {
    "name": "Elemental",
    "points": 8,
    "act": "I",
    "attack": "Range",
    "characteristics": [
      "Minion: Speed: 4, Health: 4, Defense: 6, Attack: 1 2, Abilities: Surge: Fire, Surge: Earth, Surge: Water, Surge: Air",
      "Master: Speed: 4, Health: 6, Defense: 6, Attack: 1 2, Abilities: Surge: Fire, Surge: Earth, Surge: Water, Surge: Air"
    ],
    "expansion": "Base Game",
    "image": "monsters/elemental-bg-act1-front.png",
    "xws": "elemental"
  },
  {
    "name": "Elemental",
    "points": 9,
    "act": "I",
    "traits": [
      "Cold",
      "Hot"
    ],
    "ability rules": [
      "Fire: Perform an attack targeting all figures adjacent to this monster. Each figure rolls defense dice separately.",
      "Earth: Each hero adjacent to this monster must test Awareness. Each hero that fails is Immobilized.",
      "Water: Each hero adjacent to this monster must test Willpower. Each hero that fails suffers 2 Fatigue.",
      "Air: Until the start of your next turn, this monster cannot be affected by any attacks, and heroes may move through it."
    ],
    "group size": [
      "2 Heroes: 1,0",
      "3 Heroes: 0,1",
      "4 Heroes: 1,1"
    ],
    "expansion": "Base Game",
    "image": "monsters/elemental-bg-act1-back.png",
    "xws": "elemental"
  },
  {
    "name": "Elemental",
    "points": 10,
    "act": "II",
    "attack": "Range",
    "characteristics": [
      "Minion: Speed: 4, Health: 8, Defense: 6 7, Attack: 1 3 2, Abilities: Surge: Fire, Surge: Earth, Surge: Water, Surge: Air",
      "Master: Speed: 4, Health: 10, Defense: 6 7, Attack: 1 3 2, Abilities: Surge: Fire, Surge: Earth, Surge: Water, Surge: Air"
    ],
    "expansion": "Base Game",
    "image": "monsters/elemental-bg-act2-front.png",
    "xws": "elemental"
  },
  {
    "name": "Elemental",
    "points": 11,
    "act": "II",
    "traits": [
      "Cold",
      "Hot"
    ],
    "ability rules": [
      "Fire: Perform an attack targeting all figures adjacent to this monster. Each figure rolls defense dice separately.",
      "Earth: Each hero adjacent to this monster must test Awareness. Each hero that fails is Immobilized.",
      "Water: Each hero adjacent to this monster must test Willpower. Each hero that fails suffers 2 Fatigue.",
      "Air: Until the start of your next turn, this monster cannot be affected by any attacks, and heroes may move through it."
    ],
    "group size": [
      "2 Heroes: 1,0",
      "3 Heroes: 0,1",
      "4 Heroes: 1,1"
    ],
    "expansion": "Base Game",
    "image": "monsters/elemental-bg-act2-back.png",
    "xws": "elemental"
  },
  {
    "name": "Ettin",
    "points": 12,
    "act": "I",
    "attack": "Melee",
    "characteristics": [
      "Minion: Speed: 3, Health: 5, Defense: 5 5, Attack: 1 2, Abilities: Reach, Surge: +2 Hearts",
      "Master: Speed: 3, Health: 8, Defense: 5 5, Attack: 1 2, Abilities: Reach, Action: Throw, Surge: +3 Hearts"
    ],
    "expansion": "Base Game",
    "image": "monsters/ettin-bg-act1-front.png",
    "xws": "ettin"
  },
  {
    "name": "Ettin",
    "points": 13,
    "act": "I",
    "traits": [
      "Mountain",
      "Cave"
    ],
    "ability rules": [
      "Reach: This monster may attack targets up to 2 spaces away.",
      "Throw: Choose a hero adjacent to this monster. That hero must test Might. If he fails, remove the hero from the map, then place him on any empty space within 3 spaces of his original space. He counts as entering that space. Then the hero suffers 1 Heart."
    ],
    "group size": [
      "2 Heroes: 1,0",
      "3 Heroes: 0,1",
      "4 Heroes: 1,1"
    ],
    "expansion": "Base Game",
    "image": "monsters/ettin-bg-act1-back.png",
    "xws": "ettin"
  },
  {
    "name": "Ettin",
    "points": 14,
    "act": "II",
    "attack": "Melee",
    "characteristics": [
      "Minion: Speed: 3, Health: 7, Defense: 6 5, Attack: 1 2 2, Abilities: Reach, Surge: +1 Heart",
      "Master: Speed: 3, Health: 9, Defense: 6 5, Attack: 1 2 2, Abilities: Reach, Action: Throw, Surge: +2 Hearts"
    ],
    "expansion": "Base Game",
    "image": "monsters/ettin-bg-act2-front.png",
    "xws": "ettin"
  },
  {
    "name": "Ettin",
    "points": 15,
    "act": "II",
    "traits": [
      "Mountain",
      "Cave"
    ],
    "ability rules": [
      "Reach: This monster may attack targets up to 2 spaces away.",
      "Throw: Choose a hero adjacent to this monster. That hero must test Might. If he fails, remove the hero from the map, then place him on any empty space within 3 spaces of his original space. He counts as entering that space. Then the hero suffers 1 Heart."
    ],
    "group size": [
      "2 Heroes: 1,0",
      "3 Heroes: 0,1",
      "4 Heroes: 1,1"
    ],
    "expansion": "Base Game",
    "image": "monsters/ettin-bg-act2-back.png",
    "xws": "ettin"
  },
  {
    "name": "Flesh Moulder",
    "points": 16,
    "act": "I",
    "attack": "Range",
    "characteristics": [
      "Minion: Speed: 4, Health: 4, Defense: 5, Attack: 1 3, Abilities: Surge: Mend 1, Surge: +1 Heart",
      "Master: Speed: 4, Health: 5, Defense: 5, Attack: 1 3, Abilities: Action: Heal, Surge: Mend 2, Surge: +2 Hearts"
    ],
    "expansion": "Base Game",
    "image": "monsters/flesh-moulder-bg-act1-front.png",
    "xws": "fleshmoulder"
  },
  {
    "name": "Flesh Moulder",
    "points": 17,
    "act": "I",
    "traits": [
      "Cursed",
      "Civilized"
    ],
    "ability rules": [
      "Heal: Choose a monster within 3 spaces of this monster and roll 1 2 power die. The chosen monster recovers Hearts equal to the Hearts rolled.",
      "Mend X: This monster recovers X Hearts."
    ],
    "group size": [
      "2 Heroes: 1,1",
      "3 Heroes: 2,1",
      "4 Heroes: 3,1"
    ],
    "expansion": "Base Game",
    "image": "monsters/flesh-moulder-bg-act1-back.png",
    "xws": "fleshmoulder"
  },
  {
    "name": "Flesh Moulder",
    "points": 18,
    "act": "II",
    "attack": "Range",
    "characteristics": [
      "Minion: Speed: 4, Health: 5, Defense: 5 7, Attack: 1 3, Abilities: Surge: Mend 2, Surge: +2 Hearts",
      "Master: Speed: 4, Health: 7, Defense: 5 7, Attack: 1 3 3, Abilities: Action: Heal, Surge: Mend 3, Surge: +3 Hearts"
    ],
    "expansion": "Base Game",
    "image": "monsters/flesh-moulder-bg-act2-front.png",
    "xws": "fleshmoulder"
  },
  {
    "name": "Flesh Moulder",
    "points": 19,
    "act": "II",
    "traits": [
      "Cursed",
      "Civilized"
    ],
    "ability rules": [
      "Heal: Choose a monster within 3 spaces of this monster and roll 1 2 power die. The chosen monster recovers Hearts equal to the Hearts rolled.",
      "Mend X: This monster recovers X Hearts."
    ],
    "group size": [
      "2 Heroes: 1,1",
      "3 Heroes: 2,1",
      "4 Heroes: 3,1"
    ],
    "expansion": "Base Game",
    "image": "monsters/flesh-moulder-bg-act2-back.png",
    "xws": "fleshmoulder"
  },
  {
    "name": "Goblin Archer",
    "points": 20,
    "act": "I",
    "attack": "Range",
    "characteristics": [
      "Minion: Speed: 5, Health: 2, Defense: 5, Attack: 1 3, Abilities: Scamper, Cowardly, Surge: +1 Range, Surge: +1 Heart",
      "Master: Speed: 5, Health: 4, Defense: 5, Attack: 1 3, Abilities: Scamper, Surge: +2 Range, Surge: +2 Hearts"
    ],
    "expansion": "Base Game",
    "image": "monsters/goblin-archer-bg-act1-front.png",
    "xws": "goblinarcher"
  },
  {
    "name": "Goblin Archer",
    "points": 21,
    "act": "I",
    "traits": [
      "Building",
      "Cave"
    ],
    "ability rules": [
      "Scamper: This monster may move through spaces containing heroes.",
      "Cowardly: This monster cannot spend surges on abilities unless it is within 3 spaces of any master monster or a lieutenant."
    ],
    "group size": [
      "2 Heroes: 2,1",
      "3 Heroes: 3,1",
      "4 Heroes: 4,1"
    ],
    "expansion": "Base Game",
    "image": "monsters/goblin-archer-bg-act1-back.png",
    "xws": "goblinarcher"
  },
  {
    "name": "Goblin Archer",
    "points": 22,
    "act": "II",
    "attack": "Range",
    "characteristics": [
      "Minion: Speed: 5, Health: 4, Defense: 5, Attack: 1 3, Abilities: Scamper, Cowardly, Surge: +2 Range, Surge: +2 Hearts",
      "Master: Speed: 5, Health: 6, Defense: 5, Attack: 1 3 3, Abilities: Scamper, Surge: +3 Range, Surge: +2 Hearts"
    ],
    "expansion": "Base Game",
    "image": "monsters/goblin-archer-bg-act2-front.png",
    "xws": "goblinarcher"
  },
  {
    "name": "Goblin Archer",
    "points": 23,
    "act": "II",
    "traits": [
      "Building",
      "Cave"
    ],
    "ability rules": [
      "Scamper: This monster may move through spaces containing heroes.",
      "Cowardly: This monster cannot spend surges on abilities unless it is within 3 spaces of any master monster or a lieutenant."
    ],
    "group size": [
      "2 Heroes: 2,1",
      "3 Heroes: 3,1",
      "4 Heroes: 4,1"
    ],
    "expansion": "Base Game",
    "image": "monsters/goblin-archer-bg-act2-back.png",
    "xws": "goblinarcher"
  },
  {
    "name": "Merriod",
    "points": 24,
    "act": "I",
    "attack": "Melee",
    "characteristics": [
      "Minion: Speed: 3, Health: 5, Defense: 6, Attack: 1 2, Abilities: Reach, Surge: Immobilize, Surge: +1 Heart",
      "Master: Speed: 3, Health: 7, Defense: 6, Attack: 1 2, Abilities: Reach, Flail, Surge: Immobilize, Surge: +2 Hearts"
    ],
    "expansion": "Base Game",
    "image": "monsters/merriod-bg-act1-front.png",
    "xws": "merriod"
  },
  {
    "name": "Merriod",
    "points": 25,
    "act": "I",
    "traits": [
      "Wilderness",
      "Water"
    ],
    "ability rules": [
      "Reach: This monster may attack targets up to 2 spaces away.",
      "Flail: When attacking, this monster may target 2 separate heroes. This monster makes 1 attack roll and each hero rolls defense dice separately.",
      "Immobilize: If this attack deals at least 1 Heart (after the defense roll), the target is Immobilized."
    ],
    "group size": [
      "2 Heroes: 1,0",
      "3 Heroes: 0,1",
      "4 Heroes: 1,1"
    ],
    "expansion": "Base Game",
    "image": "monsters/merriod-bg-act1-back.png",
    "xws": "merriod"
  },
  {
    "name": "Merriod",
    "points": 26,
    "act": "II",
    "attack": "Melee",
    "characteristics": [
      "Minion: Speed: 3, Health: 7, Defense: 5 5, Attack: 1 2 3, Abilities: Reach, Surge: Immobilize, Surge: +2 Hearts",
      "Master: Speed: 3, Health: 9, Defense: 5 5, Attack: 1 2 3, Abilities: Reach, Flail, Surge: Immobilize, Surge: +3 Hearts"
    ],
    "expansion": "Base Game",
    "image": "monsters/merriod-bg-act2-front.png",
    "xws": "merriod"
  },
  {
    "name": "Merriod",
    "points": 27,
    "act": "II",
    "traits": [
      "Wilderness",
      "Water"
    ],
    "ability rules": [
      "Reach: This monster may attack targets up to 2 spaces away.",
      "Flail: When attacking, this monster may target 2 separate heroes. This monster makes 1 attack roll and each hero rolls defense dice separately.",
      "Immobilize: If this attack deals at least 1 Heart (after the defense roll), the target is Immobilized."
    ],
    "group size": [
      "2 Heroes: 1,0",
      "3 Heroes: 0,1",
      "4 Heroes: 1,1"
    ],
    "expansion": "Base Game",
    "image": "monsters/merriod-bg-act2-back.png",
    "xws": "merriod"
  },
  {
    "name": "Shadow Dragon",
    "points": 28,
    "act": "I",
    "attack": "Melee",
    "characteristics": [
      "Minion: Speed: 3, Health: 6, Defense: 5 5, Attack: 1 2, Abilities: Shadow, Surge: +1 Heart",
      "Master: Speed: 3, Health: 9, Defense: 5 5, Attack: 1 2, Abilities: Shadow, Surge: Fire Breath, Surge: +2 Hearts"
    ],
    "expansion": "Base Game",
    "image": "monsters/shadow-dragon-bg-act1-front.png",
    "xws": "shadowdragon"
  },
  {
    "name": "Shadow Dragon",
    "points": 29,
    "act": "I",
    "traits": [
      "Dark",
      "Cave"
    ],
    "ability rules": [
      "Shadow: A hero adjacent to this monster that declares an attack must spend 1 Surge or the attack is conside2 a miss.",
      "Fire Breath: Starting with the target space, trace a path of 4 spaces in any direction. All figures on this path are affected by this attack. Each figure rolls defense dice separately."
    ],
    "group size": [
      "2 Heroes: 1,0",
      "3 Heroes: 0,1",
      "4 Heroes: 1,1"
    ],
    "expansion": "Base Game",
    "image": "monsters/shadow-dragon-bg-act1-back.png",
    "xws": "shadowdragon"
  },
  {
    "name": "Shadow Dragon",
    "points": 30,
    "act": "II",
    "attack": "Melee",
    "characteristics": [
      "Minion: Speed: 3, Health: 8, Defense: 6 5, Attack: 1 2 2, Abilities: Shadow, Surge: +2 Hearts",
      "Master: Speed: 3, Health: 10, Defense: 6 5, Attack: 1 2 2, Abilities: Shadow, Surge: Fire Breath, Surge: +3 Hearts"
    ],
    "expansion": "Base Game",
    "image": "monsters/shadow-dragon-bg-act2-front.png",
    "xws": "shadowdragon"
  },
  {
    "name": "Shadow Dragon",
    "points": 31,
    "act": "II",
    "traits": [
      "Dark",
      "Cave"
    ],
    "ability rules": [
      "Shadow: A hero adjacent to this monster that declares an attack must spend 1 Surge or the attack is conside2 a miss.",
      "Fire Breath: Starting with the target space, trace a path of 4 spaces in any direction. All figures on this path are affected by this attack. Each figure rolls defense dice separately."
    ],
    "group size": [
      "2 Heroes: 1,0",
      "3 Heroes: 0,1",
      "4 Heroes: 1,1"
    ],
    "expansion": "Base Game",
    "image": "monsters/shadow-dragon-bg-act2-back.png",
    "xws": "shadowdragon"
  },
  {
    "name": "Zombie",
    "points": 32,
    "act": "I",
    "attack": "Melee",
    "characteristics": [
      "Minion: Speed: 3, Health: 3, Defense: 7, Attack: 1 3, Abilities: Shambling, Surge: Disease, Surge: +1 Heart",
      "Master: Speed: 3, Health: 6, Defense: 7, Attack: 1 3, Abilities: Shambling, Action: Grab, Surge: Disease, Surge: +1 Heart"
    ],
    "expansion": "Base Game",
    "image": "monsters/zombie-bg-act1-front.png",
    "xws": "zombie"
  },
  {
    "name": "Zombie",
    "points": 33,
    "act": "I",
    "traits": [
      "Cursed",
      "Building"
    ],
    "ability rules": [
      "Shambling: This monster may not perform more than 1 move action during a single turn.",
      "Grab: Choose a hero adjacent to this monster. The hero must test Might. If he fails, he is Immobilized.",
      "Disease: If this attack deals at least 1 Heart (after the defense roll), the target is Diseased."
    ],
    "group size": [
      "2 Heroes: 2,1",
      "3 Heroes: 3,1",
      "4 Heroes: 4,1"
    ],
    "expansion": "Base Game",
    "image": "monsters/zombie-bg-act1-back.png",
    "xws": "zombie"
  },
  {
    "name": "Zombie",
    "points": 34,
    "act": "II",
    "attack": "Melee",
    "characteristics": [
      "Minion: Speed: 3, Health: 5, Defense: 7, Attack: 1 3, Abilities: Shambling, Surge: Disease, Surge: +2 Hearts",
      "Master: Speed: 3, Health: 9, Defense: 7, Attack: 1 3 2, Abilities: Shambling, Action: Grab, Surge: Disease, Surge: +2 Hearts"
    ],
    "expansion": "Base Game",
    "image": "monsters/zombie-bg-act2-front.png",
    "xws": "zombie"
  },
  {
    "name": "Zombie",
    "points": 35,
    "act": "II",
    "traits": [
      "Cursed",
      "Building"
    ],
    "ability rules": [
      "Shambling: This monster may not perform more than 1 move action during a single turn.",
      "Grab: Choose a hero adjacent to this monster. The hero must test Might. If he fails, he is Immobilized.",
      "Disease: If this attack deals at least 1 Heart (after the defense roll), the target is Diseased."
    ],
    "group size": [
      "2 Heroes: 2,1",
      "3 Heroes: 3,1",
      "4 Heroes: 4,1"
    ],
    "expansion": "Base Game",
    "image": "monsters/zombie-bg-act2-back.png",
    "xws": "zombie"
  },
  {
    "name": "CK-Bane Spider",
    "points": 36,
    "act": "I",
    "attack": "Range",
    "characteristics": [
      "Minion: Speed: 4, Health: 4, Defense: 5, Attack: 1 2, Abilities: Surge: Poison, Surge: Pierce 1",
      "Master: Speed: 4, Health: 7, Defense: 5, Attack: 1 2, Abilities: Action: Cocoon, Surge: Poison, Surge: Pierce 2"
    ],
    "expansion": "Conversion Kit",
    "image": "monsters/bane-spider-ck-act1-front.png",
    "xws": "banespider"
  },
  {
    "name": "CK-Bane Spider",
    "points": 37,
    "act": "I",
    "traits": [
      "Dark",
      "Cave"
    ],
    "ability rules": [
      "Cocoon: Each hero adjacent to this monster must test Awareness. Each hero that fails is Immobilized.",
      "Poison: If this attack deals at least 1 Heart (after the defense roll), the target is Poisoned.",
      "Pierce X: This attack ignores X Shields rolled on the defense dice."
    ],
    "group size": [
      "2 Heroes: 0,1",
      "3 Heroes: 1,1",
      "4 Heroes: 2,1"
    ],
    "expansion": "Conversion Kit",
    "image": "monsters/bane-spider-ck-act1-back.png",
    "xws": "banespider"
  },
  {
    "name": "CK-Bane Spider",
    "points": 38,
    "act": "II",
    "attack": "Range",
    "characteristics": [
      "Minion: Speed: 4, Health: 6, Defense: 5 7, Attack: 1 2, Abilities: Surge: Poison, Surge: Pierce 2",
      "Master: Speed: 4, Health: 9, Defense: 5 7, Attack: 1 2, Abilities: Action: Cocoon, Surge: Poison, Surge: Pierce 3"
    ],
    "expansion": "Conversion Kit",
    "image": "monsters/bane-spider-ck-act2-front.png",
    "xws": "banespider"
  },
  {
    "name": "CK-Bane Spider",
    "points": 39,
    "act": "II",
    "traits": [
      "Dark",
      "Cave"
    ],
    "ability rules": [
      "Cocoon: Each hero adjacent to this monster must test Awareness. Each hero that fails is Immobilized.",
      "Poison: If this attack deals at least 1 Heart (after the defense roll), the target is Poisoned.",
      "Pierce X: This attack ignores X Shields rolled on the defense dice."
    ],
    "group size": [
      "2 Heroes: 0,1",
      "3 Heroes: 1,1",
      "4 Heroes: 2,1"
    ],
    "expansion": "Conversion Kit",
    "image": "monsters/bane-spider-ck-act2-back.png",
    "xws": "banespider"
  },
  {
    "name": "CK-Beastman",
    "points": 40,
    "act": "I",
    "attack": "Melee",
    "characteristics": [
      "Minion: Speed: 4, Health: 2, Defense: 5, Attack: 1 2, Abilities: Ravage, Surge: +1 Heart",
      "Master: Speed: 4, Health: 5, Defense: 5, Attack: 1 2, Abilities: Command, Ravage, Surge: +2 Hearts"
    ],
    "expansion": "Conversion Kit",
    "image": "monsters/beastman-ck-act1-front.png",
    "xws": "beastman"
  },
  {
    "name": "CK-Beastman",
    "points": 41,
    "act": "I",
    "traits": [
      "Mountain",
      "Wilderness"
    ],
    "ability rules": [
      "Command: Each minion within 3 spaces of this monster may reroll 1 die on each of its attacks. Each minion may only benefit from one monster with Command at a time.",
      "Ravage: Both of this monster's actions on a turn may be attack actions."
    ],
    "group size": [
      "2 Heroes: 1,1",
      "3 Heroes: 2,1",
      "4 Heroes: 3,1"
    ],
    "expansion": "Conversion Kit",
    "image": "monsters/beastman-ck-act1-back.png",
    "xws": "beastman"
  },
  {
    "name": "CK-Beastman",
    "points": 42,
    "act": "II",
    "attack": "Melee",
    "characteristics": [
      "Minion: Speed: 5, Health: 4, Defense: 5, Attack: 1 2, Abilities: Ravage, Surge: +2 Hearts",
      "Master: Speed: 5, Health: 6, Defense: 5, Attack: 1 2 3, Abilities: Command, Ravage, Surge: +2 Hearts"
    ],
    "expansion": "Conversion Kit",
    "image": "monsters/beastman-ck-act2-front.png",
    "xws": "beastman"
  },
  {
    "name": "CK-Beastman",
    "points": 43,
    "act": "II",
    "traits": [
      "Mountain",
      "Wilderness"
    ],
    "ability rules": [
      "Command: Each minion within 3 spaces of this monster may reroll 1 die on each of its attacks. Each minion may only benefit from one monster with Command at a time.",
      "Ravage: Both of this monster's actions on a turn may be attack actions."
    ],
    "group size": [
      "2 Heroes: 1,1",
      "3 Heroes: 2,1",
      "4 Heroes: 3,1"
    ],
    "expansion": "Conversion Kit",
    "image": "monsters/beastman-ck-act2-back.png",
    "xws": "beastman"
  },
  {
    "name": "CK-Blood Ape",
    "points": 44,
    "act": "I",
    "attack": "Melee",
    "characteristics": [
      "Minion: Speed: 4, Health: 5, Defense: 5, Attack: 1 2, Abilities: Ravage, Surge: +1 Heart",
      "Master: Speed: 4, Health: 7, Defense: 5, Attack: 1 2, Abilities: Ravage, Action: Leap Attack, Surge: +2 Hearts"
    ],
    "expansion": "Conversion Kit",
    "image": "monsters/blood-ape-ck-act1-front.png",
    "xws": "bloodape"
  },
  {
    "name": "CK-Blood Ape",
    "points": 45,
    "act": "I",
    "traits": [
      "Cave",
      "Hot"
    ],
    "ability rules": [
      "Ravage: Both of this monster's actions on a turn may be attack actions.",
      "Leap Attack: This monster may move its Speed, ignoring enemy figures as it does so. When if finishes its movement, perform a single attack that targets each figure this monster moved through. Each figure rolls defense dice separately. Limit once per turn."
    ],
    "group size": [
      "2 Heroes: 0,1",
      "3 Heroes: 1,1",
      "4 Heroes: 2,1"
    ],
    "expansion": "Conversion Kit",
    "image": "monsters/blood-ape-ck-act1-back.png",
    "xws": "bloodape"
  },
  {
    "name": "CK-Blood Ape",
    "points": 46,
    "act": "II",
    "attack": "Melee",
    "characteristics": [
      "Minion: Speed: 4, Health: 7, Defense: 5, Attack: 1 2 3, Abilities: Ravage, Surge: +2 Hearts",
      "Master: Speed: 4, Health: 9, Defense: 5, Attack: 1 2 2, Abilities: Ravage, Action: Leap Attack, Surge: +2 Hearts"
    ],
    "expansion": "Conversion Kit",
    "image": "monsters/blood-ape-ck-act2-front.png",
    "xws": "bloodape"
  },
  {
    "name": "CK-Blood Ape",
    "points": 47,
    "act": "II",
    "traits": [
      "Cave",
      "Hot"
    ],
    "ability rules": [
      "Ravage: Both of this monster's actions on a turn may be attack actions.",
      "Leap Attack: This monster may move its Speed, ignoring enemy figures as it does so. When if finishes its movement, perform a single attack that targets each figure this monster moved through. Each figure rolls defense dice separately. Limit once per turn."
    ],
    "group size": [
      "2 Heroes: 0,1",
      "3 Heroes: 1,1",
      "4 Heroes: 2,1"
    ],
    "expansion": "Conversion Kit",
    "image": "monsters/blood-ape-ck-act2-back.png",
    "xws": "bloodape"
  },
  {
    "name": "CK-Chaos Beast",
    "points": 48,
    "act": "I",
    "attack": "Range",
    "characteristics": [
      "Minion: Speed: 3, Health: 5, Defense: 5, Attack: Blank, Abilities: Morph, Surge: +1 Heart",
      "Master: Speed: 3, Health: 6, Defense: 5, Attack: Blank, Abilities: Morph, Sorcery 2, Surge: +1 Heart"
    ],
    "expansion": "Conversion Kit",
    "image": "monsters/chaos-beast-ck-act1-front.png",
    "xws": "chaosbeast"
  },
  {
    "name": "CK-Chaos Beast",
    "points": 49,
    "act": "I",
    "traits": [
      "Dark",
      "Cursed"
    ],
    "ability rules": [
      "Morph: When this monster attacks, it uses the dice of a figure (overlord's choice) in its lind of sight. If a hero is chosen, the overlord may choose which of the hero's equipped weapons to use. The monster cannot use any of the figure's other abilities, just the dice.",
      "Sorcery X: After making an attack roll, this monster may convert up to X range to Hearts, or up to X Hearts to range."
    ],
    "group size": [
      "2 Heroes: 0,1",
      "3 Heroes: 1,1",
      "4 Heroes: 2,1"
    ],
    "expansion": "Conversion Kit",
    "image": "monsters/chaos-beast-ck-act1-back.png",
    "xws": "chaosbeast"
  },
  {
    "name": "CK-Chaos Beast",
    "points": 50,
    "act": "II",
    "attack": "Range",
    "characteristics": [
      "Minion: Speed: 3, Health: 7, Defense: 5, Attack: Blank, Abilities: Morph, Surge: +1 Heart",
      "Master: Speed: 3, Health: 10, Defense: 5, Attack: Blank, Abilities: Morph, Sorcery 3, Surge: +1 Heart"
    ],
    "expansion": "Conversion Kit",
    "image": "monsters/chaos-beast-ck-act2-front.png",
    "xws": "chaosbeast"
  },
  {
    "name": "CK-Chaos Beast",
    "points": 51,
    "act": "II",
    "traits": [
      "Dark",
      "Cursed"
    ],
    "ability rules": [
      "Morph: When this monster attacks, it uses the dice of a figure (overlord's choice) in its lind of sight. If a hero is chosen, the overlord may choose which of the hero's equipped weapons to use. The monster cannot use any of the figure's other abilities, just the dice.",
      "Sorcery X: After making an attack roll, this monster may convert up to X range to Hearts, or up to X Hearts to range."
    ],
    "group size": [
      "2 Heroes: 0,1",
      "3 Heroes: 1,1",
      "4 Heroes: 2,1"
    ],
    "expansion": "Conversion Kit",
    "image": "monsters/chaos-beast-ck-act2-back.png",
    "xws": "chaosbeast"
  },
  {
    "name": "CK-Crypt Dragon",
    "points": 52,
    "act": "I",
    "attack": "Range",
    "characteristics": [
      "Minion: Speed: 3, Health: 5, Defense: 5 5, Attack: 1 3, Abilities: Surge: Blast, Surge: +2 Hearts",
      "Master: Speed: 3, Health: 7, Defense: 5 5, Attack: 1 3, Abilities: Action: Cause Fear, Surge: Blast, Surge: +2 Hearts"
    ],
    "expansion": "Conversion Kit",
    "image": "monsters/crypt-dragon-ck-act1-front.png",
    "xws": "cryptdragon"
  },
  {
    "name": "CK-Crypt Dragon",
    "points": 53,
    "act": "I",
    "traits": [
      "Dark",
      "Cursed"
    ],
    "ability rules": [
      "Cause Fear: Choose a hero adjacent to this monster. That hero must test Willpower. If he fails, he moves 2 spaces directly away from this monster and is Immobilized.",
      "Blast: This attack affects all figures adjacent to the target space."
    ],
    "group size": [
      "2 Heroes: 1,0",
      "3 Heroes: 0,1",
      "4 Heroes: 1,1"
    ],
    "expansion": "Conversion Kit",
    "image": "monsters/crypt-dragon-ck-act1-back.png",
    "xws": "cryptdragon"
  },
  {
    "name": "CK-Crypt Dragon",
    "points": 54,
    "act": "II",
    "attack": "Range",
    "characteristics": [
      "Minion: Speed: 3, Health: 6, Defense: 6 5, Attack: 1 2 3, Abilities: Surge: Blast, Surge: +2 Hearts",
      "Master: Speed: 3, Health: 10, Defense: 6 5, Attack: 1 2 3, Abilities: Action: Cause Fear, Surge: Blast, Surge: +2 Hearts"
    ],
    "expansion": "Conversion Kit",
    "image": "monsters/crypt-dragon-ck-act2-front.png",
    "xws": "cryptdragon"
  },
  {
    "name": "CK-Crypt Dragon",
    "points": 55,
    "act": "II",
    "traits": [
      "Dark",
      "Cursed"
    ],
    "ability rules": [
      "Cause Fear: Choose a hero adjacent to this monster. That hero must test Willpower. If he fails, he moves 2 spaces directly away from this monster and is Immobilized.",
      "Blast: This attack affects all figures adjacent to the target space."
    ],
    "group size": [
      "2 Heroes: 1,0",
      "3 Heroes: 0,1",
      "4 Heroes: 1,1"
    ],
    "expansion": "Conversion Kit",
    "image": "monsters/crypt-dragon-ck-act2-back.png",
    "xws": "cryptdragon"
  },
  {
    "name": "CK-Dark Priest",
    "points": 56,
    "act": "I",
    "attack": "Range",
    "characteristics": [
      "Minion: Speed: 4, Health: 2, Defense: 7, Attack: 1 3, Abilities: Action: Dark Prayer, Surge: +1 Heart",
      "Master: Speed: 4, Health: 5, Defense: 7, Attack: 1 3, Abilities: Action: Dark Prayer, Action: Heal, Surge: +1 Heart"
    ],
    "expansion": "Conversion Kit",
    "image": "monsters/dark-priest-ck-act1-front.png",
    "xws": "darkpriest"
  },
  {
    "name": "CK-Dark Priest",
    "points": 57,
    "act": "I",
    "traits": [
      "Civilized",
      "Cursed"
    ],
    "ability rules": [
      "Dark Prayer: Each hero within 3 spaces of this monster must test Willpower. Each hero that fails suffers 1 Fatigue.",
      "Heal: Choose a monster within 3 spaces of this monster and roll 1 2 power die. That monster recovers Hearts equal to the Hearts rolled."
    ],
    "group size": [
      "2 Heroes: 1,1",
      "3 Heroes: 2,1",
      "4 Heroes: 3,1"
    ],
    "expansion": "Conversion Kit",
    "image": "monsters/dark-priest-ck-act1-back.png",
    "xws": "darkpriest"
  },
  {
    "name": "CK-Dark Priest",
    "points": 58,
    "act": "II",
    "attack": "Range",
    "characteristics": [
      "Minion: Speed: 4, Health: 5, Defense: 7, Attack: 1 3, Abilities: Action: Dark Prayer, Surge: +2 Hearts",
      "Master: Speed: 4, Health: 8, Defense: 7, Attack: 1 3 3, Abilities: Action: Dark Prayer, Action: Heal, Surge: +2 Hearts"
    ],
    "expansion": "Conversion Kit",
    "image": "monsters/dark-priest-ck-act2-front.png",
    "xws": "darkpriest"
  },
  {
    "name": "CK-Dark Priest",
    "points": 59,
    "act": "II",
    "traits": [
      "Civilized",
      "Cursed"
    ],
    "ability rules": [
      "Dark Prayer: Each hero within 3 spaces of this monster must test Willpower. Each hero that fails suffers 1 Fatigue.",
      "Heal: Choose a monster within 3 spaces of this monster and roll 1 2 power die. That monster recovers Hearts equal to the Hearts rolled."
    ],
    "group size": [
      "2 Heroes: 1,1",
      "3 Heroes: 2,1",
      "4 Heroes: 3,1"
    ],
    "expansion": "Conversion Kit",
    "image": "monsters/dark-priest-ck-act2-back.png",
    "xws": "darkpriest"
  },
  {
    "name": "CK-Deep Elf",
    "points": 60,
    "act": "I",
    "attack": "Melee",
    "characteristics": [
      "Minion: Speed: 5, Health: 7, Defense: 7, Attack: 1 3, Abilities: Stealthy, Pierce 2, Surge: +1 Heart",
      "Master: Speed: 5, Health: 9, Defense: 7, Attack: 1 3, Abilities: Stealthy, Pierce 4, Surge: +1 Heart"
    ],
    "expansion": "Conversion Kit",
    "image": "monsters/deep-elf-ck-act1-front.png",
    "xws": "deepelf"
  },
  {
    "name": "CK-Deep Elf",
    "points": 61,
    "act": "I",
    "traits": [
      "Dark",
      "Cave"
    ],
    "ability rules": [
      "Stealthy: Each attack that targets this monster must roll 3 additional range beyond the normally requi2 amount or the attack is a miss.",
      "Pierce X: This attack ignores X Shields rolled on the defense dice."
    ],
    "group size": [
      "2 Heroes: 1,0",
      "3 Heroes: 0,1",
      "4 Heroes: 1,1"
    ],
    "expansion": "Conversion Kit",
    "image": "monsters/deep-elf-ck-act1-back.png",
    "xws": "deepelf"
  },
  {
    "name": "CK-Deep Elf",
    "points": 62,
    "act": "II",
    "attack": "Melee",
    "characteristics": [
      "Minion: Speed: 5, Health: 8, Defense: 5, Attack: 1 3, Abilities: Stealthy, Pierce 2, Surge: +2 Hearts",
      "Master: Speed: 5, Health: 10, Defense: 5, Attack: 1 3, Abilities: Stealthy, Pierce 4, Surge: +2 Hearts"
    ],
    "expansion": "Conversion Kit",
    "image": "monsters/deep-elf-ck-act2-front.png",
    "xws": "deepelf"
  },
  {
    "name": "CK-Deep Elf",
    "points": 63,
    "act": "II",
    "traits": [
      "Dark",
      "Cave"
    ],
    "ability rules": [
      "Stealthy: Each attack that targets this monster must roll 3 additional range beyond the normally requi2 amount or the attack is a miss.",
      "Pierce X: This attack ignores X Shields rolled on the defense dice."
    ],
    "group size": [
      "2 Heroes: 1,0",
      "3 Heroes: 0,1",
      "4 Heroes: 1,1"
    ],
    "expansion": "Conversion Kit",
    "image": "monsters/deep-elf-ck-act2-back.png",
    "xws": "deepelf"
  },
  {
    "name": "CK-Demon Lord",
    "points": 64,
    "act": "I",
    "attack": "Range",
    "characteristics": [
      "Minion: Speed: 3, Health: 6, Defense: 5 5, Attack: 1 3, Abilities: Sorcery 2, Surge: Wither",
      "Master: Speed: 3, Health: 9, Defense: 5 5, Attack: 1 3, Abilities: Aura 1, Sorcery 3, Surge: Wither"
    ],
    "expansion": "Conversion Kit",
    "image": "monsters/demon-lord-ck-act1-front.png",
    "xws": "demonlord"
  },
  {
    "name": "CK-Demon Lord",
    "points": 65,
    "act": "I",
    "traits": [
      "Hot",
      "Cursed"
    ],
    "ability rules": [
      "Aura 1: Each time a hero enters a space adjacent to this monster, that hero suffers 1 Heart.",
      "Sorcery X: After making an attack roll, this monster may convert up to X range to Hearts, or up to X Hearts to range.",
      "Wither: The target suffers 1 Fatigue."
    ],
    "group size": [
      "2 Heroes: 1,0",
      "3 Heroes: 0,1",
      "4 Heroes: 1,1"
    ],
    "expansion": "Conversion Kit",
    "image": "monsters/demon-lord-ck-act1-back.png",
    "xws": "demonlord"
  },
  {
    "name": "CK-Demon Lord",
    "points": 66,
    "act": "II",
    "attack": "Range",
    "characteristics": [
      "Minion: Speed: 3, Health: 8, Defense: 5 5, Attack: 1 3 3, Abilities: Sorcery 2, Surge: Wither",
      "Master: Speed: 3, Health: 12, Defense: 5 5, Attack: 1 2 3, Abilities: Aura 1, Sorcery 3, Surge: Wither"
    ],
    "expansion": "Conversion Kit",
    "image": "monsters/demon-lord-ck-act2-front.png",
    "xws": "demonlord"
  },
  {
    "name": "CK-Demon Lord",
    "points": 67,
    "act": "II",
    "traits": [
      "Hot",
      "Cursed"
    ],
    "ability rules": [
      "Aura 1: Each time a hero enters a space adjacent to this monster, that hero suffers 1 Heart.",
      "Sorcery X: After making an attack roll, this monster may convert up to X range to Hearts, or up to X Hearts to range.",
      "Wither: The target suffers 1 Fatigue."
    ],
    "group size": [
      "2 Heroes: 1,0",
      "3 Heroes: 0,1",
      "4 Heroes: 1,1"
    ],
    "expansion": "Conversion Kit",
    "image": "monsters/demon-lord-ck-act2-back.png",
    "xws": "demonlord"
  },
  {
    "name": "CK-Ferrox",
    "points": 68,
    "act": "I",
    "attack": "Melee",
    "characteristics": [
      "Minion: Speed: 4, Health: 4, Defense: 5, Attack: 1 2, Abilities: Surge: Disease, Surge: Pierce 2",
      "Master: Speed: 4, Health: 5, Defense: 5, Attack: 1 2, Abilities: Action: Leech, Surge: Disease, Surge: Pierce 2"
    ],
    "expansion": "Conversion Kit",
    "image": "monsters/ferrox-ck-act1-front.png",
    "xws": "ferrox"
  },
  {
    "name": "CK-Ferrox",
    "points": 69,
    "act": "I",
    "traits": [
      "Cave",
      "Water"
    ],
    "ability rules": [
      "Leech: Choose a hero adjacent to this monster. That hero must test Might. If he fails, roll 1 3 power die. The hero suffers 1 Fatigue per Heart rolled, and this monster recovers 1 Heart per Fatigue suffe2.",
      "Disease: If this attack deals at least 1 Heart (after the defense roll), the target is Diseased.",
      "Pierce X: This attack ignores X Shields rolled on the defense dice."
    ],
    "group size": [
      "2 Heroes: 1,1",
      "3 Heroes: 2,1",
      "4 Heroes: 3,1"
    ],
    "expansion": "Conversion Kit",
    "image": "monsters/ferrox-ck-act1-back.png",
    "xws": "ferrox"
  },
  {
    "name": "CK-Ferrox",
    "points": 70,
    "act": "II",
    "attack": "Melee",
    "characteristics": [
      "Minion: Speed: 4, Health: 5, Defense: 5 7, Attack: 1 2 3, Abilities: Surge: Disease, Surge: Pierce 3",
      "Master: Speed: 4, Health: 8, Defense: 5 7, Attack: 1 2 3, Abilities: Action: Leech, Surge: Disease, Surge: Pierce 3"
    ],
    "expansion": "Conversion Kit",
    "image": "monsters/ferrox-ck-act2-front.png",
    "xws": "ferrox"
  },
  {
    "name": "CK-Ferrox",
    "points": 71,
    "act": "II",
    "traits": [
      "Cave",
      "Water"
    ],
    "ability rules": [
      "Leech: Choose a hero adjacent to this monster. That hero must test Might. If he fails, roll 1 3 power die. The hero suffers 1 Fatigue per Heart rolled, and this monster recovers 1 Heart per Fatigue suffe2.",
      "Disease: If this attack deals at least 1 Heart (after the defense roll), the target is Diseased.",
      "Pierce X: This attack ignores X Shields rolled on the defense dice."
    ],
    "group size": [
      "2 Heroes: 1,1",
      "3 Heroes: 2,1",
      "4 Heroes: 3,1"
    ],
    "expansion": "Conversion Kit",
    "image": "monsters/ferrox-ck-act2-back.png",
    "xws": "ferrox"
  },
  {
    "name": "CK-Giant",
    "points": 72,
    "act": "I",
    "attack": "Melee",
    "characteristics": [
      "Minion: Speed: 3, Health: 10, Defense: 6, Attack: 1 2, Abilities: Reach, Surge: Stun",
      "Master: Speed: 3, Health: 12, Defense: 6, Attack: 1 2, Abilities: Reach, Action: Sweep, Surge: Stun"
    ],
    "expansion": "Conversion Kit",
    "image": "monsters/giant-ck-act1-front.png",
    "xws": "giant"
  },
  {
    "name": "CK-Giant",
    "points": 73,
    "act": "I",
    "traits": [
      "Mountain",
      "Wilderness"
    ],
    "ability rules": [
      "Reach: This monster may attack targets up to 2 spaces away.",
      "Sweep: Perform an attack. This attack affects each figure within range of this monster's attack. Each figure rolls defense dice separately.",
      "Stun: If this attack deals at least 1 Heart (after the defense roll), the target is Stunned."
    ],
    "group size": [
      "2 Heroes: 1,0",
      "3 Heroes: 0,1",
      "4 Heroes: 1,1"
    ],
    "expansion": "Conversion Kit",
    "image": "monsters/giant-ck-act1-back.png",
    "xws": "giant"
  },
  {
    "name": "CK-Giant",
    "points": 74,
    "act": "II",
    "attack": "Melee",
    "characteristics": [
      "Minion: Speed: 3, Health: 12, Defense: 6, Attack: 1 2 3, Abilities: Reach, Surge: Stun",
      "Master: Speed: 3, Health: 15, Defense: 6, Attack: 1 2 2, Abilities: Reach, Action: Sweep, Surge: Stun"
    ],
    "expansion": "Conversion Kit",
    "image": "monsters/giant-ck-act2-front.png",
    "xws": "giant"
  },
  {
    "name": "CK-Giant",
    "points": 75,
    "act": "II",
    "traits": [
      "Mountain",
      "Wilderness"
    ],
    "ability rules": [
      "Reach: This monster may attack targets up to 2 spaces away.",
      "Sweep: Perform an attack. This attack affects each figure within range of this monster's attack. Each figure rolls defense dice separately.",
      "Stun: If this attack deals at least 1 Heart (after the defense roll), the target is Stunned."
    ],
    "group size": [
      "2 Heroes: 1,0",
      "3 Heroes: 0,1",
      "4 Heroes: 1,1"
    ],
    "expansion": "Conversion Kit",
    "image": "monsters/giant-ck-act2-back.png",
    "xws": "giant"
  },
  {
    "name": "CK-Golem",
    "points": 76,
    "act": "I",
    "attack": "Melee",
    "characteristics": [
      "Minion: Speed: 3, Health: 8, Defense: 6, Attack: 1 2, Abilities: Ironskin, Surge: +1 Heart",
      "Master: Speed: 3, Health: 10, Defense: 6, Attack: 1 2, Abilities: Ironskin, Unmovable, Surge: +2 Hearts"
    ],
    "expansion": "Conversion Kit",
    "image": "monsters/golem-ck-act1-front.png",
    "xws": "golem"
  },
  {
    "name": "CK-Golem",
    "points": 77,
    "act": "I",
    "traits": [
      "Mountain",
      "Building"
    ],
    "ability rules": [
      "Ironskin: This monster is immune to Pierce and to all conditions.",
      "Unmovable: This monster may choose to ignore any game effect that would force it to move."
    ],
    "group size": [
      "2 Heroes: 1,0",
      "3 Heroes: 0,1",
      "4 Heroes: 1,1"
    ],
    "expansion": "Conversion Kit",
    "image": "monsters/golem-ck-act1-back.png",
    "xws": "golem"
  },
  {
    "name": "CK-Golem",
    "points": 78,
    "act": "II",
    "attack": "Melee",
    "characteristics": [
      "Minion: Speed: 3, Health: 10, Defense: 6 5, Attack: 1 2 2, Abilities: Ironskin, Surge: +1 Heart",
      "Master: Speed: 3, Health: 12, Defense: 6 5, Attack: 1 2 2, Abilities: Ironskin, Unmovable, Surge: +2 Hearts"
    ],
    "expansion": "Conversion Kit",
    "image": "monsters/golem-ck-act2-front.png",
    "xws": "golem"
  },
  {
    "name": "CK-Golem",
    "points": 79,
    "act": "II",
    "traits": [
      "Mountain",
      "Building"
    ],
    "ability rules": [
      "Ironskin: This monster is immune to Pierce and to all conditions.",
      "Unmovable: This monster may choose to ignore any game effect that would force it to move."
    ],
    "group size": [
      "2 Heroes: 1,0",
      "3 Heroes: 0,1",
      "4 Heroes: 1,1"
    ],
    "expansion": "Conversion Kit",
    "image": "monsters/golem-ck-act2-back.png",
    "xws": "golem"
  },
  {
    "name": "CK-Hellhound",
    "points": 80,
    "act": "I",
    "attack": "Melee",
    "characteristics": [
      "Minion: Speed: 4, Health: 3, Defense: 5, Attack: 1 2, Abilities: Surge: Fire Breath, Surge: Pierce 2",
      "Master: Speed: 4, Health: 6, Defense: 5, Attack: 1 2, Abilities: Aura 1, Surge: Fire Breath, Surge: Pierce 2"
    ],
    "expansion": "Conversion Kit",
    "image": "monsters/hellhound-ck-act1-front.png",
    "xws": "hellhound"
  },
  {
    "name": "CK-Hellhound",
    "points": 81,
    "act": "I",
    "traits": [
      "Hot",
      "Cursed"
    ],
    "ability rules": [
      "Aura 1: Each time a hero enters a space adjacent to this monster, that hero suffers 1 Heart.",
      "Fire Breath: Starting with the target space, trace a path of 4 spaces in any direction. All figures on this path are affected by this attack. Each figure rolls defense dice separately.",
      "Pierce X: This attack ignores X Shields rolled on the defense dice."
    ],
    "group size": [
      "2 Heroes: 1,1",
      "3 Heroes: 2,1",
      "4 Heroes: 3,1"
    ],
    "expansion": "Conversion Kit",
    "image": "monsters/hellhound-ck-act1-back.png",
    "xws": "hellhound"
  },
  {
    "name": "CK-Hellhound",
    "points": 82,
    "act": "II",
    "attack": "Melee",
    "characteristics": [
      "Minion: Speed: 5, Health: 5, Defense: 5, Attack: 1 2, Abilities: Surge: Fire Breath, Surge: Pierce 3",
      "Master: Speed: 5, Health: 8, Defense: 5, Attack: 1 2 3, Abilities: Aura 1, Surge: Fire Breath, Surge: Pierce 4"
    ],
    "expansion": "Conversion Kit",
    "image": "monsters/hellhound-ck-act2-front.png",
    "xws": "hellhound"
  },
  {
    "name": "CK-Hellhound",
    "points": 83,
    "act": "II",
    "traits": [
      "Hot",
      "Cursed"
    ],
    "ability rules": [
      "Aura 1: Each time a hero enters a space adjacent to this monster, that hero suffers 1 Heart.",
      "Fire Breath: Starting with the target space, trace a path of 4 spaces in any direction. All figures on this path are affected by this attack. Each figure rolls defense dice separately.",
      "Pierce X: This attack ignores X Shields rolled on the defense dice."
    ],
    "group size": [
      "2 Heroes: 1,1",
      "3 Heroes: 2,1",
      "4 Heroes: 3,1"
    ],
    "expansion": "Conversion Kit",
    "image": "monsters/hellhound-ck-act2-back.png",
    "xws": "hellhound"
  },
  {
    "name": "CK-Ice Wyrm",
    "points": 84,
    "act": "I",
    "attack": "Melee",
    "characteristics": [
      "Minion: Speed: 4, Health: 7, Defense: 5 5, Attack: 1 2, Abilities: Reach, Freezing",
      "Master: Speed: 4, Health: 9, Defense: 5 5, Attack: 1 2, Abilities: Reach, Freezing, Swallow"
    ],
    "expansion": "Conversion Kit",
    "image": "monsters/ice-wyrm-ck-act1-front.png",
    "xws": "icewyrm"
  },
  {
    "name": "CK-Ice Wyrm",
    "points": 85,
    "act": "I",
    "traits": [
      "Cold",
      "Cave"
    ],
    "ability rules": [
      "Reach: This monster may attack targets up to 2 spaces away.",
      "Freezing: Each time a hero enters a space adjacent to this monster, that hero suffers 1 Fatigue.",
      "Swallow: If a hero is defeated by this monster, remove his hero token from the map and place it on this monster's base. The hero cannot be revived until this monster is defeated, at which point his hero token is placed in one of this monster's spaces."
    ],
    "group size": [
      "2 Heroes: 1,0",
      "3 Heroes: 0,1",
      "4 Heroes: 1,1"
    ],
    "expansion": "Conversion Kit",
    "image": "monsters/ice-wyrm-ck-act1-back.png",
    "xws": "icewyrm"
  },
  {
    "name": "CK-Ice Wyrm",
    "points": 86,
    "act": "II",
    "attack": "Melee",
    "characteristics": [
      "Minion: Speed: 4, Health: 11, Defense: 5 5, Attack: 1 2 2, Abilities: Reach, Freezing",
      "Master: Speed: 4, Health: 14, Defense: 5 5, Attack: 1 2 2, Abilities: Reach, Freezing, Swallow"
    ],
    "expansion": "Conversion Kit",
    "image": "monsters/ice-wyrm-ck-act2-front.png",
    "xws": "icewyrm"
  },
  {
    "name": "CK-Ice Wyrm",
    "points": 87,
    "act": "II",
    "traits": [
      "Cold",
      "Cave"
    ],
    "ability rules": [
      "Reach: This monster may attack targets up to 2 spaces away.",
      "Freezing: Each time a hero enters a space adjacent to this monster, that hero suffers 1 Fatigue.",
      "Swallow: If a hero is defeated by this monster, remove his hero token from the map and place it on this monster's base. The hero cannot be revived until this monster is defeated, at which point his hero token is placed in one of this monster's spaces."
    ],
    "group size": [
      "2 Heroes: 1,0",
      "3 Heroes: 0,1",
      "4 Heroes: 1,1"
    ],
    "expansion": "Conversion Kit",
    "image": "monsters/ice-wyrm-ck-act2-back.png",
    "xws": "icewyrm"
  },
  {
    "name": "CK-Kobold",
    "points": 88,
    "act": "I",
    "attack": "Melee",
    "characteristics": [
      "Minion: Speed: 3, Health: 2, Defense: 7, Attack: 1, Abilities: Scamper, Surge: Swarm",
      "Master: Speed: 3, Health: 4, Defense: 7, Attack: 1, Abilities: Scamper, Split, Surge: Swarm"
    ],
    "expansion": "Conversion Kit",
    "image": "monsters/kobold-ck-act1-front.png",
    "xws": "kobold"
  },
  {
    "name": "CK-Kobold",
    "points": 89,
    "act": "I",
    "traits": [
      "Building",
      "Cave"
    ],
    "ability rules": [
      "Scamper: This monster may move through spaces containing heroes.",
      "Split: When this monster is defeated, replace it with 2 minions of the same type in the closest available empty spaces, ignoring group limits.",
      "Swarm: This monster deals +1 Heart for each other monster adjacent to the target."
    ],
    "group size": [
      "2 Heroes: 3,1",
      "3 Heroes: 5,2",
      "4 Heroes: 6,3"
    ],
    "expansion": "Conversion Kit",
    "image": "monsters/kobold-ck-act1-back.png",
    "xws": "kobold"
  },
  {
    "name": "CK-Kobold",
    "points": 90,
    "act": "II",
    "attack": "Melee",
    "characteristics": [
      "Minion: Speed: 4, Health: 4, Defense: 7, Attack: 1, Abilities: Scamper, Surge: Swarm",
      "Master: Speed: 4, Health: 6, Defense: 7, Attack: 1 3, Abilities: Scamper, Split, Surge: Swarm"
    ],
    "expansion": "Conversion Kit",
    "image": "monsters/kobold-ck-act2-front.png",
    "xws": "kobold"
  },
  {
    "name": "CK-Kobold",
    "points": 91,
    "act": "II",
    "traits": [
      "Building",
      "Cave"
    ],
    "ability rules": [
      "Scamper: This monster may move through spaces containing heroes.",
      "Split: When this monster is defeated, replace it with 2 minions of the same type in the closest available empty spaces, ignoring group limits.",
      "Swarm: This monster deals +1 Heart for each other monster adjacent to the target."
    ],
    "group size": [
      "2 Heroes: 3,1",
      "3 Heroes: 5,2",
      "4 Heroes: 6,3"
    ],
    "expansion": "Conversion Kit",
    "image": "monsters/kobold-ck-act2-back.png",
    "xws": "kobold"
  },
  {
    "name": "CK-Lava Beetle",
    "points": 92,
    "act": "I",
    "attack": "Range",
    "characteristics": [
      "Minion: Speed: 3, Health: 3, Defense: 5, Attack: 1 2, Abilities: Surge: Blast, Surge: +1 Heart",
      "Master: Speed: 3, Health: 5, Defense: 5, Attack: 1 2, Abilities: Blast, Surge: +1 Heart"
    ],
    "expansion": "Conversion Kit",
    "image": "monsters/lava-beetle-ck-act1-front.png",
    "xws": "lavabeetle"
  },
  {
    "name": "CK-Lava Beetle",
    "points": 93,
    "act": "I",
    "traits": [
      "Hot",
      "Cave"
    ],
    "ability rules": [
      "Blast: This attack affects all figures adjacent to the target space. Master lava beetles do not need a Surge for this ability; their attacks may always have Blast."
    ],
    "group size": [
      "2 Heroes: 1,1",
      "3 Heroes: 2,1",
      "4 Heroes: 3,1"
    ],
    "expansion": "Conversion Kit",
    "image": "monsters/lava-beetle-ck-act1-back.png",
    "xws": "lavabeetle"
  },
  {
    "name": "CK-Lava Beetle",
    "points": 94,
    "act": "II",
    "attack": "Range",
    "characteristics": [
      "Minion: Speed: 3, Health: 5, Defense: 5, Attack: 1 2, Abilities: Surge: Blast, Surge: +2 Hearts",
      "Master: Speed: 3, Health: 7, Defense: 5, Attack: 1 2 3, Abilities: Blast, Surge: +2 Hearts, Surge: +1 Heart"
    ],
    "expansion": "Conversion Kit",
    "image": "monsters/lava-beetle-ck-act2-front.png",
    "xws": "lavabeetle"
  },
  {
    "name": "CK-Lava Beetle",
    "points": 95,
    "act": "II",
    "traits": [
      "Hot",
      "Cave"
    ],
    "ability rules": [
      "Blast: This attack affects all figures adjacent to the target space. Master lava beetles do not need a Surge for this ability; their attacks may always have Blast."
    ],
    "group size": [
      "2 Heroes: 1,1",
      "3 Heroes: 2,1",
      "4 Heroes: 3,1"
    ],
    "expansion": "Conversion Kit",
    "image": "monsters/lava-beetle-ck-act2-back.png",
    "xws": "lavabeetle"
  },
  {
    "name": "CK-Manticore",
    "points": 96,
    "act": "I",
    "attack": "Range",
    "characteristics": [
      "Minion: Speed: 4, Health: 5, Defense: 5, Attack: 1 3, Abilities: Ravage, Surge: Pierce 2, Surge: +1 Range",
      "Master: Speed: 4, Health: 7, Defense: 5, Attack: 1 3, Abilities: Ravage, Surge: Pierce 3, Surge: Poison, Surge: +1 Range"
    ],
    "expansion": "Conversion Kit",
    "image": "monsters/manticore-ck-act1-front.png",
    "xws": "manticore"
  },
  {
    "name": "CK-Manticore",
    "points": 97,
    "act": "I",
    "traits": [
      "Wilderness",
      "Dark"
    ],
    "ability rules": [
      "Ravage: Both of this monster's actions on a turn may be attack actions.",
      "Pierce X: This attack ignores X Shields rolled on the defense dice.",
      "Poison: If this attack deals at least 1 Heart (after the defense roll), the target is Poisoned."
    ],
    "group size": [
      "2 Heroes: 1,0",
      "3 Heroes: 0,1",
      "4 Heroes: 1,1"
    ],
    "expansion": "Conversion Kit",
    "image": "monsters/manticore-ck-act1-back.png",
    "xws": "manticore"
  },
  {
    "name": "CK-Manticore",
    "points": 98,
    "act": "II",
    "attack": "Range",
    "characteristics": [
      "Minion: Speed: 4, Health: 7, Defense: 5, Attack: 1 3 3, Abilities: Ravage, Surge: Pierce 3, Surge: +2 Range",
      "Master: Speed: 4, Health: 9, Defense: 5, Attack: 1 3 3, Abilities: Ravage, Surge: Pierce 4, Surge: Poison, Surge: +2 Range"
    ],
    "expansion": "Conversion Kit",
    "image": "monsters/manticore-ck-act2-front.png",
    "xws": "manticore"
  },
  {
    "name": "CK-Manticore",
    "points": 99,
    "act": "II",
    "traits": [
      "Wilderness",
      "Dark"
    ],
    "ability rules": [
      "Ravage: Both of this monster's actions on a turn may be attack actions.",
      "Pierce X: This attack ignores X Shields rolled on the defense dice.",
      "Poison: If this attack deals at least 1 Heart (after the defense roll), the target is Poisoned."
    ],
    "group size": [
      "2 Heroes: 1,0",
      "3 Heroes: 0,1",
      "4 Heroes: 1,1"
    ],
    "expansion": "Conversion Kit",
    "image": "monsters/manticore-ck-act2-back.png",
    "xws": "manticore"
  },
  {
    "name": "CK-Medusa",
    "points": 100,
    "act": "I",
    "attack": "Range",
    "characteristics": [
      "Minion: Speed: 4, Health: 4, Defense: 5, Attack: 1 3, Abilities: Surge: Immobilize, Surge: Poison",
      "Master: Speed: 4, Health: 6, Defense: 5, Attack: 1 3, Abilities: Surge: Immobilize, Surge: Poison, Surge: Stun"
    ],
    "expansion": "Conversion Kit",
    "image": "monsters/medusa-ck-act1-front.png",
    "xws": "medusa"
  },
  {
    "name": "CK-Medusa",
    "points": 101,
    "act": "I",
    "traits": [
      "Cursed",
      "Building"
    ],
    "ability rules": [
      "Immobilize: If this attack deals at least 1 Heart (after the defense roll), the target is Immobilized.",
      "Poison: If this attack deals at least 1 Heart (after the defense roll), the target is Poisoned.",
      "Stun: If this attack deals at least 1 Heart (after the defense roll), the target is Stunned."
    ],
    "group size": [
      "2 Heroes: 2,0",
      "3 Heroes: 1,1",
      "4 Heroes: 2,1"
    ],
    "expansion": "Conversion Kit",
    "image": "monsters/medusa-ck-act1-back.png",
    "xws": "medusa"
  },
  {
    "name": "CK-Medusa",
    "points": 102,
    "act": "II",
    "attack": "Range",
    "characteristics": [
      "Minion: Speed: 4, Health: 6, Defense: 5 7, Attack: 1 3 3, Abilities: Surge: Immobilize, Surge: Poison",
      "Master: Speed: 4, Health: 9, Defense: 5 7, Attack: 1 3 3, Abilities: Surge: Immobilize, Surge: Poison, Surge: Stun"
    ],
    "expansion": "Conversion Kit",
    "image": "monsters/medusa-ck-act2-front.png",
    "xws": "medusa"
  },
  {
    "name": "CK-Medusa",
    "points": 103,
    "act": "II",
    "traits": [
      "Cursed",
      "Building"
    ],
    "ability rules": [
      "Immobilize: If this attack deals at least 1 Heart (after the defense roll), the target is Immobilized.",
      "Poison: If this attack deals at least 1 Heart (after the defense roll), the target is Poisoned.",
      "Stun: If this attack deals at least 1 Heart (after the defense roll), the target is Stunned."
    ],
    "group size": [
      "2 Heroes: 2,0",
      "3 Heroes: 1,1",
      "4 Heroes: 2,1"
    ],
    "expansion": "Conversion Kit",
    "image": "monsters/medusa-ck-act2-back.png",
    "xws": "medusa"
  },
  {
    "name": "CK-Naga",
    "points": 104,
    "act": "I",
    "attack": "Range",
    "characteristics": [
      "Minion: Speed: 4, Health: 4, Defense: 6, Attack: 1 2, Abilities: Sorcery 1, Action: Grab",
      "Master: Speed: 4, Health: 5, Defense: 6, Attack: 1 2, Abilities: Sorcery 2, Action: Grab, Surge: Poison"
    ],
    "expansion": "Conversion Kit",
    "image": "monsters/naga-ck-act1-front.png",
    "xws": "naga"
  },
  {
    "name": "CK-Naga",
    "points": 105,
    "act": "I",
    "traits": [
      "Water",
      "Cave"
    ],
    "ability rules": [
      "Sorcery X: After making an attack roll, this monster may convert up to X range to Hearts, or up to X Hearts to range.",
      "Grab: Choose a hero adjacent to this monster. The hero must test Might. If he fails he is Immobilized.",
      "Poison: If this attack deals at least 1 Heart (after the defense roll), the target is Poisoned."
    ],
    "group size": [
      "2 Heroes: 0,1",
      "3 Heroes: 1,1",
      "4 Heroes: 2,1"
    ],
    "expansion": "Conversion Kit",
    "image": "monsters/naga-ck-act1-back.png",
    "xws": "naga"
  },
  {
    "name": "CK-Naga",
    "points": 106,
    "act": "II",
    "attack": "Range",
    "characteristics": [
      "Minion: Speed: 4, Health: 6, Defense: 6, Attack: 1 2, Abilities: Sorcery 3, Action: Grab",
      "Master: Speed: 4, Health: 7, Defense: 6, Attack: 1 2 3, Abilities: Sorcery 3, Action: Grab, Surge: Poison"
    ],
    "expansion": "Conversion Kit",
    "image": "monsters/naga-ck-act2-front.png",
    "xws": "naga"
  },
  {
    "name": "CK-Naga",
    "points": 107,
    "act": "II",
    "traits": [
      "Water",
      "Cave"
    ],
    "ability rules": [
      "Sorcery X: After making an attack roll, this monster may convert up to X range to Hearts, or up to X Hearts to range.",
      "Grab: Choose a hero adjacent to this monster. The hero must test Might. If he fails he is Immobilized.",
      "Poison: If this attack deals at least 1 Heart (after the defense roll), the target is Poisoned."
    ],
    "group size": [
      "2 Heroes: 0,1",
      "3 Heroes: 1,1",
      "4 Heroes: 2,1"
    ],
    "expansion": "Conversion Kit",
    "image": "monsters/naga-ck-act2-back.png",
    "xws": "naga"
  },
  {
    "name": "CK-Ogre",
    "points": 108,
    "act": "I",
    "attack": "Melee",
    "characteristics": [
      "Minion: Speed: 3, Health: 6, Defense: 5, Attack: 1 3, Abilities: Surge: Knockback, Surge: +3 Hearts",
      "Master: Speed: 3, Health: 8, Defense: 5, Attack: 1 2, Abilities: Undying, Surge: Knockback, Surge: +3 Hearts"
    ],
    "expansion": "Conversion Kit",
    "image": "monsters/ogre-ck-act1-front.png",
    "xws": "ogre"
  },
  {
    "name": "CK-Ogre",
    "points": 109,
    "act": "I",
    "traits": [
      "Building",
      "Cave"
    ],
    "ability rules": [
      "Undying: When this monster is defeated, remove it from the map and then replace it with a minion of the same type, ignoring group limits.",
      "Knockback: Remove the target from the map, then place him on any empty space within 3 spaces of his original space. He counts as entering that space."
    ],
    "group size": [
      "2 Heroes: 1,0",
      "3 Heroes: 0,1",
      "4 Heroes: 1,1"
    ],
    "expansion": "Conversion Kit",
    "image": "monsters/ogre-ck-act1-back.png",
    "xws": "ogre"
  },
  {
    "name": "CK-Ogre",
    "points": 110,
    "act": "II",
    "attack": "Melee",
    "characteristics": [
      "Minion: Speed: 3, Health: 9, Defense: 5, Attack: 1 2, Abilities: Surge: Knockback, Surge: +3 Hearts",
      "Master: Speed: 3, Health: 12, Defense: 5, Attack: 1 2 3, Abilities: Undying, Surge: Knockback, Surge: +3 Hearts"
    ],
    "expansion": "Conversion Kit",
    "image": "monsters/ogre-ck-act2-front.png",
    "xws": "ogre"
  },
  {
    "name": "CK-Ogre",
    "points": 111,
    "act": "II",
    "traits": [
      "Building",
      "Cave"
    ],
    "ability rules": [
      "Undying: When this monster is defeated, remove it from the map and then replace it with a minion of the same type, ignoring group limits.",
      "Knockback: Remove the target from the map, then place him on any empty space within 3 spaces of his original space. He counts as entering that space."
    ],
    "group size": [
      "2 Heroes: 1,0",
      "3 Heroes: 0,1",
      "4 Heroes: 1,1"
    ],
    "expansion": "Conversion Kit",
    "image": "monsters/ogre-ck-act2-back.png",
    "xws": "ogre"
  },
  {
    "name": "CK-Razorwing",
    "points": 112,
    "act": "I",
    "attack": "Melee",
    "characteristics": [
      "Minion: Speed: 5, Health: 4, Defense: 7, Attack: 1 3, Abilities: Fly, Surge: +1 Heart",
      "Master: Speed: 6, Health: 6, Defense: 7, Attack: 1 3, Abilities: Fly, Surge: Stun, Surge: +1 Heart"
    ],
    "expansion": "Conversion Kit",
    "image": "monsters/razorwing-ck-act1-front.png",
    "xws": "razorwing"
  },
  {
    "name": "CK-Razorwing",
    "points": 113,
    "act": "I",
    "traits": [
      "Wilderness",
      "Cave"
    ],
    "ability rules": [
      "Fly: This monster may ignore enemy figures and the effects of terrain while moving. It must end its movement in an empty space following normal movement rules.",
      "Stun: If this attack deals at least 1 Heart (after the defense roll), the target is Stunned."
    ],
    "group size": [
      "2 Heroes: 1,1",
      "3 Heroes: 2,1",
      "4 Heroes: 3,1"
    ],
    "expansion": "Conversion Kit",
    "image": "monsters/razorwing-ck-act1-back.png",
    "xws": "razorwing"
  },
  {
    "name": "CK-Razorwing",
    "points": 114,
    "act": "II",
    "attack": "Melee",
    "characteristics": [
      "Minion: Speed: 5, Health: 7, Defense: 7, Attack: 1 3, Abilities: Fly, Surge: +2 Hearts",
      "Master: Speed: 6, Health: 9, Defense: 7, Attack: 1 3 3, Abilities: Fly, Surge: Stun, Surge: +2 Hearts"
    ],
    "expansion": "Conversion Kit",
    "image": "monsters/razorwing-ck-act2-front.png",
    "xws": "razorwing"
  },
  {
    "name": "CK-Razorwing",
    "points": 115,
    "act": "II",
    "traits": [
      "Wilderness",
      "Cave"
    ],
    "ability rules": [
      "Fly: This monster may ignore enemy figures and the effects of terrain while moving. It must end its movement in an empty space following normal movement rules.",
      "Stun: If this attack deals at least 1 Heart (after the defense roll), the target is Stunned."
    ],
    "group size": [
      "2 Heroes: 1,1",
      "3 Heroes: 2,1",
      "4 Heroes: 3,1"
    ],
    "expansion": "Conversion Kit",
    "image": "monsters/razorwing-ck-act2-back.png",
    "xws": "razorwing"
  },
  {
    "name": "CK-Shade",
    "points": 116,
    "act": "I",
    "attack": "Melee",
    "characteristics": [
      "Minion: Speed: 4, Health: 2, Defense: 6, Attack: 1 3, Abilities: Fly, Surge: Pierce 2",
      "Master: Speed: 4, Health: 4, Defense: 6, Attack: 1 3, Abilities: Fly, Action: Leech, Surge: Pierce 2"
    ],
    "expansion": "Conversion Kit",
    "image": "monsters/shade-ck-act1-front.png",
    "xws": "shade"
  },
  {
    "name": "CK-Shade",
    "points": 117,
    "act": "I",
    "traits": [
      "Cursed",
      "Dark"
    ],
    "ability rules": [
      "Fly: This monster may ignore enemy figures and the effects of terrain while moving. It must end its movement in an empty space following normal movement rules.",
      "Leech: Choose a hero adjacent to this monster. That hero must test Might. If he fails, roll 1 3 power die. The hero suffers 1 Fatigue per Heart rolled, and this monster recovers 1 Heart per Fatigue suffe2.",
      "Pierce X: This attack ignores X Shields rolled on the defense dice."
    ],
    "group size": [
      "2 Heroes: 2,1",
      "3 Heroes: 3,1",
      "4 Heroes: 4,1"
    ],
    "expansion": "Conversion Kit",
    "image": "monsters/shade-ck-act1-back.png",
    "xws": "shade"
  },
  {
    "name": "CK-Shade",
    "points": 118,
    "act": "II",
    "attack": "Melee",
    "characteristics": [
      "Minion: Speed: 5, Health: 4, Defense: 6, Attack: 1 2, Abilities: Fly, Surge: Pierce 2",
      "Master: Speed: 5, Health: 6, Defense: 6, Attack: 1 2 3, Abilities: Fly, Action: Leech, Surge: Pierce 2"
    ],
    "expansion": "Conversion Kit",
    "image": "monsters/shade-ck-act2-front.png",
    "xws": "shade"
  },
  {
    "name": "CK-Shade",
    "points": 119,
    "act": "II",
    "traits": [
      "Cursed",
      "Dark"
    ],
    "ability rules": [
      "Fly: This monster may ignore enemy figures and the effects of terrain while moving. It must end its movement in an empty space following normal movement rules.",
      "Leech: Choose a hero adjacent to this monster. That hero must test Might. If he fails, roll 1 3 power die. The hero suffers 1 Fatigue per Heart rolled, and this monster recovers 1 Heart per Fatigue suffe2.",
      "Pierce X: This attack ignores X Shields rolled on the defense dice."
    ],
    "group size": [
      "2 Heroes: 2,1",
      "3 Heroes: 3,1",
      "4 Heroes: 4,1"
    ],
    "expansion": "Conversion Kit",
    "image": "monsters/shade-ck-act2-back.png",
    "xws": "shade"
  },
  {
    "name": "CK-Skeleton Archer",
    "points": 120,
    "act": "I",
    "attack": "Range",
    "characteristics": [
      "Minion: Speed: 4, Health: 2, Defense: 7, Attack: 1 3, Abilities: Pierce 1, Surge: +1 Range",
      "Master: Speed: 4, Health: 5, Defense: 7, Attack: 1 3, Abilities: Pierce 1, Undying, Surge: +1 Range, Surge: +1 Heart"
    ],
    "expansion": "Conversion Kit",
    "image": "monsters/skeleton-archer-ck-act1-front.png",
    "xws": "skeletonarcher"
  },
  {
    "name": "CK-Skeleton Archer",
    "points": 121,
    "act": "I",
    "traits": [
      "Cursed",
      "Civilized"
    ],
    "ability rules": [
      "Pierce X: This attack ignores X Shields rolled on the defense dice.",
      "Undying: When this monster is defeated, remove it from the map and then replace it with a minion of the same type, ignoring group limits."
    ],
    "group size": [
      "2 Heroes: 2,1",
      "3 Heroes: 3,1",
      "4 Heroes: 4,1"
    ],
    "expansion": "Conversion Kit",
    "image": "monsters/skeleton-archer-ck-act1-back.png",
    "xws": "skeletonarcher"
  },
  {
    "name": "CK-Skeleton Archer",
    "points": 122,
    "act": "II",
    "attack": "Range",
    "characteristics": [
      "Minion: Speed: 4, Health: 4, Defense: 7, Attack: 1 3, Abilities: Pierce 2, Surge: +1 Heart, Surge: +2 Range",
      "Master: Speed: 4, Health: 8, Defense: 7, Attack: 1 3, Abilities: Pierce 2, Undying, Surge: +2 Range, Surge: +2 Hearts"
    ],
    "expansion": "Conversion Kit",
    "image": "monsters/skeleton-archer-ck-act2-front.png",
    "xws": "skeletonarcher"
  },
  {
    "name": "CK-Skeleton Archer",
    "points": 123,
    "act": "II",
    "traits": [
      "Cursed",
      "Civilized"
    ],
    "ability rules": [
      "Pierce X: This attack ignores X Shields rolled on the defense dice.",
      "Undying: When this monster is defeated, remove it from the map and then replace it with a minion of the same type, ignoring group limits."
    ],
    "group size": [
      "2 Heroes: 2,1",
      "3 Heroes: 3,1",
      "4 Heroes: 4,1"
    ],
    "expansion": "Conversion Kit",
    "image": "monsters/skeleton-archer-ck-act2-back.png",
    "xws": "skeletonarcher"
  },
  {
    "name": "CK-Sorcerer",
    "points": 124,
    "act": "I",
    "attack": "Range",
    "characteristics": [
      "Minion: Speed: 4, Health: 3, Defense: 5, Attack: 1 3, Abilities: Sorcery 2, Action: Summon",
      "Master: Speed: 4, Health: 5, Defense: 5, Attack: 1 3, Abilities: Sorcery 3, Undying, Action: Summon"
    ],
    "expansion": "Conversion Kit",
    "image": "monsters/sorcerer-ck-act1-front.png",
    "xws": "sorcerer"
  },
  {
    "name": "CK-Sorcerer",
    "points": 125,
    "act": "I",
    "traits": [
      "Civilized",
      "Building"
    ],
    "ability rules": [
      "Sorcery X: After making an attack roll, this monster may convert up to X range to Hearts, or up to X Hearts to range.",
      "Undying: When this monster is defeated, remove it from the map and then replace it with a minion of the same type, ignoring group limits.",
      "Summon: Choose a minion within 3 spaces of this monster. Place that minion in an empty space adjacent to this monster."
    ],
    "group size": [
      "2 Heroes: 1,1",
      "3 Heroes: 2,1",
      "4 Heroes: 3,1"
    ],
    "expansion": "Conversion Kit",
    "image": "monsters/sorcerer-ck-act1-back.png",
    "xws": "sorcerer"
  },
  {
    "name": "CK-Sorcerer",
    "points": 126,
    "act": "II",
    "attack": "Range",
    "characteristics": [
      "Minion: Speed: 4, Health: 5, Defense: 5, Attack: 1 2, Abilities: Sorcery 2, Action: Summon",
      "Master: Speed: 4, Health: 8, Defense: 5, Attack: 1 2, Abilities: Sorcery 3, Undying, Action: Summon"
    ],
    "expansion": "Conversion Kit",
    "image": "monsters/sorcerer-ck-act2-front.png",
    "xws": "sorcerer"
  },
  {
    "name": "CK-Sorcerer",
    "points": 127,
    "act": "II",
    "traits": [
      "Civilized",
      "Building"
    ],
    "ability rules": [
      "Sorcery X: After making an attack roll, this monster may convert up to X range to Hearts, or up to X Hearts to range.",
      "Undying: When this monster is defeated, remove it from the map and then replace it with a minion of the same type, ignoring group limits.",
      "Summon: Choose a minion within 3 spaces of this monster. Place that minion in an empty space adjacent to this monster."
    ],
    "group size": [
      "2 Heroes: 1,1",
      "3 Heroes: 2,1",
      "4 Heroes: 3,1"
    ],
    "expansion": "Conversion Kit",
    "image": "monsters/sorcerer-ck-act2-back.png",
    "xws": "sorcerer"
  },
  {
    "name": "CK-Troll",
    "points": 128,
    "act": "I",
    "attack": "Melee",
    "characteristics": [
      "Minion: Speed: 3, Health: 8, Defense: 5, Attack: 1 2, Abilities: Reach, Action: Bash",
      "Master: Speed: 3, Health: 10, Defense: 5, Attack: 1 2, Abilities: Reach, Action: Bash, Action: Sweep"
    ],
    "expansion": "Conversion Kit",
    "image": "monsters/troll-ck-act1-front.png",
    "xws": "troll"
  },
  {
    "name": "CK-Troll",
    "points": 129,
    "act": "I",
    "traits": [
      "Mountains",
      "Cave"
    ],
    "ability rules": [
      "Reach: This monster may attack targets up to 2 spaces away.",
      "Bash: Choose an adjacent hero. That hero must test Awareness. If he fails, he suffers 3 Hearts.",
      "Sweep: Perform an attack. This attack effects each figure within range of this monster's attack. Each figure rolls defense dice separately."
    ],
    "group size": [
      "2 Heroes: 1,0",
      "3 Heroes: 0,1",
      "4 Heroes: 1,1"
    ],
    "expansion": "Conversion Kit",
    "image": "monsters/troll-ck-act1-back.png",
    "xws": "troll"
  },
  {
    "name": "CK-Troll",
    "points": 130,
    "act": "II",
    "attack": "Melee",
    "characteristics": [
      "Minion: Speed: 3, Health: 10, Defense: 5, Attack: 1 2 2, Abilities: Reach, Action: Bash",
      "Master: Speed: 3, Health: 13, Defense: 5, Attack: 1 2 2, Abilities: Reach, Action: Bash, Action: Sweep"
    ],
    "expansion": "Conversion Kit",
    "image": "monsters/troll-ck-act2-front.png",
    "xws": "troll"
  },
  {
    "name": "CK-Troll",
    "points": 131,
    "act": "II",
    "traits": [
      "Mountains",
      "Cave"
    ],
    "ability rules": [
      "Reach: This monster may attack targets up to 2 spaces away.",
      "Bash: Choose an adjacent hero. That hero must test Awareness. If he fails, he suffers 3 Hearts.",
      "Sweep: Perform an attack. This attack effects each figure within range of this monster's attack. Each figure rolls defense dice separately."
    ],
    "group size": [
      "2 Heroes: 1,0",
      "3 Heroes: 0,1",
      "4 Heroes: 1,1"
    ],
    "expansion": "Conversion Kit",
    "image": "monsters/troll-ck-act2-back.png",
    "xws": "troll"
  },
  {
    "name": "CK-Wendigo",
    "points": 132,
    "act": "I",
    "attack": "Melee",
    "characteristics": [
      "Minion: Speed: 4, Health: 4, Defense: 5, Attack: 1 2, Abilities: Ravage, Stealthy, Surge: +1 Heart",
      "Master: Speed: 4, Health: 7, Defense: 5, Attack: 1 2, Abilities: Freezing, Ravage, Stealthy, Surge: +1 Heart"
    ],
    "expansion": "Conversion Kit",
    "image": "monsters/wendigo-ck-act1-front.png",
    "xws": "wendigo"
  },
  {
    "name": "CK-Wendigo",
    "points": 133,
    "act": "I",
    "traits": [
      "Cold",
      "Cave"
    ],
    "ability rules": [
      "Freezing: Each time a hero enters a space adjacent to this monster, that hero suffers 1 Fatigue.",
      "Ravage: Both of this monster's actions on a turn may be attack actions.",
      "Stealthy: Each attack that targets this monster must roll 3 additional range beyond the normally requi2 amount or the attack is a miss."
    ],
    "group size": [
      "2 Heroes: 0,1",
      "3 Heroes: 1,1",
      "4 Heroes: 2,1"
    ],
    "expansion": "Conversion Kit",
    "image": "monsters/wendigo-ck-act1-back.png",
    "xws": "wendigo"
  },
  {
    "name": "CK-Wendigo",
    "points": 134,
    "act": "II",
    "attack": "Melee",
    "characteristics": [
      "Minion: Speed: 4, Health: 6, Defense: 5, Attack: 1 2, Abilities: Ravage, Stealthy, Surge: +2 Hearts",
      "Master: Speed: 4, Health: 10, Defense: 5, Attack: 1 2 3, Abilities: Freezing, Ravage, Stealthy, Surge: +2 Hearts"
    ],
    "expansion": "Conversion Kit",
    "image": "monsters/wendigo-ck-act2-front.png",
    "xws": "wendigo"
  },
  {
    "name": "CK-Wendigo",
    "points": 135,
    "act": "II",
    "traits": [
      "Cold",
      "Cave"
    ],
    "ability rules": [
      "Freezing: Each time a hero enters a space adjacent to this monster, that hero suffers 1 Fatigue.",
      "Ravage: Both of this monster's actions on a turn may be attack actions.",
      "Stealthy: Each attack that targets this monster must roll 3 additional range beyond the normally requi2 amount or the attack is a miss."
    ],
    "group size": [
      "2 Heroes: 0,1",
      "3 Heroes: 1,1",
      "4 Heroes: 2,1"
    ],
    "expansion": "Conversion Kit",
    "image": "monsters/wendigo-ck-act2-back.png",
    "xws": "wendigo"
  },
  {
    "name": "Fire Imps",
    "points": 136,
    "act": "I",
    "attack": "Range",
    "characteristics": [
      "Minion: Speed: 5, Health: 2, Defense: 5, Attack: 1 3, Abilities: Flame Fiend, Surge: +1 Range, Surge: Burn",
      "Master: Speed: 5, Health: 4, Defense: 5, Attack: 1 3, Abilities: Combustible, Flame Fiend, Surge: +1 Heart, Surge: Burn"
    ],
    "expansion": "Lair Of The Wyrm",
    "image": "monsters/fire-imps-lw-act1-front.png",
    "xws": "fireimps"
  },
  {
    "name": "Fire Imps",
    "points": 137,
    "act": "I",
    "traits": [
      "Hot",
      "Cursed"
    ],
    "ability rules": [
      "Combustible: When this monster is defeated, each hero adjacent to this monster suffers 1 Heart.",
      "Flame Fiend: This monster ignores all effects of lava. Each time this monster ends its turn in a space containing lava, it recovers 1 Heart. In addition, this monster never suffers Hearts from burning.",
      "Burn: If this attack deals at least 1 Heart (after the defense roll), the target is Burning."
    ],
    "group size": [
      "2 Heroes: 2,1",
      "3 Heroes: 3,1",
      "4 Heroes: 3,2"
    ],
    "expansion": "Lair Of The Wyrm",
    "image": "monsters/fire-imps-lw-act1-back.png",
    "xws": "fire-imps"
  },
  {
    "name": "Fire Imps",
    "points": 138,
    "act": "II",
    "attack": "Range",
    "characteristics": [
      "Minion: Speed: 5, Health: 4, Defense: 5, Attack: 1 3, Abilities: Flame Fiend, Surge: +1 Range, Surge: Burn",
      "Master: Speed: 5, Health: 6, Defense: 5, Attack: 1 3 3, Abilities: Combustible, Flame Fiend, Surge: +1 Heart, Surge: Burn"
    ],
    "expansion": "Lair Of The Wyrm",
    "image": "monsters/fire-imps-lw-act2-front.png",
    "xws": "fireimps"
  },
  {
    "name": "Fire Imps",
    "points": 139,
    "act": "II",
    "traits": [
      "Hot",
      "Cursed"
    ],
    "ability rules": [
      "Combustible: When this monster is defeated, each hero adjacent to this monster suffers 1 Heart.",
      "Flame Fiend: This monster ignores all effects of lava. Each time this monster ends its turn in a space containing lava, it recovers 1 Heart. In addition, this monster never suffers Hearts from burning.",
      "Burn: If this attack deals at least 1 Heart (after the defense roll), the target is Burning."
    ],
    "group size": [
      "2 Heroes: 2,1",
      "3 Heroes: 3,1",
      "4 Heroes: 3,2"
    ],
    "expansion": "Lair Of The Wyrm",
    "image": "monsters/fire-imps-lw-act2-back.png",
    "xws": "fire-imps"
  },
  {
    "name": "Hybrid Sentinel",
    "points": 140,
    "act": "I",
    "attack": "Melee",
    "characteristics": [
      "Minion: Speed: 4, Health: 5, Defense: 6, Attack: 1 2, Abilities: Fly, Prey on the Weak, Surge: +1 Heart",
      "Master: Speed: 4, Health: 8, Defense: 6, Attack: 1 2, Abilities: Fly, Prey on the Weak, Surge: Fire Breath, Surge: +1 Heart"
    ],
    "expansion": "Lair Of The Wyrm",
    "image": "monsters/hybrid-sentinel-lw-act1-front.png",
    "xws": "hybridsentinel"
  },
  {
    "name": "Hybrid Sentinel",
    "points": 141,
    "act": "I",
    "traits": [
      "Mountain",
      "Cave"
    ],
    "ability rules": [
      "Fly: This monster may ignore enemy figures and the effects of terrain while moving. It must end its movement in an empty space following normal movement rules.",
      "Prey on the Weak: Each of this monster's attacks targeting a hero with 2 or less Might gains +1 Heart.",
      "Fire Breath: Starting with the target space, trace a path of 4 spaces in any direction. All figures on this path are affected by this attack. Each figure rolls defense dice separately."
    ],
    "group size": [
      "2 Heroes: 0,1",
      "3 Heroes: 1,1",
      "4 Heroes: 2,1"
    ],
    "expansion": "Lair Of The Wyrm",
    "image": "monsters/hybrid-sentinel-lw-act1-back.png",
    "xws": "hybridsentinel"
  },
  {
    "name": "Hybrid Sentinel",
    "points": 142,
    "act": "II",
    "attack": "Melee",
    "characteristics": [
      "Minion: Speed: 4, Health: 6, Defense: 6 7, Attack: 1 2, Abilities: Fly, Prey on the Weak, Surge: +1 Heart, Surge: +1 Heart",
      "Master: Speed: 4, Health: 9, Defense: 6 5, Attack: 1 3 2, Abilities: Fly, Prey on the Weak, Surge: Fire Breath, Surge: +2 Hearts"
    ],
    "expansion": "Lair Of The Wyrm",
    "image": "monsters/hybrid-sentinel-lw-act2-front.png",
    "xws": "hybridsentinel"
  },
  {
    "name": "Hybrid Sentinel",
    "points": 143,
    "act": "II",
    "traits": [
      "Mountain",
      "Cave"
    ],
    "ability rules": [
      "Fly: This monster may ignore enemy figures and the effects of terrain while moving. It must end its movement in an empty space following normal movement rules.",
      "Prey on the Weak: Each of this monster's attacks targeting a hero with 2 or less Might gains +1 Heart.",
      "Fire Breath: Starting with the target space, trace a path of 4 spaces in any direction. All figures on this path are affected by this attack. Each figure rolls defense dice separately."
    ],
    "group size": [
      "2 Heroes: 0,1",
      "3 Heroes: 1,1",
      "4 Heroes: 2,1"
    ],
    "expansion": "Lair Of The Wyrm",
    "image": "monsters/hybrid-sentinel-lw-act2-back.png",
    "xws": "hybridsentinel"
  },
  {
    "name": "Arachyura",
    "points": 144,
    "act": "I",
    "attack": "Melee",
    "characteristics": [
      "Minion: Speed: 3, Health: 5, Defense: 5 7, Attack: 1 2 4, Abilities: Action: Pincer Attack, Surge Surge: Pierce 1",
      "Master: Speed: 3, Health: 7, Defense: 5 7, Attack: 1 2 4, Abilities: Action: Lingering Curse, Pincer Attack, Surge Surge: Pierce 2"
    ],
    "expansion": "Labyrinth Of Ruin",
    "image": "monsters/arachyura-lr-act1-front.png",
    "xws": "arachyura"
  },
  {
    "name": "Arachyura",
    "points": 145,
    "act": "I",
    "traits": [
      "Wilderness",
      "Cursed"
    ],
    "ability rules": [
      "Lingering Curse: When this monster is defeated, each adjacent hero must test Willpower. Each hero that fails is Cursed.",
      "Pincer Attack: Perform an attack targeting up to 2 heroes adjacent to this monster. 1 attack roll is made but each hero rolls defense dice separately. Each target that suffers at least 1 Heart from this attack (after the defense roll) is Immobilized.",
      "Pierce X: This attack ignores X Shields rolled on the defense dice."
    ],
    "group size": [
      "2 Heroes: 1,0",
      "3 Heroes: 0,1",
      "4 Heroes: 1,1"
    ],
    "expansion": "Labyrinth Of Ruin",
    "image": "monsters/arachyura-lr-act1-back.png",
    "xws": "arachyura"
  },
  {
    "name": "Arachyura",
    "points": 146,
    "act": "II",
    "attack": "Melee",
    "characteristics": [
      "Minion: Speed: 3, Health: 7, Defense: 6, Attack: 1 2 4, Abilities: Action: Pincer Attack, Surge Surge: Pierce 2",
      "Master: Speed: 3, Health: 9, Defense: 6, Attack: 1 2 4, Abilities: Action: Lingering Curse, Pincer Attack, Surge Surge: Pierce 3"
    ],
    "expansion": "Labyrinth Of Ruin",
    "image": "monsters/arachyura-lr-act2-front.png",
    "xws": "arachyura"
  },
  {
    "name": "Arachyura",
    "points": 147,
    "act": "II",
    "traits": [
      "Wilderness",
      "Cursed"
    ],
    "ability rules": [
      "Lingering Curse: When this monster is defeated, each adjacent hero must test Willpower. Each hero that fails is Cursed.",
      "Pincer Attack: Perform an attack targeting up to 2 heroes adjacent to this monster. 1 attack roll is made but each hero rolls defense dice separately. Each target that suffers at least 1 Heart from this attack (after the defense roll) is Immobilized.",
      "Pierce X: This attack ignores X Shields rolled on the defense dice."
    ],
    "group size": [
      "2 Heroes: 1,0",
      "3 Heroes: 0,1",
      "4 Heroes: 1,1"
    ],
    "expansion": "Labyrinth Of Ruin",
    "image": "monsters/arachyura-lr-act2-back.png",
    "xws": "arachyura"
  },
  {
    "name": "Carrion Drake",
    "points": 148,
    "act": "I",
    "attack": "Melee",
    "characteristics": [
      "Minion: Speed: 5, Health: 6, Defense: 5, Attack: 1 3, Abilities: Fly, Surge: Disease, Surge: +1 Heart",
      "Master: Speed: 5, Health: 8, Defense: 5, Attack: 1 3, Abilities: Fly, Plagued, Surge: Disease, Surge: +2 Hearts"
    ],
    "expansion": "Labyrinth Of Ruin",
    "image": "monsters/carrion-drake-lr-act1-front.png",
    "xws": "carriondrake"
  },
  {
    "name": "Carrion Drake",
    "points": 149,
    "act": "I",
    "traits": [
      "Water",
      "Dark"
    ],
    "ability rules": [
      "Fly: This monster may ignore enemy figures and the effects of terrain while moving. It must end its movement in an empty space following normal movement rules.",
      "Plagued: When a hero adjacent to this monster would discard a Poisoned or a Diseased Condition card, he instead keeps the card.",
      "Disease: If this attack deals at least 1 Heart (after the defense roll), the target is Diseased."
    ],
    "group size": [
      "2 Heroes: 0,1",
      "3 Heroes: 1,1",
      "4 Heroes: 2,1"
    ],
    "expansion": "Labyrinth Of Ruin",
    "image": "monsters/carrion-drake-lr-act1-back.png",
    "xws": "carriondrake"
  },
  {
    "name": "Carrion Drake",
    "points": 150,
    "act": "II",
    "attack": "Melee",
    "characteristics": [
      "Minion: Speed: 5, Health: 7, Defense: 5 7, Attack: 1 3 4, Abilities: Fly, Surge: Disease, Surge: +1 Heart, Surge: +1 Heart",
      "Master: Speed: 5, Health: 10, Defense: 5 5, Attack: 1 3 4, Abilities: Fly, Plagued, Surge: Disease, Surge: +2 Hearts"
    ],
    "expansion": "Labyrinth Of Ruin",
    "image": "monsters/carrion-drake-lr-act2-front.png",
    "xws": "carriondrake"
  },
  {
    "name": "Carrion Drake",
    "points": 151,
    "act": "II",
    "traits": [
      "Water",
      "Dark"
    ],
    "ability rules": [
      "Fly: This monster may ignore enemy figures and the effects of terrain while moving. It must end its movement in an empty space following normal movement rules.",
      "Plagued: When a hero adjacent to this monster would discard a Poisoned or a Diseased Condition card, he instead keeps the card.",
      "Disease: If this attack deals at least 1 Heart (after the defense roll), the target is Diseased."
    ],
    "group size": [
      "2 Heroes: 0,1",
      "3 Heroes: 1,1",
      "4 Heroes: 2,1"
    ],
    "expansion": "Labyrinth Of Ruin",
    "image": "monsters/carrion-drake-lr-act2-back.png",
    "xws": "carriondrake"
  },
  {
    "name": "Goblin Witcher",
    "points": 152,
    "act": "I",
    "attack": "Range",
    "characteristics": [
      "Minion: Speed: 4, Health: 3, Defense: 5, Attack: 1 3, Abilities: Surge: Curse, Surge: +1 Range",
      "Master: Speed: 4, Health: 5, Defense: 5, Attack: 1 3, Abilities: Action: Bewitch, Surge: Curse, Surge: +2 Range"
    ],
    "expansion": "Labyrinth Of Ruin",
    "image": "monsters/goblin-witcher-lr-act1-front.png",
    "xws": "goblinwitcher"
  },
  {
    "name": "Goblin Witcher",
    "points": 153,
    "act": "I",
    "traits": [
      "Building",
      "Cursed"
    ],
    "ability rules": [
      "Bewitch: Move each adjacent Cursed hero up to 2 spaces in any direction of your choice.",
      "Curse: If this attack deals at least 1 Heart (after the defense roll), the target is Cursed."
    ],
    "group size": [
      "2 Heroes: 1,1",
      "3 Heroes: 2,1",
      "4 Heroes: 3,1"
    ],
    "expansion": "Labyrinth Of Ruin",
    "image": "monsters/goblin-witcher-lr-act1-back.png",
    "xws": "goblinwitcher"
  },
  {
    "name": "Goblin Witcher",
    "points": 154,
    "act": "II",
    "attack": "Range",
    "characteristics": [
      "Minion: Speed: 4, Health: 6, Defense: 5, Attack: 1 3, Abilities: Surge: Curse, Surge: +2 Range, Surge: +1 Heart",
      "Master: Speed: 4, Health: 8, Defense: 5, Attack: 1 3 4, Abilities: Action: Bewitch, Surge: Curse, Surge: +2 Range, Surge: +2 Hearts"
    ],
    "expansion": "Labyrinth Of Ruin",
    "image": "monsters/goblin-witcher-lr-act2-front.png",
    "xws": "goblinwitcher"
  },
  {
    "name": "Goblin Witcher",
    "points": 155,
    "act": "II",
    "traits": [
      "Building",
      "Cursed"
    ],
    "ability rules": [
      "Bewitch: Move each adjacent Cursed hero up to 2 spaces in any direction of your choice.",
      "Curse: If this attack deals at least 1 Heart (after the defense roll), the target is Cursed."
    ],
    "group size": [
      "2 Heroes: 1,1",
      "3 Heroes: 2,1",
      "4 Heroes: 3,1"
    ],
    "expansion": "Labyrinth Of Ruin",
    "image": "monsters/goblin-witcher-lr-act2-back.png",
    "xws": "goblinwitcher"
  },
  {
    "name": "Volucrix Reaver",
    "points": 156,
    "act": "I",
    "attack": "Melee",
    "characteristics": [
      "Minion: Speed: 4, Health: 3, Defense: 5, Attack: 1 3, Abilities: Action: Skirmish, Surge: Pierce 2",
      "Master: Speed: 4, Health: 5, Defense: 5, Attack: 1 3, Abilities: Ravage, Action: Skirmish, Surge: Pierce 2, Surge: +1 Heart"
    ],
    "expansion": "Labyrinth Of Ruin",
    "image": "monsters/volucrix-reaver-lr-act1-front.png",
    "xws": "volucrixreaver"
  },
  {
    "name": "Volucrix Reaver",
    "points": 157,
    "act": "I",
    "traits": [
      "Building",
      "Mountain"
    ],
    "ability rules": [
      "Ravage: Both of this monster's actions on a turn may be attack actions.",
      "Skirmish: This monster may move 3 spaces, the perform an attack.",
      "Pierce 2: This attack ignores 2 Shields rolled on the defense dice."
    ],
    "group size": [
      "2 Heroes: 2,0",
      "3 Heroes: 2,1",
      "4 Heroes: 3,1"
    ],
    "expansion": "Labyrinth Of Ruin",
    "image": "monsters/volucrix-reaver-lr-act1-back.png",
    "xws": "volucrixreaver"
  },
  {
    "name": "Volucrix Reaver",
    "points": 158,
    "act": "II",
    "attack": "Melee",
    "characteristics": [
      "Minion: Speed: 4, Health: 4, Defense: 5 7, Attack: 1 2, Abilities: Action: Skirmish, Surge: Pierce 2",
      "Master: Speed: 4, Health: 6, Defense: 5 7, Attack: 1 2, Abilities: Ravage, Action: Skirmish, Surge: Pierce 3, Surge: +2 Hearts"
    ],
    "expansion": "Labyrinth Of Ruin",
    "image": "monsters/volucrix-reaver-lr-act2-front.png",
    "xws": "volucrixreaver"
  },
  {
    "name": "Volucrix Reaver",
    "points": 159,
    "act": "II",
    "traits": [
      "Building",
      "Mountain"
    ],
    "ability rules": [
      "Ravage: Both of this monster's actions on a turn may be attack actions.",
      "Skirmish: This monster may move 3 spaces, the perform an attack.",
      "Pierce 2: This attack ignores 2 Shields rolled on the defense dice."
    ],
    "group size": [
      "2 Heroes: 2,0",
      "3 Heroes: 2,1",
      "4 Heroes: 3,1"
    ],
    "expansion": "Labyrinth Of Ruin",
    "image": "monsters/volucrix-reaver-lr-act2-back.png",
    "xws": "volucrixreaver"
  },
  {
    "name": "Harpy",
    "points": 160,
    "act": "I",
    "attack": "Melee",
    "characteristics": [
      "Minion: Speed: 5, Health: 3, Defense: 5, Attack: 1 4, Abilities: Fly, Surge: Swarm",
      "Master: Speed: 5, Health: 5, Defense: 5, Attack: 1 2, Abilities: Fly, Action: Flock, Surge: Swarm"
    ],
    "expansion": "The Trollfens",
    "image": "monsters/harpy-tf-act1-front.png",
    "xws": "harpy"
  },
  {
    "name": "Harpy",
    "points": 161,
    "act": "I",
    "traits": [
      "Wilderness",
      "Mountain"
    ],
    "ability rules": [
      "Fly: This monster may ignore enemy figures and the effects of terrain while moving. It must end its movement in an empty space following normal movement rules.",
      "Flock: Each minion monster from this monster group within 5 spaces of this monster may immediately move 2 spaces.",
      "Swarm: This monster deals +1 Heart for each other monster adjacent to the target."
    ],
    "group size": [
      "2 Heroes: 1,1",
      "3 Heroes: 2,1",
      "4 Heroes: 3,1"
    ],
    "expansion": "The Trollfens",
    "image": "monsters/harpy-tf-act1-back.png",
    "xws": "harpy"
  },
  {
    "name": "Harpy",
    "points": 162,
    "act": "II",
    "attack": "Melee",
    "characteristics": [
      "Minion: Speed: 5, Health: 4, Defense: 5, Attack: 1 3 4, Abilities: Fly, Surge: Swarm",
      "Master: Speed: 5, Health: 6, Defense: 5, Attack: 1 3 2, Abilities: Fly, Action: Flock, Surge: Swarm"
    ],
    "expansion": "The Trollfens",
    "image": "monsters/harpy-tf-act2-front.png",
    "xws": "harpy"
  },
  {
    "name": "Harpy",
    "points": 163,
    "act": "II",
    "traits": [
      "Wilderness",
      "Mountain"
    ],
    "ability rules": [
      "Fly: This monster may ignore enemy figures and the effects of terrain while moving. It must end its movement in an empty space following normal movement rules.",
      "Flock: Each minion monster from this monster group within 5 spaces of this monster may immediately move 2 spaces.",
      "Swarm: This monster deals +1 Heart for each other monster adjacent to the target."
    ],
    "group size": [
      "2 Heroes: 1,1",
      "3 Heroes: 2,1",
      "4 Heroes: 3,1"
    ],
    "expansion": "The Trollfens",
    "image": "monsters/harpy-tf-act2-back.png",
    "xws": "harpy"
  },
  {
    "name": "Plague Worm",
    "points": 164,
    "act": "I",
    "attack": "Melee",
    "characteristics": [
      "Minion: Speed: 2, Health: 5, Defense: 5 7, Attack: 1 2, Abilities: Action: Burrow, Surge: +1 Heart, Surge: Weaken",
      "Master: Speed: 2, Health: 7, Defense: 5 7, Attack: 1 2, Abilities: Pestilence, Action: Burrow, Surge: +2 Hearts, Surge: Weaken"
    ],
    "expansion": "The Trollfens",
    "image": "monsters/plague-worm-tf-act1-front.png",
    "xws": "Plague Worm"
  },
  {
    "name": "Plague Worm",
    "points": 165,
    "act": "I",
    "traits": [
      "Water",
      "Cave"
    ],
    "ability rules": [
      "Pestilence: Each hero adjacent to this monster at the start of his turn must test Willpower. Each hero that fails is Diseased.",
      "Burrow: Remove this figure from the map and place it in an empty or occupied space up to 3 spaces away. Each figure occupying any target space is moved to the closest empty space of your choice and suffers 1 Fatigue. Limit once per monster per turn.",
      "Weaken: If this attack deals at least 1 Heart (after the defense roll), the target is Weakened."
    ],
    "group size": [
      "2 Heroes: 0,1",
      "3 Heroes: 1,1",
      "4 Heroes: 2,1"
    ],
    "expansion": "The Trollfens",
    "image": "monsters/plague-worm-tf-act1-back.png",
    "xws": "plagueworm"
  },
  {
    "name": "Plague Worm",
    "points": 166,
    "act": "II",
    "attack": "Melee",
    "characteristics": [
      "Minion: Speed: 2, Health: 6, Defense: 5 5, Attack: 1 2 4, Abilities: Action: Burrow, Surge: +2 Hearts, Surge: Weaken",
      "Master: Speed: 2, Health: 9, Defense: 5 5, Attack: 1 2 4, Abilities: Pestilence, Action: Burrow, Surge: +3 Hearts, Surge: Weaken"
    ],
    "expansion": "The Trollfens",
    "image": "monsters/plague-worm-tf-act2-front.png",
    "xws": "Plague Worm"
  },
  {
    "name": "Plague Worm",
    "points": 167,
    "act": "II",
    "traits": [
      "Water",
      "Cave"
    ],
    "ability rules": [
      "Pestilence: Each hero adjacent to this monster at the start of his turn must test Willpower. Each hero that fails is Diseased.",
      "Burrow: Remove this figure from the map and place it in an empty or occupied space up to 3 spaces away. Each figure occupying any target space is moved to the closest empty space of your choice and suffers 1 Fatigue. Limit once per monster per turn.",
      "Weaken: If this attack deals at least 1 Heart (after the defense roll), the target is Weakened."
    ],
    "group size": [
      "2 Heroes: 0,1",
      "3 Heroes: 1,1",
      "4 Heroes: 2,1"
    ],
    "expansion": "The Trollfens",
    "image": "monsters/plague-worm-tf-act2-back.png",
    "xws": "plagueworm"
  },
  {
    "name": "Changeling",
    "points": 168,
    "act": "I",
    "attack": "Melee",
    "characteristics": [
      "Minion: Speed: 4, Health: 4, Defense: 5 7, Attack: 1 2, Abilities: Action: Whisper, Surge: Wither, Surge: Bleed",
      "Master: Speed: 4, Health: 6, Defense: 5 7, Attack: 1 2, Abilities: Hideous Laughter, Action: Whisper, Surge: Wither, Surge: Bleed"
    ],
    "expansion": "Shadow Of Nerekhall",
    "image": "monsters/changeling-sn-act1-front.png",
    "xws": "changeling"
  },
  {
    "name": "Changeling",
    "points": 169,
    "act": "I",
    "traits": [
      "Civilized",
      "Cursed"
    ],
    "ability rules": [
      "Hideous Laughter: Each hero within 3 spaces of this monster applies -1 to his Might, Willpower, Knowledge, and Awareness (to a minimum of 1).",
      "Whisper: Each hero adjacent to this monster tests Willpower. Each hero who fails moves 1 space in the direction of your choice.",
      "Wither: The target suffers 1 Fatigue.",
      "Bleed: If this attack deals at least 1 Heart (after the defense roll), the target is Bleeding."
    ],
    "group size": [
      "2 Heroes: 1,1",
      "3 Heroes: 2,1",
      "4 Heroes: 3,1"
    ],
    "expansion": "Shadow Of Nerekhall",
    "image": "monsters/changeling-sn-act1-back.png",
    "xws": "changeling"
  },
  {
    "name": "Changeling",
    "points": 170,
    "act": "II",
    "attack": "Melee",
    "characteristics": [
      "Minion: Speed: 4, Health: 6, Defense: 6 7, Attack: 1 2, Abilities: Action: Whisper, Surge: Wither, Surge: Bleed",
      "Master: Speed: 4, Health: 8, Defense: 6 7, Attack: 1 2 3, Abilities: Hideous Laughter, Action: Whisper, Surge: Wither, Surge: Bleed"
    ],
    "expansion": "Shadow Of Nerekhall",
    "image": "monsters/changeling-sn-act2-front.png",
    "xws": "changeling"
  },
  {
    "name": "Changeling",
    "points": 171,
    "act": "II",
    "traits": [
      "Civilized",
      "Cursed"
    ],
    "ability rules": [
      "Hideous Laughter: Each hero within 3 spaces of this monster applies -1 to his Might, Willpower, Knowledge, and Awareness (to a minimum of 1).",
      "Whisper: Each hero adjacent to this monster tests Willpower. Each hero who fails moves 1 space in the direction of your choice.",
      "Wither: The target suffers 1 Fatigue.",
      "Bleed: If this attack deals at least 1 Heart (after the defense roll), the target is Bleeding."
    ],
    "group size": [
      "2 Heroes: 1,1",
      "3 Heroes: 2,1",
      "4 Heroes: 3,1"
    ],
    "expansion": "Shadow Of Nerekhall",
    "image": "monsters/changeling-sn-act2-back.png",
    "xws": "changeling"
  },
  {
    "name": "Ironbound",
    "points": 172,
    "act": "I",
    "attack": "Melee",
    "characteristics": [
      "Minion: Speed: 2, Health: 8, Defense: 5 5, Attack: 1 2, Abilities: Reach, Ironskin, Protect",
      "Master: Speed: 2, Health: 10, Defense: 5 5, Attack: 1 2, Abilities: Reach, Ironskin, Protect, Surge: +2 Hearts"
    ],
    "expansion": "Shadow Of Nerekhall",
    "image": "monsters/ironbound-sn-act1-front.png",
    "xws": "ironbound"
  },
  {
    "name": "Ironbound",
    "points": 173,
    "act": "I",
    "traits": [
      "Civilized",
      "Building"
    ],
    "ability rules": [
      "Reach: This monster may attack targets up to 2 spaces away.",
      "Ironskin: This monster is immune to Pierce and to all Conditions.",
      "Protect: Each time a figure in this monster's line of sight performs an attack targeting a figure adjacent to this monster, before dice are rolled, this monster may suffer 1 Heart to become the target of the attack. Range and line of sight are still measu2 to the original target space."
    ],
    "group size": [
      "2 Heroes: 1,0",
      "3 Heroes: 0,1",
      "4 Heroes: 1,1"
    ],
    "expansion": "Shadow Of Nerekhall",
    "image": "monsters/ironbound-sn-act1-back.png",
    "xws": "ironbound"
  },
  {
    "name": "Ironbound",
    "points": 174,
    "act": "II",
    "attack": "Melee",
    "characteristics": [
      "Minion: Speed: 2, Health: 10, Defense: 5 6, Attack: 1 2 4, Abilities: Reach, Ironskin, Protect, Surge Surge: +2 Hearts",
      "Master: Speed: 2, Health: 12, Defense: 5 6, Attack: 1 2 4, Abilities: Reach, Ironskin, Protect, Surge: +3 Hearts"
    ],
    "expansion": "Shadow Of Nerekhall",
    "image": "monsters/ironbound-sn-act2-front.png",
    "xws": "ironbound"
  },
  {
    "name": "Ironbound",
    "points": 175,
    "act": "II",
    "traits": [
      "Civilized",
      "Building"
    ],
    "ability rules": [
      "Reach: This monster may attack targets up to 2 spaces away.",
      "Ironskin: This monster is immune to Pierce and to all Conditions.",
      "Protect: Each time a figure in this monster's line of sight performs an attack targeting a figure adjacent to this monster, before dice are rolled, this monster may suffer 1 Heart to become the target of the attack. Range and line of sight are still measu2 to the original target space."
    ],
    "group size": [
      "2 Heroes: 1,0",
      "3 Heroes: 0,1",
      "4 Heroes: 1,1"
    ],
    "expansion": "Shadow Of Nerekhall",
    "image": "monsters/ironbound-sn-act2-back.png",
    "xws": "ironbound"
  },
  {
    "name": "Rat Swarm",
    "points": 176,
    "act": "I",
    "attack": "Melee",
    "characteristics": [
      "Minion: Speed: 3, Health: 4, Defense: 7, Attack: 4, Abilities: Action: Merge, Action: Rend, Surge: Feast",
      "Master: Speed: 3, Health: 5, Defense: 7, Attack: 4, Abilities: Ravenous, Action: Merge, Action: Rend, Surge: Feast"
    ],
    "expansion": "Shadow Of Nerekhall",
    "image": "monsters/rat-swarm-sn-act1-front.png",
    "xws": "ratswarm"
  },
  {
    "name": "Rat Swarm",
    "points": 177,
    "act": "I",
    "traits": [
      "Building",
      "Dark"
    ],
    "ability rules": [
      "Ravenous: When attacking a hero that is Bleeding, this monster adds 1 Surge to its attack results.",
      "Merge: Choose 1 monster adjacent to this monster of the same group. This monster suffers Hearts equal to its remaining health, and the chosen monster recovers an equal amount of Hearts.",
      "Rend: Choose a hero adjacent to this monster to test Might. If he fails, he is Bleeding.",
      "Feast: This attack gains +X Hearts, where X is equal to this monster's remaining Health."
    ],
    "group size": [
      "2 Heroes: 2,0",
      "3 Heroes: 2,1",
      "4 Heroes: 3,1"
    ],
    "expansion": "Shadow Of Nerekhall",
    "image": "monsters/rat-swarm-sn-act1-back.png",
    "xws": "ratswarm"
  },
  {
    "name": "Rat Swarm",
    "points": 178,
    "act": "II",
    "attack": "Melee",
    "characteristics": [
      "Minion: Speed: 3, Health: 5, Defense: 5, Attack: 4, Abilities: Action: Merge, Action: Rend, Surge: Feast",
      "Master: Speed: 3, Health: 6, Defense: 5, Attack: 4, Abilities: Ravenous, Action: Merge, Action: Rend, Surge: Feast"
    ],
    "expansion": "Shadow Of Nerekhall",
    "image": "monsters/rat-swarm-sn-act2-front.png",
    "xws": "ratswarm"
  },
  {
    "name": "Rat Swarm",
    "points": 179,
    "act": "II",
    "traits": [
      "Building",
      "Dark"
    ],
    "ability rules": [
      "Ravenous: When attacking a hero that is Bleeding, this monster adds 1 Surge to its attack results.",
      "Merge: Choose 1 monster adjacent to this monster of the same group. This monster suffers Hearts equal to its remaining health, and the chosen monster recovers an equal amount of Hearts.",
      "Rend: Choose a hero adjacent to this monster to test Might. If he fails, he is Bleeding.",
      "Feast: This attack gains +X Hearts, where X is equal to this monster's remaining Health."
    ],
    "group size": [
      "2 Heroes: 2,0",
      "3 Heroes: 2,1",
      "4 Heroes: 3,1"
    ],
    "expansion": "Shadow Of Nerekhall",
    "image": "monsters/rat-swarm-sn-act2-back.png",
    "xws": "ratswarm"
  },
  {
    "name": "Ynfernael Hulk",
    "points": 180,
    "act": "I",
    "attack": "Melee",
    "characteristics": [
      "Minion: Speed: 3, Health: 8, Defense: 5, Attack: 1 2, Abilities: Action: Bloodrush, Surge: Knockback",
      "Master: Speed: 3, Health: 9, Defense: 5, Attack: 1 2, Abilities: Action: Bloodrush, Surge: Charge, Surge: Knockback"
    ],
    "expansion": "Shadow Of Nerekhall",
    "image": "monsters/ynfernael-hulk-sn-act1-front.png",
    "xws": "ynfernaelhulk"
  },
  {
    "name": "Ynfernael Hulk",
    "points": 181,
    "act": "I",
    "traits": [
      "Cursed",
      "Hot"
    ],
    "ability rules": [
      "Bloodrush: This monster suffers 1 Heart and gains 5 movement points. Limit once per turn.",
      "Charge: If this monster was not adjacent to the target at the start of this turn, this attack gains +3 Hearts.",
      "Knockback: Remove the target from the map, then place him on any empty space within 3 spaces of his original space. He counts as entering that space."
    ],
    "group size": [
      "2 Heroes: 1,0",
      "3 Heroes: 0,1",
      "4 Heroes: 1,1"
    ],
    "expansion": "Shadow Of Nerekhall",
    "image": "monsters/ynfernael-hulk-sn-act1-back.png",
    "xws": "ynfernaelhulk"
  },
  {
    "name": "Ynfernael Hulk",
    "points": 182,
    "act": "II",
    "attack": "Melee",
    "characteristics": [
      "Minion: Speed: 3, Health: 9, Defense: 6, Attack: 1 2 3, Abilities: Action: Bloodrush, Surge: Knockback",
      "Master: Speed: 3, Health: 10, Defense: 6, Attack: 1 2 3, Abilities: Action: Bloodrush, Surge: Charge, Surge: Knockback"
    ],
    "expansion": "Shadow Of Nerekhall",
    "image": "monsters/ynfernael-hulk-sn-act2-front.png",
    "xws": "ynfernaelhulk"
  },
  {
    "name": "Ynfernael Hulk",
    "points": 183,
    "act": "II",
    "traits": [
      "Cursed",
      "Hot"
    ],
    "ability rules": [
      "Bloodrush: This monster suffers 1 Heart and gains 5 movement points. Limit once per turn.",
      "Charge: If this monster was not adjacent to the target at the start of this turn, this attack gains +3 Hearts.",
      "Knockback: Remove the target from the map, then place him on any empty space within 3 spaces of his original space. He counts as entering that space."
    ],
    "group size": [
      "2 Heroes: 1,0",
      "3 Heroes: 0,1",
      "4 Heroes: 1,1"
    ],
    "expansion": "Shadow Of Nerekhall",
    "image": "monsters/ynfernael-hulk-sn-act2-back.png",
    "xws": "ynfernaelhulk"
  },
  {
    "name": "Bandit",
    "points": 184,
    "act": "I",
    "attack": "Range",
    "characteristics": [
      "Minion: Speed: 4, Health: 4, Defense: 7, Attack: 1 2, Abilities: Action: Pillage, Surge: Poison, Surge: +1 Heart",
      "Master: Speed: 4, Health: 5, Defense: 5, Attack: 1 3, Abilities: Action: Pillage, Surge: 6 Venom, Surge: +2 Hearts"
    ],
    "expansion": "Manor Of Ravens",
    "image": "monsters/bandit-mr-act1-front.png",
    "xws": "bandit"
  },
  {
    "name": "Bandit",
    "points": 185,
    "act": "I",
    "traits": [
      "Wilderness",
      "Building"
    ],
    "ability rules": [
      "Pillage: Perform an attack that targets an adjacent hero. If that hero is knocked out by this attack, choose 1 of his Search cards and shuffle it into the Search deck.",
      "6 Venom: If this attack deals at least 1 Heart (after the defense roll), the target is Doomed and Poisoned.",
      "Poison: If this attack deals at least 1 Heart (after the defense roll), the target is Poisoned."
    ],
    "group size": [
      "2 Heroes: 2,1",
      "3 Heroes: 3,1",
      "4 Heroes: 4,1"
    ],
    "expansion": "Manor Of Ravens",
    "image": "monsters/bandit-mr-act1-back.png",
    "xws": "bandit"
  },
  {
    "name": "Bandit",
    "points": 186,
    "act": "II",
    "attack": "Range",
    "characteristics": [
      "Minion: Speed: 4, Health: 6, Defense: 5, Attack: 1 2, Abilities: Action: Pillage, Surge: Poison, Surge: +2 Hearts",
      "Master: Speed: 4, Health: 7, Defense: 6, Attack: 1 3 3, Abilities: Action: Pillage, Surge: 6 Venom, Surge: +2 Hearts"
    ],
    "expansion": "Manor Of Ravens",
    "image": "monsters/bandit-mr-act2-front.png",
    "xws": "bandit"
  },
  {
    "name": "Bandit",
    "points": 187,
    "act": "II",
    "traits": [
      "Wilderness",
      "Building"
    ],
    "ability rules": [
      "Pillage: Perform an attack that targets an adjacent hero. If that hero is knocked out by this attack, choose 1 of his Search cards and shuffle it into the Search deck.",
      "6 Venom: If this attack deals at least 1 Heart (after the defense roll), the target is Doomed and Poisoned.",
      "Poison: If this attack deals at least 1 Heart (after the defense roll), the target is Poisoned."
    ],
    "group size": [
      "2 Heroes: 2,1",
      "3 Heroes: 3,1",
      "4 Heroes: 4,1"
    ],
    "expansion": "Manor Of Ravens",
    "image": "monsters/bandit-mr-act2-back.png",
    "xws": "bandit"
  },
  {
    "name": "Wraith",
    "points": 188,
    "act": "I",
    "attack": "Range",
    "characteristics": [
      "Minion: Speed: 4, Health: 5, Defense: 5, Attack: 1 3, Abilities: Action: Death Cry, Surge: Doom, Surge: +1 Heart",
      "Master: Speed: 4, Health: 7, Defense: 5, Attack: 1 3, Abilities: Reaper, Action: Death Cry, Surge: Doom, Surge: +2 Hearts"
    ],
    "expansion": "Manor Of Ravens",
    "image": "monsters/wraith-mr-act1-front.png",
    "xws": "wraith"
  },
  {
    "name": "Wraith",
    "points": 189,
    "act": "I",
    "traits": [
      "Civilized",
      "Cursed"
    ],
    "ability rules": [
      "Reaper: When a hero within 5 spaces of this monster is knocked out, this monster may immediately move up to its Speed and then perform an attack. Limit once per round.",
      "Death Cry: Choose 1 hero within 3 spaces of this monster. That hero tests Willpower. If he fails, he is Doomed or suffers 1 Heart, your choice. Limit once per round.",
      "Doom: If this attack deals at least 1 Heart (after the defense roll), the target is Doomed."
    ],
    "group size": [
      "2 Heroes: 0,1",
      "3 Heroes: 1,1",
      "4 Heroes: 2,1"
    ],
    "expansion": "Manor Of Ravens",
    "image": "monsters/wraith-mr-act1-back.png",
    "xws": "wraith"
  },
  {
    "name": "Wraith",
    "points": 190,
    "act": "II",
    "attack": "Range",
    "characteristics": [
      "Minion: Speed: 5, Health: 6, Defense: 6, Attack: 1 3, Abilities: Action: Death Cry, Surge: Doom, Surge: +2 Hearts",
      "Master: Speed: 5, Health: 8, Defense: 6, Attack: 1 2 3, Abilities: Reaper, Action: Death Cry, Surge: Doom, Surge: +3 Hearts"
    ],
    "expansion": "Manor Of Ravens",
    "image": "monsters/wraith-mr-act2-front.png",
    "xws": "wraith"
  },
  {
    "name": "Wraith",
    "points": 191,
    "act": "II",
    "traits": [
      "Civilized",
      "Cursed"
    ],
    "ability rules": [
      "Reaper: When a hero within 5 spaces of this monster is knocked out, this monster may immediately move up to its Speed and then perform an attack. Limit once per round.",
      "Death Cry: Choose 1 hero within 3 spaces of this monster. That hero tests Willpower. If he fails, he is Doomed or suffers 1 Heart, your choice. Limit once per round.",
      "Doom: If this attack deals at least 1 Heart (after the defense roll), the target is Doomed."
    ],
    "group size": [
      "2 Heroes: 0,1",
      "3 Heroes: 1,1",
      "4 Heroes: 2,1"
    ],
    "expansion": "Manor Of Ravens",
    "image": "monsters/wraith-mr-act2-back.png",
    "xws": "wraith"
  },
  {
    "name": "Bone Horror",
    "points": 192,
    "act": "I",
    "attack": "Melee",
    "characteristics": [
      "Minion: Speed: 5, Health: 5, Defense: 5, Attack: 1 3, Abilities: Extend, Lithesome, Surge: Pierce 1",
      "Master: Speed: 5, Health: 7, Defense: 5, Attack: 1 3, Abilities: Extend, Lithesome, Surge: Lash, Surge: Pierce 2"
    ],
    "expansion": "Mists Of Bilehall",
    "image": "monsters/bone-horror-mb-act1-front.png",
    "xws": "bonehorror"
  },
  {
    "name": "Bone Horror",
    "points": 193,
    "act": "I",
    "traits": [
      "Cave",
      "Cursed"
    ],
    "ability rules": [
      "Extend: Each time this monster performs an attack, it may target a figure up to 3 spaces away and in its line of sight.",
      "Lithesome: Friendly figures do not block this monster's line of sight.",
      "Lash: After this attack resolves, place the target in an empty space up to 2 spaces away from this monster.",
      "Pierce X: This attack ignores X Shields rolled on the defense dice."
    ],
    "group size": [
      "2 Heroes: 0,1",
      "3 Heroes: 1,1",
      "4 Heroes: 2,1"
    ],
    "expansion": "Mists Of Bilehall",
    "image": "monsters/bone-horror-mb-act1-back.png",
    "xws": "bonehorror"
  },
  {
    "name": "Bone Horror",
    "points": 194,
    "act": "II",
    "attack": "Melee",
    "characteristics": [
      "Minion: Speed: 5, Health: 6, Defense: 6, Attack: 1 2, Abilities: Extend, Lithesome, Surge: Pierce 1",
      "Master: Speed: 5, Health: 9, Defense: 6, Attack: 1 2 3, Abilities: Extend, Lithesome, Surge: Lash, Surge: Pierce 2"
    ],
    "expansion": "Mists Of Bilehall",
    "image": "monsters/bone-horror-mb-act2-front.png",
    "xws": "bonehorror"
  },
  {
    "name": "Bone Horror",
    "points": 195,
    "act": "II",
    "traits": [
      "Cave",
      "Cursed"
    ],
    "ability rules": [
      "Extend: Each time this monster performs an attack, it may target a figure up to 3 spaces away and in its line of sight.",
      "Lithesome: Friendly figures do not block this monster's line of sight.",
      "Lash: After this attack resolves, place the target in an empty space up to 2 spaces away from this monster.",
      "Pierce X: This attack ignores X Shields rolled on the defense dice."
    ],
    "group size": [
      "2 Heroes: 0,1",
      "3 Heroes: 1,1",
      "4 Heroes: 2,1"
    ],
    "expansion": "Mists Of Bilehall",
    "image": "monsters/bone-horror-mb-act2-back.png",
    "xws": "bonehorror"
  },
  {
    "name": "Broodwalker",
    "points": 196,
    "act": "I",
    "attack": "Melee",
    "characteristics": [
      "Minion: Speed: 2, Health: 7, Defense: 7, Attack: 1 3, Abilities: Overflowing, Surge: Terrify, Surge: +1 Heart",
      "Master: Speed: 2, Health: 10, Defense: 7, Attack: 1 3, Abilities: Hive Defense, Overflowing, Surge: +1 Heart, Surge: Colonize"
    ],
    "expansion": "Mists Of Bilehall",
    "image": "monsters/broodwalker-mb-act1-front.png",
    "xws": "broodwalker"
  },
  {
    "name": "Broodwalker",
    "points": 197,
    "act": "I",
    "traits": [
      "Dark",
      "Building"
    ],
    "ability rules": [
      "Hive Defense: Heroes treat each space adjacent to this monster as a Hazard space.",
      "Overflowing: Heroes treat each space adjacent to this monster as a Sludge space.",
      "Colonize: If this attack deals at least 1 Heart (after the defense roll), the target is Immobilized and Terrified.",
      "Terrify: If this attack deals at least 1 Heart (after the defense roll), the target is Terrified."
    ],
    "group size": [
      "2 Heroes: 0,1",
      "3 Heroes: 1,1",
      "4 Heroes: 2,1"
    ],
    "expansion": "Mists Of Bilehall",
    "image": "monsters/broodwalker-mb-act1-back.png",
    "xws": "broodwalker"
  },
  {
    "name": "Broodwalker",
    "points": 198,
    "act": "II",
    "attack": "Melee",
    "characteristics": [
      "Minion: Speed: 2, Health: 8, Defense: 5, Attack: 1 3 3, Abilities: Overflowing, Surge: Terrify, Surge: +1 Heart",
      "Master: Speed: 2, Health: 12, Defense: 5, Attack: 1 2 3, Abilities: Hive Defense, Overflowing, Surge: +1 Heart, Surge: Colonize"
    ],
    "expansion": "Mists Of Bilehall",
    "image": "monsters/broodwalker-mb-act2-front.png",
    "xws": "broodwalker"
  },
  {
    "name": "Broodwalker",
    "points": 199,
    "act": "II",
    "traits": [
      "Dark",
      "Building"
    ],
    "ability rules": [
      "Hive Defense: Heroes treat each space adjacent to this monster as a Hazard space.",
      "Overflowing: Heroes treat each space adjacent to this monster as a Sludge space.",
      "Colonize: If this attack deals at least 1 Heart (after the defense roll), the target is Immobilized and Terrified.",
      "Terrify: If this attack deals at least 1 Heart (after the defense roll), the target is Terrified."
    ],
    "group size": [
      "2 Heroes: 0,1",
      "3 Heroes: 1,1",
      "4 Heroes: 2,1"
    ],
    "expansion": "Mists Of Bilehall",
    "image": "monsters/broodwalker-mb-act2-back.png",
    "xws": "broodwalker"
  },
  {
    "name": "Reanimate",
    "points": 200,
    "act": "I",
    "attack": "Melee",
    "characteristics": [
      "Minion: Speed: 3, Health: 3, Defense: 7, Attack: 1, Abilities: Phalanx, Reanimation, Surge: Swarm",
      "Master: Speed: 3, Health: 5, Defense: 7, Attack: 1 2, Abilities: Phalanx, Reanimation, Action: Maneuver, Surge: Swarm"
    ],
    "expansion": "Mists Of Bilehall",
    "image": "monsters/reanimate-mb-act1-front.png",
    "xws": "reanimate"
  },
  {
    "name": "Reanimate",
    "points": 201,
    "act": "I",
    "traits": [
      "Civilized",
      "Cursed"
    ],
    "ability rules": [
      "Phalanx: If this monster is adjacent to a figure from its monster group, replace its 7 defense die with 1 5 defense die.",
      "Reanimation: Each time this monster suffers Hearts and is not defeated, it recovers Hearts equal to either the amount suffe2 or the number of monsters from this monster group within 3 spaces of it, whichever is less.",
      "Maneuver: Choose 1 minion monster adjacent to this monster. That monster gains 2 movement points.",
      "Swarm: This monster deals +1 Heart for each other monster adjacent to the target."
    ],
    "group size": [
      "2 Heroes: 3,1",
      "3 Heroes: 3,2",
      "4 Heroes: 4,2"
    ],
    "expansion": "Mists Of Bilehall",
    "image": "monsters/reanimate-mb-act1-back.png",
    "xws": "reanimate"
  },
  {
    "name": "Reanimate",
    "points": 202,
    "act": "II",
    "attack": "Melee",
    "characteristics": [
      "Minion: Speed: 3, Health: 5, Defense: 7, Attack: 3, Abilities: Phalanx, Reanimation, Surge: Swarm",
      "Master: Speed: 3, Health: 8, Defense: 7, Attack: 1 3 3, Abilities: Phalanx, Reanimation, Action: Maneuver, Surge: Swarm"
    ],
    "expansion": "Mists Of Bilehall",
    "image": "monsters/reanimate-mb-act2-front.png",
    "xws": "reanimate"
  },
  {
    "name": "Reanimate",
    "points": 203,
    "act": "II",
    "traits": [
      "Civilized",
      "Cursed"
    ],
    "ability rules": [
      "Phalanx: If this monster is adjacent to a figure from its monster group, replace its 7 defense die with 1 5 defense die.",
      "Reanimation: Each time this monster suffers Hearts and is not defeated, it recovers Hearts equal to either the amount suffe2 or the number of monsters from this monster group within 3 spaces of it, whichever is less.",
      "Maneuver: Choose 1 minion monster adjacent to this monster. That monster gains 2 movement points.",
      "Swarm: This monster deals +1 Heart for each other monster adjacent to the target."
    ],
    "group size": [
      "2 Heroes: 3,1",
      "3 Heroes: 3,2",
      "4 Heroes: 4,2"
    ],
    "expansion": "Mists Of Bilehall",
    "image": "monsters/reanimate-mb-act2-back.png",
    "xws": "reanimate"
  },
  {
    "name": "Marrow Priest",
    "points": 204,
    "act": "I",
    "attack": "Melee",
    "characteristics": [
      "Minion: Speed: 5, Health: 7, Defense: 5 7, Attack: 1 3, Abilities: Shadow Step, Surge: Mortal Binding, Surge: +1 Heart",
      "Master: Speed: 5, Health: 9, Defense: 5 7, Attack: 1 3, Abilities: Shadow Step, Surge: Mortal Binding, Surge: +1 Heart"
    ],
    "expansion": "The Chains That Rust",
    "image": "monsters/marrow-priest-cr-act1-front.png",
    "xws": "marrowpriest"
  },
  {
    "name": "Marrow Priest",
    "points": 205,
    "act": "I",
    "traits": [
      "Dark",
      "Building"
    ],
    "ability rules": [
      "Shadow Step: Each time a hero performs an attack that targets this monster, he may spend 1 Surge. If he does not, this monster gains 5 movement points after the attack resolves.",
      "Mortal Binding: The target tests Knowledge. If he fails, place his hero token on this card. A hero whose token is on this card cannot recover Hearts by any means. When a monster from this group is defeated or a hero is knocked out, discard all hero tokens from this card."
    ],
    "group size": [
      "2 Heroes: 1,0",
      "3 Heroes: 0,1",
      "4 Heroes: 1,1"
    ],
    "expansion": "The Chains That Rust",
    "image": "monsters/marrow-priest-cr-act1-back.png",
    "xws": "marrowpriest"
  },
  {
    "name": "Marrow Priest",
    "points": 206,
    "act": "II",
    "attack": "Melee",
    "characteristics": [
      "Minion: Speed: 5, Health: 8, Defense: 5 5, Attack: 1 3 3, Abilities: Shadow Step, Surge: Mortal Binding, Surge: +1 Heart",
      "Master: Speed: 5, Health: 10, Defense: 5 5, Attack: 1 3 3, Abilities: Shadow Step, Surge: Mortal Binding, Surge: +1 Heart"
    ],
    "expansion": "The Chains That Rust",
    "image": "monsters/marrow-priest-cr-act2-front.png",
    "xws": "marrowpriest"
  },
  {
    "name": "Marrow Priest",
    "points": 207,
    "act": "II",
    "traits": [
      "Dark",
      "Building"
    ],
    "ability rules": [
      "Shadow Step: Each time a hero performs an attack that targets this monster, he may spend 1 Surge. If he does not, this monster gains 5 movement points after the attack resolves.",
      "Mortal Binding: The target tests Knowledge. If he fails, place his hero token on this card. A hero whose token is on this card cannot recover Hearts by any means. When a monster from this group is defeated or a hero is knocked out, discard all hero tokens from this card."
    ],
    "group size": [
      "2 Heroes: 1,0",
      "3 Heroes: 0,1",
      "4 Heroes: 1,1"
    ],
    "expansion": "The Chains That Rust",
    "image": "monsters/marrow-priest-cr-act2-back.png",
    "xws": "marrowpriest"
  },
  {
    "name": "Shambling Colossus",
    "points": 208,
    "act": "I",
    "attack": "Melee",
    "characteristics": [
      "Minion: Speed: 3, Health: 5, Defense: 6, Attack: 1 2, Abilities: Puncture, Surge: +1 Heart",
      "Master: Speed: 3, Health: 8, Defense: 6, Attack: 1 2, Abilities: Harrowing, Puncture, Surge: +1 Heart"
    ],
    "expansion": "The Chains That Rust",
    "image": "monsters/shambling-colossus-cr-act1-front.png",
    "xws": "shamblingcolossus"
  },
  {
    "name": "Shambling Colossus",
    "points": 209,
    "act": "I",
    "traits": [
      "Wilderness",
      "Cursed"
    ],
    "ability rules": [
      "Harrowing: Each time a hero within 3 spaces of this monster performs an attack that targets this monster, before rolling dice, that hero tests Willpower. If he fails, he is Terrified.",
      "Puncture: Each of this monster's attacks igno2 1 Shield for each defense die rolled by the target of that attack."
    ],
    "group size": [
      "2 Heroes: 0,1",
      "3 Heroes: 1,1",
      "4 Heroes: 2,1"
    ],
    "expansion": "The Chains That Rust",
    "image": "monsters/shambling-colossus-cr-act1-back.png",
    "xws": "shamblingcolossus"
  },
  {
    "name": "Shambling Colossus",
    "points": 210,
    "act": "II",
    "attack": "Melee",
    "characteristics": [
      "Minion: Speed: 3, Health: 7, Defense: 6, Attack: 1 2, Abilities: Puncture, Surge: +2 Hearts",
      "Master: Speed: 3, Health: 9, Defense: 6, Attack: 1 2 3, Abilities: Harrowing, Puncture, Surge: +2 Hearts"
    ],
    "expansion": "The Chains That Rust",
    "image": "monsters/shambling-colossus-cr-act2-front.png",
    "xws": "shamblingcolossus"
  },
  {
    "name": "Shambling Colossus",
    "points": 211,
    "act": "II",
    "traits": [
      "Wilderness",
      "Cursed"
    ],
    "ability rules": [
      "Harrowing: Each time a hero within 3 spaces of this monster performs an attack that targets this monster, before rolling dice, that hero tests Willpower. If he fails, he is Terrified.",
      "Puncture: Each of this monster's attacks igno2 1 Shield for each defense die rolled by the target of that attack."
    ],
    "group size": [
      "2 Heroes: 0,1",
      "3 Heroes: 1,1",
      "4 Heroes: 2,1"
    ],
    "expansion": "The Chains That Rust",
    "image": "monsters/shambling-colossus-cr-act2-back.png",
    "xws": "shamblingcolossus"
  },
  {
    "name": "The Dispossessed",
    "points": 212,
    "act": "I",
    "attack": "Melee",
    "characteristics": [
      "Minion: Speed: 2, Health: 6, Defense: 5, Attack: 1 3, Abilities: Ethereal Hold, Surge: Fearbrand, Surge: +1 Heart",
      "Master: Speed: 2, Health: 8, Defense: 5, Attack: 1 3, Abilities: Ethereal Hold, Surge: Fearbrand, Surge: Terrify, Surge: +1 Heart"
    ],
    "expansion": "The Chains That Rust",
    "image": "monsters/the-dispossessed-cr-act1-front.png",
    "xws": "thedispossessed"
  },
  {
    "name": "The Dispossessed",
    "points": 213,
    "act": "I",
    "traits": [
      "Civilized",
      "Cursed"
    ],
    "ability rules": [
      "Ethereal Hold: At the start of the overlord's turn, this monster may discard 1 hero token from its base. If it does, remove it from the map and place it within 3 spaces of the corresponding hero.",
      "Fearbrand: Choose a hero in this monster's line of sight and place that hero's hero token on this monster's base.",
      "Terrify: If this attack deals at least 1 Heart (after the defense roll), the target is Terrified."
    ],
    "group size": [
      "2 Heroes: 2,0",
      "3 Heroes: 1,1",
      "4 Heroes: 2,1"
    ],
    "expansion": "The Chains That Rust",
    "image": "monsters/the-dispossessed-cr-act1-back.png",
    "xws": "thedispossessed"
  },
  {
    "name": "The Dispossessed",
    "points": 214,
    "act": "II",
    "attack": "Melee",
    "characteristics": [
      "Minion: Speed: 2, Health: 8, Defense: 5 7, Attack: 1 3, Abilities: Ethereal Hold, Surge: Fearbrand, Surge: +2 Hearts",
      "Master: Speed: 2, Health: 10, Defense: 5 7, Attack: 1 3 3, Abilities: Ethereal Hold, Surge: Fearbrand, Surge: Terrify, Surge: +2 Hearts"
    ],
    "expansion": "The Chains That Rust",
    "image": "monsters/the-dispossessed-cr-act2-front.png",
    "xws": "thedispossessed"
  },
  {
    "name": "The Dispossessed",
    "points": 215,
    "act": "II",
    "traits": [
      "Civilized",
      "Cursed"
    ],
    "ability rules": [
      "Ethereal Hold: At the start of the overlord's turn, this monster may discard 1 hero token from its base. If it does, remove it from the map and place it within 3 spaces of the corresponding hero.",
      "Fearbrand: Choose a hero in this monster's line of sight and place that hero's hero token on this monster's base.",
      "Terrify: If this attack deals at least 1 Heart (after the defense roll), the target is Terrified."
    ],
    "group size": [
      "2 Heroes: 2,0",
      "3 Heroes: 1,1",
      "4 Heroes: 2,1"
    ],
    "expansion": "The Chains That Rust",
    "image": "monsters/the-dispossessed-cr-act2-back.png",
    "xws": "thedispossessed"
  },
  {
    "name": "Bane Spider",
    "points": 216,
    "act": "I",
    "attack": "Range",
    "characteristics": [
      "Minion: Speed: 4, Health: 4, Defense: 5, Attack: 1 2, Abilities: Surge: Poison, Surge: Pierce 1",
      "Master: Speed: 4, Health: 7, Defense: 5, Attack: 1 2, Abilities: Action: Cocoon, Surge: Poison, Surge: Pierce 2"
    ],
    "expansion": "Oath Of The Outcast",
    "image": "monsters/bane-spider-oo-act1-front.png",
    "xws": "banespider"
  },
  {
    "name": "Bane Spider",
    "points": 217,
    "act": "I",
    "traits": [
      "Dark",
      "Cave"
    ],
    "ability rules": [
      "Cocoon: Each hero adjacent to this monster must test Awareness. Each hero who fails is Immobilized.",
      "Poison: If this attack deals at least 1 Heart (after the defense roll), the target is Poisoned.",
      "Pierce X: This attack ignores X Shields rolled on the defense dice."
    ],
    "group size": [
      "2 Heroes: 0,1",
      "3 Heroes: 1,1",
      "4 Heroes: 2,1"
    ],
    "expansion": "Oath Of The Outcast",
    "image": "monsters/bane-spider-oo-act1-back.png",
    "xws": "banespider"
  },
  {
    "name": "Bane Spider",
    "points": 218,
    "act": "II",
    "attack": "Range",
    "characteristics": [
      "Minion: Speed: 4, Health: 6, Defense: 5 7, Attack: 1 2, Abilities: Surge: Poison, Surge: Pierce 2",
      "Master: Speed: 4, Health: 9, Defense: 5 7, Attack: 1 2 3, Abilities: Action: Cocoon, Surge: Poison, Surge: Pierce 3"
    ],
    "expansion": "Oath Of The Outcast",
    "image": "monsters/bane-spider-oo-act2-front.png",
    "xws": "banespider"
  },
  {
    "name": "Bane Spider",
    "points": 219,
    "act": "II",
    "traits": [
      "Dark",
      "Cave"
    ],
    "ability rules": [
      "Cocoon: Each hero adjacent to this monster must test Awareness. Each hero that fails is Immobilized.",
      "Poison: If this attack deals at least 1 Heart (after the defense roll), the target is Poisoned.",
      "Pierce X: This attack ignores X Shields rolled on the defense dice."
    ],
    "group size": [
      "2 Heroes: 0,1",
      "3 Heroes: 1,1",
      "4 Heroes: 2,1"
    ],
    "expansion": "Oath Of The Outcast",
    "image": "monsters/bane-spider-oo-act2-back.png",
    "xws": "banespider"
  },
  {
    "name": "Beastman",
    "points": 220,
    "act": "I",
    "attack": "Melee",
    "characteristics": [
      "Minion: Speed: 4, Health: 4, Defense: 5, Attack: 1 3, Abilities: Ravage, Surge: +1 Heart",
      "Master: Speed: 4, Health: 5, Defense: 5, Attack: 1 2, Abilities: Command, Ravage, Surge: +2 Hearts"
    ],
    "expansion": "Oath Of The Outcast",
    "image": "monsters/beastman-oo-act1-front.png",
    "xws": "beastman"
  },
  {
    "name": "Beastman",
    "points": 221,
    "act": "I",
    "traits": [
      "Mountain",
      "Wilderness"
    ],
    "ability rules": [
      "Command: Each minion within 3 spaces of this monster may reroll 1 die on each of its attacks. Each minion can only benefit from 1 monster with Command during each of its attacks.",
      "Ravage: Both of this monster's actions on a turn may be attack actions."
    ],
    "group size": [
      "2 Heroes: 1,1",
      "3 Heroes: 2,1",
      "4 Heroes: 3,1"
    ],
    "expansion": "Oath Of The Outcast",
    "image": "monsters/beastman-oo-act1-back.png",
    "xws": "beastman"
  },
  {
    "name": "Beastman",
    "points": 222,
    "act": "II",
    "attack": "Melee",
    "characteristics": [
      "Minion: Speed: 5, Health: 5, Defense: 5, Attack: 1 2, Abilities: Ravage, Surge: +2 Hearts",
      "Master: Speed: 5, Health: 6, Defense: 5, Attack: 1 2 3, Abilities: Command, Ravage, Surge: +2 Hearts"
    ],
    "expansion": "Oath Of The Outcast",
    "image": "monsters/beastman-oo-act2-front.png",
    "xws": "beastman"
  },
  {
    "name": "Beastman",
    "points": 223,
    "act": "II",
    "traits": [
      "Mountain",
      "Wilderness"
    ],
    "ability rules": [
      "Command: Each minion within 3 spaces of this monster may reroll 1 die on each of its attacks. Each minion can only benefit from 1 monster with Command during each of its attacks.",
      "Ravage: Both of this monster's actions on a turn may be attack actions."
    ],
    "group size": [
      "2 Heroes: 1,1",
      "3 Heroes: 2,1",
      "4 Heroes: 3,1"
    ],
    "expansion": "Oath Of The Outcast",
    "image": "monsters/beastman-oo-act2-back.png",
    "xws": "beastman"
  },
  {
    "name": "Razorwing",
    "points": 224,
    "act": "I",
    "attack": "Melee",
    "characteristics": [
      "Minion: Speed: 5, Health: 4, Defense: 7, Attack: 1 3, Abilities: Fly, Surge: +1 Heart",
      "Master: Speed: 6, Health: 6, Defense: 7, Attack: 1 3, Abilities: Fly, Surge: Stun, Surge: +1 Heart"
    ],
    "expansion": "Oath Of The Outcast",
    "image": "monsters/razorwing-oo-act1-front.png",
    "xws": "razorwing"
  },
  {
    "name": "Razorwing",
    "points": 225,
    "act": "I",
    "traits": [
      "Wilderness",
      "Cave"
    ],
    "ability rules": [
      "Fly: This monster may ignore enemy figures and the effects of terrain while moving. It must end its movement in an empty space following normal movement rules.",
      "Stun: If this attack deals at least 1 Heart (after the defense roll), the target is Stunned."
    ],
    "group size": [
      "2 Heroes: 1,1",
      "3 Heroes: 2,1",
      "4 Heroes: 3,1"
    ],
    "expansion": "Oath Of The Outcast",
    "image": "monsters/razorwing-oo-act1-back.png",
    "xws": "razorwing"
  },
  {
    "name": "Razorwing",
    "points": 226,
    "act": "II",
    "attack": "Melee",
    "characteristics": [
      "Minion: Speed: 5, Health: 7, Defense: 7, Attack: 1 3, Abilities: Fly, Surge: +2 Hearts",
      "Master: Speed: 6, Health: 9, Defense: 7, Attack: 1 3 3, Abilities: Fly, Surge: Stun, Surge: +2 Hearts"
    ],
    "expansion": "Oath Of The Outcast",
    "image": "monsters/razorwing-oo-act2-front.png",
    "xws": "razorwing"
  },
  {
    "name": "Razorwing",
    "points": 227,
    "act": "II",
    "traits": [
      "Wilderness",
      "Cave"
    ],
    "ability rules": [
      "Fly: This monster may ignore enemy figures and the effects of terrain while moving. It must end its movement in an empty space following normal movement rules.",
      "Stun: If this attack deals at least 1 Heart (after the defense roll), the target is Stunned."
    ],
    "group size": [
      "2 Heroes: 1,1",
      "3 Heroes: 2,1",
      "4 Heroes: 3,1"
    ],
    "expansion": "Oath Of The Outcast",
    "image": "monsters/razorwing-oo-act2-back.png",
    "xws": "razorwing"
  },
  {
    "name": "Chaos Beast",
    "points": 228,
    "act": "I",
    "attack": "Range",
    "characteristics": [
      "Minion: Speed: 3, Health: 5, Defense: 5, Attack: Blank, Abilities: Morph, Surge: +1 Heart",
      "Master: Speed: 3, Health: 6, Defense: 5, Attack: Blank, Abilities: Morph, Sorcery 2, Surge: +1 Heart"
    ],
    "expansion": "Crown Of Destiny",
    "image": "monsters/chaos-beast-cd-act1-front.png",
    "xws": "chaosbeast"
  },
  {
    "name": "Chaos Beast",
    "points": 229,
    "act": "I",
    "traits": [
      "Dark",
      "Cursed"
    ],
    "ability rules": [
      "Morph: When this monster attacks, it uses the dice of a figure (overlord's choice) in its lind of sight. If a hero is chosen, the overlord may choose which of the hero's equipped weapons to use. The monster cannot use any of the figure's other abilities.",
      "Sorcery X: After making an attack roll, this monster may convert up to X range to Hearts, or up to X Hearts to range."
    ],
    "group size": [
      "2 Heroes: 0,1",
      "3 Heroes: 1,1",
      "4 Heroes: 2,1"
    ],
    "expansion": "Crown Of Destiny",
    "image": "monsters/chaos-beast-cd-act1-back.png",
    "xws": "chaosbeast"
  },
  {
    "name": "Chaos Beast",
    "points": 230,
    "act": "II",
    "attack": "Range",
    "characteristics": [
      "Minion: Speed: 3, Health: 7, Defense: 5, Attack: Blank, Abilities: Morph, Surge: +1 Heart",
      "Master: Speed: 3, Health: 10, Defense: 5, Attack: Blank, Abilities: Morph, Sorcery 3, Surge: +1 Heart"
    ],
    "expansion": "Crown Of Destiny",
    "image": "monsters/chaos-beast-cd-act2-front.png",
    "xws": "chaosbeast"
  },
  {
    "name": "Chaos Beast",
    "points": 231,
    "act": "II",
    "traits": [
      "Dark",
      "Cursed"
    ],
    "ability rules": [
      "Morph: When this monster attacks, it uses the dice of a figure (overlord's choice) in its lind of sight. If a hero is chosen, the overlord may choose which of the hero's equipped weapons to use. The monster cannot use any of the figure's other abilities.",
      "Sorcery X: After making an attack roll, this monster may convert up to X range to Hearts, or up to X Hearts to range."
    ],
    "group size": [
      "2 Heroes: 0,1",
      "3 Heroes: 1,1",
      "4 Heroes: 2,1"
    ],
    "expansion": "Crown Of Destiny",
    "image": "monsters/chaos-beast-cd-act2-back.png",
    "xws": "chaosbeast"
  },
  {
    "name": "Giant",
    "points": 232,
    "act": "I",
    "attack": "Melee",
    "characteristics": [
      "Minion: Speed: 3, Health: 10, Defense: 6, Attack: 1 2, Abilities: Reach, Surge: Stun",
      "Master: Speed: 3, Health: 12, Defense: 6, Attack: 1 2, Abilities: Reach, Action: Sweep, Surge: Stun"
    ],
    "expansion": "Crown Of Destiny",
    "image": "monsters/giant-cd-act1-front.png",
    "xws": "giant"
  },
  {
    "name": "Giant",
    "points": 233,
    "act": "I",
    "traits": [
      "Mountain",
      "Wilderness"
    ],
    "ability rules": [
      "Reach: This monster may attack targets up to 2 spaces away.",
      "Sweep: Perform an attack. This attack affects each figure within two spaces and line of sight of this monster. Each figure rolls defense dice separately.",
      "Stun: If this attack deals at least 1 Heart (after the defense roll), the target is Stunned."
    ],
    "group size": [
      "2 Heroes: 1,0",
      "3 Heroes: 0,1",
      "4 Heroes: 1,1"
    ],
    "expansion": "Crown Of Destiny",
    "image": "monsters/giant-cd-act1-back.png",
    "xws": "giant"
  },
  {
    "name": "Giant",
    "points": 234,
    "act": "II",
    "attack": "Melee",
    "characteristics": [
      "Minion: Speed: 3, Health: 12, Defense: 6, Attack: 1 2 3, Abilities: Reach, Surge: Stun",
      "Master: Speed: 3, Health: 15, Defense: 6, Attack: 1 2 2, Abilities: Reach, Action: Sweep, Surge: Stun"
    ],
    "expansion": "Crown Of Destiny",
    "image": "monsters/giant-cd-act2-front.png",
    "xws": "giant"
  },
  {
    "name": "Giant",
    "points": 235,
    "act": "II",
    "traits": [
      "Mountain",
      "Wilderness"
    ],
    "ability rules": [
      "Reach: This monster may attack targets up to 2 spaces away.",
      "Sweep: Perform an attack. This attack affects each figure within two spaces and line of sight of this monster. Each figure rolls defense dice separately.",
      "Stun: If this attack deals at least 1 Heart (after the defense roll), the target is Stunned."
    ],
    "group size": [
      "2 Heroes: 1,0",
      "3 Heroes: 0,1",
      "4 Heroes: 1,1"
    ],
    "expansion": "Crown Of Destiny",
    "image": "monsters/giant-cd-act2-back.png",
    "xws": "giant"
  },
  {
    "name": "Lava Beetle",
    "points": 236,
    "act": "I",
    "attack": "Range",
    "characteristics": [
      "Minion: Speed: 3, Health: 3, Defense: 5, Attack: 1 2, Abilities: Surge: Blast, Surge: +1 Heart",
      "Master: Speed: 3, Health: 5, Defense: 5, Attack: 1 2, Abilities: Blast, Surge: +1 Heart"
    ],
    "expansion": "Crown Of Destiny",
    "image": "monsters/lava-beetle-cd-act1-front.png",
    "xws": "lavabeetle"
  },
  {
    "name": "Lava Beetle",
    "points": 237,
    "act": "I",
    "traits": [
      "Hot",
      "Cave"
    ],
    "ability rules": [
      "Blast: This attack affects all figures adjacent to the target space."
    ],
    "group size": [
      "2 Heroes: 1,1",
      "3 Heroes: 2,1",
      "4 Heroes: 3,1"
    ],
    "expansion": "Crown Of Destiny",
    "image": "monsters/lava-beetle-cd-act1-back.png",
    "xws": "lavabeetle"
  },
  {
    "name": "Lava Beetle",
    "points": 238,
    "act": "II",
    "attack": "Range",
    "characteristics": [
      "Minion: Speed: 3, Health: 5, Defense: 5, Attack: 1 2, Abilities: Surge: Blast, Surge: +2 Hearts",
      "Master: Speed: 3, Health: 7, Defense: 5, Attack: 1 2 3, Abilities: Blast, Surge: +2 Hearts, Surge: +1 Heart"
    ],
    "expansion": "Crown Of Destiny",
    "image": "monsters/lava-beetle-cd-act2-front.png",
    "xws": "lavabeetle"
  },
  {
    "name": "Lava Beetle",
    "points": 239,
    "act": "II",
    "traits": [
      "Hot",
      "Cave"
    ],
    "ability rules": [
      "Blast: This attack affects all figures adjacent to the target space."
    ],
    "group size": [
      "2 Heroes: 1,1",
      "3 Heroes: 2,1",
      "4 Heroes: 3,1"
    ],
    "expansion": "Crown Of Destiny",
    "image": "monsters/lava-beetle-cd-act2-back.png",
    "xws": "lavabeetle"
  },
  {
    "name": "Golem",
    "points": 240,
    "act": "I",
    "attack": "Melee",
    "characteristics": [
      "Minion: Speed: 3, Health: 8, Defense: 6, Attack: 1 2, Abilities: Ironskin, Surge: +1 Heart",
      "Master: Speed: 3, Health: 10, Defense: 6, Attack: 1 2, Abilities: Ironskin, Unmovable, Surge: +2 Hearts"
    ],
    "expansion": "Crusade Of The Forgotten",
    "image": "monsters/golem-cf-act1-front.png",
    "xws": "golem"
  },
  {
    "name": "Golem",
    "points": 241,
    "act": "I",
    "traits": [
      "Mountain",
      "Building"
    ],
    "ability rules": [
      "Ironskin: This monster is immune to Pierce and to all conditions.",
      "Unmovable: This monster may choose to ignore any game effect that would force it to move."
    ],
    "group size": [
      "2 Heroes: 1,0",
      "3 Heroes: 0,1",
      "4 Heroes: 1,1"
    ],
    "expansion": "Crusade Of The Forgotten",
    "image": "monsters/golem-cf-act1-back.png",
    "xws": "golem"
  },
  {
    "name": "Golem",
    "points": 242,
    "act": "II",
    "attack": "Melee",
    "characteristics": [
      "Minion: Speed: 3, Health: 10, Defense: 6 5, Attack: 1 2 2, Abilities: Ironskin, Surge: +1 Heart",
      "Master: Speed: 3, Health: 12, Defense: 6 5, Attack: 1 2 2, Abilities: Ironskin, Unmovable, Surge: +2 Hearts"
    ],
    "expansion": "Crusade Of The Forgotten",
    "image": "monsters/golem-cf-act2-front.png",
    "xws": "golem"
  },
  {
    "name": "Golem",
    "points": 243,
    "act": "II",
    "traits": [
      "Mountain",
      "Building"
    ],
    "ability rules": [
      "Ironskin: This monster is immune to Pierce and to all conditions.",
      "Unmovable: This monster may choose to ignore any game effect that would force it to move."
    ],
    "group size": [
      "2 Heroes: 1,0",
      "3 Heroes: 0,1",
      "4 Heroes: 1,1"
    ],
    "expansion": "Crusade Of The Forgotten",
    "image": "monsters/golem-cf-act2-back.png",
    "xws": "golem"
  },
  {
    "name": "Medusa",
    "points": 244,
    "act": "I",
    "attack": "Range",
    "characteristics": [
      "Minion: Speed: 4, Health: 4, Defense: 5, Attack: 1 3, Abilities: Surge: Immobilize, Surge: Poison",
      "Master: Speed: 4, Health: 6, Defense: 5, Attack: 1 3, Abilities: Surge: Immobilize, Surge: Poison, Surge: Stun"
    ],
    "expansion": "Crusade Of The Forgotten",
    "image": "monsters/medusa-cf-act1-front.png",
    "xws": "medusa"
  },
  {
    "name": "Medusa",
    "points": 245,
    "act": "I",
    "traits": [
      "Cursed",
      "Building"
    ],
    "ability rules": [
      "Immobilize: If this attack deals at least 1 Heart (after the defense roll), the target is Immobilized.",
      "Poison: If this attack deals at least 1 Heart (after the defense roll), the target is Poisoned.",
      "Stun: If this attack deals at least 1 Heart (after the defense roll), the target is Stunned."
    ],
    "group size": [
      "2 Heroes: 2,0",
      "3 Heroes: 1,1",
      "4 Heroes: 2,1"
    ],
    "expansion": "Crusade Of The Forgotten",
    "image": "monsters/medusa-cf-act1-back.png",
    "xws": "medusa"
  },
  {
    "name": "Medusa",
    "points": 246,
    "act": "II",
    "attack": "Range",
    "characteristics": [
      "Minion: Speed: 4, Health: 6, Defense: 5 7, Attack: 1 3 3, Abilities: Surge: Immobilize, Surge: Poison",
      "Master: Speed: 4, Health: 9, Defense: 5 7, Attack: 1 3 3, Abilities: Surge: Immobilize, Surge: Poison, Surge: Stun"
    ],
    "expansion": "Crusade Of The Forgotten",
    "image": "monsters/medusa-cf-act2-front.png",
    "xws": "medusa"
  },
  {
    "name": "Medusa",
    "points": 247,
    "act": "II",
    "traits": [
      "Cursed",
      "Building"
    ],
    "ability rules": [
      "Immobilize: If this attack deals at least 1 Heart (after the defense roll), the target is Immobilized.",
      "Poison: If this attack deals at least 1 Heart (after the defense roll), the target is Poisoned.",
      "Stun: If this attack deals at least 1 Heart (after the defense roll), the target is Stunned."
    ],
    "group size": [
      "2 Heroes: 2,0",
      "3 Heroes: 1,1",
      "4 Heroes: 2,1"
    ],
    "expansion": "Crusade Of The Forgotten",
    "image": "monsters/medusa-cf-act2-back.png",
    "xws": "medusa"
  },
  {
    "name": "Sorcerer",
    "points": 248,
    "act": "I",
    "attack": "Range",
    "characteristics": [
      "Minion: Speed: 4, Health: 3, Defense: 5, Attack: 1 3, Abilities: Sorcery 2, Action: Summon",
      "Master: Speed: 4, Health: 5, Defense: 5, Attack: 1 3, Abilities: Death Wish, Sorcery 3, Action: Summon"
    ],
    "expansion": "Crusade Of The Forgotten",
    "image": "monsters/sorcerer-cf-act1-front.png",
    "xws": "sorcerer"
  },
  {
    "name": "Sorcerer",
    "points": 249,
    "act": "I",
    "traits": [
      "Civilized",
      "Building"
    ],
    "ability rules": [
      "Death Wish: When this master monster would be defeated, the overlord may choose 1 minion monster of the same group to be defeated instead. If he does, this master monster recovers all Hearts.",
      "Sorcery X: After making an attack roll, this monster may convert up to X range to Hearts, or up to X Hearts to range.",
      "Summon: Choose a minion monster within 3 spaces of this monster. Place that minion monster in an empty space adjacent to this monster."
    ],
    "group size": [
      "2 Heroes: 1,1",
      "3 Heroes: 2,1",
      "4 Heroes: 3,1"
    ],
    "expansion": "Crusade Of The Forgotten",
    "image": "monsters/sorcerer-cf-act1-back.png",
    "xws": "sorcerer"
  },
  {
    "name": "Sorcerer",
    "points": 250,
    "act": "II",
    "attack": "Range",
    "characteristics": [
      "Minion: Speed: 4, Health: 5, Defense: 5, Attack: 1 2, Abilities: Sorcery 2, Action: Summon",
      "Master: Speed: 4, Health: 8, Defense: 5, Attack: 1 2, Abilities: Death Wish, Sorcery 3, Action: Summon"
    ],
    "expansion": "Crusade Of The Forgotten",
    "image": "monsters/sorcerer-cf-act2-front.png",
    "xws": "sorcerer"
  },
  {
    "name": "Sorcerer",
    "points": 251,
    "act": "II",
    "traits": [
      "Civilized",
      "Building"
    ],
    "ability rules": [
      "Death Wish: When this master monster would be defeated, the overlord may choose 1 minion monster of the same group to be defeated instead. If he does, this master monster recovers all Hearts.",
      "Sorcery X: After making an attack roll, this monster may convert up to X range to Hearts, or up to X Hearts to range.",
      "Summon: Choose a minion monster within 3 spaces of this monster. Place that minion monster in an empty space adjacent to this monster."
    ],
    "group size": [
      "2 Heroes: 1,1",
      "3 Heroes: 2,1",
      "4 Heroes: 3,1"
    ],
    "expansion": "Crusade Of The Forgotten",
    "image": "monsters/sorcerer-cf-act2-back.png",
    "xws": "sorcerer"
  },
  {
    "name": "Crypt Dragon",
    "points": 252,
    "act": "I",
    "attack": "Range",
    "characteristics": [
      "Minion: Speed: 3, Health: 5, Defense: 5 5, Attack: 1 3, Abilities: Surge: Blast, Surge: +2 Hearts",
      "Master: Speed: 3, Health: 7, Defense: 5 5, Attack: 1 3, Abilities: Action: Cause Fear, Surge: Blast, Surge: +2 Hearts"
    ],
    "expansion": "Guardians Of Deephall",
    "image": "monsters/crypt-dragon-gd-act1-front.png",
    "xws": "cryptdragon"
  },
  {
    "name": "Crypt Dragon",
    "points": 253,
    "act": "I",
    "traits": [
      "Dark",
      "Cursed"
    ],
    "ability rules": [
      "Cause Fear: Choose a hero adjacent to this monster. That hero must test Willpower. If he fails, he moves 2 spaces directly away from this monster and is Immobilized.",
      "Blast: This attack affects all figures adjacent to the target space."
    ],
    "group size": [
      "2 Heroes: 1,0",
      "3 Heroes: 0,1",
      "4 Heroes: 1,1"
    ],
    "expansion": "Guardians Of Deephall",
    "image": "monsters/crypt-dragon-gd-act1-back.png",
    "xws": "cryptdragon"
  },
  {
    "name": "Crypt Dragon",
    "points": 254,
    "act": "II",
    "attack": "Range",
    "characteristics": [
      "Minion: Speed: 3, Health: 7, Defense: 6 5, Attack: 1 2 3, Abilities: Horrifying, Surge: Blast, Surge: +2 Hearts",
      "Master: Speed: 3, Health: 10, Defense: 6 5, Attack: 1 2 3, Abilities: Horrifying, Action: Cause Fear, Surge: Blast, Surge: +2 Hearts"
    ],
    "expansion": "Guardians Of Deephall",
    "image": "monsters/crypt-dragon-gd-act2-front.png",
    "xws": "cryptdragon"
  },
  {
    "name": "Crypt Dragon",
    "points": 255,
    "act": "II",
    "traits": [
      "Dark",
      "Cursed"
    ],
    "ability rules": [
      "Horrifying: Each hero adjacent to 1 or more monsters with Horrifying applies -1 to his Willpower (to a minimum of 1).",
      "Cause Fear: Choose a hero adjacent to this monster. That hero must test Willpower. If he fails, he moves 2 spaces directly away from this monster and is Immobilized.",
      "Blast: This attack affects all figures adjacent to the target space."
    ],
    "group size": [
      "2 Heroes: 1,0",
      "3 Heroes: 0,1",
      "4 Heroes: 1,1"
    ],
    "expansion": "Guardians Of Deephall",
    "image": "monsters/crypt-dragon-gd-act2-back.png",
    "xws": "cryptdragon"
  },
  {
    "name": "Dark Priest",
    "points": 256,
    "act": "I",
    "attack": "Range",
    "characteristics": [
      "Minion: Speed: 4, Health: 4, Defense: 7, Attack: 1 3, Abilities: Action: Dark Prayer, Surge: +1 Heart",
      "Master: Speed: 4, Health: 6, Defense: 7, Attack: 1 3, Abilities: Action: Dark Prayer, Action: Heal, Surge: +1 Heart"
    ],
    "expansion": "Guardians Of Deephall",
    "image": "monsters/dark-priest-gd-act1-front.png",
    "xws": "darkpriest"
  },
  {
    "name": "Dark Priest",
    "points": 257,
    "act": "I",
    "traits": [
      "Civilized",
      "Cursed"
    ],
    "ability rules": [
      "Dark Prayer: Choose 1 hero within 5 spaces of this monster. That hero tests Willpower. If he fails, he suffers 1 Fatigue.",
      "Heal: Choose a monster within 3 spaces of this monster and roll 1 2 power die. That monster recovers Hearts equal to the Hearts rolled."
    ],
    "group size": [
      "2 Heroes: 1,1",
      "3 Heroes: 2,1",
      "4 Heroes: 3,1"
    ],
    "expansion": "Guardians Of Deephall",
    "image": "monsters/dark-priest-gd-act1-back.png",
    "xws": "darkpriest"
  },
  {
    "name": "Dark Priest",
    "points": 258,
    "act": "II",
    "attack": "Range",
    "characteristics": [
      "Minion: Speed: 4, Health: 7, Defense: 7, Attack: 1 3, Abilities: Action: Dark Prayer, Surge: +2 Hearts",
      "Master: Speed: 4, Health: 9, Defense: 7, Attack: 1 3 3, Abilities: Horrifying, Action: Dark Prayer, Action: Heal, Surge: +2 Hearts"
    ],
    "expansion": "Guardians Of Deephall",
    "image": "monsters/dark-priest-gd-act2-front.png",
    "xws": "darkpriest"
  },
  {
    "name": "Dark Priest",
    "points": 259,
    "act": "II",
    "traits": [
      "Civilized",
      "Cursed"
    ],
    "ability rules": [
      "Horrifying: Each hero adjacent to 1 or more monsters with Horrifying applies -1 to his Willpower (to a minimum of 1).",
      "Dark Prayer: Choose 1 hero within 5 spaces of this monster. That hero tests Willpower. If he fails, he suffers 1 Fatigue.",
      "Heal: Choose a monster within 3 spaces of this monster and roll 1 2 power die. That monster recovers Hearts equal to the Hearts rolled."
    ],
    "group size": [
      "2 Heroes: 1,1",
      "3 Heroes: 2,1",
      "4 Heroes: 3,1"
    ],
    "expansion": "Guardians Of Deephall",
    "image": "monsters/dark-priest-gd-act2-back.png",
    "xws": "darkpriest"
  },
  {
    "name": "Wendigo",
    "points": 260,
    "act": "I",
    "attack": "Melee",
    "characteristics": [
      "Minion: Speed: 4, Health: 5, Defense: 5, Attack: 1 2, Abilities: Ravage, Stealthy, Surge: +1 Heart",
      "Master: Speed: 4, Health: 7, Defense: 5, Attack: 1 2, Abilities: Freezing, Ravage, Stealthy, Surge: +1 Heart"
    ],
    "expansion": "Guardians Of Deephall",
    "image": "monsters/wendigo-gd-act1-front.png",
    "xws": "wendigo"
  },
  {
    "name": "Wendigo",
    "points": 261,
    "act": "I",
    "traits": [
      "Cold",
      "Cave"
    ],
    "ability rules": [
      "Freezing: Each time a hero enters a space adjacent to this monster, that hero suffers 1 Fatigue.",
      "Ravage: Both of this monster's actions on a turn may be attack actions.",
      "Stealthy: Each attack that targets this monster must roll 3 additional range beyond the normally requi2 amount or the attack is a miss."
    ],
    "group size": [
      "2 Heroes: 0,1",
      "3 Heroes: 1,1",
      "4 Heroes: 2,1"
    ],
    "expansion": "Guardians Of Deephall",
    "image": "monsters/wendigo-gd-act1-back.png",
    "xws": "wendigo"
  },
  {
    "name": "Wendigo",
    "points": 262,
    "act": "II",
    "attack": "Melee",
    "characteristics": [
      "Minion: Speed: 4, Health: 7, Defense: 5, Attack: 1 2, Abilities: Ravage, Stealthy, Surge: +2 Hearts",
      "Master: Speed: 4, Health: 10, Defense: 5, Attack: 1 2 3, Abilities: Freezing, Ravage, Stealthy, Surge: +2 Hearts"
    ],
    "expansion": "Guardians Of Deephall",
    "image": "monsters/wendigo-gd-act2-front.png",
    "xws": "wendigo"
  },
  {
    "name": "Wendigo",
    "points": 263,
    "act": "II",
    "traits": [
      "Cold",
      "Cave"
    ],
    "ability rules": [
      "Freezing: Each time a hero enters a space adjacent to this monster, that hero suffers 1 Fatigue.",
      "Ravage: Both of this monster's actions on a turn may be attack actions.",
      "Stealthy: Each attack that targets this monster must roll 3 additional range beyond the normally requi2 amount or the attack is a miss."
    ],
    "group size": [
      "2 Heroes: 0,1",
      "3 Heroes: 1,1",
      "4 Heroes: 2,1"
    ],
    "expansion": "Guardians Of Deephall",
    "image": "monsters/wendigo-gd-act2-back.png",
    "xws": "wendigo"
  },
  {
    "name": "Manticore",
    "points": 264,
    "act": "I",
    "attack": "Range",
    "characteristics": [
      "Minion: Speed: 4, Health: 5, Defense: 5, Attack: 1 3, Abilities: Ravage, Surge: Pierce 2, Surge: +1 Range",
      "Master: Speed: 4, Health: 7, Defense: 5, Attack: 1 3, Abilities: Ravage, Surge: Pierce 3, Surge: Poison, Surge: +1 Range"
    ],
    "expansion": "Visions Of Dawn",
    "image": "monsters/manticore-vd-act1-front.png",
    "xws": "manticore"
  },
  {
    "name": "Manticore",
    "points": 265,
    "act": "I",
    "traits": [
      "Wilderness",
      "Dark"
    ],
    "ability rules": [
      "Ravage: Both of this monster's actions on a turn may be attack actions.",
      "Pierce X: This attack ignores X Shields rolled on the defense dice.",
      "Poison: If this attack deals at least 1 Heart (after the defense roll), the target is Poisoned."
    ],
    "group size": [
      "2 Heroes: 1,0",
      "3 Heroes: 0,1",
      "4 Heroes: 1,1"
    ],
    "expansion": "Visions Of Dawn",
    "image": "monsters/manticore-vd-act1-back.png",
    "xws": "manticore"
  },
  {
    "name": "Manticore",
    "points": 266,
    "act": "II",
    "attack": "Range",
    "characteristics": [
      "Minion: Speed: 4, Health: 7, Defense: 5, Attack: 1 3 3, Abilities: Ravage, Surge: Pierce 3, Surge: +2 Range",
      "Master: Speed: 4, Health: 9, Defense: 5, Attack: 1 3 3, Abilities: Ravage, Surge: Pierce 4, Surge: Poison, Surge: +2 Range"
    ],
    "expansion": "Visions Of Dawn",
    "image": "monsters/manticore-vd-act2-front.png",
    "xws": "manticore"
  },
  {
    "name": "Manticore",
    "points": 267,
    "act": "II",
    "traits": [
      "Wilderness",
      "Dark"
    ],
    "ability rules": [
      "Ravage: Both of this monster's actions on a turn may be attack actions.",
      "Pierce X: This attack ignores X Shields rolled on the defense dice.",
      "Poison: If this attack deals at least 1 Heart (after the defense roll), the target is Poisoned."
    ],
    "group size": [
      "2 Heroes: 1,0",
      "3 Heroes: 0,1",
      "4 Heroes: 1,1"
    ],
    "expansion": "Visions Of Dawn",
    "image": "monsters/manticore-vd-act2-back.png",
    "xws": "manticore"
  },
  {
    "name": "Ogre",
    "points": 268,
    "act": "I",
    "attack": "Melee",
    "characteristics": [
      "Minion: Speed: 3, Health: 6, Defense: 5, Attack: 1 3, Abilities: Surge: Knockback, Surge: +3 Hearts",
      "Master: Speed: 3, Health: 9, Defense: 5, Attack: 1 2, Abilities: Fleshmonger, Surge: Knockback, Surge: +3 Hearts"
    ],
    "expansion": "Visions Of Dawn",
    "image": "monsters/ogre-vd-act1-front.png",
    "xws": "ogre"
  },
  {
    "name": "Ogre",
    "points": 269,
    "act": "I",
    "traits": [
      "Building",
      "Cave"
    ],
    "ability rules": [
      "Fleshmonger: Each time a hero whose hero token is not on this card suffers 1 or more Hearts from an attack performed by this monster, place 1 of his hero tokens on this card. For each hero token on this card, this monster applies +2 to its Health. When this monster is defeated, discard all hero tokens from this card.",
      "Knockback: Remove the target from the map, then place him on any empty space within 3 spaces of his original space. He counts as entering that space."
    ],
    "group size": [
      "2 Heroes: 1,0",
      "3 Heroes: 0,1",
      "4 Heroes: 1,1"
    ],
    "expansion": "Visions Of Dawn",
    "image": "monsters/ogre-vd-act1-back.png",
    "xws": "ogre"
  },
  {
    "name": "Ogre",
    "points": 270,
    "act": "II",
    "attack": "Melee",
    "characteristics": [
      "Minion: Speed: 3, Health: 9, Defense: 5, Attack: 1 3, Abilities: Surge: Knockback, Surge: +3 Hearts",
      "Master: Speed: 3, Health: 12, Defense: 5, Attack: 1 2 3, Abilities: Fleshmonger, Surge: Knockback, Surge: +3 Hearts"
    ],
    "expansion": "Visions Of Dawn",
    "image": "monsters/ogre-vd-act2-front.png",
    "xws": "ogre"
  },
  {
    "name": "Ogre",
    "points": 271,
    "act": "II",
    "traits": [
      "Building",
      "Cave"
    ],
    "ability rules": [
      "Fleshmonger: Each time a hero whose hero token is not on this card suffers 1 or more Hearts from an attack performed by this monster, place 1 of his hero tokens on this card. For each hero token on this card, this monster applies +2 to its Health. When this monster is defeated, discard all hero tokens from this card.",
      "Knockback: Remove the target from the map, then place him on any empty space within 3 spaces of his original space. He counts as entering that space."
    ],
    "group size": [
      "2 Heroes: 1,0",
      "3 Heroes: 0,1",
      "4 Heroes: 1,1"
    ],
    "expansion": "Visions Of Dawn",
    "image": "monsters/ogre-vd-act2-back.png",
    "xws": "ogre"
  },
  {
    "name": "Troll",
    "points": 272,
    "act": "I",
    "attack": "Melee",
    "characteristics": [
      "Minion: Speed: 3, Health: 8, Defense: 5, Attack: 1 2, Abilities: Backswing, Reach",
      "Master: Speed: 3, Health: 10, Defense: 5, Attack: 1 2, Abilities: Backswing, Reach, Action: Sweep"
    ],
    "expansion": "Visions Of Dawn",
    "image": "monsters/troll-vd-act1-front.png",
    "xws": "troll"
  },
  {
    "name": "Troll",
    "points": 273,
    "act": "I",
    "traits": [
      "Mountains",
      "Cave"
    ],
    "ability rules": [
      "Backswing: Use immediately after performing an attack to choose any number of figures affected by that attack. Each of those figures tests Awareness. If none of the figures pass, each chose figure suffers 2 Hearts and is Stunned.",
      "Reach: This monster may attack targets up to 2 spaces away.",
      "Sweep: Perform an attack. This attack effects each figure within 2 spaces and line of sight of this monster. Each figure rolls defense dice separately."
    ],
    "group size": [
      "2 Heroes: 1,0",
      "3 Heroes: 0,1",
      "4 Heroes: 1,1"
    ],
    "expansion": "Visions Of Dawn",
    "image": "monsters/troll-vd-act1-back.png",
    "xws": "troll"
  },
  {
    "name": "Troll",
    "points": 274,
    "act": "II",
    "attack": "Melee",
    "characteristics": [
      "Minion: Speed: 3, Health: 10, Defense: 5, Attack: 1 2 2, Abilities: Backswing, Reach",
      "Master: Speed: 3, Health: 13, Defense: 5, Attack: 1 2 2, Abilities: Backswing, Reach, Action: Sweep"
    ],
    "expansion": "Visions Of Dawn",
    "image": "monsters/troll-vd-act2-front.png",
    "xws": "troll"
  },
  {
    "name": "Troll",
    "points": 275,
    "act": "II",
    "traits": [
      "Mountains",
      "Cave"
    ],
    "ability rules": [
      "Backswing: Use immediately after performing an attack to choose any number of figures affected by that attack. Each of those figures tests Awareness. If none of the figures pass, each chose figure suffers 2 Hearts and is Stunned.",
      "Reach: This monster may attack targets up to 2 spaces away.",
      "Sweep: Perform an attack. This attack effects each figure within 2 spaces and line of sight of this monster. Each figure rolls defense dice separately."
    ],
    "group size": [
      "2 Heroes: 1,0",
      "3 Heroes: 0,1",
      "4 Heroes: 1,1"
    ],
    "expansion": "Visions Of Dawn",
    "image": "monsters/troll-vd-act2-back.png",
    "xws": "troll"
  },
  {
    "name": "Deep Elf",
    "points": 276,
    "act": "I",
    "attack": "Melee",
    "characteristics": [
      "Minion: Speed: 5, Health: 7, Defense: 7, Attack: 1 3, Abilities: Stealthy, Pierce 2, Surge: +1 Heart",
      "Master: Speed: 5, Health: 9, Defense: 7, Attack: 1 3, Abilities: Riposte, Stealthy, Pierce 3, Surge: +1 Heart"
    ],
    "expansion": "Bonds Of The Wild",
    "image": "monsters/deep-elf-bw-act1-front.png",
    "xws": "deepelf"
  },
  {
    "name": "Deep Elf",
    "points": 277,
    "act": "I",
    "traits": [
      "Dark",
      "Cave"
    ],
    "ability rules": [
      "Riposte: Each time an adjacent figure resolves an attack that affects this monster, that figure suffers Hearts equal to the defense results; if the attack is a miss, the figure suffers Hearts equal to the Hearts rolled instead.",
      "Stealthy: Each attack that targets this monster must roll 3 additional range beyond the normally requi2 amount or the attack is a miss.",
      "Pierce X: This attack ignores X Shields rolled on the defense dice."
    ],
    "group size": [
      "2 Heroes: 1,0",
      "3 Heroes: 0,1",
      "4 Heroes: 1,1"
    ],
    "expansion": "Bonds Of The Wild",
    "image": "monsters/deep-elf-bw-act1-back.png",
    "xws": "deepelf"
  },
  {
    "name": "Deep Elf",
    "points": 278,
    "act": "II",
    "attack": "Melee",
    "characteristics": [
      "Minion: Speed: 5, Health: 8, Defense: 5, Attack: 1 3, Abilities: Stealthy, Pierce 2, Surge: +2 Hearts",
      "Master: Speed: 5, Health: 10, Defense: 5, Attack: 1 3, Abilities: Riposte, Stealthy, Pierce 4, Surge: +2 Hearts"
    ],
    "expansion": "Bonds Of The Wild",
    "image": "monsters/deep-elf-bw-act2-front.png",
    "xws": "deepelf"
  },
  {
    "name": "Deep Elf",
    "points": 279,
    "act": "II",
    "traits": [
      "Dark",
      "Cave"
    ],
    "ability rules": [
      "Riposte: Each time an adjacent figure resolves an attack that affects this monster, that figure suffers Hearts equal to the defense results; if the attack is a miss, the figure suffers Hearts equal to the Hearts rolled instead.",
      "Stealthy: Each attack that targets this monster must roll 3 additional range beyond the normally requi2 amount or the attack is a miss.",
      "Pierce X: This attack ignores X Shields rolled on the defense dice."
    ],
    "group size": [
      "2 Heroes: 1,0",
      "3 Heroes: 0,1",
      "4 Heroes: 1,1"
    ],
    "expansion": "Bonds Of The Wild",
    "image": "monsters/deep-elf-bw-act2-back.png",
    "xws": "deepelf"
  },
  {
    "name": "Hellhound",
    "points": 280,
    "act": "I",
    "attack": "Melee",
    "characteristics": [
      "Minion: Speed: 4, Health: 4, Defense: 5, Attack: 1 2, Abilities: Surge: Hunt, Surge: Pierce 2",
      "Master: Speed: 4, Health: 6, Defense: 5, Attack: 1 2, Abilities: Surge: Hunt, Surge: Fire Breath, Surge: Pierce 2"
    ],
    "expansion": "Bonds Of The Wild",
    "image": "monsters/hellhound-bw-act1-front.png",
    "xws": "hellhound"
  },
  {
    "name": "Hellhound",
    "points": 281,
    "act": "I",
    "traits": [
      "Hot",
      "Cursed"
    ],
    "ability rules": [
      "Hunt: After this attack resolves, you may remove the target from the map and place it in an empty space adjacent to this monster.",
      "Fire Breath: Starting with the target space, trace a path of 4 spaces in any direction. All figures on this path are affected by this attack. Each figure rolls defense dice separately.",
      "Pierce X: This attack ignores X Shields rolled on the defense dice."
    ],
    "group size": [
      "2 Heroes: 1,1",
      "3 Heroes: 2,1",
      "4 Heroes: 3,1"
    ],
    "expansion": "Bonds Of The Wild",
    "image": "monsters/hellhound-bw-act1-back.png",
    "xws": "hellhound"
  },
  {
    "name": "Hellhound",
    "points": 282,
    "act": "II",
    "attack": "Melee",
    "characteristics": [
      "Minion: Speed: 5, Health: 6, Defense: 6, Attack: 1 2, Abilities: Surge: Hunt, Surge: Pierce 3",
      "Master: Speed: 5, Health: 8, Defense: 6, Attack: 1 2 3, Abilities: Surge: Hunt, Surge: Fire Breath, Surge: Pierce 3"
    ],
    "expansion": "Bonds Of The Wild",
    "image": "monsters/hellhound-bw-act2-front.png",
    "xws": "hellhound"
  },
  {
    "name": "Hellhound",
    "points": 283,
    "act": "II",
    "traits": [
      "Hot",
      "Cursed"
    ],
    "ability rules": [
      "Hunt: After this attack resolves, you may remove the target from the map and place it in an empty space adjacent to this monster.",
      "Fire Breath: Starting with the target space, trace a path of 4 spaces in any direction. All figures on this path are affected by this attack. Each figure rolls defense dice separately.",
      "Pierce X: This attack ignores X Shields rolled on the defense dice."
    ],
    "group size": [
      "2 Heroes: 1,1",
      "3 Heroes: 2,1",
      "4 Heroes: 3,1"
    ],
    "expansion": "Bonds Of The Wild",
    "image": "monsters/hellhound-bw-act2-back.png",
    "xws": "hellhound"
  },
  {
    "name": "Kobold",
    "points": 284,
    "act": "I",
    "attack": "Melee",
    "characteristics": [
      "Minion: Speed: 3, Health: 2, Defense: 7, Attack: 1, Abilities: Scamper, Small Beginnings, Surge: Swarm",
      "Master: Speed: 3, Health: 5, Defense: 7, Attack: 1 3, Abilities: Scamper, Spawner, Surge: Swarm"
    ],
    "expansion": "Bonds Of The Wild",
    "image": "monsters/kobold-bw-act1-front.png",
    "xws": "kobold"
  },
  {
    "name": "Kobold",
    "points": 285,
    "act": "I",
    "traits": [
      "Building",
      "Cave"
    ],
    "ability rules": [
      "Scamper: This monster may move through spaces containing heroes.",
      "Small Beginnings: Do not place this monster during Setup.",
      "Spawner: At the start of each overlord turn, place 1 minion Kobold adjacent to this monster, respecting group limits.",
      "Swarm: This monster deals +1 Heart for each other monster adjacent to the target."
    ],
    "group size": [
      "2 Heroes: 4,2",
      "3 Heroes: 8,2",
      "4 Heroes: 9,3"
    ],
    "expansion": "Bonds Of The Wild",
    "image": "monsters/kobold-bw-act1-back.png",
    "xws": "kobold"
  },
  {
    "name": "Kobold",
    "points": 286,
    "act": "II",
    "attack": "Melee",
    "characteristics": [
      "Minion: Speed: 4, Health: 4, Defense: 7, Attack: 1, Abilities: Scamper, Small Beginnings, Surge: Swarm",
      "Master: Speed: 4, Health: 7, Defense: 7, Attack: 1 3, Abilities: Scamper, Spawner, Surge: Swarm"
    ],
    "expansion": "Bonds Of The Wild",
    "image": "monsters/kobold-bw-act2-front.png",
    "xws": "kobold"
  },
  {
    "name": "Kobold",
    "points": 287,
    "act": "II",
    "traits": [
      "Building",
      "Cave"
    ],
    "ability rules": [
      "Scamper: This monster may move through spaces containing heroes.",
      "Small Beginnings: Do not place this monster during Setup.",
      "Spawner: At the start of each overlord turn, place 1 minion Kobold adjacent to this monster, respecting group limits.",
      "Swarm: This monster deals +1 Heart for each other monster adjacent to the target."
    ],
    "group size": [
      "2 Heroes: 4,2",
      "3 Heroes: 8,2",
      "4 Heroes: 9,3"
    ],
    "expansion": "Bonds Of The Wild",
    "image": "monsters/kobold-bw-act2-back.png",
    "xws": "kobold"
  },
  {
    "name": "Crow Hag",
    "points": 288,
    "act": "I",
    "attack": "Range",
    "characteristics": [
      "Minion: Speed: 4, Health: 5, Defense: 5, Attack: 1 3, Abilities: Lifethirst 1, Surge: +1 Heart",
      "Master: Speed: 4, Health: 7, Defense: 6, Attack: 1 3, Abilities: Lifethirst 1, Action: Death Omen, Surge: +1 Heart"
    ],
    "expansion": "Treaty Of Champions",
    "image": "monsters/crow-hag-tc-act1-front.png",
    "xws": "crowhag"
  },
  {
    "name": "Crow Hag",
    "points": 289,
    "act": "I",
    "traits": [
      "Dark",
      "Civilized"
    ],
    "ability rules": [
      "Lifethirst X: Each time a hero within 5 spaces of this monster recovers 1 or more Hearts, that hero 2uces the amount of Hearts recove2 by X (to a minimum of 0).",
      "Death Omen: Choose 1 hero in this monster's line of sight. That hero may choose to suffer 2 Hearts. If he does not, he suffers 1 condition of your choice."
    ],
    "group size": [
      "2 Heroes: 0,1",
      "3 Heroes: 1,1",
      "4 Heroes: 2,1"
    ],
    "expansion": "Treaty Of Champions",
    "image": "monsters/crow-hag-tc-act1-back.png",
    "xws": "crowhag"
  },
  {
    "name": "Crow Hag",
    "points": 290,
    "act": "II",
    "attack": "Range",
    "characteristics": [
      "Minion: Speed: 5, Health: 7, Defense: 6, Attack: 1 3, Abilities: Lifethirst 1, Surge: +2 Hearts",
      "Master: Speed: 5, Health: 9, Defense: 6 5, Attack: 1 3, Abilities: Lifethirst 2, Action: Death Omen, Surge: +2 Hearts"
    ],
    "expansion": "Treaty Of Champions",
    "image": "monsters/crow-hag-tc-act2-front.png",
    "xws": "crowhag"
  },
  {
    "name": "Crow Hag",
    "points": 291,
    "act": "II",
    "traits": [
      "Dark",
      "Civilized"
    ],
    "ability rules": [
      "Lifethirst X: Each time a hero within 5 spaces of this monster recovers 1 or more Hearts, that hero 2uces the amount of Hearts recove2 by X (to a minimum of 0).",
      "Death Omen: Choose 1 hero in this monster's line of sight. That hero may choose to suffer 2 Hearts. If he does not, he suffers 1 condition of your choice."
    ],
    "group size": [
      "2 Heroes: 0,1",
      "3 Heroes: 1,1",
      "4 Heroes: 2,1"
    ],
    "expansion": "Treaty Of Champions",
    "image": "monsters/crow-hag-tc-act2-back.png",
    "xws": "crowhag"
  },
  {
    "name": "Demon Lord",
    "points": 292,
    "act": "I",
    "attack": "Range",
    "characteristics": [
      "Minion: Speed: 3, Health: 6, Defense: 5 5, Attack: 1 3, Abilities: Sorcery 2, Surge: Wither",
      "Master: Speed: 3, Health: 9, Defense: 5 5, Attack: 1 3, Abilities: Aura 1, Sorcery 3, Surge: Wither"
    ],
    "expansion": "Treaty Of Champions",
    "image": "monsters/demon-lord-tc-act1-front.png",
    "xws": "demonlord"
  },
  {
    "name": "Demon Lord",
    "points": 293,
    "act": "I",
    "traits": [
      "Hot",
      "Cursed"
    ],
    "ability rules": [
      "Aura X: Each time a hero enters a space adjacent to this monster, that hero suffers X Hearts.",
      "Sorcery X: After making an attack roll, this monster may convert up to X range to Hearts, or up to X Hearts to range.",
      "Wither: The target suffers 1 Fatigue."
    ],
    "group size": [
      "2 Heroes: 1,0",
      "3 Heroes: 0,1",
      "4 Heroes: 1,1"
    ],
    "expansion": "Treaty Of Champions",
    "image": "monsters/demon-lord-tc-act1-back.png",
    "xws": "demonlord"
  },
  {
    "name": "Demon Lord",
    "points": 294,
    "act": "II",
    "attack": "Range",
    "characteristics": [
      "Minion: Speed: 3, Health: 8, Defense: 5 5, Attack: 1 3 3, Abilities: Sorcery 2, Surge: Wither",
      "Master: Speed: 3, Health: 12, Defense: 5 5, Attack: 1 2 3, Abilities: Aura 1, Sorcery 3, Surge: Wither"
    ],
    "expansion": "Treaty Of Champions",
    "image": "monsters/demon-lord-tc-act2-front.png",
    "xws": "demonlord"
  },
  {
    "name": "Demon Lord",
    "points": 295,
    "act": "II",
    "traits": [
      "Hot",
      "Cursed"
    ],
    "ability rules": [
      "Aura 1: Each time a hero enters a space adjacent to this monster, that hero suffers 1 Heart.",
      "Sorcery X: After making an attack roll, this monster may convert up to X range to Hearts, or up to X Hearts to range.",
      "Wither: That target suffers 1 Fatigue."
    ],
    "group size": [
      "2 Heroes: 1,0",
      "3 Heroes: 0,1",
      "4 Heroes: 1,1"
    ],
    "expansion": "Treaty Of Champions",
    "image": "monsters/demon-lord-tc-act2-back.png",
    "xws": "demonlord"
  },
  {
    "name": "Skeleton Archer",
    "points": 296,
    "act": "I",
    "attack": "Range",
    "characteristics": [
      "Minion: Speed: 4, Health: 3, Defense: 7, Attack: 1 3, Abilities: Pierce 1, Reanimation, Surge: +1 Range",
      "Master: Speed: 4, Health: 6, Defense: 7, Attack: 1 3, Abilities: Pierce 1, Reanimation, Surge: +1 Range, Surge: +1 Heart"
    ],
    "expansion": "Treaty Of Champions",
    "image": "monsters/skeleton-archer-tc-act1-front.png",
    "xws": "skeletonarcher"
  },
  {
    "name": "Skeleton Archer",
    "points": 297,
    "act": "I",
    "traits": [
      "Cursed",
      "Civilized"
    ],
    "ability rules": [
      "Pierce X: This attack ignores X Shields rolled on the defense dice.",
      "Reanimation: Each time this monster suffers Hearts and is not defeated, it recovers Hearts equal to either the amount suffe2 or the number of monsters from this monster group within 3 spaces of it, whichever is less."
    ],
    "group size": [
      "2 Heroes: 2,1",
      "3 Heroes: 3,1",
      "4 Heroes: 4,1"
    ],
    "expansion": "Treaty Of Champions",
    "image": "monsters/skeleton-archer-tc-act1-back.png",
    "xws": "skeletonarcher"
  },
  {
    "name": "Skeleton Archer",
    "points": 298,
    "act": "II",
    "attack": "Range",
    "characteristics": [
      "Minion: Speed: 4, Health: 4, Defense: 7, Attack: 1 3, Abilities: Pierce 1, Reanimation, Surge: +2 Range, Surge: +1 Heart",
      "Master: Speed: 4, Health: 8, Defense: 7, Attack: 1 3, Abilities: Pierce 2, Reanimation, Surge: +2 Range, Surge: +2 Hearts"
    ],
    "expansion": "Treaty Of Champions",
    "image": "monsters/skeleton-archer-tc-act2-front.png",
    "xws": "skeletonarcher"
  },
  {
    "name": "Skeleton Archer",
    "points": 299,
    "act": "II",
    "traits": [
      "Cursed",
      "Civilized"
    ],
    "ability rules": [
      "Pierce X: This attack ignores X Shields rolled on the defense dice.",
      "Reanimation: Each time this monster suffers Hearts and is not defeated, it recovers Hearts equal to either the amount suffe2 or the number of monsters from this monster group within 3 spaces of it, whichever is less."
    ],
    "group size": [
      "2 Heroes: 2,1",
      "3 Heroes: 3,1",
      "4 Heroes: 4,1"
    ],
    "expansion": "Treaty Of Champions",
    "image": "monsters/skeleton-archer-tc-act2-back.png",
    "xws": "skeletonarcher"
  },
  {
    "name": "Blood Ape",
    "points": 300,
    "act": "I",
    "attack": "Melee",
    "characteristics": [
      "Minion: Speed: 4, Health: 5, Defense: 5, Attack: 1 2, Abilities: Ravage, Surge: +1 Heart",
      "Master: Speed: 4, Health: 7, Defense: 5, Attack: 1 2, Abilities: Ravage, Action: Leap Attack, Surge: +2 Hearts"
    ],
    "expansion": "Stewards Of The Secret",
    "image": "monsters/blood-ape-ss-act1-front.png",
    "xws": "bloodape"
  },
  {
    "name": "Blood Ape",
    "points": 301,
    "act": "I",
    "traits": [
      "Cave",
      "Hot"
    ],
    "ability rules": [
      "Ravage: Both of this monster's actions on a turn may be attack actions.",
      "Leap Attack: This monster moves up to its Speed. During this movement, it may move through spaces containing enemy figures. Then, perform an attack that affects each figure this monster moved through during this action."
    ],
    "group size": [
      "2 Heroes: 0,1",
      "3 Heroes: 1,1",
      "4 Heroes: 2,1"
    ],
    "expansion": "Stewards Of The Secret",
    "image": "monsters/blood-ape-ss-act1-back.png",
    "xws": "bloodape"
  },
  {
    "name": "Blood Ape",
    "points": 302,
    "act": "II",
    "attack": "Melee",
    "characteristics": [
      "Minion: Speed: 4, Health: 7, Defense: 5, Attack: 1 2 3, Abilities: Ravage, Surge: +2 Hearts",
      "Master: Speed: 4, Health: 9, Defense: 5, Attack: 1 2 2, Abilities: Ravage, Action: Leap Attack, Surge: +2 Hearts"
    ],
    "expansion": "Stewards Of The Secret",
    "image": "monsters/blood-ape-ss-act2-front.png",
    "xws": "bloodape"
  },
  {
    "name": "Blood Ape",
    "points": 303,
    "act": "II",
    "traits": [
      "Cave",
      "Hot"
    ],
    "ability rules": [
      "Ravage: Both of this monster's actions on a turn may be attack actions.",
      "Leap Attack: This monster moves up to its Speed. During this movement, it may move through spaces containing enemy figures. Then, perform an attack that affects each figure this monster moved through during this action."
    ],
    "group size": [
      "2 Heroes: 0,1",
      "3 Heroes: 1,1",
      "4 Heroes: 2,1"
    ],
    "expansion": "Stewards Of The Secret",
    "image": "monsters/blood-ape-ss-act2-back.png",
    "xws": "bloodape"
  },
  {
    "name": "Ferrox",
    "points": 304,
    "act": "I",
    "attack": "Melee",
    "characteristics": [
      "Minion: Speed: 4, Health: 4, Defense: 5, Attack: 1 2, Abilities: Surge: Disease, Surge: Pierce 2",
      "Master: Speed: 4, Health: 5, Defense: 5, Attack: 1 2, Abilities: Action: Extract, Surge: Disease, Surge: Pierce 2"
    ],
    "expansion": "Stewards Of The Secret",
    "image": "monsters/ferrox-ss-act1-front.png",
    "xws": "ferrox"
  },
  {
    "name": "Ferrox",
    "points": 305,
    "act": "I",
    "traits": [
      "Cave",
      "Water"
    ],
    "ability rules": [
      "Extract: Choose a hero adjacent to this monster. That hero tests Might. If he fails, that hero suffers 2 Fatigue, and this monster recovers 2 Hearts.",
      "Disease: If this attack deals at least 1 Heart (after the defense roll), the target is Diseased.",
      "Pierce X: This attack ignores X Shields rolled on the defense dice."
    ],
    "group size": [
      "2 Heroes: 1,1",
      "3 Heroes: 2,1",
      "4 Heroes: 3,1"
    ],
    "expansion": "Stewards Of The Secret",
    "image": "monsters/ferrox-ss-act1-back.png",
    "xws": "ferrox"
  },
  {
    "name": "Ferrox",
    "points": 306,
    "act": "II",
    "attack": "Melee",
    "characteristics": [
      "Minion: Speed: 4, Health: 5, Defense: 5 7, Attack: 1 2 3, Abilities: Surge: Disease, Surge: Pierce 3",
      "Master: Speed: 4, Health: 8, Defense: 5 7, Attack: 1 2 3, Abilities: Action: Extract, Surge: Disease, Surge: Pierce 3"
    ],
    "expansion": "Stewards Of The Secret",
    "image": "monsters/ferrox-ss-act2-front.png",
    "xws": "ferrox"
  },
  {
    "name": "Ferrox",
    "points": 307,
    "act": "II",
    "traits": [
      "Cave",
      "Water"
    ],
    "ability rules": [
      "Extract: Choose a hero adjacent to this monster. That hero tests Might. If he fails, that hero suffers 2 Fatigue, and this monster recovers 2 Hearts.",
      "Disease: If this attack deals at least 1 Heart (after the defense roll), the target is Diseased.",
      "Pierce X: This attack ignores X Shields rolled on the defense dice."
    ],
    "group size": [
      "2 Heroes: 1,1",
      "3 Heroes: 2,1",
      "4 Heroes: 3,1"
    ],
    "expansion": "Stewards Of The Secret",
    "image": "monsters/ferrox-ss-act2-back.png",
    "xws": "ferrox"
  },
  {
    "name": "Naga",
    "points": 308,
    "act": "I",
    "attack": "Range",
    "characteristics": [
      "Minion: Speed: 4, Health: 5, Defense: 6, Attack: 1 2, Abilities: Sorcery 1, Surge: Poison",
      "Master: Speed: 4, Health: 6, Defense: 6, Attack: 1 2, Abilities: Sorcery 2, Action: Constrict, Surge: Poison"
    ],
    "expansion": "Stewards Of The Secret",
    "image": "monsters/naga-ss-act1-front.png",
    "xws": "naga"
  },
  {
    "name": "Naga",
    "points": 309,
    "act": "I",
    "traits": [
      "Water",
      "Cave"
    ],
    "ability rules": [
      "Sorcery X: After making an attack roll, this monster may convert up to X range to Hearts, or up to X Hearts to range.",
      "Constrict: Choose 1 hero adjacent to this monster. That hero tests Might. If he fails, he is Immobilized, this monster may move 1 space, and then you may place the hero in an empty space adjacent to this monster.",
      "Poison: If this attack deals at least 1 Heart (after the defense roll), the target is Poisoned."
    ],
    "group size": [
      "2 Heroes: 0,1",
      "3 Heroes: 1,1",
      "4 Heroes: 2,1"
    ],
    "expansion": "Stewards Of The Secret",
    "image": "monsters/naga-ss-act1-back.png",
    "xws": "naga"
  },
  {
    "name": "Naga",
    "points": 310,
    "act": "II",
    "attack": "Range",
    "characteristics": [
      "Minion: Speed: 4, Health: 6, Defense: 6, Attack: 1 2, Abilities: Sorcery 3, Surge: Poison",
      "Master: Speed: 4, Health: 8, Defense: 6, Attack: 1 2 3, Abilities: Sorcery 3, Action: Constrict, Surge: Poison"
    ],
    "expansion": "Stewards Of The Secret",
    "image": "monsters/naga-ss-act2-front.png",
    "xws": "naga"
  },
  {
    "name": "Naga",
    "points": 311,
    "act": "II",
    "traits": [
      "Water",
      "Cave"
    ],
    "ability rules": [
      "Sorcery X: After making an attack roll, this monster may convert up to X range to Hearts, or up to X Hearts to range.",
      "Constrict: Choose 1 hero adjacent to this monster. That hero tests Might. If he fails, he is Immobilized, this monster may move 1 space, and then you may place the hero in an empty space adjacent to this monster.",
      "Poison: If this attack deals at least 1 Heart (after the defense roll), the target is Poisoned."
    ],
    "group size": [
      "2 Heroes: 0,1",
      "3 Heroes: 1,1",
      "4 Heroes: 2,1"
    ],
    "expansion": "Stewards Of The Secret",
    "image": "monsters/naga-ss-act2-back.png",
    "xws": "naga"
  },
  {
    "name": "Dark Minotaur",
    "points": 312,
    "act": "I",
    "attack": "Melee",
    "characteristics": [
      "Minion: Speed: 4, Health: 8, Defense: 7, Attack: 1, Abilities: Bull Rush, Filthy Murk, Putrid Boils, Surge: Pierce 1",
      "Master: Speed: 4, Health: 8, Defense: 6, Attack: 1, Abilities: Bull Rush, Filthy Murk, Putrid Boils, Surge: Pierce 2"
    ],
    "expansion": "Shards Of Everdark",
    "image": "monsters/dark-minotaur-se-act1-front.png",
    "xws": "darkminotaur"
  },
  {
    "name": "Dark Minotaur",
    "points": 313,
    "act": "I",
    "traits": [
      "Civilized",
      "Dark"
    ],
    "ability rules": [
      "Bull Rush: Each time this monster targets a space that it was not adjacent to at the start of its activation, add 1 2 power die to its attack pool.",
      "Filthy Murk: At the end of this monster's activation, each hero within 3 spaces of it is Diseased.",
      "Putrid Boils: Each time a Diseased hero within 3 spaces of 1 or more monsters with Putrid Boils voluntarily suffers 1 or more Fatigue, that hero suffers 1 Heart.",
      "Pierce X: This attack ignores X Shields rolled on the defense dice."
    ],
    "group size": [
      "2 Heroes: 1,0",
      "3 Heroes: 0,1",
      "4 Heroes: 1,1"
    ],
    "expansion": "Shards Of Everdark",
    "image": "monsters/dark-minotaur-se-act1-back.png",
    "xws": "darkminotaur"
  },
  {
    "name": "Dark Minotaur",
    "points": 314,
    "act": "II",
    "attack": "Melee",
    "characteristics": [
      "Minion: Speed: 4, Health: 10, Defense: 5 7, Attack: 1 2, Abilities: Bull Rush, Filthy Murk, Putrid Boils, Surge: Pierce 2",
      "Master: Speed: 4, Health: 10, Defense: 5 6, Attack: 1 2, Abilities: Bull Rush, Filthy Murk, Putrid Boils, Surge: Pierce 4"
    ],
    "expansion": "Shards Of Everdark",
    "image": "monsters/dark-minotaur-se-act2-front.png",
    "xws": "darkminotaur"
  },
  {
    "name": "Dark Minotaur",
    "points": 315,
    "act": "II",
    "traits": [
      "Civilized",
      "Dark"
    ],
    "ability rules": [
      "Bull Rush: Each time this monster targets a space that it was not adjacent to at the start of its activation, add 1 2 power die to its attack pool.",
      "Filthy Murk: At the end of this monster's activation, each hero within 3 spaces of it is Diseased.",
      "Putrid Boils: Each time a Diseased hero within 3 spaces of 1 or more monsters with Putrid Boils voluntarily suffers 1 or more Fatigue, that hero suffers 1 Heart.",
      "Pierce X: This attack ignores X Shields rolled on the defense dice."
    ],
    "group size": [
      "2 Heroes: 1,0",
      "3 Heroes: 0,1",
      "4 Heroes: 1,1"
    ],
    "expansion": "Shards Of Everdark",
    "image": "monsters/dark-minotaur-se-act2-back.png",
    "xws": "darkminotaur"
  },
  {
    "name": "Ice Wyrm",
    "points": 316,
    "act": "I",
    "attack": "Melee",
    "characteristics": [
      "Minion: Speed: 4, Health: 7, Defense: 5 5, Attack: 1 2, Abilities: Entomb, Reach",
      "Master: Speed: 4, Health: 9, Defense: 5 5, Attack: 1 2, Abilities: Entomb, Freezing, Reach"
    ],
    "expansion": "Shards Of Everdark",
    "image": "monsters/ice-wyrm-se-act1-front.png",
    "xws": "icewyrm"
  },
  {
    "name": "Ice Wyrm",
    "points": 317,
    "act": "I",
    "traits": [
      "Cold",
      "Cave"
    ],
    "ability rules": [
      "Entomb: Knocked-out heroes within 1 space of this monster can only recover Hearts from stand up actions and heroic feats.",
      "Freezing: Each time a hero enters a space adjacent to this monster, that hero suffers 1 Fatigue.",
      "Reach: This monster may attack targets up to 2 spaces away."
    ],
    "group size": [
      "2 Heroes: 1,0",
      "3 Heroes: 0,1",
      "4 Heroes: 1,1"
    ],
    "expansion": "Shards Of Everdark",
    "image": "monsters/ice-wyrm-se-act1-back.png",
    "xws": "icewyrm"
  },
  {
    "name": "Ice Wyrm",
    "points": 318,
    "act": "II",
    "attack": "Melee",
    "characteristics": [
      "Minion: Speed: 4, Health: 11, Defense: 5 5, Attack: 1 2 2, Abilities: Entomb, Reach",
      "Master: Speed: 4, Health: 14, Defense: 5 5, Attack: 1 2 2, Abilities: Entomb, Freezing, Reach"
    ],
    "expansion": "Shards Of Everdark",
    "image": "monsters/ice-wyrm-se-act2-front.png",
    "xws": "icewyrm"
  },
  {
    "name": "Ice Wyrm",
    "points": 319,
    "act": "II",
    "traits": [
      "Cold",
      "Cave"
    ],
    "ability rules": [
      "Entomb: Knocked-out heroes within 1 space of this monster can only recover Hearts from stand up actions and heroic feats.",
      "Freezing: Each time a hero enters a space adjacent to this monster, that hero suffers 1 Fatigue.",
      "Reach: This monster may attack targets up to 2 spaces away."
    ],
    "group size": [
      "2 Heroes: 1,0",
      "3 Heroes: 0,1",
      "4 Heroes: 1,1"
    ],
    "expansion": "Shards Of Everdark",
    "image": "monsters/ice-wyrm-se-act2-back.png",
    "xws": "icewyrm"
  },
  {
    "name": "Shade",
    "points": 320,
    "act": "I",
    "attack": "Melee",
    "characteristics": [
      "Minion: Speed: 4, Health: 2, Defense: 6, Attack: 1 3, Abilities: Soul Shackle 1, Action: Flicker, Surge: Pierce 1",
      "Master: Speed: 4, Health: 5, Defense: 6, Attack: 1 3, Abilities: Soul Shackle 2, Action: Flicker, Surge: Pierce 2"
    ],
    "expansion": "Shards Of Everdark",
    "image": "monsters/shade-se-act1-front.png",
    "xws": "shade"
  },
  {
    "name": "Shade",
    "points": 321,
    "act": "I",
    "traits": [
      "Cursed",
      "Dark"
    ],
    "ability rules": [
      "Soul Shackle X: Each time a hero within 3 spaces of this monster recovers 1 or more Fatigue, this monster may suffer X Hearts to 2uce the amount of Fatigue recove2 by X (to a minimum of 0).",
      "Flicker: Choose 1 hero within 3 spaces of this monster. Remove this monster from the map and place it adjacent to that hero. Then, if this monster has not performed an attack this activation, that hero tests Awareness. If he fails, perform an attack with this monster that targets that hero.",
      "Pierce X: This attack ignores X Shields rolled on the defense dice."
    ],
    "group size": [
      "2 Heroes: 2,1",
      "3 Heroes: 3,1",
      "4 Heroes: 4,1"
    ],
    "expansion": "Shards Of Everdark",
    "image": "monsters/shade-se-act1-back.png",
    "xws": "shade"
  },
  {
    "name": "Shade",
    "points": 322,
    "act": "II",
    "attack": "Melee",
    "characteristics": [
      "Minion: Speed: 5, Health: 4, Defense: 6, Attack: 1 2, Abilities: Soul Shackle 1, Action: Flicker, Surge: Pierce 1",
      "Master: Speed: 5, Health: 7, Defense: 6, Attack: 1 2 3, Abilities: Soul Shackle 3, Action: Flicker, Surge: Pierce 2"
    ],
    "expansion": "Shards Of Everdark",
    "image": "monsters/shade-se-act2-front.png",
    "xws": "shade"
  },
  {
    "name": "Shade",
    "points": 323,
    "act": "II",
    "traits": [
      "Cursed",
      "Dark"
    ],
    "ability rules": [
      "Soul Shackle X: Each time a hero within 3 spaces of this monster recovers 1 or more Fatigue, this monster may suffer X Hearts to 2uce the amount of Fatigue recove2 by X (to a minimum of 0).",
      "Flicker: Choose 1 hero within 3 spaces of this monster. Remove this monster from the map and place it adjacent to that hero. Then, if this monster has not performed an attack this activation, that hero tests Awareness. If he fails, perform an attack with this monster that targets that hero.",
      "Pierce X: This attack ignores X Shields rolled on the defense dice."
    ],
    "group size": [
      "2 Heroes: 2,1",
      "3 Heroes: 3,1",
      "4 Heroes: 4,1"
    ],
    "expansion": "Shards Of Everdark",
    "image": "monsters/shade-se-act2-back.png",
    "xws": "shade"
  },
  {
    "name": "Burrowing Horror",
    "points": 324,
    "expansion": "Sands Of The Past",
    "image": "monsters/burrowing-horror-sotp-act1-front.png",
    "xws": "burrowinghorror"
  },
  {
    "name": "Burrowing Horror",
    "points": 325,
    "expansion": "Sands Of The Past",
    "image": "monsters/burrowing-horror-sotp-act1-back.png",
    "xws": "burrowinghorror"
  },
  {
    "name": "Burrowing Horror",
    "points": 326,
    "expansion": "Sands Of The Past",
    "image": "monsters/burrowing-horror-sotp-act2-front.png",
    "xws": "burrowinghorror"
  },
  {
    "name": "Burrowing Horror",
    "points": 327,
    "expansion": "Sands Of The Past",
    "image": "monsters/burrowing-horror-sotp-act2-back.png",
    "xws": "burrowinghorror"
  },
  {
    "name": "Sarcophagus Guard",
    "points": 328,
    "expansion": "Sands Of The Past",
    "image": "monsters/sarcophagus-guard-sotp-act1-front.png",
    "xws": "sarcophagusguard"
  },
  {
    "name": "Sarcophagus Guard",
    "points": 329,
    "expansion": "Sands Of The Past",
    "image": "monsters/sarcophagus-guard-sotp-act1-back.png",
    "xws": "sarcophagusguard"
  },
  {
    "name": "Sarcophagus Guard",
    "points": 330,
    "expansion": "Sands Of The Past",
    "image": "monsters/sarcophagus-guard-sotp-act2-front.png",
    "xws": "sarcophagusguard"
  },
  {
    "name": "Sarcophagus Guard",
    "points": 331,
    "expansion": "Sands Of The Past",
    "image": "monsters/sarcophagus-guard-sotp-act2-back.png",
    "xws": "sarcophagusguard"
  }
]

const lieutenants = [
  {
    "name": "Baron Zachareth",
    "points": 0,
    "act": "I",
    "attack": "Melee: 1 2",
    "might": 4,
    "knowledge": 3,
    "willpower": 3,
    "awareness": 4,
    "abilities": [
      "Action: Dominion",
      "Surge: Pierce 2",
      "Surge: Subdue"
    ],
    "characteristics": [
      "2 Heroes: Speed: 4, Health: 10, Defense: 6",
      "3 Heroes: Speed: 4, Health: 13, Defense: 6",
      "4 Heroes: Speed: 4, Health: 16, Defense: 6"
    ],
    "expansion": "Base Game",
    "image": "lieutenants/baron-zachareth-bg-act1-front.png",
    "xws": "baronzachareth"
  },
  {
    "name": "Baron Zachareth",
    "points": 1,
    "act": "I",
    "ability rules": [
      "Dominion: Baron Zachareth tests Willpower. If he passes, he may move a hero within his line of sight 2 spaces in any direction. After the movement, the hero tests Willpower. If he fails, the hero is Immobilized.",
      "Pierce 2: This attack ignores 2 Shields rolled on the defense dice.",
      "Subdue: If this attack deals at least 1 Heart, choose 1 condition. The target suffers the chosen condition."
    ],
    "expansion": "Base Game",
    "image": "lieutenants/baron-zachareth-bg-act1-back.png",
    "xws": "baronzachareth"
  },
  {
    "name": "Baron Zachareth",
    "points": 2,
    "act": "II",
    "attack": "Melee: 1 2 2",
    "might": 4,
    "knowledge": 3,
    "willpower": 3,
    "awareness": 4,
    "abilities": [
      "Action: Dominion",
      "Action: Shadow Bolt",
      "Surge: Pierce 2",
      "Surge: Subdue"
    ],
    "characteristics": [
      "2 Heroes: Speed: 4, Health: 16, Defense: 6 5",
      "3 Heroes: Speed: 4, Health: 18, Defense: 6 5 7",
      "4 Heroes: Speed: 4, Health: 20, Defense: 6 5 5"
    ],
    "expansion": "Base Game",
    "image": "lieutenants/baron-zachareth-bg-act2-front.png",
    "xws": "baronzachareth"
  },
  {
    "name": "Baron Zachareth",
    "points": 3,
    "act": "II",
    "ability rules": [
      "Dominion: Baron Zachareth tests Willpower. If he passes, he may move a hero within his line of sight 2 spaces in any direction. After the movement, the hero tests Willpower. If he fails, the hero is Immobilized.",
      "Pierce 2: This attack ignores 2 Shields rolled on the defense dice.",
      "Subdue: If this attack deals at least 1 Heart, choose 1 condition. The target suffers the chosen condition.",
      "Shadow Bolt: Zachareth performs a Ranged attack: 1 2 3"
    ],
    "expansion": "Base Game",
    "image": "lieutenants/baron-zachareth-bg-act2-back.png",
    "xws": "baronzachareth"
  },
  {
    "name": "Belthir",
    "points": 4,
    "act": "I",
    "attack": "Melee: 1 2",
    "might": 4,
    "knowledge": 3,
    "willpower": 4,
    "awareness": 1,
    "abilities": [
      "Fly",
      "Reach",
      "Surge: Poison"
    ],
    "characteristics": [
      "2 Heroes: Speed: 4, Health: 9, Defense: 5",
      "3 Heroes: Speed: 4, Health: 11, Defense: 5 7",
      "4 Heroes: Speed: 4, Health: 13, Defense: 5 7"
    ],
    "expansion": "Base Game",
    "image": "lieutenants/belthir-bg-act1-front.png",
    "xws": "belthir"
  },
  {
    "name": "Belthir",
    "points": 5,
    "act": "I",
    "ability rules": [
      "Fly: This monster may ignore enemy figures and the effects of terrain while moving. It must end its movement in an empty space following normal movement rules.",
      "Reach: This monster may attack targets up to 2 spaces away.",
      "Poison: If this attack deals at least 1 Heart (after the defense roll), the target is Poisoned."
    ],
    "expansion": "Base Game",
    "image": "lieutenants/belthir-bg-act1-back.png",
    "xws": "belthir"
  },
  {
    "name": "Belthir",
    "points": 6,
    "act": "II",
    "attack": "Melee: 1 2 2",
    "might": 4,
    "knowledge": 3,
    "willpower": 4,
    "awareness": 1,
    "abilities": [
      "Fly",
      "Reach",
      "Action: Cry Havoc",
      "Surge: Poison"
    ],
    "characteristics": [
      "2 Heroes: Speed: 4, Health: 13, Defense: 5 7",
      "3 Heroes: Speed: 4, Health: 15, Defense: 5 5",
      "4 Heroes: Speed: 4, Health: 18, Defense: 5 5 7"
    ],
    "expansion": "Base Game",
    "image": "lieutenants/belthir-bg-act2-front.png",
    "xws": "belthir"
  },
  {
    "name": "Belthir",
    "points": 7,
    "act": "II",
    "ability rules": [
      "Fly: This monster may ignore enemy figures and the effects of terrain while moving. It must end its movement in an empty space following normal movement rules.",
      "Reach: This monster may attack targets up to 2 spaces away.",
      "Cry Havoc: Belthir performs a move action and then attack action. The attack targets every figure he moves through.",
      "Poison: If this attack deals at least 1 Heart (after the defense roll), the target is Poisoned."
    ],
    "expansion": "Base Game",
    "image": "lieutenants/belthir-bg-act2-back.png",
    "xws": "belthir"
  },
  {
    "name": "Lady Eliza Farrow",
    "points": 8,
    "act": "I",
    "attack": "Range: 1 3",
    "might": 1,
    "knowledge": 3,
    "willpower": 3,
    "awareness": 5,
    "abilities": [
      "Action: Sacrifice",
      "Action: Seduce",
      "Surge: Blood Call"
    ],
    "characteristics": [
      "2 Heroes: Speed: 5, Health: 7, Defense: 7",
      "3 Heroes: Speed: 5, Health: 9, Defense: 5",
      "4 Heroes: Speed: 5, Health: 12, Defense: 5"
    ],
    "expansion": "Base Game",
    "image": "lieutenants/lady-eliza-farrow-bg-act1-front.png",
    "xws": "ladyelizafarrow"
  },
  {
    "name": "Lady Eliza Farrow",
    "points": 9,
    "act": "I",
    "ability rules": [
      "Sacrifice: Deal up to 5 Hearts to an adjacent monster to allow Lady Eliza Farrow to recover an equal amount of Hearts.",
      "Seduce: You may choose a hero within 3 spaces of Lady Eliza Farrow and test Eliza's Willpower. If Eliza passes, move the hero 1 space in any direction and the hero is Stunned.",
      "Blood Call: Lady Eliza Farrow recovers Hearts equal to the amount of Hearts dealt with this attack (after rolling defense dice)."
    ],
    "expansion": "Base Game",
    "image": "lieutenants/lady-eliza-farrow-bg-act1-back.png",
    "xws": "ladyelizafarrow"
  },
  {
    "name": "Lady Eliza Farrow",
    "points": 10,
    "act": "II",
    "attack": "Range: 1 3 2",
    "might": 1,
    "knowledge": 3,
    "willpower": 3,
    "awareness": 5,
    "abilities": [
      "Action: Sacrifice",
      "Action: Seduce",
      "Action: Wail",
      "Surge: Blood Call"
    ],
    "characteristics": [
      "2 Heroes: Speed: 5, Health: 9, Defense: 5",
      "3 Heroes: Speed: 5, Health: 11, Defense: 5 7",
      "4 Heroes: Speed: 5, Health: 15, Defense: 5 7"
    ],
    "expansion": "Base Game",
    "image": "lieutenants/lady-eliza-farrow-bg-act2-front.png",
    "xws": "ladyelizafarrow"
  },
  {
    "name": "Lady Eliza Farrow",
    "points": 11,
    "act": "II",
    "ability rules": [
      "Sacrifice: Deal up to 5 Hearts to an adjacent monster to allow Lady Eliza Farrow to recover an equal amount of Hearts.",
      "Seduce: You may choose a hero within 3 spaces of Lady Eliza Farrow and test Eliza's Willpower. If Eliza passes, move the hero 1 space in any direction and the hero is Stunned.",
      "Wail: All heroes within 3 spaces of Lady Eliza Farrow must test Willpower. Each hero that fails suffers 2 Fatigue.",
      "Blood Call: Lady Eliza Farrow recovers Hearts equal to the amount of Hearts dealt with this attack (after rolling defense dice)."
    ],
    "expansion": "Base Game",
    "image": "lieutenants/lady-eliza-farrow-bg-act2-back.png",
    "xws": "ladyelizafarrow"
  },
  {
    "name": "Lord Merick Farrow",
    "points": 12,
    "act": "I",
    "attack": "Range: 1 2",
    "might": 2,
    "knowledge": 4,
    "willpower": 2,
    "awareness": 3,
    "abilities": [
      "Aftershock",
      "Action: Ignite",
      "Surge: +1 Heart"
    ],
    "characteristics": [
      "2 Heroes: Speed: 4, Health: 8, Defense: 5",
      "3 Heroes: Speed: 4, Health: 11, Defense: 5",
      "4 Heroes: Speed: 4, Health: 13, Defense: 5 7"
    ],
    "expansion": "Base Game",
    "image": "lieutenants/lord-merick-farrow-bg-act1-front.png",
    "xws": "lordmerickfarrow"
  },
  {
    "name": "Lord Merick Farrow",
    "points": 13,
    "act": "I",
    "ability rules": [
      "Aftershock: When an adjacent hero attacks this monster, after the attack is resolved, the hero must test Willpower. If he fails, he suffers 1 Fatigue.",
      "Ignite: Lord Merick Farrow suffers 1 Heart to perform an attack that targets all adjacent figures. Each figure rolls defense dice separately. Merick may not perform this action if suffering the Hearts would defeat him."
    ],
    "expansion": "Base Game",
    "image": "lieutenants/lord-merick-farrow-bg-act1-back.png",
    "xws": "lordmerickfarrow"
  },
  {
    "name": "Lord Merick Farrow",
    "points": 14,
    "act": "II",
    "attack": "Range: 1 2 3",
    "might": 2,
    "knowledge": 4,
    "willpower": 2,
    "awareness": 3,
    "abilities": [
      "Aftershock",
      "Action: Ignite",
      "Surge: Wither",
      "Surge: +1 Heart"
    ],
    "characteristics": [
      "2 Heroes: Speed: 4, Health: 10, Defense: 5 7",
      "3 Heroes: Speed: 4, Health: 12, Defense: 5 5",
      "4 Heroes: Speed: 4, Health: 15, Defense: 6 5"
    ],
    "expansion": "Base Game",
    "image": "lieutenants/lord-merick-farrow-bg-act2-front.png",
    "xws": "lordmerickfarrow"
  },
  {
    "name": "Lord Merick Farrow",
    "points": 15,
    "act": "II",
    "ability rules": [
      "Aftershock: When an adjacent hero attacks this monster, after the attack is resolved, the hero must test Willpower. If he fails, he suffers 1 Fatigue.",
      "Ignite: Lord Merick Farrow suffers 1 Heart to perform an attack that targets all adjacent figures. Each figure rolls defense dice separately. Merick may not perform this action if suffering the Hearts would defeat him.",
      "Wither: The target suffers 1 Fatigue."
    ],
    "expansion": "Base Game",
    "image": "lieutenants/lord-merick-farrow-bg-act2-back.png",
    "xws": "lordmerickfarrow"
  },
  {
    "name": "Sir Alric Farrow",
    "points": 16,
    "act": "I",
    "attack": "Melee: 1 2",
    "might": 5,
    "knowledge": 1,
    "willpower": 2,
    "awareness": 4,
    "abilities": [
      "Unmovable",
      "Action: Overpower",
      "Surge: +1 Heart"
    ],
    "characteristics": [
      "2 Heroes: Speed: 3, Health: 10, Defense: 5 7",
      "3 Heroes: Speed: 3, Health: 14, Defense: 5 7",
      "4 Heroes: Speed: 3, Health: 16, Defense: 5 5"
    ],
    "expansion": "Base Game",
    "image": "lieutenants/sir-alric-farrow-bg-act1-front.png",
    "xws": "siralricfarrow"
  },
  {
    "name": "Sir Alric Farrow",
    "points": 17,
    "act": "I",
    "ability rules": [
      "Unmovable: This monster may choose to ignore any game effect that would force it to move.",
      "Overpower: Sir Alric Farrow performs a move action. Each time he moves into a space adjacent to a hero, Alric may test Might. If he passes, he may trade spaces with that hero and the hero suffers 1 Fatigue."
    ],
    "expansion": "Base Game",
    "image": "lieutenants/sir-alric-farrow-bg-act1-back.png",
    "xws": "siralricfarrow"
  },
  {
    "name": "Sir Alric Farrow",
    "points": 18,
    "act": "II",
    "attack": "Melee: 1 2 2",
    "might": 5,
    "knowledge": 1,
    "willpower": 2,
    "awareness": 4,
    "abilities": [
      "Regeneration 1",
      "Unmovable",
      "Action: Overpower",
      "Surge: +2 Hearts"
    ],
    "characteristics": [
      "2 Heroes: Speed: 3, Health: 12, Defense: 5 5",
      "3 Heroes: Speed: 3, Health: 15, Defense: 6 5",
      "4 Heroes: Speed: 3, Health: 18, Defense: 6 5 7"
    ],
    "expansion": "Base Game",
    "image": "lieutenants/sir-alric-farrow-bg-act2-front.png",
    "xws": "siralricfarrow"
  },
  {
    "name": "Sir Alric Farrow",
    "points": 19,
    "act": "II",
    "ability rules": [
      "Regeneration 1: At the beginning of the overlord player's turn, this monster recovers 1 Heart.",
      "Unmovable: This monster may choose to ignore any game effect that would force it to move.",
      "Overpower: Sir Alric Farrow performs a move action. Each time he moves into a space adjacent to a hero, Alric may test Might. If he passes, he may trade spaces with that hero and the hero suffers 1 Fatigue."
    ],
    "expansion": "Base Game",
    "image": "lieutenants/sir-alric-farrow-bg-act2-back.png",
    "xws": "siralricfarrow"
  },
  {
    "name": "Splig",
    "points": 20,
    "act": "I",
    "attack": "Melee: 1 2",
    "might": 4,
    "knowledge": 3,
    "willpower": 3,
    "awareness": 2,
    "abilities": [
      "Not Me!",
      "Surge: Knockback",
      "Surge: +1 Heart"
    ],
    "characteristics": [
      "2 Heroes: Speed: 3, Health: 7, Defense: 7",
      "3 Heroes: Speed: 3, Health: 9, Defense: 5",
      "4 Heroes: Speed: 3, Health: 13, Defense: 5"
    ],
    "expansion": "Base Game",
    "image": "lieutenants/splig-bg-act1-front.png",
    "xws": "splig"
  },
  {
    "name": "Splig",
    "points": 21,
    "act": "I",
    "ability rules": [
      "Not Me!: Each time Splig is attacked, before dice are rolled, test his Awareness. If he passes, a monster adjacent to him becomes the target of the attack. Range and line of sight are still measu2 to Splig's space.",
      "Knockback: Remove the target from the map, then place him on any empty space within 3 spaces of his original space. He counts as entering that space."
    ],
    "expansion": "Base Game",
    "image": "lieutenants/splig-bg-act1-back.png",
    "xws": "splig"
  },
  {
    "name": "Splig",
    "points": 22,
    "act": "II",
    "attack": "Melee: 1 2 3",
    "might": 4,
    "knowledge": 3,
    "willpower": 3,
    "awareness": 2,
    "abilities": [
      "Not Me!",
      "Action: Promotion",
      "Surge: Knockback",
      "Surge: +2 Hearts"
    ],
    "characteristics": [
      "2 Heroes: Speed: 3, Health: 10, Defense: 5",
      "3 Heroes: Speed: 3, Health: 12, Defense: 5 7",
      "4 Heroes: Speed: 3, Health: 16, Defense: 5 7"
    ],
    "expansion": "Base Game",
    "image": "lieutenants/splig-bg-act2-front.png",
    "xws": "splig"
  },
  {
    "name": "Splig",
    "points": 23,
    "act": "II",
    "ability rules": [
      "Not Me!: Each time Splig is attacked, before dice are rolled, test his Awareness. If he passes, a monster adjacent to him becomes the target of the attack. Range and line of sight are still measu2 to Splig's space.",
      "Promotion: Splig tests Willpower. If he passes, you may replace an adjacent minion monster with a master monster of that type. This may not exceed that monster group limit.",
      "Knockback: Remove the target from the map, then place him on any empty space within 3 spaces of his original space. He counts as entering that space."
    ],
    "expansion": "Base Game",
    "image": "lieutenants/splig-bg-act2-back.png",
    "xws": "splig"
  },
  {
    "name": "Valyndra",
    "points": 24,
    "act": "I",
    "attack": "Melee: 1 2",
    "might": 5,
    "knowledge": 2,
    "willpower": 1,
    "awareness": 3,
    "abilities": [
      "Hoarder",
      "Surge: Fire Breath",
      "Surge: Burn"
    ],
    "characteristics": [
      "2 Heroes: Speed: 3, Health: 12, Defense: 5 7",
      "3 Heroes: Speed: 3, Health: 14, Defense: 5 7",
      "4 Heroes: Speed: 3, Health: 17, Defense: 6 7"
    ],
    "expansion": "Lair Of The Wyrm",
    "image": "lieutenants/valyndra-lw-act1-front.png",
    "xws": "valyndra"
  },
  {
    "name": "Valyndra",
    "points": 25,
    "act": "I",
    "ability rules": [
      "Hoarder: When a hero performs a search action, Valyndra may test Awareness. If she passes, she may immediately move up to 2 spaces. Then the hero's turn resumes.",
      "Fire Breath: Starting with the target space, trace a path of 4 spaces in any direction. All figures on this path are affected by this attack. Each figure rolls defense dice separately.",
      "Burn: If this attack deals at least 1 Heart (after the defense roll), the target is Burning."
    ],
    "expansion": "Lair Of The Wyrm",
    "image": "lieutenants/valyndra-lw-act1-back.png",
    "xws": "valyndra"
  },
  {
    "name": "Valyndra",
    "points": 26,
    "act": "II",
    "attack": "Melee: 1 2 3",
    "might": 5,
    "knowledge": 2,
    "willpower": 1,
    "awareness": 3,
    "abilities": [
      "Hoarder",
      "Surge: Fire Breath",
      "Surge: Burn",
      "Surge: +2 Hearts"
    ],
    "characteristics": [
      "2 Heroes: Speed: 3, Health: 15, Defense: 5 5",
      "3 Heroes: Speed: 3, Health: 17, Defense: 5 5 7",
      "4 Heroes: Speed: 3, Health: 22, Defense: 6 5 7"
    ],
    "expansion": "Lair Of The Wyrm",
    "image": "lieutenants/valyndra-lw-act2-front.png",
    "xws": "valyndra"
  },
  {
    "name": "Valyndra",
    "points": 27,
    "act": "II",
    "ability rules": [
      "Hoarder: When a hero performs a search action, Valyndra may test Awareness. If she passes, she may immediately move up to 2 spaces. Then the hero's turn resumes.",
      "Fire Breath: Starting with the target space, trace a path of 4 spaces in any direction. All figures on this path are affected by this attack. Each figure rolls defense dice separately.",
      "Burn: If this attack deals at least 1 Heart (after the defense roll), the target is Burning."
    ],
    "expansion": "Lair Of The Wyrm",
    "image": "lieutenants/valyndra-lw-act2-back.png",
    "xws": "valyndra"
  },
  {
    "name": "Ariad",
    "points": 28,
    "act": "I",
    "attack": "Range: 1 4",
    "might": 2,
    "knowledge": 4,
    "willpower": 2,
    "awareness": 4,
    "abilities": [
      "Corrupted",
      "Action: Ancient Curse",
      "Surge Surge: +3 Hearts"
    ],
    "characteristics": [
      "2 Heroes: Speed: 4, Health: 12, Defense: 5",
      "3 Heroes: Speed: 4, Health: 14, Defense: 5 7",
      "4 Heroes: Speed: 4, Health: 16, Defense: 6 7"
    ],
    "expansion": "Labyrinth Of Ruin",
    "image": "lieutenants/ariad-lr-act1-front.png",
    "xws": "ariad"
  },
  {
    "name": "Ariad",
    "points": 29,
    "act": "I",
    "ability rules": [
      "Corrupted: Each time Ariad performs an attack targeting a Cursed hero, add 1 additional 3 power die to her attack pool for each Cursed hero targeted.",
      "Ancient Curse: Each hero within 3 spaces of Ariad must test Willpower. Each hero that fails is Cursed."
    ],
    "expansion": "Labyrinth Of Ruin",
    "image": "lieutenants/ariad-lr-act1-back.png",
    "xws": "ariad"
  },
  {
    "name": "Ariad",
    "points": 30,
    "act": "II",
    "attack": "Range: 1 4 4",
    "might": 2,
    "knowledge": 4,
    "willpower": 2,
    "awareness": 4,
    "abilities": [
      "Corrupted",
      "Action: Ancient Curse",
      "Action: Cursed Blast",
      "Surge Surge: +3 Hearts"
    ],
    "characteristics": [
      "2 Heroes: Speed: 4, Health: 20, Defense: 6 5",
      "3 Heroes: Speed: 4, Health: 24, Defense: 6 6",
      "4 Heroes: Speed: 4, Health: 28, Defense: 6 5 5"
    ],
    "expansion": "Labyrinth Of Ruin",
    "image": "lieutenants/ariad-lr-act2-front.png",
    "xws": "ariad"
  },
  {
    "name": "Ariad",
    "points": 31,
    "act": "II",
    "ability rules": [
      "Corrupted: Each time Ariad performs an attack targeting a Cursed hero, add 1 additional 3 power die to her attack pool for each Cursed hero targeted.",
      "Ancient Curse: Each hero within 3 spaces of Ariad must test Willpower. Each hero that fails is Cursed.",
      "Cursed Blast: Perform an attack targeting each Cursed hero in Ariad's line of sight. If insufficient range is rolled for any one target, the entire attack is conside2 a miss."
    ],
    "expansion": "Labyrinth Of Ruin",
    "image": "lieutenants/ariad-lr-act2-back.png",
    "xws": "ariad"
  },
  {
    "name": "Queen Ariad",
    "points": 32,
    "act": "II",
    "attack": "Melee: 1 2 4",
    "might": 3,
    "knowledge": 3,
    "willpower": 3,
    "awareness": 3,
    "abilities": [
      "Corrupted",
      "Action: Ancient Curse",
      "Action: Pincer Attack",
      "Surge Surge: +3 Hearts"
    ],
    "characteristics": [
      "2 Heroes: Speed: 3, Health: 20, Defense: 6 5",
      "3 Heroes: Speed: 3, Health: 24, Defense: 6 6",
      "4 Heroes: Speed: 3, Health: 28, Defense: 6 5 5"
    ],
    "expansion": "Labyrinth Of Ruin",
    "image": "lieutenants/queen-ariad-lr-act2-front.png",
    "xws": "queenariad"
  },
  {
    "name": "Queen Ariad",
    "points": 33,
    "act": "II",
    "ability rules": [
      "Corrupted: Each time Ariad performs an attack targeting a Cursed hero, add 1 additional 3 power die to her attack pool for each Cursed hero targeted.",
      "Ancient Curse: Each hero within 3 spaces of Ariad must test Willpower. Each hero that fails is Cursed.",
      "Pincer Attack: Perform an attack targeting up to 2 heroes adjacent to this monster. 1 attack roll is made but each hero rolls defense dice separately. Each target that suffers at least 1 Heart from this attack (after the defense roll) is Immobilized."
    ],
    "expansion": "Labyrinth Of Ruin",
    "image": "lieutenants/queen-ariad-lr-act2-back.png",
    "xws": "queenariad"
  },
  {
    "name": "Raythen",
    "points": 34,
    "act": "II",
    "attack": "Range: 1 2 4",
    "might": 3,
    "knowledge": 3,
    "willpower": 3,
    "awareness": 3,
    "abilities": [
      "Opportunist",
      "Action: Pilfer",
      "Surge: Stun",
      "Surge Surge: +3 Hearts"
    ],
    "characteristics": [
      "2 Heroes: Speed: 5, Health: 8, Defense: 7",
      "3 Heroes: Speed: 5, Health: 10, Defense: 5",
      "4 Heroes: Speed: 5, Health: 12, Defense: 5"
    ],
    "expansion": "Labyrinth Of Ruin",
    "image": "lieutenants/raythen-lr-act2-front.png",
    "xws": "raythen"
  },
  {
    "name": "Raythen",
    "points": 35,
    "act": "II",
    "ability rules": [
      "Opportunist: Each of Raythen's attacks gains 1 Surge for each adjacent Immobilized hero and 2 Surges for each adjacent Stunned hero.",
      "Pilfer: If Raythen is adjacent to a search token, the overlord may look at the top card of the Search deck. Then he may place it at the top or the bottom of the deck.",
      "Stun: If this attack deals at least 1 Heart (after the defense roll), the target is Stunned."
    ],
    "expansion": "Labyrinth Of Ruin",
    "image": "lieutenants/raythen-lr-act2-back.png",
    "xws": "raythen"
  },
  {
    "name": "Serena",
    "points": 36,
    "act": "II",
    "attack": "Melee: 1 2 4",
    "might": 3,
    "knowledge": 3,
    "willpower": 3,
    "awareness": 3,
    "abilities": [
      "Strong Spirit",
      "Action: Miasma",
      "Surge: +2 Hearts, Curse",
      "Surge: +2 Hearts, Disease"
    ],
    "characteristics": [
      "2 Heroes: Speed: 3, Health: 8, Defense: 7",
      "3 Heroes: Speed: 3, Health: 10, Defense: 5",
      "4 Heroes: Speed: 3, Health: 10, Defense: 5"
    ],
    "expansion": "Labyrinth Of Ruin",
    "image": "lieutenants/serena-lr-act2-front.png",
    "xws": "serena"
  },
  {
    "name": "Serena",
    "points": 37,
    "act": "II",
    "ability rules": [
      "Strong Spirit: Serena cannot be Cursed or Poisoned.",
      "Miasma: Each hero within 3 spaces of Serena must test Willpower. Each hero that fails suffers 1 Heart and 1 Fatigue.",
      "Curse: If this attack deals at least 1 Heart (after the defense roll), the target is Cursed.",
      "Disease: If this attack deals at least 1 Heart (after the defense roll), the target is Diseased."
    ],
    "expansion": "Labyrinth Of Ruin",
    "image": "lieutenants/serena-lr-act2-back.png",
    "xws": "serena"
  },
  {
    "name": "Bol'Goreth",
    "points": 38,
    "act": "I",
    "attack": "Melee: 1 2",
    "might": 6,
    "knowledge": 1,
    "willpower": 2,
    "awareness": 2,
    "abilities": [
      "Reach",
      "Action: Rampage",
      "Surge Surge: +1 Heart"
    ],
    "characteristics": [
      "2 Heroes: Speed: 3, Health: 12, Defense: 5",
      "3 Heroes: Speed: 3, Health: 14, Defense: 5 7",
      "4 Heroes: Speed: 3, Health: 18, Defense: 5 7"
    ],
    "expansion": "The Trollfens",
    "image": "lieutenants/bolgoreth-tf-act1-front.png",
    "xws": "bolgoreth"
  },
  {
    "name": "Bol'Goreth",
    "points": 39,
    "act": "I",
    "ability rules": [
      "Reach: Bol'Goreth may attack targets up to 2 spaces away.",
      "Rampage: Bol'Goreth performs a move action followed by an attack action. This attack affects each figure within 2 spaces of each space he ente2 during this movement. After this attack is resolved, Bol'Goreth is Stunned and Weakened."
    ],
    "expansion": "The Trollfens",
    "image": "lieutenants/bolgoreth-tf-act1-back.png",
    "xws": "bolgoreth"
  },
  {
    "name": "Bol'Goreth",
    "points": 40,
    "act": "II",
    "attack": "Melee: 1 2 2",
    "might": 6,
    "knowledge": 1,
    "willpower": 2,
    "awareness": 2,
    "abilities": [
      "Reach",
      "Resilient",
      "Action: Rampage",
      "Surge Surge: +2 Hearts"
    ],
    "characteristics": [
      "2 Heroes: Speed: 3, Health: 16, Defense: 5 7",
      "3 Heroes: Speed: 3, Health: 19, Defense: 5 5",
      "4 Heroes: Speed: 3, Health: 23, Defense: 5 5"
    ],
    "expansion": "The Trollfens",
    "image": "lieutenants/bolgoreth-tf-act2-front.png",
    "xws": "bolgoreth"
  },
  {
    "name": "Bol'Goreth",
    "points": 41,
    "act": "II",
    "ability rules": [
      "Reach: Bol'Goreth may attack targets up to 2 spaces away.",
      "Resilient: At the start of the overlord player's turn, discard 1 Condition token from Bol'Goreth.",
      "Rampage: Bol'Goreth performs a move action followed by an attack action. This attack affects each figure within 2 spaces of each space he ente2 during this movement. After this attack is resolved, Bol'Goreth is Stunned and Weakened."
    ],
    "expansion": "The Trollfens",
    "image": "lieutenants/bolgoreth-tf-act2-back.png",
    "xws": "bolgoreth"
  },
  {
    "name": "Mirklace",
    "points": 42,
    "act": "I",
    "attack": "Range: 1 2 4",
    "might": 3,
    "knowledge": 5,
    "willpower": 3,
    "awareness": 2,
    "abilities": [
      "Aura 1",
      "Action: Split Earth",
      "Surge Surge: Blast"
    ],
    "characteristics": [
      "2 Heroes: Speed: 3, Health: 13, Defense: 6",
      "3 Heroes: Speed: 3, Health: 16, Defense: 6",
      "4 Heroes: Speed: 3, Health: 19, Defense: 6 7"
    ],
    "expansion": "Shadow Of Nerekhall",
    "image": "lieutenants/mirklace-sn-act1-front.png",
    "xws": "mirklace"
  },
  {
    "name": "Mirklace",
    "points": 43,
    "act": "I",
    "ability rules": [
      "Aura X: Each time a hero enters a space adjacent to Mirklace, that hero suffers X Hearts.",
      "Split Earth: Starting with a space adjacent to Mirklace, trace a path of 4 spaces in any direction. Each figure on this path suffers 1 Heart and moves to an empty adjacent space of your choice. Limit once per round.",
      "Blast: This attack affects all figures adjacent to the target space."
    ],
    "expansion": "Shadow Of Nerekhall",
    "image": "lieutenants/mirklace-sn-act1-back.png",
    "xws": "mirklace"
  },
  {
    "name": "Mirklace",
    "points": 44,
    "act": "II",
    "attack": "Range: 1 2 3 4",
    "might": 3,
    "knowledge": 5,
    "willpower": 3,
    "awareness": 2,
    "abilities": [
      "Aura 2",
      "Action: Split Earth",
      "Sorcery 3",
      "Surge Surge: Blast"
    ],
    "characteristics": [
      "2 Heroes: Speed: 3, Health: 18, Defense: 6 7",
      "3 Heroes: Speed: 3, Health: 21, Defense: 6 7",
      "4 Heroes: Speed: 3, Health: 24, Defense: 6 5"
    ],
    "expansion": "Shadow Of Nerekhall",
    "image": "lieutenants/mirklace-sn-act2-front.png",
    "xws": "mirklace"
  },
  {
    "name": "Mirklace",
    "points": 45,
    "act": "II",
    "ability rules": [
      "Aura X: Each time a hero enters a space adjacent to Mirklace, that hero suffers X Hearts.",
      "Sorcery X: After making an attack roll, Mirklace may convert up to X range to Hearts, or up to X Hearts to range.",
      "Split Earth: Starting with a space adjacent to Mirklace, trace a path of 4 spaces in any direction. Each figure on this path suffers 1 Heart and moves to an empty adjacent space of your choice. Limit once per round.",
      "Blast: This attack affects all figures adjacent to the target space."
    ],
    "expansion": "Shadow Of Nerekhall",
    "image": "lieutenants/mirklace-sn-act2-back.png",
    "xws": "mirklace"
  },
  {
    "name": "Rylan Olliven",
    "points": 46,
    "act": "I",
    "attack": "Range: 1 4 4",
    "might": 1,
    "knowledge": 4,
    "willpower": 3,
    "awareness": 3,
    "abilities": [
      "Action: Influence",
      "Surge Surge: Subdue",
      "Surge: +2 Hearts"
    ],
    "characteristics": [
      "2 Heroes: Speed: 3, Health: 8, Defense: 5",
      "3 Heroes: Speed: 3, Health: 10, Defense: 5",
      "4 Heroes: Speed: 3, Health: 12, Defense: 6"
    ],
    "expansion": "Shadow Of Nerekhall",
    "image": "lieutenants/rylan-olliven-sn-act1-front.png",
    "xws": "rylanolliven"
  },
  {
    "name": "Rylan Olliven",
    "points": 47,
    "act": "I",
    "ability rules": [
      "Influence: Choose 1 figure adjacent to Rylan Olliven. That figure immediately performs one action of the controlling player's choice. Limit once per round.",
      "Subdue: If this attack deals at least 1 Heart, choose 1 condition. The target suffers the chosen condition."
    ],
    "expansion": "Shadow Of Nerekhall",
    "image": "lieutenants/rylan-olliven-sn-act1-back.png",
    "xws": "rylanolliven"
  },
  {
    "name": "Rylan Olliven",
    "points": 48,
    "act": "II",
    "attack": "Range: 1 2 4 4",
    "might": 1,
    "knowledge": 4,
    "willpower": 3,
    "awareness": 3,
    "abilities": [
      "Precise",
      "Action: Influence",
      "Surge Surge: Subdue",
      "Surge: +2 Hearts"
    ],
    "characteristics": [
      "2 Heroes: Speed: 3, Health: 12, Defense: 6",
      "3 Heroes: Speed: 3, Health: 14, Defense: 5 5",
      "4 Heroes: Speed: 3, Health: 17, Defense: 5 5"
    ],
    "expansion": "Shadow Of Nerekhall",
    "image": "lieutenants/rylan-olliven-sn-act2-front.png",
    "xws": "rylanolliven"
  },
  {
    "name": "Rylan Olliven",
    "points": 49,
    "act": "II",
    "ability rules": [
      "Precise: Adjacent figures do not block Rylan Olliven's line of sight.",
      "Influence: Choose 1 figure adjacent to Rylan Olliven. That figure immediately performs one action of the controlling player's choice. Limit once per round.",
      "Subdue: If this attack deals at least 1 Heart, choose 1 condition. The target suffers the chosen condition."
    ],
    "expansion": "Shadow Of Nerekhall",
    "image": "lieutenants/rylan-olliven-sn-act2-back.png",
    "xws": "rylanolliven"
  },
  {
    "name": "Tristayne Olliven",
    "points": 50,
    "act": "I",
    "attack": "Range: 1 2",
    "might": 2,
    "knowledge": 4,
    "willpower": 4,
    "awareness": 2,
    "abilities": [
      "Ravage",
      "Chaotic Energy",
      "Surge: +2 Hearts"
    ],
    "characteristics": [
      "2 Heroes: Speed: 4, Health: 10, Defense: 5",
      "3 Heroes: Speed: 4, Health: 12, Defense: 5",
      "4 Heroes: Speed: 4, Health: 14, Defense: 6"
    ],
    "expansion": "Shadow Of Nerekhall",
    "image": "lieutenants/tristayne-olliven-sn-act1-front.png",
    "xws": "tristayneolliven"
  },
  {
    "name": "Tristayne Olliven",
    "points": 51,
    "act": "I",
    "ability rules": [
      "Ravage: Both of Tristayne Olliven's actions on a turn may be attack actions.",
      "Chaotic Energy: Each time Tristayne Olliven performs an attack, before dice are rolled, he may suffer up to 3 Hearts. This attack deals additional Hearts equal to the Hearts suffe2. Tristayne cannot do this if suffering the Hearts would defeat him."
    ],
    "expansion": "Shadow Of Nerekhall",
    "image": "lieutenants/tristayne-olliven-sn-act1-back.png",
    "xws": "tristayneolliven"
  },
  {
    "name": "Tristayne Olliven",
    "points": 52,
    "act": "II",
    "attack": "Range: 1 2 3",
    "might": 2,
    "knowledge": 4,
    "willpower": 4,
    "awareness": 2,
    "abilities": [
      "Ravage",
      "Soul Siphon",
      "Chaotic Energy",
      "Surge: +3 Hearts"
    ],
    "characteristics": [
      "2 Heroes: Speed: 4, Health: 12, Defense: 6",
      "3 Heroes: Speed: 4, Health: 15, Defense: 5 5",
      "4 Heroes: Speed: 4, Health: 18, Defense: 6 5"
    ],
    "expansion": "Shadow Of Nerekhall",
    "image": "lieutenants/tristayne-olliven-sn-act2-front.png",
    "xws": "tristayneolliven"
  },
  {
    "name": "Tristayne Olliven",
    "points": 53,
    "act": "II",
    "ability rules": [
      "Ravage: Both of Tristayne Olliven's actions on a turn may be attack actions.",
      "Soul Siphon: Each time Tristayne Olliven suffers Hearts from any source other than an attack, you may choose an adjacent monster. That monster suffers that amount of Hearts instead.",
      "Chaotic Energy: Each time Tristayne Olliven performs an attack, before dice are rolled, he may suffer up to 3 Hearts. This attack deals additional Hearts equal to the Hearts suffe2. Tristayne cannot do this if suffering the Hearts would defeat him."
    ],
    "expansion": "Shadow Of Nerekhall",
    "image": "lieutenants/tristayne-olliven-sn-act2-back.png",
    "xws": "tristayneolliven"
  },
  {
    "name": "Verminous",
    "points": 54,
    "act": "I",
    "attack": "Melee: 1 3 4",
    "might": 2,
    "knowledge": 3,
    "willpower": 2,
    "awareness": 5,
    "abilities": [
      "Stealthy",
      "Surge: Scheme",
      "Surge Surge: +2 Hearts"
    ],
    "characteristics": [
      "2 Heroes: Speed: 3, Health: 8, Defense: 5",
      "3 Heroes: Speed: 3, Health: 10, Defense: 5 7",
      "4 Heroes: Speed: 3, Health: 13, Defense: 5 7"
    ],
    "expansion": "Shadow Of Nerekhall",
    "image": "lieutenants/verminous-sn-act1-front.png",
    "xws": "verminous"
  },
  {
    "name": "Verminous",
    "points": 55,
    "act": "I",
    "ability rules": [
      "Stealthy: Each attack that targets Verminous must roll 3 additional range beyond the normally requi2 amount or the attack is a miss.",
      "Scheme: Discard 1 Overlord card from your hand to draw 1 Overlord card."
    ],
    "expansion": "Shadow Of Nerekhall",
    "image": "lieutenants/verminous-sn-act1-back.png",
    "xws": "verminous"
  },
  {
    "name": "Verminous",
    "points": 56,
    "act": "II",
    "attack": "Melee: 1 3 4 4",
    "might": 2,
    "knowledge": 3,
    "willpower": 2,
    "awareness": 5,
    "abilities": [
      "Master Plan",
      "Stealthy",
      "Surge: Scheme",
      "Surge Surge: +3 Hearts"
    ],
    "characteristics": [
      "2 Heroes: Speed: 3, Health: 11, Defense: 5 7",
      "3 Heroes: Speed: 3, Health: 13, Defense: 6 7",
      "4 Heroes: Speed: 3, Health: 17, Defense: 6 5"
    ],
    "expansion": "Shadow Of Nerekhall",
    "image": "lieutenants/verminous-sn-act2-front.png",
    "xws": "verminous"
  },
  {
    "name": "Verminous",
    "points": 57,
    "act": "II",
    "ability rules": [
      "Master Plan: Each time you play an Overlord card, choose one hero within 3 spaces of Verminous. That hero suffers 1 Heart.",
      "Stealthy: Each attack that targets Verminous must roll 3 additional range beyond the normally requi2 amount or the attack is a miss.",
      "Scheme: Discard 1 Overlord card from your hand to draw 1 Overlord card."
    ],
    "expansion": "Shadow Of Nerekhall",
    "image": "lieutenants/verminous-sn-act2-back.png",
    "xws": "verminous"
  },
  {
    "name": "Skarn",
    "points": 58,
    "act": "I",
    "attack": "Melee: 1 3",
    "might": 4,
    "knowledge": 3,
    "willpower": 4,
    "awareness": 1,
    "abilities": [
      "Flail",
      "Energy Drain 3",
      "Surge: Mend 3"
    ],
    "characteristics": [
      "2 Heroes: Speed: 3, Health: 10, Defense: 6",
      "3 Heroes: Speed: 3, Health: 13, Defense: 5 5",
      "4 Heroes: Speed: 3, Health: 18, Defense: 5 5"
    ],
    "expansion": "Manor Of Ravens",
    "image": "lieutenants/skarn-mr-act1-front.png",
    "xws": "skarn"
  },
  {
    "name": "Skarn",
    "points": 59,
    "act": "I",
    "ability rules": [
      "Flail: When attacking Skarn may target 2 separate heroes. It makes 1 attack roll, and each hero rolls defense dice separately.",
      "Energy Drain X: If a hero suffers Hearts as the result of an attack from Skarn, you may cause that hero to suffer up to X Hearts as Fatigue instead.",
      "Mend X: Skarn recovers X Hearts."
    ],
    "expansion": "Manor Of Ravens",
    "image": "lieutenants/skarn-mr-act1-back.png",
    "xws": "skarn"
  },
  {
    "name": "Skarn",
    "points": 60,
    "act": "II",
    "attack": "Melee: 1 2 3",
    "might": 4,
    "knowledge": 3,
    "willpower": 4,
    "awareness": 1,
    "abilities": [
      "Energy Drain 4",
      "Action: Thrash",
      "Surge: Mend 4",
      "Surge: +1 Heart"
    ],
    "characteristics": [
      "2 Heroes: Speed: 3, Health: 16, Defense: 5 5",
      "3 Heroes: Speed: 3, Health: 19, Defense: 6 5",
      "4 Heroes: Speed: 3, Health: 24, Defense: 6 5"
    ],
    "expansion": "Manor Of Ravens",
    "image": "lieutenants/skarn-mr-act2-front.png",
    "xws": "skarn"
  },
  {
    "name": "Skarn",
    "points": 61,
    "act": "II",
    "ability rules": [
      "Energy Drain X: If a hero suffers Hearts as the result of an attack from Skarn, you may cause that hero to suffer up to X Hearts as Fatigue instead.",
      "Thrash: Perform an attack. This attack affects each figure adjacent to Skarn. Each figure rolls defense dice separately.",
      "Mend X: Skarn recovers X Hearts."
    ],
    "expansion": "Manor Of Ravens",
    "image": "lieutenants/skarn-mr-act2-back.png",
    "xws": "skarn"
  },
  {
    "name": "Ardus Ix'Erebus",
    "points": 62,
    "act": "I",
    "attack": "Melee: 1 2",
    "might": 5,
    "knowledge": 2,
    "willpower": 1,
    "awareness": 4,
    "abilities": [
      "Flanking",
      "Fury",
      "Surge: Pierce 1"
    ],
    "characteristics": [
      "2 Heroes: Speed: 4, Health: 12, Defense: 6",
      "3 Heroes: Speed: 4, Health: 14, Defense: 6 7",
      "4 Heroes: Speed: 4, Health: 17, Defense: 6 7"
    ],
    "expansion": "Mists Of Bilehall",
    "image": "lieutenants/ardus-ixerebus-mb-act1-front.png",
    "xws": "ardusixerebus"
  },
  {
    "name": "Ardus Ix'Erebus",
    "points": 63,
    "act": "I",
    "ability rules": [
      "Flanking: Each time Ardus Ix'Erebus performs an attack, he may choose 1 monster with the Cursed monster trait adjacent to the target. During that attack, Ardus Ix'Erebus may use the Surge abilities of that monster.",
      "Fury: Each time Ardus Ix'Erebus performs an attack, after rolling dice, he may test Might. If he passes, add 1 Surge to the results. If he fails, that attack is a miss.",
      "Pierce X: This attack ignores X Shields on the defense dice."
    ],
    "expansion": "Mists Of Bilehall",
    "image": "lieutenants/ardus-ixerebus-mb-act1-back.png",
    "xws": "ardusixerebus"
  },
  {
    "name": "Ardus Ix'Erebus",
    "points": 64,
    "act": "II",
    "attack": "Melee: 1 2 2",
    "might": 5,
    "knowledge": 2,
    "willpower": 1,
    "awareness": 4,
    "abilities": [
      "Flanking",
      "Fury",
      "Action: Rally",
      "Surge: Pierce 1"
    ],
    "characteristics": [
      "2 Heroes: Speed: 4, Health: 13, Defense: 6 7",
      "3 Heroes: Speed: 4, Health: 16, Defense: 6 5",
      "4 Heroes: Speed: 4, Health: 19, Defense: 6 5"
    ],
    "expansion": "The Chains That Rust",
    "image": "lieutenants/ardus-ixerebus-cr-act2-front.png",
    "xws": "ardusixerebus"
  },
  {
    "name": "Ardus Ix'Erebus",
    "points": 65,
    "act": "II",
    "ability rules": [
      "Flanking: Each time Ardus Ix'Erebus performs an attack, he may choose 1 monster with the Cursed monster trait adjacent to the target. During that attack, Ardus Ix'Erebus may use the Surge abilities of that monster.",
      "Fury: Each time Ardus Ix'Erebus performs an attack, after rolling dice, he may test Might. If he passes, add 1 Surge to the results. If he fails, that attack is a miss.",
      "Rally: Each monster within 3 spaces of Ardus Ix'Erebus discards 1 condition.",
      "Pierce X: This attack ignores X Shields on the defense dice."
    ],
    "expansion": "The Chains That Rust",
    "image": "lieutenants/ardus-ixerebus-cr-act2-back.png",
    "xws": "ardusixerebus"
  },
  {
    "name": "Kyndrithul",
    "points": 66,
    "act": "I",
    "attack": "Range: 1 3",
    "might": 3,
    "knowledge": 4,
    "willpower": 4,
    "awareness": 1,
    "abilities": [
      "Bloodlines",
      "Sorcery 3",
      "Surge: Bone Splinter"
    ],
    "characteristics": [
      "2 Heroes: Speed: 4, Health: 11, Defense: 5",
      "3 Heroes: Speed: 4, Health: 13, Defense: 6",
      "4 Heroes: Speed: 4, Health: 16, Defense: 6"
    ],
    "expansion": "Mists Of Bilehall",
    "image": "lieutenants/kyndrithul-mb-act1-front.png",
    "xws": "kyndrithul"
  },
  {
    "name": "Kyndrithul",
    "points": 67,
    "act": "I",
    "ability rules": [
      "Bloodlines: Add 1 Shield to Kyndrithul's defense results for each knocked-out hero.",
      "Sorcery X: After making an attack roll, Kyndrithul may convert up to X range to Hearts, or up to X Hearts to range.",
      "Bone Splinter: Each figure adjacent to the target suffers 2 Hearts."
    ],
    "expansion": "Mists Of Bilehall",
    "image": "lieutenants/kyndrithul-mb-act1-back.png",
    "xws": "kyndrithul"
  },
  {
    "name": "Kyndrithul",
    "points": 68,
    "act": "II",
    "attack": "Range: 1 3 3",
    "might": 3,
    "knowledge": 4,
    "willpower": 4,
    "awareness": 1,
    "abilities": [
      "Bloodlines",
      "Enthrall",
      "Sorcery 4",
      "Surge: Bone Splinter"
    ],
    "characteristics": [
      "2 Heroes: Speed: 4, Health: 13, Defense: 6",
      "3 Heroes: Speed: 4, Health: 15, Defense: 6 7",
      "4 Heroes: Speed: 4, Health: 19, Defense: 6 7"
    ],
    "expansion": "The Chains That Rust",
    "image": "lieutenants/kyndrithul-cr-act2-front.png",
    "xws": "kyndrithul"
  },
  {
    "name": "Kyndrithul",
    "points": 69,
    "act": "II",
    "ability rules": [
      "Bloodlines: Add 1 Shield to Kyndrithul's defense results for each knocked-out hero.",
      "Enthrall: At the start of Kyndrithul's activation, you may choose any number of heroes in his line of sight to test Willpower. If none of the heroes pass, perform an attack with each chosen hero as if it were a monster.",
      "Sorcery X: After making an attack roll, Kyndrithul may convert up to X range to Hearts, or up to X Hearts to range.",
      "Bone Splinter: Each figure adjacent to the target suffers 2 Hearts."
    ],
    "expansion": "The Chains That Rust",
    "image": "lieutenants/kyndrithul-cr-act2-back.png",
    "xws": "kyndrithul"
  },
  {
    "name": "Zarihell",
    "points": 70,
    "act": "I",
    "attack": "Range: 1 2",
    "might": 2,
    "knowledge": 4,
    "willpower": 3,
    "awareness": 3,
    "abilities": [
      "Soul Mastery",
      "Tormentress",
      "Surge: +2 Hearts"
    ],
    "characteristics": [
      "2 Heroes: Speed: 5, Health: 9, Defense: 5",
      "3 Heroes: Speed: 5, Health: 12, Defense: 5",
      "4 Heroes: Speed: 5, Health: 15, Defense: 6"
    ],
    "expansion": "Mists Of Bilehall",
    "image": "lieutenants/zarihell-mb-act1-front.png",
    "xws": "zarihell"
  },
  {
    "name": "Zarihell",
    "points": 71,
    "act": "I",
    "ability rules": [
      "Soul Mastery: Each time a hero within 3 spaces of Zarihell would be defeated, she may test Knowledge. If she passes, before that hero is knocked-out, perform an attack with that hero as if it were a monster. Then, that hero is defeated.",
      "Tormentress: When a hero starts his turn within 3 spaces of Zarihell or enters a space within 3 spaces of Zarihell, that hero is Terrified."
    ],
    "expansion": "Mists Of Bilehall",
    "image": "lieutenants/zarihell-mb-act1-back.png",
    "xws": "zarihell"
  },
  {
    "name": "Zarihell",
    "points": 72,
    "act": "II",
    "attack": "Range: 1 2 3",
    "might": 2,
    "knowledge": 4,
    "willpower": 3,
    "awareness": 3,
    "abilities": [
      "Soul Mastery",
      "Tormentress",
      "Action: Exploit Fear",
      "Surge: +2 Hearts"
    ],
    "characteristics": [
      "2 Heroes: Speed: 5, Health: 11, Defense: 6",
      "3 Heroes: Speed: 5, Health: 14, Defense: 5 5",
      "4 Heroes: Speed: 5, Health: 17, Defense: 5 5"
    ],
    "expansion": "The Chains That Rust",
    "image": "lieutenants/zarihell-cr-act2-front.png",
    "xws": "zarihell"
  },
  {
    "name": "Zarihell",
    "points": 73,
    "act": "II",
    "ability rules": [
      "Soul Mastery: Each time a hero within 3 spaces of Zarihell would be defeated, she may test Knowledge. If she passes, before that hero is knocked-out, perform an attack with that hero as if it were a monster. Then, that hero is defeated.",
      "Tormentress: When a hero starts his turn within 3 spaces of Zarihell or enters a space within 3 spaces of Zarihell, that hero is Terrified.",
      "Exploit Fear: Each Terrified hero within 3 spaces of Zarihell tests Willpower. Each hero that fails is Stunned."
    ],
    "expansion": "The Chains That Rust",
    "image": "lieutenants/zarihell-cr-act2-back.png",
    "xws": "zarihell"
  }
]

const regex = /Health: (\d+),/;
const attackRegex = /Attack:\s*((\d+\s*)+)/g;
const defenseRegex = /Defense:\s*((\d+\s*)+)/g;
const meleeRegex = /Melee:\s*((\d+\s*)+)/g;
const rangeRegex = /Range:\s*((\d+\s*)+)/g;
const stats = {
  monsters: {
    act1: {},
    act2: {}
  },
  lieutenants: {
    act1: {},
    act2: {}
  }
}
monsters.forEach(monster => {
  if (monster.act === "I") {
    if (!stats.monsters.act1[monster.name]) {
      if (monster.characteristics && monster.characteristics[0]) {
        const minionData = monster.characteristics[0]
        const masterData = monster.characteristics[1]

        const minionHp = minionData.match(regex)[1]
        const masterHp = masterData.match(regex)[1]

        const minionAttackDice = [];
        let match;
        while ((match = attackRegex.exec(minionData)) !== null) {
          match[1].trim().split(/\s+/).forEach((num) => {
            minionAttackDice.push(parseInt(num));
          });
        }

        const masterAttackDice = [];
        while ((match = attackRegex.exec(masterData)) !== null) {
          match[1].trim().split(/\s+/).forEach((num) => {
            masterAttackDice.push(parseInt(num));
          });
        }

        const minionDefenseDice = [];
        while ((match = defenseRegex.exec(minionData)) !== null) {
          match[1].trim().split(/\s+/).forEach((num) => {
            minionDefenseDice.push(parseInt(num));
          });
        }

        const masterDefenseDice = [];
        while ((match = defenseRegex.exec(masterData)) !== null) {
          match[1].trim().split(/\s+/).forEach((num) => {
            masterDefenseDice.push(parseInt(num));
          });
        }

        stats.monsters.act1[monster.name] = {
          master: {
            hp: parseInt(masterHp),
            attackDice: masterAttackDice,
            defenseDice: masterDefenseDice
          },
          minion: {
            hp: parseInt(minionHp),
            attackDice: minionAttackDice,
            defenseDice: minionDefenseDice
          }
        }
      }
    } else {
      if (monster["group size"][2]) {
        const groupRegex = /(\d+),(\d+)/;
        const match = monster["group size"][2].match(groupRegex);

        stats.monsters.act1[monster.name].groupSize = {
          master: parseInt(match[1]),
          minion: parseInt(match[2])
        }
      }
    }
  }

  if (monster.act === "II") {
    if (!stats.monsters.act2[monster.name]) {
      if (monster.characteristics && monster.characteristics[0]) {
        const minionData = monster.characteristics[0]
        const masterData = monster.characteristics[1]

        const minionHp = minionData.match(regex)[1]
        const masterHp = masterData.match(regex)[1]

        const minionAttackDice = [];
        let match;
        while ((match = attackRegex.exec(minionData)) !== null) {
          match[1].trim().split(/\s+/).forEach((num) => {
            minionAttackDice.push(parseInt(num));
          });
        }

        const masterAttackDice = [];
        while ((match = attackRegex.exec(masterData)) !== null) {
          match[1].trim().split(/\s+/).forEach((num) => {
            masterAttackDice.push(parseInt(num));
          });
        }

        const minionDefenseDice = [];
        while ((match = defenseRegex.exec(minionData)) !== null) {
          match[1].trim().split(/\s+/).forEach((num) => {
            minionDefenseDice.push(parseInt(num));
          });
        }

        const masterDefenseDice = [];
        while ((match = defenseRegex.exec(masterData)) !== null) {
          match[1].trim().split(/\s+/).forEach((num) => {
            masterDefenseDice.push(parseInt(num));
          });
        }

        stats.monsters.act2[monster.name] = {
          master: {
            hp: parseInt(masterHp),
            attackDice: masterAttackDice,
            defenseDice: masterDefenseDice
          },
          minion: {
            hp: parseInt(minionHp),
            attackDice: minionAttackDice,
            defenseDice: minionDefenseDice
          }
        }
      }
    } else {
      if (monster["group size"][2]) {
        const groupRegex = /(\d+),(\d+)/;
        const match = monster["group size"][2].match(groupRegex);

        stats.monsters.act2[monster.name].groupSize = {
          master: parseInt(match[1]),
          minion: parseInt(match[2])
        }
      }
    }
  }
});

lieutenants.forEach(monster => {
  if (monster.act === "I" && !stats.lieutenants.act1[monster.name]) {
    if (monster.characteristics && monster.characteristics[0]) {
      const twoPlayerData = monster.characteristics[0]
      const threePlayerData = monster.characteristics[1]
      const fourPlayerData = monster.characteristics[2]

      const twoHp = twoPlayerData.match(regex)[1]
      const threeHp = threePlayerData.match(regex)[1]
      const fourHp = fourPlayerData.match(regex)[1]

      let match;
      const attackDice = [];
      while ((match = meleeRegex.exec(monster.attack)) !== null) {
        match[1].trim().split(/\s+/).forEach((num) => {
          attackDice.push(parseInt(num));
        });
      }

      if (attackDice.length === 0) {
        while ((match = rangeRegex.exec(monster.attack)) !== null) {
          match[1].trim().split(/\s+/).forEach((num) => {
            attackDice.push(parseInt(num));
          });
        }
      }

      const twoDefenseDice = [];
      while ((match = defenseRegex.exec(twoPlayerData)) !== null) {
        match[1].trim().split(/\s+/).forEach((num) => {
          twoDefenseDice.push(parseInt(num));
        });
      }

      const threeDefenseDice = [];
      while ((match = defenseRegex.exec(threePlayerData)) !== null) {
        match[1].trim().split(/\s+/).forEach((num) => {
          threeDefenseDice.push(parseInt(num));
        });
      }

      const fourDefenseDice = [];
      while ((match = defenseRegex.exec(fourPlayerData)) !== null) {
        match[1].trim().split(/\s+/).forEach((num) => {
          fourDefenseDice.push(parseInt(num));
        });
      }

      stats.lieutenants.act1[monster.name] = {
        two: {
          hp: parseInt(twoHp),
          attackDice: attackDice,
          defenseDice: twoDefenseDice
        },
        three: {
          hp: parseInt(threeHp),
          attackDice: attackDice,
          defenseDice: threeDefenseDice
        },
        four: {
          hp: parseInt(fourHp),
          attackDice: attackDice,
          defenseDice: fourDefenseDice
        }
      }
    }
  }

  if (monster.act === "II" && !stats.lieutenants.act2[monster.name]) {
    if (monster.characteristics && monster.characteristics[0]) {
      const twoPlayerData = monster.characteristics[0]
      const threePlayerData = monster.characteristics[1]
      const fourPlayerData = monster.characteristics[2]

      const twoHp = twoPlayerData.match(regex)[1]
      const threeHp = threePlayerData.match(regex)[1]
      const fourHp = fourPlayerData.match(regex)[1]

      let match;
      const attackDice = [];
      while ((match = meleeRegex.exec(monster.attack)) !== null) {
        match[1].trim().split(/\s+/).forEach((num) => {
          attackDice.push(parseInt(num));
        });
      }

      if (attackDice.length === 0) {
        while ((match = rangeRegex.exec(monster.attack)) !== null) {
          match[1].trim().split(/\s+/).forEach((num) => {
            attackDice.push(parseInt(num));
          });
        }
      }

      const twoDefenseDice = [];
      while ((match = defenseRegex.exec(twoPlayerData)) !== null) {
        match[1].trim().split(/\s+/).forEach((num) => {
          twoDefenseDice.push(parseInt(num));
        });
      }

      const threeDefenseDice = [];
      while ((match = defenseRegex.exec(threePlayerData)) !== null) {
        match[1].trim().split(/\s+/).forEach((num) => {
          threeDefenseDice.push(parseInt(num));
        });
      }

      const fourDefenseDice = [];
      while ((match = defenseRegex.exec(fourPlayerData)) !== null) {
        match[1].trim().split(/\s+/).forEach((num) => {
          fourDefenseDice.push(parseInt(num));
        });
      }

      stats.lieutenants.act2[monster.name] = {
        two: {
          hp: parseInt(twoHp),
          attackDice: attackDice,
          defenseDice: twoDefenseDice
        },
        three: {
          hp: parseInt(threeHp),
          attackDice: attackDice,
          defenseDice: threeDefenseDice
        },
        four: {
          hp: parseInt(fourHp),
          attackDice: attackDice,
          defenseDice: fourDefenseDice
        }
      }
    }
  }
});

console.log(JSON.stringify(stats))