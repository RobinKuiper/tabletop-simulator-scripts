familiars = {
  ["reanimate"] = { hp = 4, attack = true, defend = false, dice = { 1, 2} },
  ["summoned stone"] = { hp = 2, attack = false, defend = true, dice = { 7} },
  ["wolf"] = { hp = 3, attack = true, defend = false, dice = { 1, 2} },
}

// https://github.com/any2cards/d2e/blob/master/data/monsters.js
const items = [
  {
    "name": "Shadow Soul",
    "points": 6,
    "archetype": "Scout",
    "class": "Shadow Walker",
    "speed": "1",
    "health": "-",
    "defense": "-",
    "attack": "-",
    "dice": "-",
    "rules": "The Shadow SOul may occupy any space containing figures or terrain. Each time a monster in a space containing or adjacent to a Shadow Soul suffers Hearts from an attack, it suffers 1 additional Heart.",
    "expansion": "Shadow Of Nerekhall",
    "image": "class-familiars/shadow-soul-sn-scout-shadow-walker.png",
    "xws": "shadowsoul"
  },
  {
    "name": "Bandaged Servant",
    "points": 8,
    "archetype": "Healer",
    "class": "Hierophant",
    "expansion": "Sands Of The Past",
    "image": "class-familiars/bandaged-servant-sotp-healer-hierophant.png",
    "xws": "bandagedservant"
  },
]

const regex = /((\d+\s*)+)/g;
const i = {
  shop: {},
  relics: {}
}
items.forEach(item => {
  if (!i.shop[item.name.toLowerCase()]) {
    if (item.attack === "Range" || item.attack === "Melee" || item.equip === "Armor") {
      const dice = [];
      let match;
      while ((match = regex.exec(item.dice)) !== null) {
        match[1].trim().split(/\s+/).forEach((num) => {
          dice.push(parseInt(num));
        });
      }

      i.shop[item.name.toLowerCase()] = {
        attack: item.equip !== "Armor",
        defend: item.equip === "Armor",
        dice: dice,
      }
    }
  }
});

relics.forEach(item => {
  if (!i.relics[item.name.toLowerCase()]) {
    if (item.attack === "Range" || item.attack === "Melee" || item.equip === "Armor") {
      const dice = [];
      let match;
      while ((match = regex.exec(item.dice)) !== null) {
        match[1].trim().split(/\s+/).forEach((num) => {
          dice.push(parseInt(num));
        });
      }

      i.relics[item.name.toLowerCase()] = {
        attack: item.equip !== "Armor",
        defend: item.equip === "Armor",
        dice: dice,
      }
    }
  }
});

console.log(JSON.stringify(i))