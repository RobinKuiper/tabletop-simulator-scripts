creatures = {
    monsters = {
        act1 = {
            Barghest = {
                master = {hp = 6, attackDice = {1, 2}, defenseDice = {5}},
                minion = {hp = 4, attackDice = {1, 2}, defenseDice = {5}},
                groupSize = {master = 3, minion = 1}
            },
            ["Cave Spider"] = {
                master = {hp = 5, attackDice = {1, 3}, defenseDice = {5}},
                minion = {hp = 3, attackDice = {1, 3}, defenseDice = {5}},
                groupSize = {master = 4, minion = 1}
            },
            Elemental = {
                master = {hp = 6, attackDice = {1, 2}, defenseDice = {6}},
                minion = {hp = 4, attackDice = {1, 2}, defenseDice = {6}},
                groupSize = {master = 1, minion = 1}
            },
            Ettin = {
                master = {hp = 8, attackDice = {1, 2}, defenseDice = {5, 5}},
                minion = {hp = 5, attackDice = {1, 2}, defenseDice = {5, 5}},
                groupSize = {master = 1, minion = 1}
            },
            ["Flesh Moulder"] = {
                master = {hp = 5, attackDice = {1, 3}, defenseDice = {5}},
                minion = {hp = 4, attackDice = {1, 3}, defenseDice = {5}},
                groupSize = {master = 3, minion = 1}
            },
            ["Goblin Archer"] = {
                master = {hp = 4, attackDice = {1, 3}, defenseDice = {5}},
                minion = {hp = 2, attackDice = {1, 3}, defenseDice = {5}},
                groupSize = {master = 4, minion = 1}
            },
            Merriod = {
                master = {hp = 7, attackDice = {1, 2}, defenseDice = {6}},
                minion = {hp = 5, attackDice = {1, 2}, defenseDice = {6}},
                groupSize = {master = 1, minion = 1}
            },
            ["Shadow Dragon"] = {
                master = {hp = 9, attackDice = {1, 2}, defenseDice = {5, 5}},
                minion = {hp = 6, attackDice = {1, 2}, defenseDice = {5, 5}},
                groupSize = {master = 1, minion = 1}
            },
            Zombie = {
                master = {hp = 6, attackDice = {1, 3}, defenseDice = {7}},
                minion = {hp = 3, attackDice = {1, 3}, defenseDice = {7}},
                groupSize = {master = 4, minion = 1}
            },
            ["CK-Bane Spider"] = {
                master = {hp = 7, attackDice = {1, 2}, defenseDice = {5}},
                minion = {hp = 4, attackDice = {1, 2}, defenseDice = {5}},
                groupSize = {master = 2, minion = 1}
            },
            ["CK-Beastman"] = {
                master = {hp = 5, attackDice = {1, 2}, defenseDice = {5}},
                minion = {hp = 2, attackDice = {1, 2}, defenseDice = {5}},
                groupSize = {master = 3, minion = 1}
            },
            ["CK-Blood Ape"] = {
                master = {hp = 7, attackDice = {1, 2}, defenseDice = {5}},
                minion = {hp = 5, attackDice = {1, 2}, defenseDice = {5}},
                groupSize = {master = 2, minion = 1}
            },
            ["CK-Chaos Beast"] = {
                master = {hp = 6, attackDice = {}, defenseDice = {5}},
                minion = {hp = 5, attackDice = {}, defenseDice = {5}},
                groupSize = {master = 2, minion = 1}
            },
            ["CK-Crypt Dragon"] = {
                master = {hp = 7, attackDice = {1, 3}, defenseDice = {5, 5}},
                minion = {hp = 5, attackDice = {1, 3}, defenseDice = {5, 5}},
                groupSize = {master = 1, minion = 1}
            },
            ["CK-Dark Priest"] = {
                master = {hp = 5, attackDice = {1, 3}, defenseDice = {7}},
                minion = {hp = 2, attackDice = {1, 3}, defenseDice = {7}},
                groupSize = {master = 3, minion = 1}
            },
            ["CK-Deep Elf"] = {
                master = {hp = 9, attackDice = {1, 3}, defenseDice = {7}},
                minion = {hp = 7, attackDice = {1, 3}, defenseDice = {7}},
                groupSize = {master = 1, minion = 1}
            },
            ["CK-Demon Lord"] = {
                master = {hp = 9, attackDice = {1, 3}, defenseDice = {5, 5}},
                minion = {hp = 6, attackDice = {1, 3}, defenseDice = {5, 5}},
                groupSize = {master = 1, minion = 1}
            },
            ["CK-Ferrox"] = {
                master = {hp = 5, attackDice = {1, 2}, defenseDice = {5}},
                minion = {hp = 4, attackDice = {1, 2}, defenseDice = {5}},
                groupSize = {master = 3, minion = 1}
            },
            ["CK-Giant"] = {
                master = {hp = 12, attackDice = {1, 2}, defenseDice = {6}},
                minion = {hp = 10, attackDice = {1, 2}, defenseDice = {6}},
                groupSize = {master = 1, minion = 1}
            },
            ["CK-Golem"] = {
                master = {hp = 10, attackDice = {1, 2}, defenseDice = {6}},
                minion = {hp = 8, attackDice = {1, 2}, defenseDice = {6}},
                groupSize = {master = 1, minion = 1}
            },
            ["CK-Hellhound"] = {
                master = {hp = 6, attackDice = {1, 2}, defenseDice = {5}},
                minion = {hp = 3, attackDice = {1, 2}, defenseDice = {5}},
                groupSize = {master = 3, minion = 1}
            },
            ["CK-Ice Wyrm"] = {
                master = {hp = 9, attackDice = {1, 2}, defenseDice = {5, 5}},
                minion = {hp = 7, attackDice = {1, 2}, defenseDice = {5, 5}},
                groupSize = {master = 1, minion = 1}
            },
            ["CK-Kobold"] = {
                master = {hp = 4, attackDice = {1}, defenseDice = {7}},
                minion = {hp = 2, attackDice = {1}, defenseDice = {7}},
                groupSize = {master = 6, minion = 3}
            },
            ["CK-Lava Beetle"] = {
                master = {hp = 5, attackDice = {1, 2}, defenseDice = {5}},
                minion = {hp = 3, attackDice = {1, 2}, defenseDice = {5}},
                groupSize = {master = 3, minion = 1}
            },
            ["CK-Manticore"] = {
                master = {hp = 7, attackDice = {1, 3}, defenseDice = {5}},
                minion = {hp = 5, attackDice = {1, 3}, defenseDice = {5}},
                groupSize = {master = 1, minion = 1}
            },
            ["CK-Medusa"] = {
                master = {hp = 6, attackDice = {1, 3}, defenseDice = {5}},
                minion = {hp = 4, attackDice = {1, 3}, defenseDice = {5}},
                groupSize = {master = 2, minion = 1}
            },
            ["CK-Naga"] = {
                master = {hp = 5, attackDice = {1, 2}, defenseDice = {6}},
                minion = {hp = 4, attackDice = {1, 2}, defenseDice = {6}},
                groupSize = {master = 2, minion = 1}
            },
            ["CK-Ogre"] = {
                master = {hp = 8, attackDice = {1, 2}, defenseDice = {5}},
                minion = {hp = 6, attackDice = {1, 3}, defenseDice = {5}},
                groupSize = {master = 1, minion = 1}
            },
            ["CK-Razorwing"] = {
                master = {hp = 6, attackDice = {1, 3}, defenseDice = {7}},
                minion = {hp = 4, attackDice = {1, 3}, defenseDice = {7}},
                groupSize = {master = 3, minion = 1}
            },
            ["CK-Shade"] = {
                master = {hp = 4, attackDice = {1, 3}, defenseDice = {6}},
                minion = {hp = 2, attackDice = {1, 3}, defenseDice = {6}},
                groupSize = {master = 4, minion = 1}
            },
            ["CK-Skeleton Archer"] = {
                master = {hp = 5, attackDice = {1, 3}, defenseDice = {7}},
                minion = {hp = 2, attackDice = {1, 3}, defenseDice = {7}},
                groupSize = {master = 4, minion = 1}
            },
            ["CK-Sorcerer"] = {
                master = {hp = 5, attackDice = {1, 3}, defenseDice = {5}},
                minion = {hp = 3, attackDice = {1, 3}, defenseDice = {5}},
                groupSize = {master = 3, minion = 1}
            },
            ["CK-Troll"] = {
                master = {hp = 10, attackDice = {1, 2}, defenseDice = {5}},
                minion = {hp = 8, attackDice = {1, 2}, defenseDice = {5}},
                groupSize = {master = 1, minion = 1}
            },
            ["CK-Wendigo"] = {
                master = {hp = 7, attackDice = {1, 2}, defenseDice = {5}},
                minion = {hp = 4, attackDice = {1, 2}, defenseDice = {5}},
                groupSize = {master = 2, minion = 1}
            },
            ["Fire Imps"] = {
                master = {hp = 4, attackDice = {1, 3}, defenseDice = {5}},
                minion = {hp = 2, attackDice = {1, 3}, defenseDice = {5}},
                groupSize = {master = 3, minion = 2}
            },
            ["Hybrid Sentinel"] = {
                master = {hp = 8, attackDice = {1, 2}, defenseDice = {6}},
                minion = {hp = 5, attackDice = {1, 2}, defenseDice = {6}},
                groupSize = {master = 2, minion = 1}
            },
            Arachyura = {
                master = {hp = 7, attackDice = {1, 2, 4}, defenseDice = {5, 7}},
                minion = {hp = 5, attackDice = {1, 2, 4}, defenseDice = {5, 7}},
                groupSize = {master = 1, minion = 1}
            },
            ["Carrion Drake"] = {
                master = {hp = 8, attackDice = {1, 3}, defenseDice = {5}},
                minion = {hp = 6, attackDice = {1, 3}, defenseDice = {5}},
                groupSize = {master = 2, minion = 1}
            },
            ["Goblin Witcher"] = {
                master = {hp = 5, attackDice = {1, 3}, defenseDice = {5}},
                minion = {hp = 3, attackDice = {1, 3}, defenseDice = {5}},
                groupSize = {master = 3, minion = 1}
            },
            ["Volucrix Reaver"] = {
                master = {hp = 5, attackDice = {1, 3}, defenseDice = {5}},
                minion = {hp = 3, attackDice = {1, 3}, defenseDice = {5}},
                groupSize = {master = 3, minion = 1}
            },
            Harpy = {
                master = {hp = 5, attackDice = {1, 2}, defenseDice = {5}},
                minion = {hp = 3, attackDice = {1, 4}, defenseDice = {5}},
                groupSize = {master = 3, minion = 1}
            },
            ["Plague Worm"] = {
                master = {hp = 7, attackDice = {1, 2}, defenseDice = {5, 7}},
                minion = {hp = 5, attackDice = {1, 2}, defenseDice = {5, 7}},
                groupSize = {master = 2, minion = 1}
            },
            Changeling = {
                master = {hp = 6, attackDice = {1, 2}, defenseDice = {5, 7}},
                minion = {hp = 4, attackDice = {1, 2}, defenseDice = {5, 7}},
                groupSize = {master = 3, minion = 1}
            },
            Ironbound = {
                master = {hp = 10, attackDice = {1, 2}, defenseDice = {5, 5}},
                minion = {hp = 8, attackDice = {1, 2}, defenseDice = {5, 5}},
                groupSize = {master = 1, minion = 1}
            },
            ["Rat Swarm"] = {
                master = {hp = 5, attackDice = {4}, defenseDice = {7}},
                minion = {hp = 4, attackDice = {4}, defenseDice = {7}},
                groupSize = {master = 3, minion = 1}
            },
            ["Ynfernael Hulk"] = {
                master = {hp = 9, attackDice = {1, 2}, defenseDice = {5}},
                minion = {hp = 8, attackDice = {1, 2}, defenseDice = {5}},
                groupSize = {master = 1, minion = 1}
            },
            Bandit = {
                master = {hp = 5, attackDice = {1, 3}, defenseDice = {5}},
                minion = {hp = 4, attackDice = {1, 2}, defenseDice = {7}},
                groupSize = {master = 4, minion = 1}
            },
            Wraith = {
                master = {hp = 7, attackDice = {1, 3}, defenseDice = {5}},
                minion = {hp = 5, attackDice = {1, 3}, defenseDice = {5}},
                groupSize = {master = 2, minion = 1}
            },
            ["Bone Horror"] = {
                master = {hp = 7, attackDice = {1, 3}, defenseDice = {5}},
                minion = {hp = 5, attackDice = {1, 3}, defenseDice = {5}},
                groupSize = {master = 2, minion = 1}
            },
            Broodwalker = {
                master = {hp = 10, attackDice = {1, 3}, defenseDice = {7}},
                minion = {hp = 7, attackDice = {1, 3}, defenseDice = {7}},
                groupSize = {master = 2, minion = 1}
            },
            Reanimate = {
                master = {hp = 5, attackDice = {1, 2}, defenseDice = {7}},
                minion = {hp = 3, attackDice = {1}, defenseDice = {7}},
                groupSize = {master = 4, minion = 2}
            },
            ["Marrow Priest"] = {
                master = {hp = 9, attackDice = {1, 3}, defenseDice = {5, 7}},
                minion = {hp = 7, attackDice = {1, 3}, defenseDice = {5, 7}},
                groupSize = {master = 1, minion = 1}
            },
            ["Shambling Colossus"] = {
                master = {hp = 8, attackDice = {1, 2}, defenseDice = {6}},
                minion = {hp = 5, attackDice = {1, 2}, defenseDice = {6}},
                groupSize = {master = 2, minion = 1}
            },
            ["The Dispossessed"] = {
                master = {hp = 8, attackDice = {1, 3}, defenseDice = {5}},
                minion = {hp = 6, attackDice = {1, 3}, defenseDice = {5}},
                groupSize = {master = 2, minion = 1}
            },
            ["Bane Spider"] = {
                master = {hp = 7, attackDice = {1, 2}, defenseDice = {5}},
                minion = {hp = 4, attackDice = {1, 2}, defenseDice = {5}},
                groupSize = {master = 2, minion = 1}
            },
            Beastman = {
                master = {hp = 5, attackDice = {1, 2}, defenseDice = {5}},
                minion = {hp = 4, attackDice = {1, 3}, defenseDice = {5}},
                groupSize = {master = 3, minion = 1}
            },
            Razorwing = {
                master = {hp = 6, attackDice = {1, 3}, defenseDice = {7}},
                minion = {hp = 4, attackDice = {1, 3}, defenseDice = {7}},
                groupSize = {master = 3, minion = 1}
            },
            ["Chaos Beast"] = {
                master = {hp = 6, attackDice = {}, defenseDice = {5}},
                minion = {hp = 5, attackDice = {}, defenseDice = {5}},
                groupSize = {master = 2, minion = 1}
            },
            Giant = {
                master = {hp = 12, attackDice = {1, 2}, defenseDice = {6}},
                minion = {hp = 10, attackDice = {1, 2}, defenseDice = {6}},
                groupSize = {master = 1, minion = 1}
            },
            ["Lava Beetle"] = {
                master = {hp = 5, attackDice = {1, 2}, defenseDice = {5}},
                minion = {hp = 3, attackDice = {1, 2}, defenseDice = {5}},
                groupSize = {master = 3, minion = 1}
            },
            Golem = {
                master = {hp = 10, attackDice = {1, 2}, defenseDice = {6}},
                minion = {hp = 8, attackDice = {1, 2}, defenseDice = {6}},
                groupSize = {master = 1, minion = 1}
            },
            Medusa = {
                master = {hp = 6, attackDice = {1, 3}, defenseDice = {5}},
                minion = {hp = 4, attackDice = {1, 3}, defenseDice = {5}},
                groupSize = {master = 2, minion = 1}
            },
            Sorcerer = {
                master = {hp = 5, attackDice = {1, 3}, defenseDice = {5}},
                minion = {hp = 3, attackDice = {1, 3}, defenseDice = {5}},
                groupSize = {master = 3, minion = 1}
            },
            ["Crypt Dragon"] = {
                master = {hp = 7, attackDice = {1, 3}, defenseDice = {5, 5}},
                minion = {hp = 5, attackDice = {1, 3}, defenseDice = {5, 5}},
                groupSize = {master = 1, minion = 1}
            },
            ["Dark Priest"] = {
                master = {hp = 6, attackDice = {1, 3}, defenseDice = {7}},
                minion = {hp = 4, attackDice = {1, 3}, defenseDice = {7}},
                groupSize = {master = 3, minion = 1}
            },
            Wendigo = {
                master = {hp = 7, attackDice = {1, 2}, defenseDice = {5}},
                minion = {hp = 5, attackDice = {1, 2}, defenseDice = {5}},
                groupSize = {master = 2, minion = 1}
            },
            Manticore = {
                master = {hp = 7, attackDice = {1, 3}, defenseDice = {5}},
                minion = {hp = 5, attackDice = {1, 3}, defenseDice = {5}},
                groupSize = {master = 1, minion = 1}
            },
            Ogre = {
                master = {hp = 9, attackDice = {1, 2}, defenseDice = {5}},
                minion = {hp = 6, attackDice = {1, 3}, defenseDice = {5}},
                groupSize = {master = 1, minion = 1}
            },
            Troll = {
                master = {hp = 10, attackDice = {1, 2}, defenseDice = {5}},
                minion = {hp = 8, attackDice = {1, 2}, defenseDice = {5}},
                groupSize = {master = 1, minion = 1}
            },
            ["Deep Elf"] = {
                master = {hp = 9, attackDice = {1, 3}, defenseDice = {7}},
                minion = {hp = 7, attackDice = {1, 3}, defenseDice = {7}},
                groupSize = {master = 1, minion = 1}
            },
            Hellhound = {
                master = {hp = 6, attackDice = {1, 2}, defenseDice = {5}},
                minion = {hp = 4, attackDice = {1, 2}, defenseDice = {5}},
                groupSize = {master = 3, minion = 1}
            },
            Kobold = {
                master = {hp = 5, attackDice = {1, 3}, defenseDice = {7}},
                minion = {hp = 2, attackDice = {1}, defenseDice = {7}},
                groupSize = {master = 9, minion = 3}
            },
            ["Crow Hag"] = {
                master = {hp = 7, attackDice = {1, 3}, defenseDice = {6}},
                minion = {hp = 5, attackDice = {1, 3}, defenseDice = {5}},
                groupSize = {master = 2, minion = 1}
            },
            ["Demon Lord"] = {
                master = {hp = 9, attackDice = {1, 3}, defenseDice = {5, 5}},
                minion = {hp = 6, attackDice = {1, 3}, defenseDice = {5, 5}},
                groupSize = {master = 1, minion = 1}
            },
            ["Skeleton Archer"] = {
                master = {hp = 6, attackDice = {1, 3}, defenseDice = {7}},
                minion = {hp = 3, attackDice = {1, 3}, defenseDice = {7}},
                groupSize = {master = 4, minion = 1}
            },
            ["Blood Ape"] = {
                master = {hp = 7, attackDice = {1, 2}, defenseDice = {5}},
                minion = {hp = 5, attackDice = {1, 2}, defenseDice = {5}},
                groupSize = {master = 2, minion = 1}
            },
            Ferrox = {
                master = {hp = 5, attackDice = {1, 2}, defenseDice = {5}},
                minion = {hp = 4, attackDice = {1, 2}, defenseDice = {5}},
                groupSize = {master = 3, minion = 1}
            },
            Naga = {
                master = {hp = 6, attackDice = {1, 2}, defenseDice = {6}},
                minion = {hp = 5, attackDice = {1, 2}, defenseDice = {6}},
                groupSize = {master = 2, minion = 1}
            },
            ["Dark Minotaur"] = {
                master = {hp = 8, attackDice = {1}, defenseDice = {6}},
                minion = {hp = 8, attackDice = {1}, defenseDice = {7}},
                groupSize = {master = 1, minion = 1}
            },
            ["Ice Wyrm"] = {
                master = {hp = 9, attackDice = {1, 2}, defenseDice = {5, 5}},
                minion = {hp = 7, attackDice = {1, 2}, defenseDice = {5, 5}},
                groupSize = {master = 1, minion = 1}
            },
            Shade = {
                master = {hp = 5, attackDice = {1, 3}, defenseDice = {6}},
                minion = {hp = 2, attackDice = {1, 3}, defenseDice = {6}},
                groupSize = {master = 4, minion = 1}
            }
        },
        act2 = {
            Barghest = {
                master = {hp = 8, attackDice = {1, 2, 3}, defenseDice = {6}},
                minion = {hp = 6, attackDice = {1, 2}, defenseDice = {6}},
                groupSize = {master = 3, minion = 1}
            },
            ["Cave Spider"] = {
                master = {hp = 7, attackDice = {1, 3, 3}, defenseDice = {5}},
                minion = {hp = 5, attackDice = {1, 3, 3}, defenseDice = {5}},
                groupSize = {master = 4, minion = 1}
            },
            Elemental = {
                master = {hp = 10, attackDice = {1, 3, 2}, defenseDice = {6, 7}},
                minion = {hp = 8, attackDice = {1, 3, 2}, defenseDice = {6, 7}},
                groupSize = {master = 1, minion = 1}
            },
            Ettin = {
                master = {hp = 9, attackDice = {1, 2, 2}, defenseDice = {6, 5}},
                minion = {hp = 7, attackDice = {1, 2, 2}, defenseDice = {6, 5}},
                groupSize = {master = 1, minion = 1}
            },
            ["Flesh Moulder"] = {
                master = {hp = 7, attackDice = {1, 3, 3}, defenseDice = {5, 7}},
                minion = {hp = 5, attackDice = {1, 3}, defenseDice = {5, 7}},
                groupSize = {master = 3, minion = 1}
            },
            ["Goblin Archer"] = {
                master = {hp = 6, attackDice = {1, 3, 3}, defenseDice = {5}},
                minion = {hp = 4, attackDice = {1, 3}, defenseDice = {5}},
                groupSize = {master = 4, minion = 1}
            },
            Merriod = {
                master = {hp = 9, attackDice = {1, 2, 3}, defenseDice = {5, 5}},
                minion = {hp = 7, attackDice = {1, 2, 3}, defenseDice = {5, 5}},
                groupSize = {master = 1, minion = 1}
            },
            ["Shadow Dragon"] = {
                master = {hp = 10, attackDice = {1, 2, 2}, defenseDice = {6, 5}},
                minion = {hp = 8, attackDice = {1, 2, 2}, defenseDice = {6, 5}},
                groupSize = {master = 1, minion = 1}
            },
            Zombie = {
                master = {hp = 9, attackDice = {1, 3, 2}, defenseDice = {7}},
                minion = {hp = 5, attackDice = {1, 3}, defenseDice = {7}},
                groupSize = {master = 4, minion = 1}
            },
            ["CK-Bane Spider"] = {
                master = {hp = 9, attackDice = {1, 2}, defenseDice = {5, 7}},
                minion = {hp = 6, attackDice = {1, 2}, defenseDice = {5, 7}},
                groupSize = {master = 2, minion = 1}
            },
            ["CK-Beastman"] = {
                master = {hp = 6, attackDice = {1, 2, 3}, defenseDice = {5}},
                minion = {hp = 4, attackDice = {1, 2}, defenseDice = {5}},
                groupSize = {master = 3, minion = 1}
            },
            ["CK-Blood Ape"] = {
                master = {hp = 9, attackDice = {1, 2, 2}, defenseDice = {5}},
                minion = {hp = 7, attackDice = {1, 2, 3}, defenseDice = {5}},
                groupSize = {master = 2, minion = 1}
            },
            ["CK-Chaos Beast"] = {
                master = {hp = 10, attackDice = {}, defenseDice = {5}},
                minion = {hp = 7, attackDice = {}, defenseDice = {5}},
                groupSize = {master = 2, minion = 1}
            },
            ["CK-Crypt Dragon"] = {
                master = {hp = 10, attackDice = {1, 2, 3}, defenseDice = {6, 5}},
                minion = {hp = 6, attackDice = {1, 2, 3}, defenseDice = {6, 5}},
                groupSize = {master = 1, minion = 1}
            },
            ["CK-Dark Priest"] = {
                master = {hp = 8, attackDice = {1, 3, 3}, defenseDice = {7}},
                minion = {hp = 5, attackDice = {1, 3}, defenseDice = {7}},
                groupSize = {master = 3, minion = 1}
            },
            ["CK-Deep Elf"] = {
                master = {hp = 10, attackDice = {1, 3}, defenseDice = {5}},
                minion = {hp = 8, attackDice = {1, 3}, defenseDice = {5}},
                groupSize = {master = 1, minion = 1}
            },
            ["CK-Demon Lord"] = {
                master = {hp = 12, attackDice = {1, 2, 3}, defenseDice = {5, 5}},
                minion = {hp = 8, attackDice = {1, 3, 3}, defenseDice = {5, 5}},
                groupSize = {master = 1, minion = 1}
            },
            ["CK-Ferrox"] = {
                master = {hp = 8, attackDice = {1, 2, 3}, defenseDice = {5, 7}},
                minion = {hp = 5, attackDice = {1, 2, 3}, defenseDice = {5, 7}},
                groupSize = {master = 3, minion = 1}
            },
            ["CK-Giant"] = {
                master = {hp = 15, attackDice = {1, 2, 2}, defenseDice = {6}},
                minion = {hp = 12, attackDice = {1, 2, 3}, defenseDice = {6}},
                groupSize = {master = 1, minion = 1}
            },
            ["CK-Golem"] = {
                master = {hp = 12, attackDice = {1, 2, 2}, defenseDice = {6, 5}},
                minion = {hp = 10, attackDice = {1, 2, 2}, defenseDice = {6, 5}},
                groupSize = {master = 1, minion = 1}
            },
            ["CK-Hellhound"] = {
                master = {hp = 8, attackDice = {1, 2, 3}, defenseDice = {5}},
                minion = {hp = 5, attackDice = {1, 2}, defenseDice = {5}},
                groupSize = {master = 3, minion = 1}
            },
            ["CK-Ice Wyrm"] = {
                master = {hp = 14, attackDice = {1, 2, 2}, defenseDice = {5, 5}},
                minion = {hp = 11, attackDice = {1, 2, 2}, defenseDice = {5, 5}},
                groupSize = {master = 1, minion = 1}
            },
            ["CK-Kobold"] = {
                master = {hp = 6, attackDice = {1, 3}, defenseDice = {7}},
                minion = {hp = 4, attackDice = {1}, defenseDice = {7}},
                groupSize = {master = 6, minion = 3}
            },
            ["CK-Lava Beetle"] = {
                master = {hp = 7, attackDice = {1, 2, 3}, defenseDice = {5}},
                minion = {hp = 5, attackDice = {1, 2}, defenseDice = {5}},
                groupSize = {master = 3, minion = 1}
            },
            ["CK-Manticore"] = {
                master = {hp = 9, attackDice = {1, 3, 3}, defenseDice = {5}},
                minion = {hp = 7, attackDice = {1, 3, 3}, defenseDice = {5}},
                groupSize = {master = 1, minion = 1}
            },
            ["CK-Medusa"] = {
                master = {hp = 9, attackDice = {1, 3, 3}, defenseDice = {5, 7}},
                minion = {hp = 6, attackDice = {1, 3, 3}, defenseDice = {5, 7}},
                groupSize = {master = 2, minion = 1}
            },
            ["CK-Naga"] = {
                master = {hp = 7, attackDice = {1, 2, 3}, defenseDice = {6}},
                minion = {hp = 6, attackDice = {1, 2}, defenseDice = {6}},
                groupSize = {master = 2, minion = 1}
            },
            ["CK-Ogre"] = {
                master = {hp = 12, attackDice = {1, 2, 3}, defenseDice = {5}},
                minion = {hp = 9, attackDice = {1, 2}, defenseDice = {5}},
                groupSize = {master = 1, minion = 1}
            },
            ["CK-Razorwing"] = {
                master = {hp = 9, attackDice = {1, 3, 3}, defenseDice = {7}},
                minion = {hp = 7, attackDice = {1, 3}, defenseDice = {7}},
                groupSize = {master = 3, minion = 1}
            },
            ["CK-Shade"] = {
                master = {hp = 6, attackDice = {1, 2, 3}, defenseDice = {6}},
                minion = {hp = 4, attackDice = {1, 2}, defenseDice = {6}},
                groupSize = {master = 4, minion = 1}
            },
            ["CK-Skeleton Archer"] = {
                master = {hp = 8, attackDice = {1, 3}, defenseDice = {7}},
                minion = {hp = 4, attackDice = {1, 3}, defenseDice = {7}},
                groupSize = {master = 4, minion = 1}
            },
            ["CK-Sorcerer"] = {
                master = {hp = 8, attackDice = {1, 2}, defenseDice = {5}},
                minion = {hp = 5, attackDice = {1, 2}, defenseDice = {5}},
                groupSize = {master = 3, minion = 1}
            },
            ["CK-Troll"] = {
                master = {hp = 13, attackDice = {1, 2, 2}, defenseDice = {5}},
                minion = {hp = 10, attackDice = {1, 2, 2}, defenseDice = {5}},
                groupSize = {master = 1, minion = 1}
            },
            ["CK-Wendigo"] = {
                master = {hp = 10, attackDice = {1, 2, 3}, defenseDice = {5}},
                minion = {hp = 6, attackDice = {1, 2}, defenseDice = {5}},
                groupSize = {master = 2, minion = 1}
            },
            ["Fire Imps"] = {
                master = {hp = 6, attackDice = {1, 3, 3}, defenseDice = {5}},
                minion = {hp = 4, attackDice = {1, 3}, defenseDice = {5}},
                groupSize = {master = 3, minion = 2}
            },
            ["Hybrid Sentinel"] = {
                master = {hp = 9, attackDice = {1, 3, 2}, defenseDice = {6, 5}},
                minion = {hp = 6, attackDice = {1, 2}, defenseDice = {6, 7}},
                groupSize = {master = 2, minion = 1}
            },
            Arachyura = {
                master = {hp = 9, attackDice = {1, 2, 4}, defenseDice = {6}},
                minion = {hp = 7, attackDice = {1, 2, 4}, defenseDice = {6}},
                groupSize = {master = 1, minion = 1}
            },
            ["Carrion Drake"] = {
                master = {hp = 10, attackDice = {1, 3, 4}, defenseDice = {5, 5}},
                minion = {hp = 7, attackDice = {1, 3, 4}, defenseDice = {5, 7}},
                groupSize = {master = 2, minion = 1}
            },
            ["Goblin Witcher"] = {
                master = {hp = 8, attackDice = {1, 3, 4}, defenseDice = {5}},
                minion = {hp = 6, attackDice = {1, 3}, defenseDice = {5}},
                groupSize = {master = 3, minion = 1}
            },
            ["Volucrix Reaver"] = {
                master = {hp = 6, attackDice = {1, 2}, defenseDice = {5, 7}},
                minion = {hp = 4, attackDice = {1, 2}, defenseDice = {5, 7}},
                groupSize = {master = 3, minion = 1}
            },
            Harpy = {
                master = {hp = 6, attackDice = {1, 3, 2}, defenseDice = {5}},
                minion = {hp = 4, attackDice = {1, 3, 4}, defenseDice = {5}},
                groupSize = {master = 3, minion = 1}
            },
            ["Plague Worm"] = {
                master = {hp = 9, attackDice = {1, 2, 4}, defenseDice = {5, 5}},
                minion = {hp = 6, attackDice = {1, 2, 4}, defenseDice = {5, 5}},
                groupSize = {master = 2, minion = 1}
            },
            Changeling = {
                master = {hp = 8, attackDice = {1, 2, 3}, defenseDice = {6, 7}},
                minion = {hp = 6, attackDice = {1, 2}, defenseDice = {6, 7}},
                groupSize = {master = 3, minion = 1}
            },
            Ironbound = {
                master = {hp = 12, attackDice = {1, 2, 4}, defenseDice = {5, 6}},
                minion = {hp = 10, attackDice = {1, 2, 4}, defenseDice = {5, 6}},
                groupSize = {master = 1, minion = 1}
            },
            ["Rat Swarm"] = {
                master = {hp = 6, attackDice = {4}, defenseDice = {5}},
                minion = {hp = 5, attackDice = {4}, defenseDice = {5}},
                groupSize = {master = 3, minion = 1}
            },
            ["Ynfernael Hulk"] = {
                master = {hp = 10, attackDice = {1, 2, 3}, defenseDice = {6}},
                minion = {hp = 9, attackDice = {1, 2, 3}, defenseDice = {6}},
                groupSize = {master = 1, minion = 1}
            },
            Bandit = {
                master = {hp = 7, attackDice = {1, 3, 3}, defenseDice = {6}},
                minion = {hp = 6, attackDice = {1, 2}, defenseDice = {5}},
                groupSize = {master = 4, minion = 1}
            },
            Wraith = {
                master = {hp = 8, attackDice = {1, 2, 3}, defenseDice = {6}},
                minion = {hp = 6, attackDice = {1, 3}, defenseDice = {6}},
                groupSize = {master = 2, minion = 1}
            },
            ["Bone Horror"] = {
                master = {hp = 9, attackDice = {1, 2, 3}, defenseDice = {6}},
                minion = {hp = 6, attackDice = {1, 2}, defenseDice = {6}},
                groupSize = {master = 2, minion = 1}
            },
            Broodwalker = {
                master = {hp = 12, attackDice = {1, 2, 3}, defenseDice = {5}},
                minion = {hp = 8, attackDice = {1, 3, 3}, defenseDice = {5}},
                groupSize = {master = 2, minion = 1}
            },
            Reanimate = {
                master = {hp = 8, attackDice = {1, 3, 3}, defenseDice = {7}},
                minion = {hp = 5, attackDice = {3}, defenseDice = {7}},
                groupSize = {master = 4, minion = 2}
            },
            ["Marrow Priest"] = {
                master = {hp = 10, attackDice = {1, 3, 3}, defenseDice = {5, 5}},
                minion = {hp = 8, attackDice = {1, 3, 3}, defenseDice = {5, 5}},
                groupSize = {master = 1, minion = 1}
            },
            ["Shambling Colossus"] = {
                master = {hp = 9, attackDice = {1, 2, 3}, defenseDice = {6}},
                minion = {hp = 7, attackDice = {1, 2}, defenseDice = {6}},
                groupSize = {master = 2, minion = 1}
            },
            ["The Dispossessed"] = {
                master = {hp = 10, attackDice = {1, 3, 3}, defenseDice = {5, 7}},
                minion = {hp = 8, attackDice = {1, 3}, defenseDice = {5, 7}},
                groupSize = {master = 2, minion = 1}
            },
            ["Bane Spider"] = {
                master = {hp = 9, attackDice = {1, 2, 3}, defenseDice = {5, 7}},
                minion = {hp = 6, attackDice = {1, 2}, defenseDice = {5, 7}},
                groupSize = {master = 2, minion = 1}
            },
            Beastman = {
                master = {hp = 6, attackDice = {1, 2, 3}, defenseDice = {5}},
                minion = {hp = 5, attackDice = {1, 2}, defenseDice = {5}},
                groupSize = {master = 3, minion = 1}
            },
            Razorwing = {
                master = {hp = 9, attackDice = {1, 3, 3}, defenseDice = {7}},
                minion = {hp = 7, attackDice = {1, 3}, defenseDice = {7}},
                groupSize = {master = 3, minion = 1}
            },
            ["Chaos Beast"] = {
                master = {hp = 10, attackDice = {}, defenseDice = {5}},
                minion = {hp = 7, attackDice = {}, defenseDice = {5}},
                groupSize = {master = 2, minion = 1}
            },
            Giant = {
                master = {hp = 15, attackDice = {1, 2, 2}, defenseDice = {6}},
                minion = {hp = 12, attackDice = {1, 2, 3}, defenseDice = {6}},
                groupSize = {master = 1, minion = 1}
            },
            ["Lava Beetle"] = {
                master = {hp = 7, attackDice = {1, 2, 3}, defenseDice = {5}},
                minion = {hp = 5, attackDice = {1, 2}, defenseDice = {5}},
                groupSize = {master = 3, minion = 1}
            },
            Golem = {
                master = {hp = 12, attackDice = {1, 2, 2}, defenseDice = {6, 5}},
                minion = {hp = 10, attackDice = {1, 2, 2}, defenseDice = {6, 5}},
                groupSize = {master = 1, minion = 1}
            },
            Medusa = {
                master = {hp = 9, attackDice = {1, 3, 3}, defenseDice = {5, 7}},
                minion = {hp = 6, attackDice = {1, 3, 3}, defenseDice = {5, 7}},
                groupSize = {master = 2, minion = 1}
            },
            Sorcerer = {
                master = {hp = 8, attackDice = {1, 2}, defenseDice = {5}},
                minion = {hp = 5, attackDice = {1, 2}, defenseDice = {5}},
                groupSize = {master = 3, minion = 1}
            },
            ["Crypt Dragon"] = {
                master = {hp = 10, attackDice = {1, 2, 3}, defenseDice = {6, 5}},
                minion = {hp = 7, attackDice = {1, 2, 3}, defenseDice = {6, 5}},
                groupSize = {master = 1, minion = 1}
            },
            ["Dark Priest"] = {
                master = {hp = 9, attackDice = {1, 3, 3}, defenseDice = {7}},
                minion = {hp = 7, attackDice = {1, 3}, defenseDice = {7}},
                groupSize = {master = 3, minion = 1}
            },
            Wendigo = {
                master = {hp = 10, attackDice = {1, 2, 3}, defenseDice = {5}},
                minion = {hp = 7, attackDice = {1, 2}, defenseDice = {5}},
                groupSize = {master = 2, minion = 1}
            },
            Manticore = {
                master = {hp = 9, attackDice = {1, 3, 3}, defenseDice = {5}},
                minion = {hp = 7, attackDice = {1, 3, 3}, defenseDice = {5}},
                groupSize = {master = 1, minion = 1}
            },
            Ogre = {
                master = {hp = 12, attackDice = {1, 2, 3}, defenseDice = {5}},
                minion = {hp = 9, attackDice = {1, 3}, defenseDice = {5}},
                groupSize = {master = 1, minion = 1}
            },
            Troll = {
                master = {hp = 13, attackDice = {1, 2, 2}, defenseDice = {5}},
                minion = {hp = 10, attackDice = {1, 2, 2}, defenseDice = {5}},
                groupSize = {master = 1, minion = 1}
            },
            ["Deep Elf"] = {
                master = {hp = 10, attackDice = {1, 3}, defenseDice = {5}},
                minion = {hp = 8, attackDice = {1, 3}, defenseDice = {5}},
                groupSize = {master = 1, minion = 1}
            },
            Hellhound = {
                master = {hp = 8, attackDice = {1, 2, 3}, defenseDice = {6}},
                minion = {hp = 6, attackDice = {1, 2}, defenseDice = {6}},
                groupSize = {master = 3, minion = 1}
            },
            Kobold = {
                master = {hp = 7, attackDice = {1, 3}, defenseDice = {7}},
                minion = {hp = 4, attackDice = {1}, defenseDice = {7}},
                groupSize = {master = 9, minion = 3}
            },
            ["Crow Hag"] = {
                master = {hp = 9, attackDice = {1, 3}, defenseDice = {6, 5}},
                minion = {hp = 7, attackDice = {1, 3}, defenseDice = {6}},
                groupSize = {master = 2, minion = 1}
            },
            ["Demon Lord"] = {
                master = {hp = 12, attackDice = {1, 2, 3}, defenseDice = {5, 5}},
                minion = {hp = 8, attackDice = {1, 3, 3}, defenseDice = {5, 5}},
                groupSize = {master = 1, minion = 1}
            },
            ["Skeleton Archer"] = {
                master = {hp = 8, attackDice = {1, 3}, defenseDice = {7}},
                minion = {hp = 4, attackDice = {1, 3}, defenseDice = {7}},
                groupSize = {master = 4, minion = 1}
            },
            ["Blood Ape"] = {
                master = {hp = 9, attackDice = {1, 2, 2}, defenseDice = {5}},
                minion = {hp = 7, attackDice = {1, 2, 3}, defenseDice = {5}},
                groupSize = {master = 2, minion = 1}
            },
            Ferrox = {
                master = {hp = 8, attackDice = {1, 2, 3}, defenseDice = {5, 7}},
                minion = {hp = 5, attackDice = {1, 2, 3}, defenseDice = {5, 7}},
                groupSize = {master = 3, minion = 1}
            },
            Naga = {
                master = {hp = 8, attackDice = {1, 2, 3}, defenseDice = {6}},
                minion = {hp = 6, attackDice = {1, 2}, defenseDice = {6}},
                groupSize = {master = 2, minion = 1}
            },
            ["Dark Minotaur"] = {
                master = {hp = 10, attackDice = {1, 2}, defenseDice = {5, 6}},
                minion = {hp = 10, attackDice = {1, 2}, defenseDice = {5, 7}},
                groupSize = {master = 1, minion = 1}
            },
            ["Ice Wyrm"] = {
                master = {hp = 14, attackDice = {1, 2, 2}, defenseDice = {5, 5}},
                minion = {hp = 11, attackDice = {1, 2, 2}, defenseDice = {5, 5}},
                groupSize = {master = 1, minion = 1}
            },
            Shade = {
                master = {hp = 7, attackDice = {1, 2, 3}, defenseDice = {6}},
                minion = {hp = 4, attackDice = {1, 2}, defenseDice = {6}},
                groupSize = {master = 4, minion = 1}
            }
        }
    },
    lieutenants = {
        act1 = {
            ["Baron Zachareth"] = {
                two = {hp = 10, attackDice = {1, 2}, defenseDice = {6}},
                three = {hp = 13, attackDice = {1, 2}, defenseDice = {6}},
                four = {hp = 16, attackDice = {1, 2}, defenseDice = {6}}
            },
            Belthir = {
                two = {hp = 9, attackDice = {1, 2}, defenseDice = {5}},
                three = {hp = 11, attackDice = {1, 2}, defenseDice = {5, 7}},
                four = {hp = 13, attackDice = {1, 2}, defenseDice = {5, 7}}
            },
            ["Lady Eliza Farrow"] = {
                two = {hp = 7, attackDice = {1, 3}, defenseDice = {7}},
                three = {hp = 9, attackDice = {1, 3}, defenseDice = {5}},
                four = {hp = 12, attackDice = {1, 3}, defenseDice = {5}}
            },
            ["Lord Merick Farrow"] = {
                two = {hp = 8, attackDice = {1, 2}, defenseDice = {5}},
                three = {hp = 11, attackDice = {1, 2}, defenseDice = {5}},
                four = {hp = 13, attackDice = {1, 2}, defenseDice = {5, 7}}
            },
            ["Sir Alric Farrow"] = {
                two = {hp = 10, attackDice = {1, 2}, defenseDice = {5, 7}},
                three = {hp = 14, attackDice = {1, 2}, defenseDice = {5, 7}},
                four = {hp = 16, attackDice = {1, 2}, defenseDice = {5, 5}}
            },
            Splig = {
                two = {hp = 7, attackDice = {1, 2}, defenseDice = {7}},
                three = {hp = 9, attackDice = {1, 2}, defenseDice = {5}},
                four = {hp = 13, attackDice = {1, 2}, defenseDice = {5}}
            },
            Valyndra = {
                two = {hp = 12, attackDice = {1, 2}, defenseDice = {5, 7}},
                three = {hp = 14, attackDice = {1, 2}, defenseDice = {5, 7}},
                four = {hp = 17, attackDice = {1, 2}, defenseDice = {6, 7}}
            },
            Ariad = {
                two = {hp = 12, attackDice = {1, 4}, defenseDice = {5}},
                three = {hp = 14, attackDice = {1, 4}, defenseDice = {5, 7}},
                four = {hp = 16, attackDice = {1, 4}, defenseDice = {6, 7}}
            },
            ["Bol'Goreth"] = {
                two = {hp = 12, attackDice = {1, 2}, defenseDice = {5}},
                three = {hp = 14, attackDice = {1, 2}, defenseDice = {5, 7}},
                four = {hp = 18, attackDice = {1, 2}, defenseDice = {5, 7}}
            },
            Mirklace = {
                two = {hp = 13, attackDice = {1, 2, 4}, defenseDice = {6}},
                three = {hp = 16, attackDice = {1, 2, 4}, defenseDice = {6}},
                four = {hp = 19, attackDice = {1, 2, 4}, defenseDice = {6, 7}}
            },
            ["Rylan Olliven"] = {
                two = {hp = 8, attackDice = {1, 4, 4}, defenseDice = {5}},
                three = {hp = 10, attackDice = {1, 4, 4}, defenseDice = {5}},
                four = {hp = 12, attackDice = {1, 4, 4}, defenseDice = {6}}
            },
            ["Tristayne Olliven"] = {
                two = {hp = 10, attackDice = {1, 2}, defenseDice = {5}},
                three = {hp = 12, attackDice = {1, 2}, defenseDice = {5}},
                four = {hp = 14, attackDice = {1, 2}, defenseDice = {6}}
            },
            Verminous = {
                two = {hp = 8, attackDice = {1, 3, 4}, defenseDice = {5}},
                three = {hp = 10, attackDice = {1, 3, 4}, defenseDice = {5, 7}},
                four = {hp = 13, attackDice = {1, 3, 4}, defenseDice = {5, 7}}
            },
            Skarn = {
                two = {hp = 10, attackDice = {1, 3}, defenseDice = {6}},
                three = {hp = 13, attackDice = {1, 3}, defenseDice = {5, 5}},
                four = {hp = 18, attackDice = {1, 3}, defenseDice = {5, 5}}
            },
            ["Ardus Ix'Erebus"] = {
                two = {hp = 12, attackDice = {1, 2}, defenseDice = {6}},
                three = {hp = 14, attackDice = {1, 2}, defenseDice = {6, 7}},
                four = {hp = 17, attackDice = {1, 2}, defenseDice = {6, 7}}
            },
            Kyndrithul = {
                two = {hp = 11, attackDice = {1, 3}, defenseDice = {5}},
                three = {hp = 13, attackDice = {1, 3}, defenseDice = {6}},
                four = {hp = 16, attackDice = {1, 3}, defenseDice = {6}}
            },
            Zarihell = {
                two = {hp = 9, attackDice = {1, 2}, defenseDice = {5}},
                three = {hp = 12, attackDice = {1, 2}, defenseDice = {5}},
                four = {hp = 15, attackDice = {1, 2}, defenseDice = {6}}
            }
        },
        act2 = {
            ["Baron Zachareth"] = {
                two = {hp = 16, attackDice = {1, 2, 2}, defenseDice = {6, 5}},
                three = {
                    hp = 18,
                    attackDice = {1, 2, 2},
                    defenseDice = {6, 5, 7}
                },
                four = {
                    hp = 20,
                    attackDice = {1, 2, 2},
                    defenseDice = {6, 5, 5}
                }
            },
            Belthir = {
                two = {hp = 13, attackDice = {1, 2, 2}, defenseDice = {5, 7}},
                three = {hp = 15, attackDice = {1, 2, 2}, defenseDice = {5, 5}},
                four = {
                    hp = 18,
                    attackDice = {1, 2, 2},
                    defenseDice = {5, 5, 7}
                }
            },
            ["Lady Eliza Farrow"] = {
                two = {hp = 9, attackDice = {1, 3, 2}, defenseDice = {5}},
                three = {hp = 11, attackDice = {1, 3, 2}, defenseDice = {5, 7}},
                four = {hp = 15, attackDice = {1, 3, 2}, defenseDice = {5, 7}}
            },
            ["Lord Merick Farrow"] = {
                two = {hp = 10, attackDice = {1, 2, 3}, defenseDice = {5, 7}},
                three = {hp = 12, attackDice = {1, 2, 3}, defenseDice = {5, 5}},
                four = {hp = 15, attackDice = {1, 2, 3}, defenseDice = {6, 5}}
            },
            ["Sir Alric Farrow"] = {
                two = {hp = 12, attackDice = {1, 2, 2}, defenseDice = {5, 5}},
                three = {hp = 15, attackDice = {1, 2, 2}, defenseDice = {6, 5}},
                four = {
                    hp = 18,
                    attackDice = {1, 2, 2},
                    defenseDice = {6, 5, 7}
                }
            },
            Splig = {
                two = {hp = 10, attackDice = {1, 2, 3}, defenseDice = {5}},
                three = {hp = 12, attackDice = {1, 2, 3}, defenseDice = {5, 7}},
                four = {hp = 16, attackDice = {1, 2, 3}, defenseDice = {5, 7}}
            },
            Valyndra = {
                two = {hp = 15, attackDice = {1, 2, 3}, defenseDice = {5, 5}},
                three = {
                    hp = 17,
                    attackDice = {1, 2, 3},
                    defenseDice = {5, 5, 7}
                },
                four = {
                    hp = 22,
                    attackDice = {1, 2, 3},
                    defenseDice = {6, 5, 7}
                }
            },
            Ariad = {
                two = {hp = 20, attackDice = {1, 4, 4}, defenseDice = {6, 5}},
                three = {hp = 24, attackDice = {1, 4, 4}, defenseDice = {6, 6}},
                four = {
                    hp = 28,
                    attackDice = {1, 4, 4},
                    defenseDice = {6, 5, 5}
                }
            },
            ["Queen Ariad"] = {
                two = {hp = 20, attackDice = {1, 2, 4}, defenseDice = {6, 5}},
                three = {hp = 24, attackDice = {1, 2, 4}, defenseDice = {6, 6}},
                four = {
                    hp = 28,
                    attackDice = {1, 2, 4},
                    defenseDice = {6, 5, 5}
                }
            },
            Raythen = {
                two = {hp = 8, attackDice = {1, 2, 4}, defenseDice = {7}},
                three = {hp = 10, attackDice = {1, 2, 4}, defenseDice = {5}},
                four = {hp = 12, attackDice = {1, 2, 4}, defenseDice = {5}}
            },
            Serena = {
                two = {hp = 8, attackDice = {1, 2, 4}, defenseDice = {7}},
                three = {hp = 10, attackDice = {1, 2, 4}, defenseDice = {5}},
                four = {hp = 10, attackDice = {1, 2, 4}, defenseDice = {5}}
            },
            ["Bol'Goreth"] = {
                two = {hp = 16, attackDice = {1, 2, 2}, defenseDice = {5, 7}},
                three = {hp = 19, attackDice = {1, 2, 2}, defenseDice = {5, 5}},
                four = {hp = 23, attackDice = {1, 2, 2}, defenseDice = {5, 5}}
            },
            Mirklace = {
                two = {hp = 18, attackDice = {1, 2, 3, 3}, defenseDice = {6, 7}},
                three = {
                    hp = 21,
                    attackDice = {1, 2, 3, 3},
                    defenseDice = {6, 7}
                },
                four = {
                    hp = 24,
                    attackDice = {1, 2, 3, 3},
                    defenseDice = {6, 5}
                }
            },
            ["Rylan Olliven"] = {
                two = {hp = 12, attackDice = {1, 2, 4, 4}, defenseDice = {6}},
                three = {
                    hp = 14,
                    attackDice = {1, 2, 4, 4},
                    defenseDice = {5, 5}
                },
                four = {
                    hp = 17,
                    attackDice = {1, 2, 4, 4},
                    defenseDice = {5, 5}
                }
            },
            ["Tristayne Olliven"] = {
                two = {hp = 12, attackDice = {1, 2, 3}, defenseDice = {6}},
                three = {hp = 15, attackDice = {1, 2, 3}, defenseDice = {5, 5}},
                four = {hp = 18, attackDice = {1, 2, 3}, defenseDice = {6, 5}}
            },
            Verminous = {
                two = {hp = 11, attackDice = {1, 3, 4, 4}, defenseDice = {5, 7}},
                three = {
                    hp = 13,
                    attackDice = {1, 3, 4, 4},
                    defenseDice = {6, 7}
                },
                four = {
                    hp = 17,
                    attackDice = {1, 3, 4, 4},
                    defenseDice = {6, 5}
                }
            },
            Skarn = {
                two = {hp = 16, attackDice = {1, 2, 3}, defenseDice = {5, 5}},
                three = {hp = 19, attackDice = {1, 2, 3}, defenseDice = {6, 5}},
                four = {hp = 24, attackDice = {1, 2, 3}, defenseDice = {6, 5}}
            },
            ["Ardus Ix'Erebus"] = {
                two = {hp = 13, attackDice = {1, 2, 2}, defenseDice = {6, 7}},
                three = {hp = 16, attackDice = {1, 2, 2}, defenseDice = {6, 5}},
                four = {hp = 19, attackDice = {1, 2, 2}, defenseDice = {6, 5}}
            },
            Kyndrithul = {
                two = {hp = 13, attackDice = {1, 3, 3}, defenseDice = {6}},
                three = {hp = 15, attackDice = {1, 3, 3}, defenseDice = {6, 7}},
                four = {hp = 19, attackDice = {1, 3, 3}, defenseDice = {6, 7}}
            },
            Zarihell = {
                two = {hp = 11, attackDice = {1, 2, 3}, defenseDice = {6}},
                three = {hp = 14, attackDice = {1, 2, 3}, defenseDice = {5, 5}},
                four = {hp = 17, attackDice = {1, 2, 3}, defenseDice = {5, 5}}
            }
        }
    }
}

function getCreatures() return creatures end
function getMonster(params) return creatures.monsters[params.act][params.name] end
function getLieutenant(params)
    return creatures.lieutenants[params.act][params.name]
end

items = {
    shop = {
        ["baron's cloak"] = {attack = false, defend = true, dice = {7}},
        ["battle tome"] = {attack = true, defend = false, dice = {1, 3}},
        ["bearded axe"] = {attack = true, defend = false, dice = {1, 2}},
        ["bloody dagger"] = {attack = true, defend = false, dice = {1, 4}},
        ["bone blade"] = {attack = true, defend = false, dice = {1, 2}},
        ["bow of bone"] = {attack = true, defend = false, dice = {1, 3}},
        chainmail = {attack = false, defend = true, dice = {5}},
        ["city guard's bow"] = {attack = true, defend = false, dice = {1, 3}},
        ["cloak of mists"] = {attack = false, defend = true, dice = {}},
        crossbow = {attack = true, defend = false, dice = {1, 3}},
        ["dire flail"] = {attack = true, defend = false, dice = {1, 3, 4}},
        ["elm greatbow"] = {attack = true, defend = false, dice = {1, 3}},
        ["guardian axe"] = {attack = true, defend = false, dice = {1, 2}},
        halberd = {attack = true, defend = false, dice = {1, 2}},
        handbow = {attack = true, defend = false, dice = {1, 3}},
        ["heavy cloak"] = {attack = false, defend = true, dice = {}},
        immolation = {attack = true, defend = false, dice = {1, 2}},
        ["iron battleaxe"] = {attack = true, defend = false, dice = {1, 2}},
        ["iron spear"] = {attack = true, defend = false, dice = {1, 3}},
        ["ironbound rune"] = {attack = true, defend = false, dice = {1, 2}},
        ["jeweled mace"] = {attack = true, defend = false, dice = {1, 4}},
        ["leather armor"] = {attack = false, defend = true, dice = {7}},
        ["lifedrain scepter"] = {attack = true, defend = false, dice = {1, 4}},
        ["light hammer"] = {attack = true, defend = false, dice = {1, 3}},
        ["mace of aver"] = {attack = true, defend = false, dice = {1, 4, 4}},
        ["magic staff"] = {attack = true, defend = false, dice = {1, 2}},
        ["magma blast"] = {attack = true, defend = false, dice = {1, 2}},
        ["marsh cloak"] = {attack = false, defend = true, dice = {7}},
        mistbane = {attack = true, defend = false, dice = {1, 2}},
        ["poisoned blowgun"] = {attack = true, defend = false, dice = {1, 4}},
        ["rune plate"] = {attack = false, defend = true, dice = {5}},
        ["serpent dagger"] = {attack = true, defend = false, dice = {1, 4}},
        sling = {attack = true, defend = false, dice = {1, 3}},
        ["soulbound sword"] = {attack = true, defend = false, dice = {1, 3}},
        soulstone = {attack = true, defend = false, dice = {1, 2}},
        ["staff of greyhaven"] = {attack = true, defend = false, dice = {1, 3}},
        ["steel broadsword"] = {attack = true, defend = false, dice = {1, 2}},
        sunburst = {attack = true, defend = false, dice = {1, 3}},
        ["teleportation rune"] = {attack = true, defend = false, dice = {1, 3}},
        ["thief's vest"] = {attack = false, defend = true, dice = {7}},
        trident = {attack = true, defend = false, dice = {1, 2}},
        ["white wolf cloak"] = {attack = false, defend = true, dice = {7}},
        ["witch hazel bow"] = {attack = true, defend = false, dice = {1, 3}},
        ["golden orb mace"] = {attack = true, defend = false, dice = {1, 3}},
        ["sun-blessed rune"] = {attack = true, defend = false, dice = {1, 3}},
        sunspear = {attack = true, defend = false, dice = {1, 3}},
        ["vizier's garment"] = {attack = false, defend = true, dice = {7}},
        ["blasting rune"] = {attack = true, defend = false, dice = {1, 2, 4}},
        ["blessed armor"] = {attack = false, defend = true, dice = {5}},
        ["bone wand"] = {attack = true, defend = false, dice = {1, 3, 3}},
        boomerang = {attack = true, defend = false, dice = {1, 3, 3, 4}},
        ["bow of the eclipse"] = {
            attack = true,
            defend = false,
            dice = {1, 2, 4}
        },
        ["bow of the sky"] = {attack = true, defend = false, dice = {1, 2, 3}},
        ["cloak of deception"] = {attack = false, defend = true, dice = {5}},
        ["demonhide leather"] = {attack = false, defend = true, dice = {5}},
        ["dragontooth hammer"] = {
            attack = true,
            defend = false,
            dice = {1, 2, 2}
        },
        ["dwarven firebomb"] = {attack = true, defend = false, dice = {1, 2, 3}},
        ["elven cloak"] = {attack = false, defend = true, dice = {}},
        ["fists of iron"] = {attack = true, defend = false, dice = {1, 3, 3}},
        glaive = {attack = true, defend = false, dice = {1, 2, 4}},
        ["grinding axe"] = {attack = true, defend = false, dice = {1, 2, 2}},
        ["hammer of doom"] = {attack = true, defend = false, dice = {1, 2, 2}},
        ["heart seeker"] = {attack = true, defend = false, dice = {1, 3, 3}},
        ["ice storm"] = {attack = true, defend = false, dice = {1, 3, 3}},
        ["inscribed robes"] = {attack = false, defend = true, dice = {}},
        ["iron claws"] = {attack = true, defend = false, dice = {1, 3, 3}},
        ["ironbound glaive"] = {attack = true, defend = false, dice = {1, 2, 3}},
        ["latari longbow"] = {attack = true, defend = false, dice = {1, 3, 3}},
        ["lightning javelin"] = {
            attack = true,
            defend = false,
            dice = {1, 3, 3}
        },
        ["lightning strike"] = {attack = true, defend = false, dice = {1, 3, 3}},
        ["mace of kellos"] = {attack = true, defend = false, dice = {1, 2, 3}},
        ["nerekhall plate"] = {attack = false, defend = true, dice = {5}},
        ["obsidian greataxe"] = {
            attack = true,
            defend = false,
            dice = {1, 2, 2}
        },
        ["obsidian scalemail"] = {attack = false, defend = true, dice = {6}},
        platemail = {attack = false, defend = true, dice = {6}},
        ["rage blade"] = {attack = true, defend = false, dice = {1, 2, 3}},
        ["rat-tooth dagger"] = {attack = true, defend = false, dice = {1, 3, 3}},
        ["repeating crossbow"] = {
            attack = true,
            defend = false,
            dice = {1, 2, 3}
        },
        ["rune of blades"] = {attack = true, defend = false, dice = {1, 3, 4}},
        ["rune of fate"] = {attack = true, defend = false, dice = {1, 3, 3}},
        ["rune of misery"] = {attack = true, defend = false, dice = {1, 2, 4}},
        ["rune-touched leather"] = {attack = false, defend = true, dice = {7}},
        scalemail = {attack = false, defend = true, dice = {5}},
        ["staff of kellos"] = {attack = true, defend = false, dice = {1, 2, 3}},
        ["staff of the wild"] = {
            attack = true,
            defend = false,
            dice = {1, 3, 4, 4}
        },
        ["steel greatsword"] = {attack = true, defend = false, dice = {1, 2, 3}},
        ["stone armor"] = {attack = false, defend = true, dice = {6, 6}},
        ["vestments of kellos"] = {attack = false, defend = true, dice = {7}},
        ["winged blade"] = {attack = true, defend = false, dice = {1, 2, 3}},
        ["chain-sickle"] = {attack = true, defend = false, dice = {1, 3, 3}},
        ["ibis bow"] = {attack = true, defend = false, dice = {1, 3, 3}},
        ["lost caliph's crook"] = {
            attack = true,
            defend = false,
            dice = {1, 2, 3}
        },
        ["starmetal khopesh"] = {
            attack = true,
            defend = false,
            dice = {1, 2, 3}
        }
    },
    relics = {
        ["aurium mail"] = {attack = false, defend = true, dice = {6}},
        ["boneborn bow"] = {attack = true, defend = false, dice = {1, 3, 3}},
        dawnblade = {attack = true, defend = false, dice = {1, 2, 3}},
        ["fear eater"] = {attack = true, defend = false, dice = {1, 2}},
        ["robes of the last"] = {attack = false, defend = true, dice = {5}},
        ["shards of ithyndrus"] = {attack = true, defend = false, dice = {1, 3}},
        ["spirited scythe"] = {attack = true, defend = false, dice = {1, 2, 2}},
        ["staff of light"] = {attack = true, defend = false, dice = {1, 3}},
        ["the shadow rune"] = {attack = true, defend = false, dice = {1, 3, 3}},
        trueshot = {attack = true, defend = false, dice = {1, 2, 3}},
        ["undertaker's coat"] = {attack = false, defend = true, dice = {}},
        ["valyndra's bane"] = {attack = true, defend = false, dice = {1, 2, 3}},
        ["ynfernal rune"] = {attack = true, defend = false, dice = {1, 2, 4}}
    },
    class = {
        ["chipped greataxe"] = {
            attack = true,
            defenseDice = false,
            dice = {1, 2}
        },
        ["iron longsword"] = {attack = true, defenseDice = false, dice = {1, 2}},
        ["wooden shield"] = {attack = false, defenseDice = false, dice = {}},
        ["iron mace"] = {attack = true, defenseDice = false, dice = {1, 3}},
        ["oak staff"] = {attack = true, defenseDice = false, dice = {1, 3}},
        ["reaper's scythe"] = {
            attack = true,
            defenseDice = false,
            dice = {1, 3}
        },
        ["arcane bolt"] = {attack = true, defenseDice = false, dice = {1, 3}},
        ["throwing knives"] = {
            attack = true,
            defenseDice = false,
            dice = {1, 3}
        },
        ["lucky charm"] = {attack = false, defenseDice = false, dice = {}},
        ["yew shortbow"] = {attack = true, defenseDice = false, dice = {1, 3}},
        ["worn greatsword"] = {
            attack = true,
            defenseDice = false,
            dice = {1, 2}
        },
        ["horn of courage"] = {attack = false, defenseDice = false, dice = {}},
        ["stasis rune"] = {attack = true, defenseDice = false, dice = {1, 3}},
        ["hunting spear"] = {attack = true, defenseDice = false, dice = {1, 2}},
        ["skinning knife"] = {attack = true, defenseDice = false, dice = {1, 4}},
        ["smoking vials"] = {attack = true, defenseDice = false, dice = {1, 4}},
        ["staff of the grave"] = {
            attack = true,
            defenseDice = false,
            dice = {1, 4}
        },
        ["leather whip"] = {attack = true, defenseDice = false, dice = {1, 3}},
        ["the dead man's compass"] = {
            attack = false,
            defenseDice = false,
            dice = {}
        },
        ["iron flail"] = {attack = true, defenseDice = false, dice = {1, 3}},
        ["sage's tome"] = {attack = false, defenseDice = false, dice = {}},
        ["black widow's web"] = {
            attack = true,
            defenseDice = false,
            dice = {1, 4}
        },
        ["hunting knife"] = {attack = true, defenseDice = false, dice = {1, 3}},
        ["jagged handaxe"] = {attack = true, defenseDice = false, dice = {1, 3}},
        ["rusted handaxe"] = {attack = true, defenseDice = false, dice = {1, 4}},
        ["traveler's blade"] = {
            attack = true,
            defenseDice = false,
            dice = {1, 3}
        },
        lute = {attack = false, defenseDice = false, dice = {}},
        ["prismatic staff"] = {
            attack = true,
            defenseDice = false,
            dice = {1, 4}
        },
        ["feathered hatchet"] = {
            attack = true,
            defenseDice = false,
            dice = {1, 2}
        },
        ["tribal cloak"] = {attack = false, defenseDice = true, dice = {}},
        ["war hammer"] = {attack = true, defenseDice = false, dice = {1, 2}},
        ["signet ring"] = {attack = false, defenseDice = false, dice = {}},
        ["double crossbow"] = {
            attack = true,
            defenseDice = false,
            dice = {1, 2}
        },
        ["harvester scythe"] = {
            attack = true,
            defenseDice = false,
            dice = {1, 3}
        },
        ["mirror of souls"] = {attack = false, defenseDice = false, dice = {}},
        ["runeshard cache"] = {attack = false, defenseDice = false, dice = {}},
        ["spire of conflux"] = {
            attack = true,
            defenseDice = false,
            dice = {1, 3}
        },
        ["ceremonial staff"] = {
            attack = true,
            defenseDice = false,
            dice = {1, 2}
        },
        ["sacred scriptures"] = {attack = false, defenseDice = false, dice = {}},
        ["shadow darts"] = {attack = true, defenseDice = false, dice = {1, 4}},
        ["mind's eye turban"] = {attack = false, defenseDice = false, dice = {}}
    }
}

function getItems() return items end
function getClassItem(params) return items.class[params.name:lower()] end
function getShopItem(params) return items.shop[params.name:lower()] end
function getRelic(params) return items.relics[params.name:lower()] end
function findItem(params)
    for _, table in pairs(items) do
        if table[params.name:lower()] then
            return table[params.name:lower()]
        end
    end
end

skills = {
    rage = true,
    brute = true,
    ["counter attack"] = true,
    cripple = true,
    charge = true,
    ["weapon mastery"] = true,
    whirlwind = true,
    ["death rage"] = true,
    execute = true,
    ["berserker-back"] = true,
    ["oath of honor"] = true,
    advance = true,
    challenge = true,
    defenseDice = true,
    ["defense training"] = true,
    guard = true,
    ["shield slam"] = true,
    inspiration = true,
    stalwart = true,
    ["knight-back"] = true,
    ["prayer of healing"] = true,
    ["armor of faith"] = true,
    ["blessed strike"] = true,
    ["cleansing touch"] = true,
    ["divine fury"] = true,
    ["prayer of peace"] = true,
    ["time of need"] = true,
    ["holy power"] = true,
    ["radiant light"] = true,
    ["disciple-back"] = true,
    stoneskin = true,
    ["drain spirit"] = true,
    ["healing rain"] = true,
    ["shared pain"] = true,
    ["cloud of mist"] = true,
    ["nature's bounty"] = true,
    tempest = true,
    ["ancestor spirits"] = true,
    vigor = true,
    ["spiritspeaker-back"] = true,
    ["raise dead"] = true,
    ["corpse blast"] = true,
    ["deathly haste"] = true,
    ["fury of undeath"] = true,
    ["dark pact"] = true,
    ["undead might"] = true,
    ["vampiric blood"] = true,
    ["army of death"] = true,
    ["dying command"] = true,
    ["necromancer-back"] = true,
    ["runic knowledge"] = true,
    ["exploding rune"] = true,
    ["ghost armor"] = true,
    ["inscribe rune"] = true,
    ["iron will"] = true,
    ["rune mastery"] = true,
    ["runic sorcery"] = true,
    ["break the rune"] = true,
    ["quick casting"] = true,
    ["runemaster-back"] = true,
    greedy = true,
    sneaky = true,
    appraisal = true,
    ["dirty tricks"] = true,
    unseen = true,
    caltrops = true,
    tumble = true,
    lurk = true,
    bushwhack = true,
    ["thief-back"] = true,
    nimble = true,
    accurate = true,
    ["eagle eyes"] = true,
    ["danger sense"] = true,
    ["fleet of foot"] = true,
    ["first strike"] = true,
    ["bow mastery"] = true,
    ["black arrow"] = true,
    ["running shot"] = true,
    ["wildlander-back"] = true,
    ["valor of heroes"] = true,
    ["a living legend"] = true,
    ["glory of battle"] = true,
    ["inspiring presence"] = true,
    ["motivating charge"] = true,
    ["no mercy"] = true,
    ["stoic resolve"] = true,
    ["for the cause"] = true,
    ["valorous strike"] = true,
    ["champion-back"] = true,
    terracall = true,
    ["earthen anguish"] = true,
    ["quaking word"] = true,
    ["stone tongue"] = true,
    ["ley line"] = true,
    ["molten fury"] = true,
    ["ways of stone"] = true,
    cataclysm = true,
    ["gravity spike"] = true,
    ["geomancer-back"] = true,
    ["bound by the hunt"] = true,
    ["bestial rage"] = true,
    stalker = true,
    survivalist = true,
    ["feral frenzy"] = true,
    savagery = true,
    ["shadow hunter"] = true,
    ["changing skins"] = true,
    predator = true,
    ["beastmaster-back"] = true,
    ["brew elixir"] = true,
    concoction = true,
    ["herbal lore"] = true,
    ["inky substance"] = true,
    ["bottled courage"] = true,
    ["protective tonic"] = true,
    ["secret formula"] = true,
    ["hidden stash"] = true,
    ["potent remedies"] = true,
    ["apothecary-back"] = true,
    ["enfeebling hex"] = true,
    affliction = true,
    ["plague spasm"] = true,
    ["viral hex"] = true,
    ["crippling curse"] = true,
    ["fel command"] = true,
    ["internal rot"] = true,
    ["accursed arms"] = true,
    ["plague cloud"] = true,
    ["hexer-back"] = true,
    delver = true,
    dungeoneer = true,
    survey = true,
    ["gold rush"] = true,
    ["guard the spoils"] = true,
    ["sleight of hand"] = true,
    ["lure of fortune"] = true,
    ["finder's keeper's"] = true,
    ["trail of riches"] = true,
    ["treasure-hunter-back"] = true,
    ["soothing insight"] = true,
    ["battle vision"] = true,
    forewarning = true,
    ["grim fate"] = true,
    ["all-seeing"] = true,
    lifeline = true,
    ["victory foretold"] = true,
    ["focused insights"] = true,
    omniscience = true,
    ["prophet-back"] = true,
    ["set trap"] = true,
    ["hunter's mark"] = true,
    exploit = true,
    ["makeshift trap"] = true,
    ["lay of the land"] = true,
    ["easy prey"] = true,
    ["poison barbs"] = true,
    ambush = true,
    ["upper hand"] = true,
    ["stalker-back"] = true,
    ["dual strike"] = true,
    ["back in action"] = true,
    ["deep wounds"] = true,
    ["keen edge"] = true,
    ["born in battle"] = true,
    ["ever in motion"] = true,
    unrelenting = true,
    ["carve a path"] = true,
    unstoppable = true,
    ["skirmisher-back"] = true,
    ["song of mending"] = true,
    dissonance = true,
    ["peaceful rest"] = true,
    understudy = true,
    ["aria of war"] = true,
    concentration = true,
    rehearsal = true,
    cacophony = true,
    wayfarer = true,
    ["bard-back"] = true,
    channeling = true,
    ["mirror image"] = true,
    ["illusory path"] = true,
    ["many friends"] = true,
    refraction = true,
    ["blinding light"] = true,
    ["focus fire"] = true,
    ["sleight of mind"] = true,
    ["prismatic assault"] = true,
    vortex = true,
    ["conjurer-back"] = true,
    ["soul bound"] = true,
    ["dark servant"] = true,
    ["faithful friend"] = true,
    ["through the veil"] = true,
    ["dark shift"] = true,
    ["endless void"] = true,
    otherworldly = true,
    ["shadow step"] = true,
    ["shadow puppet"] = true,
    ["shadow-walker-back"] = true,
    retribution = true,
    ["just reward"] = true,
    shockwave = true,
    ["zealous fire"] = true,
    ["by the book"] = true,
    ["i am the law"] = true,
    ["vigilant watch"] = true,
    ["crushing blow"] = true,
    ["last stand"] = true,
    ["marshal-back"] = true,
    ["chosen target"] = true,
    longshot = true,
    ["lie in wait"] = true,
    ["not so fast"] = true,
    ["dark iron chains"] = true,
    undercover = true,
    ["evil eye"] = true,
    payday = true,
    ["rapid fire"] = true,
    ["bounty-hunter-back"] = true,
    ["blight extraction"] = true,
    ["cursed soul"] = true,
    ["essence harvest"] = true,
    ["ethereal armor"] = true,
    ["font of vitality"] = true,
    galvanize = true,
    ["reaper's pact"] = true,
    ["spirit link"] = true,
    ["stream of life"] = true,
    ["unholy bond"] = true,
    ["soul-reaper-back"] = true,
    blaze = true,
    ["earth and sky"] = true,
    ["elemental focus"] = true,
    grasp = true,
    gust = true,
    ["nature's embrace"] = true,
    ["nature's fury"] = true,
    ["primal harmony"] = true,
    ["spiritual balance"] = true,
    ["storm's fury"] = true,
    ["sun and sea"] = true,
    tide = true,
    ["volcanic might"] = true,
    ["elementalist-back"] = true,
    ["embalming ritual"] = true,
    ["eternal service"] = true,
    ["path of prophecy"] = true,
    hymn = true,
    ["sands of vengeance"] = true,
    ["shroud of respite"] = true,
    ["words of wisdom"] = true,
    ["gift of the desert"] = true,
    ["royal guard"] = true,
    ["hierophant-back"] = true,
    ["mental assault"] = true,
    prescience = true,
    ["empathetic resolve"] = true,
    overload = true,
    ["mental fortitude"] = true,
    possession = true,
    ["reaching out"] = true,
    ["mind over matter"] = true,
    ["towering intellect"] = true,
    ["psychic-back"] = true
}

function getSkills() return skills end
function getSkill(params) return skills[params.name:lower()] end

heroes = {
    ["andira runehand"] = {
        hp = 12,
        speed = 4,
        stamina = 4,
        willpower = 4,
        might = 2,
        knowledge = 3,
        awareness = 2,
        color = "Blue",
        defend = {5}
    },
    ashrian = {
        hp = 10,
        speed = 5,
        stamina = 4,
        willpower = 3,
        might = 2,
        knowledge = 2,
        awareness = 4,
        color = "Blue",
        defend = {5}
    },
    ["augur grisom"] = {
        hp = 12,
        speed = 3,
        stamina = 5,
        willpower = 3,
        might = 4,
        knowledge = 2,
        awareness = 2,
        color = "Blue",
        defend = {5}
    },
    aurim = {
        hp = 8,
        speed = 5,
        stamina = 5,
        willpower = 3,
        might = 2,
        knowledge = 4,
        awareness = 2,
        color = "Blue",
        defend = {5}
    },
    ["aurim uc"] = {
        hp = 8,
        speed = 5,
        stamina = 5,
        willpower = 3,
        might = 2,
        knowledge = 4,
        awareness = 2,
        color = "Blue",
        defend = {5}
    },
    ["avric albright"] = {
        hp = 12,
        speed = 4,
        stamina = 4,
        willpower = 4,
        might = 2,
        knowledge = 3,
        awareness = 2,
        color = "Blue",
        defend = {5}
    },
    ["brother gherinn"] = {
        hp = 12,
        speed = 3,
        stamina = 4,
        willpower = 4,
        might = 1,
        knowledge = 4,
        awareness = 2,
        color = "Blue",
        defend = {5}
    },
    ["brother glyr"] = {
        hp = 12,
        speed = 2,
        stamina = 3,
        willpower = 3,
        might = 4,
        knowledge = 1,
        awareness = 3,
        color = "Blue",
        defend = {5}
    },
    ["brother glyr uc"] = {
        hp = 12,
        speed = 2,
        stamina = 3,
        willpower = 3,
        might = 4,
        knowledge = 1,
        awareness = 3,
        color = "Blue",
        defend = {5}
    },
    ["elder mok"] = {
        hp = 10,
        speed = 4,
        stamina = 4,
        willpower = 4,
        might = 2,
        knowledge = 3,
        awareness = 2,
        color = "Blue",
        defend = {5}
    },
    ["elga the pilgrim"] = {
        hp = 10,
        speed = 4,
        stamina = 4,
        willpower = 3,
        might = 2,
        knowledge = 4,
        awareness = 2,
        color = "Blue",
        defend = {5}
    },
    gaia = {
        hp = 9,
        speed = 4,
        stamina = 5,
        willpower = 5,
        might = 2,
        knowledge = 2,
        awareness = 2,
        color = "Blue",
        defend = {7, 7}
    },
    ["gristtun the merry"] = {
        hp = 10,
        speed = 3,
        stamina = 4,
        willpower = 2,
        might = 4,
        knowledge = 4,
        awareness = 1,
        color = "Blue",
        defend = {5}
    },
    ispher = {
        hp = 10,
        speed = 4,
        stamina = 4,
        willpower = 3,
        might = 2,
        knowledge = 3,
        awareness = 3,
        color = "Blue",
        defend = {5}
    },
    ["jonas the kind"] = {
        hp = 10,
        speed = 4,
        stamina = 4,
        willpower = 4,
        might = 2,
        knowledge = 3,
        awareness = 2,
        color = "Blue",
        defend = {5}
    },
    ["lymina the resolute"] = {
        hp = 8,
        speed = 4,
        stamina = 4,
        willpower = 3,
        might = 3,
        knowledge = 3,
        awareness = 3,
        color = "Blue",
        defend = {6}
    },
    ["okaluk and rakash"] = {
        hp = 8,
        speed = 2,
        stamina = 3,
        willpower = 3,
        might = 3,
        knowledge = 2,
        awareness = 3,
        color = "Blue",
        defend = {5}
    },
    rendiel = {
        hp = 10,
        speed = 5,
        stamina = 4,
        willpower = 5,
        might = 2,
        knowledge = 3,
        awareness = 1,
        color = "Blue",
        defend = {5}
    },
    sahla = {
        hp = 10,
        speed = 4,
        stamina = 4,
        willpower = 3,
        might = 2,
        knowledge = 3,
        awareness = 3,
        color = "Blue",
        defend = {5}
    },
    serena = {
        hp = 8,
        speed = 3,
        stamina = 6,
        willpower = 5,
        might = 1,
        knowledge = 3,
        awareness = 2,
        color = "Blue",
        defend = {7}
    },
    ["ulma grimstone"] = {
        hp = 8,
        speed = 4,
        stamina = 5,
        willpower = 3,
        might = 2,
        knowledge = 4,
        awareness = 2,
        color = "Blue",
        defend = {5}
    },
    astarra = {
        hp = 10,
        speed = 4,
        stamina = 5,
        willpower = 4,
        might = 1,
        knowledge = 4,
        awareness = 2,
        color = "Yellow",
        defend = {5}
    },
    aurora = {
        hp = 8,
        speed = 4,
        stamina = 5,
        willpower = 4,
        might = 1,
        knowledge = 4,
        awareness = 2,
        color = "Yellow",
        defend = {5}
    },
    challara = {
        hp = 10,
        speed = 3,
        stamina = 4,
        willpower = 3,
        might = 3,
        knowledge = 4,
        awareness = 1,
        color = "Yellow",
        defend = {5}
    },
    ["dezra the vile"] = {
        hp = 8,
        speed = 5,
        stamina = 4,
        willpower = 2,
        might = 2,
        knowledge = 4,
        awareness = 3,
        color = "Yellow",
        defend = {5}
    },
    ["high mage quellen"] = {
        hp = 10,
        speed = 4,
        stamina = 4,
        willpower = 3,
        might = 1,
        knowledge = 5,
        awareness = 2,
        color = "Yellow",
        defend = {5}
    },
    ["jaes the exile"] = {
        hp = 12,
        speed = 4,
        stamina = 3,
        willpower = 2,
        might = 3,
        knowledge = 4,
        awareness = 2,
        color = "Yellow",
        defend = {5}
    },
    ["landrec the wise"] = {
        hp = 10,
        speed = 3,
        stamina = 4,
        willpower = 3,
        might = 1,
        knowledge = 5,
        awareness = 2,
        color = "Yellow",
        defend = {7}
    },
    ["landrec the wise uc"] = {
        hp = 10,
        speed = 3,
        stamina = 4,
        willpower = 3,
        might = 1,
        knowledge = 5,
        awareness = 2,
        color = "Yellow",
        defend = {7}
    },
    ["leoric of the book"] = {
        hp = 8,
        speed = 4,
        stamina = 5,
        willpower = 2,
        might = 1,
        knowledge = 5,
        awareness = 3,
        color = "Yellow",
        defend = {5}
    },
    lyssa = {
        hp = 8,
        speed = 5,
        stamina = 5,
        willpower = 2,
        might = 2,
        knowledge = 3,
        awareness = 4,
        color = "Yellow",
        defend = {5}
    },
    ["mad carthos"] = {
        hp = 8,
        speed = 4,
        stamina = 3,
        willpower = 4,
        might = 1,
        knowledge = 4,
        awareness = 2,
        color = "Yellow",
        defend = {5}
    },
    ["mad carthos uc"] = {
        hp = 8,
        speed = 4,
        stamina = 3,
        willpower = 4,
        might = 1,
        knowledge = 4,
        awareness = 2,
        color = "Yellow",
        defend = {5}
    },
    ["master thorn"] = {
        hp = 8,
        speed = 5,
        stamina = 4,
        willpower = 3,
        might = 1,
        knowledge = 5,
        awareness = 2,
        color = "Yellow",
        defend = {5}
    },
    ["ravaella lightfoot"] = {
        hp = 8,
        speed = 5,
        stamina = 5,
        willpower = 2,
        might = 1,
        knowledge = 4,
        awareness = 4,
        color = "Yellow",
        defend = {6}
    },
    ["runemaker tara"] = {
        hp = 10,
        speed = 4,
        stamina = 4,
        willpower = 3,
        might = 2,
        knowledge = 4,
        awareness = 2,
        color = "Yellow",
        defend = {5}
    },
    ["seer kel"] = {
        hp = 10,
        speed = 4,
        stamina = 4,
        willpower = 2,
        might = 1,
        knowledge = 4,
        awareness = 4,
        color = "Yellow",
        defend = {5}
    },
    shiver = {
        hp = 10,
        speed = 4,
        stamina = 4,
        willpower = 3,
        might = 2,
        knowledge = 3,
        awareness = 3,
        color = "Yellow",
        defend = {5}
    },
    ["truthseer kel"] = {
        hp = 10,
        speed = 4,
        stamina = 4,
        willpower = 2,
        might = 1,
        knowledge = 4,
        awareness = 4,
        color = "Yellow",
        defend = {5}
    },
    ["widow tarha"] = {
        hp = 10,
        speed = 4,
        stamina = 4,
        willpower = 3,
        might = 2,
        knowledge = 4,
        awareness = 2,
        color = "Yellow",
        defend = {5}
    },
    zyla = {
        hp = 8,
        speed = 5,
        stamina = 5,
        willpower = 3,
        might = 1,
        knowledge = 4,
        awareness = 3,
        color = "Yellow",
        defend = {5}
    },
    ["arvel worldwalker"] = {
        hp = 10,
        speed = 4,
        stamina = 4,
        willpower = 3,
        might = 3,
        knowledge = 3,
        awareness = 3,
        color = "Green",
        defend = {5}
    },
    ["bogran the shadow"] = {
        hp = 10,
        speed = 4,
        stamina = 4,
        willpower = 3,
        might = 2,
        knowledge = 2,
        awareness = 4,
        color = "Green",
        defend = {5}
    },
    ["bogran the shadow uc"] = {
        hp = 10,
        speed = 4,
        stamina = 4,
        willpower = 3,
        might = 2,
        knowledge = 2,
        awareness = 4,
        color = "Green",
        defend = {5}
    },
    ["grey ker"] = {
        hp = 12,
        speed = 4,
        stamina = 5,
        willpower = 3,
        might = 3,
        knowledge = 2,
        awareness = 4,
        color = "Green",
        defend = {5}
    },
    ["jain fairwood"] = {
        hp = 8,
        speed = 5,
        stamina = 5,
        willpower = 2,
        might = 2,
        knowledge = 3,
        awareness = 4,
        color = "Green",
        defend = {5}
    },
    kirga = {
        hp = 12,
        speed = 4,
        stamina = 3,
        willpower = 1,
        might = 3,
        knowledge = 2,
        awareness = 5,
        color = "Green",
        defend = {5}
    },
    ["kirga uc"] = {
        hp = 12,
        speed = 4,
        stamina = 4,
        willpower = 1,
        might = 3,
        knowledge = 2,
        awareness = 5,
        color = "Green",
        defend = {5}
    },
    ["laurel of bloodwood"] = {
        hp = 8,
        speed = 4,
        stamina = 5,
        willpower = 2,
        might = 2,
        knowledge = 3,
        awareness = 4,
        color = "Green",
        defend = {5}
    },
    lindel = {
        hp = 10,
        speed = 5,
        stamina = 5,
        willpower = 3,
        might = 3,
        knowledge = 3,
        awareness = 3,
        color = "Green",
        defend = {5}
    },
    ["logan lashley"] = {
        hp = 10,
        speed = 4,
        stamina = 4,
        willpower = 2,
        might = 3,
        knowledge = 2,
        awareness = 4,
        color = "Green",
        defend = {5}
    },
    ["ravvis the ranger"] = {
        hp = 8,
        speed = 6,
        stamina = 3,
        willpower = 3,
        might = 2,
        knowledge = 2,
        awareness = 4,
        color = "Green",
        defend = {7}
    },
    raythen = {
        hp = 14,
        speed = 4,
        stamina = 4,
        willpower = 1,
        might = 3,
        knowledge = 2,
        awareness = 5,
        color = "Green",
        defend = {7}
    },
    ["red scorpion"] = {
        hp = 8,
        speed = 5,
        stamina = 5,
        willpower = 3,
        might = 3,
        knowledge = 3,
        awareness = 3,
        color = "Green",
        defend = {5}
    },
    ["red scorpion uc"] = {
        hp = 8,
        speed = 5,
        stamina = 5,
        willpower = 3,
        might = 3,
        knowledge = 3,
        awareness = 3,
        color = "Green",
        defend = {5}
    },
    ["roganna the shade"] = {
        hp = 10,
        speed = 5,
        stamina = 4,
        willpower = 4,
        might = 2,
        knowledge = 2,
        awareness = 3,
        color = "Green",
        defend = {5}
    },
    ["ronan of the wild"] = {
        hp = 10,
        speed = 4,
        stamina = 5,
        willpower = 4,
        might = 3,
        knowledge = 1,
        awareness = 3,
        color = "Green",
        defend = {5}
    },
    silhouette = {
        hp = 10,
        speed = 5,
        stamina = 4,
        willpower = 1,
        might = 3,
        knowledge = 2,
        awareness = 5,
        color = "Green",
        defend = {5}
    },
    tatianna = {
        hp = 12,
        speed = 5,
        stamina = 4,
        willpower = 2,
        might = 2,
        knowledge = 2,
        awareness = 5,
        color = "Green",
        defend = {5}
    },
    tetherys = {
        hp = 10,
        speed = 4,
        stamina = 4,
        willpower = 1,
        might = 3,
        knowledge = 2,
        awareness = 5,
        color = "Green",
        defend = {5}
    },
    ["thaiden mistpeak"] = {
        hp = 10,
        speed = 4,
        stamina = 5,
        willpower = 2,
        might = 3,
        knowledge = 1,
        awareness = 5,
        color = "Green",
        defend = {5}
    },
    ["tinashi the wanderer"] = {
        hp = 12,
        speed = 4,
        stamina = 4,
        willpower = 3,
        might = 3,
        knowledge = 2,
        awareness = 3,
        color = "Green",
        defend = {5}
    },
    ["tobin farslayer"] = {
        hp = 12,
        speed = 4,
        stamina = 3,
        willpower = 2,
        might = 3,
        knowledge = 2,
        awareness = 4,
        color = "Green",
        defend = {5}
    },
    ["tobin farslayer uc"] = {
        hp = 12,
        speed = 4,
        stamina = 3,
        willpower = 2,
        might = 3,
        knowledge = 2,
        awareness = 4,
        color = "Green",
        defend = {5}
    },
    ["tomble burrowell"] = {
        hp = 8,
        speed = 4,
        stamina = 5,
        willpower = 3,
        might = 1,
        knowledge = 2,
        awareness = 5,
        color = "Green",
        defend = {5}
    },
    ["vyrah the falconer"] = {
        hp = 10,
        speed = 4,
        stamina = 4,
        willpower = 2,
        might = 3,
        knowledge = 2,
        awareness = 4,
        color = "Green",
        defend = {5}
    },
    ["alys raine"] = {
        hp = 12,
        speed = 4,
        stamina = 4,
        willpower = 3,
        might = 3,
        knowledge = 4,
        awareness = 1,
        color = "Red",
        defend = {5}
    },
    corbin = {
        hp = 12,
        speed = 3,
        stamina = 5,
        willpower = 2,
        might = 5,
        knowledge = 2,
        awareness = 2,
        color = "Red",
        defend = {5}
    },
    eliam = {
        hp = 12,
        speed = 5,
        stamina = 5,
        willpower = 3,
        might = 3,
        knowledge = 2,
        awareness = 3,
        color = "Red",
        defend = {7}
    },
    ["eliam uc"] = {
        hp = 12,
        speed = 5,
        stamina = 5,
        willpower = 3,
        might = 3,
        knowledge = 2,
        awareness = 3,
        color = "Red",
        defend = {7}
    },
    ["grisban the thirsty"] = {
        hp = 14,
        speed = 3,
        stamina = 4,
        willpower = 3,
        might = 5,
        knowledge = 2,
        awareness = 1,
        color = "Red",
        defend = {5}
    },
    ["hugo the glorious"] = {
        hp = 12,
        speed = 3,
        stamina = 3,
        willpower = 4,
        might = 4,
        knowledge = 1,
        awareness = 2,
        color = "Red",
        defend = {6}
    },
    ["hugo the glorious uc"] = {
        hp = 12,
        speed = 3,
        stamina = 4,
        willpower = 4,
        might = 4,
        knowledge = 1,
        awareness = 2,
        color = "Red",
        defend = {6}
    },
    karnon = {
        hp = 14,
        speed = 4,
        stamina = 3,
        willpower = 2,
        might = 6,
        knowledge = 1,
        awareness = 2,
        color = "Red",
        defend = {5}
    },
    krutzbeck = {
        hp = 12,
        speed = 3,
        stamina = 4,
        willpower = 3,
        might = 4,
        knowledge = 2,
        awareness = 2,
        color = "Red",
        defend = {5}
    },
    ["laughin buldar"] = {
        hp = 14,
        speed = 3,
        stamina = 3,
        willpower = 2,
        might = 4,
        knowledge = 2,
        awareness = 3,
        color = "Red",
        defend = {5}
    },
    ["laughin buldar uc"] = {
        hp = 14,
        speed = 3,
        stamina = 3,
        willpower = 2,
        might = 4,
        knowledge = 2,
        awareness = 3,
        color = "Red",
        defend = {5}
    },
    ["lord hawthorne"] = {
        hp = 12,
        speed = 4,
        stamina = 3,
        willpower = 2,
        might = 4,
        knowledge = 3,
        awareness = 2,
        color = "Red",
        defend = {5}
    },
    mordrog = {
        hp = 14,
        speed = 4,
        stamina = 4,
        willpower = 3,
        might = 5,
        knowledge = 1,
        awareness = 2,
        color = "Red",
        defend = {5}
    },
    ["nanok of the blade"] = {
        hp = 12,
        speed = 4,
        stamina = 4,
        willpower = 2,
        might = 4,
        knowledge = 2,
        awareness = 3,
        color = "Red",
        defend = {6}
    },
    ["nara the fang"] = {
        hp = 10,
        speed = 5,
        stamina = 4,
        willpower = 2,
        might = 4,
        knowledge = 1,
        awareness = 4,
        color = "Red",
        defend = {5}
    },
    ["ocvist the dragonslayer"] = {
        hp = 12,
        speed = 4,
        stamina = 4,
        willpower = 3,
        might = 5,
        knowledge = 2,
        awareness = 1,
        color = "Red",
        defend = {5}
    },
    ["one fist"] = {
        hp = 10,
        speed = 4,
        stamina = 4,
        willpower = 3,
        might = 3,
        knowledge = 2,
        awareness = 3,
        color = "Red",
        defend = {5}
    },
    ["orkell the swift"] = {
        hp = 10,
        speed = 5,
        stamina = 5,
        willpower = 2,
        might = 4,
        knowledge = 1,
        awareness = 4,
        color = "Red",
        defend = {7}
    },
    ["pathfinder durik"] = {
        hp = 10,
        speed = 5,
        stamina = 4,
        willpower = 2,
        might = 3,
        knowledge = 2,
        awareness = 4,
        color = "Red",
        defend = {5}
    },
    ["reynhart the worthy"] = {
        hp = 12,
        speed = 4,
        stamina = 4,
        willpower = 4,
        might = 3,
        knowledge = 1,
        awareness = 3,
        color = "Red",
        defend = {5}
    },
    ["sir valadir"] = {
        hp = 12,
        speed = 4,
        stamina = 4,
        willpower = 4,
        might = 3,
        knowledge = 3,
        awareness = 1,
        color = "Red",
        defend = {5}
    },
    steelhorns = {
        hp = 14,
        speed = 4,
        stamina = 3,
        willpower = 3,
        might = 5,
        knowledge = 1,
        awareness = 2,
        color = "Red",
        defend = {5}
    },
    syndrael = {
        hp = 12,
        speed = 4,
        stamina = 4,
        willpower = 2,
        might = 4,
        knowledge = 3,
        awareness = 2,
        color = "Red",
        defend = {5}
    },
    tahlia = {
        hp = 14,
        speed = 3,
        stamina = 4,
        willpower = 3,
        might = 3,
        knowledge = 2,
        awareness = 3,
        color = "Red",
        defend = {5}
    },
    ["trenloe the strong"] = {
        hp = 12,
        speed = 3,
        stamina = 3,
        willpower = 4,
        might = 4,
        knowledge = 1,
        awareness = 2,
        color = "Red",
        defend = {5}
    },
    ["varikas the dead"] = {
        hp = 12,
        speed = 3,
        stamina = 3,
        willpower = 3,
        might = 4,
        knowledge = 2,
        awareness = 2,
        color = "Red",
        defend = {5}
    },
    ["varikas the dead uc"] = {
        hp = 12,
        speed = 3,
        stamina = 3,
        willpower = 3,
        might = 4,
        knowledge = 2,
        awareness = 2,
        color = "Red",
        defend = {5}
    }
}

function getHeroes() return heroes end
function getHero(params) return heroes[params.name:lower()] end

familiars = {
    ["reanimate"] = {hp = 4, attack = true, defend = false, dice = {1, 2}},
    ["summoned stone"] = {hp = 2, attack = false, defend = true, dice = {7}},
    ["wolf"] = {hp = 3, attack = true, defend = false, dice = {1, 2}}
}

function getFamiliars() return familiars end
function getFamiliar(params) return familiars[params.name:lower()] end
