Storages = {
    figures = {
        familiars = nil,
        heroes = nil,
        minions = nil,
        masters = nil,
        lieutenants = nil
    },
    cards = {
        items = {},
        monsters = {act1 = nil, act2 = nil},
        lieutenants = {act1 = nil, act2 = nil}
    },
    heroes = nil,
    classes = nil
}

function onload()
    for _, obj in pairs(getObjectsWithTag("Storage")) do
        if obj.hasTag("Figures") and obj.hasTag("Familiars") then
            Storages.figures.familiars = obj
        end

        if obj.hasTag("Figures") and obj.hasTag("Heroes") then
            Storages.figures.heroes = obj
        end

        if obj.hasTag("Figures") and obj.hasTag("Minions") then
            Storages.figures.minions = obj
        end

        if obj.hasTag("Figures") and obj.hasTag("Masters") then
            Storages.figures.masters = obj
        end

        if obj.hasTag("Figures") and obj.hasTag("Lieutenants") then
            Storages.figures.lieutenants = obj
        end

        if obj.hasTag("ItemCards") then Storages.cards.items = obj end

        if obj.hasTag("Cards") then
            if obj.hasTag("Act1") then
                if obj.hasTag("Monsters") then
                    Storages.cards.monsters.act1 = obj
                end

                if obj.hasTag("Lieutenants") then
                    Storages.cards.lieutenants.act1 = obj
                end
            end

            if obj.hasTag("Act2") then
                if obj.hasTag("Monsters") then
                    Storages.cards.monsters.act2 = obj
                end

                if obj.hasTag("Lieutenants") then
                    Storages.cards.lieutenants.act2 = obj
                end
            end

            if obj.hasTag("Items") then
                table.insert(Storages.cards.items, obj)
            end
        end

        if obj.hasTag("Generation") then
            if obj.hasTag("Heroes") then Storages.heroes = obj end

            if obj.hasTag("Classes") then Storages.classes = obj end
        end
    end
end

function getHero(params)
    local name = params.name:lower()
    local pos = params.pos
    local bag = Storages.heroes

    return get(bag, name, pos)
end

function getClass(params)
    local name = params.name:lower()
    local pos = params.pos
    local bag = Storages.classes

    return get(bag, name, pos)
end

function getMonsterCard(params)
    local name = params.name:lower()
    local act = params.act
    local pos = params.pos

    return getCard(name, pos, "monsters", act)
end

function getLieutenantCard(params)
    local name = params.name:lower()
    local act = params.act
    local pos = params.pos

    return getCard(name, pos, "lieutenants", act)
end

function getItemCard(params)
    local name = params.name:lower()
    local pos = params.pos

    return getCard(name, pos, "items")
end

function getCard(name, pos, from, act)
    local bag = Storages.cards[from]
    if act then bag = bag[act] end

    return get(bag, name, pos)
end

function getHeroFigure(params)
    local name = params.name:lower()
    local pos = params.pos

    return getFigure(name, pos, "heroes")
end

function getFamiliarFigure(params)
    local name = params.name:lower()
    local pos = params.pos

    return getFigure(name, pos, "familiars")
end

function getMinionFigure(params)
    local name = params.name:lower()
    local pos = params.pos

    return getFigure(name, pos, "minions")
end

function getMasterFigure(params)
    local name = params.name:lower()
    local pos = params.pos

    return getFigure(name, pos, "masters")
end

function getLieutenantFigure(params)
    local name = params.name:lower()
    local pos = params.pos

    return getFigure(name, pos, "lieutenants")
end

function getFigure(name, pos, from)
    local bag = Storages.figures[from]

    return get(bag, name, pos)
end

function get(bag, name, pos)
    message = message or true
    local obj = nil

    if type(bag) == "table" then
        for _, b in pairs(bag) do
            if obj then break end
            obj = finder(b, name, pos)
        end
    else
        obj = finder(bag, name, pos)
    end

    if not obj then
        broadcastToAll("Couldn't find: " .. name)
    else
        return obj
    end
end

function finder(bag, name, pos)
    local item = bag.takeObject({pos = pos})
    for i, obj in ipairs(item.getObjects()) do
        if string.match(obj.name:lower(), name) then
            obj = item.takeObject({
                position = pos,
                index = i - 1,
                smooth = false
            })
            item.destruct()
            return obj
        end
    end

    item.destruct()
    return nil
end
