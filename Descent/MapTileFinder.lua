inputValue = ""
findOnlyOne = false

function onload()
    self.createButton({
        click_function = "search",
        function_owner = self,
        label = "Search",
        position = {0, 0.1, 0.35},
        scale = {0.3, 0.3, 0.3},
        width = 3600,
        height = 700,
        font_size = 600,
        color = {0.7573, 0.7573, 0.7573, 0},
        font_color = {0, 0, 0, 0}
    })

    self.createInput({
        input_function = "input",
        function_owner = self,
        label = "Tile Name",
        position = {0, 0.1, -0.25},
        scale = {0.5, 0.5, 0.5},
        width = 4000,
        height = 400,
        font_size = 200,
        alignment = 3,
        font_color = {0, 0, 0, 95},
        color = {0, 0, 0, 0}
    })
end

function isempty(s) return s == nil or s == '' end

function input(obj, player_clicker_color, input_value, selected)
    if not selected then inputValue = input_value end
    if input_value:sub(-1) == "\n" then
        inputValue = input_value:sub(1, -2)
        Wait.time(function()
            self.editInput({index = 0, value = inputValue})
        end, 0.2)
        search()
    end
end

function search()
    for num in string.gmatch(inputValue, "%d+") do doSearch(num) end

    Wait.time(function() self.editInput({index = 0, value = ""}) end, 1)
end

function doSearch(inputValue)
    local infinityBag_GUID = ""
    local pos = self.getPosition()
    local newPosBag = {x = pos.x - 20, y = pos.y, z = pos.z}
    local newPosObj = {x = pos.x - 20, y = pos.y, z = pos.z}
    local found = false

    if string.match(inputValue, "S1") then
        infinityBag_GUID = "966033"
    elseif string.match(inputValue, "S2") then
        infinityBag_GUID = "69d9dc"
    else
        local inputValueInt = tonumber(inputValue)

        if not isempty(inputValueInt) then
            if (1 <= inputValueInt and inputValueInt <= 30) then
                infinityBag_GUID = "5236c3"
            elseif (31 <= inputValueInt and inputValueInt <= 35) then
                infinityBag_GUID = "966033"
            elseif (36 <= inputValueInt and inputValueInt <= 43) then
                infinityBag_GUID = "53fb31"
            elseif (44 <= inputValueInt and inputValueInt <= 49) then
                infinityBag_GUID = "69d9dc"
            elseif (50 <= inputValueInt and inputValueInt <= 69) then
                infinityBag_GUID = "3d1786"
            elseif (70 <= inputValueInt and inputValueInt <= 77) then
                infinityBag_GUID = "6efc7d"
            elseif (78 <= inputValueInt and inputValueInt <= 87) then
                infinityBag_GUID = "9338ac"
            elseif (88 <= inputValueInt and inputValueInt <= 98) then
                infinityBag_GUID = "952487"
            end
        end
    end

    if not isempty(infinityBag_GUID) then
        local infinityBag = getObjectFromGUID(infinityBag_GUID)
        local bag = infinityBag.takeObject({position = newPosBag})

        for _, obj in pairs(bag.getObjects()) do
            if obj.name:lower() == inputValue:lower() then
                local newObj = bag.takeObject({
                    position = newPosObj,
                    smooth = false,
                    guid = obj.guid
                })
                newObj.setScale({x = 1.76, y = 1.76, z = 1.76})
                bag.destruct()
                if findOnlyOne then return end
                found = true
            end
        end
    end
    if not found then broadcastToAll("Not Found", {1, 0, 0}) end
end
