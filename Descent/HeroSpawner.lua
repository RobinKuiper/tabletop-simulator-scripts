ScriptAdder = nil
StorageFinder = nil

index = 1

function onLoad()
    for _, v in pairs(getObjects()) do
        if v.getName() == "ScriptAdder" then ScriptAdder = v end
        if v.getName() == "StorageFinder" then StorageFinder = v end
    end
end

function spawn()
    index = 1
    spawnHeroes()
    spawnFamiliars()

    Wait.frames(function() Global.call("sync") end, 20)
end

function spawnHeroes()
    local pos = self.getPosition()
    for k, v in pairs(getObjectsWithTag("HeroCard")) do
        local heroName = v.getName()
        local heroStats = v.getTable("Stats")

        local newPos = {
            x = (pos.x - 5) + (2 * index),
            y = pos.y,
            z = (pos.z + -3)
        }
        local obj = StorageFinder.call("getHeroFigure",
                                       {name = heroName, pos = newPos})
        obj.setRotation({0, 270, 0})

        obj.setScale({1.55, 1.55, 1.55})
        local color = heroStats.color and Color.fromString(heroStats.color) or
                          v.getColorTint()
        obj.setColorTint(color)
        heroStats["player"] = true
        obj.addTag("HeroFigure")
        ScriptAdder.call("addHpBarScript", {object = obj, data = heroStats})
        index = index + 1
    end
end

function spawnFamiliars()
    local pos = self.getPosition()
    for i, v in ipairs(getObjectsWithTag("FamiliarCard")) do
        local familiarName = v.getName()
        local familiarStats = v.getTable("Stats")
        local script = familiarStats and type(familiarStats) == "table" and
                           next(familiarStats) and true or false

        local newPos = {
            x = (pos.x - 5) + (2 * index),
            y = pos.y,
            z = (pos.z + -3)
        }

        local obj = StorageFinder.call("getFamiliarFigure",
                                       {name = familiarName, pos = newPos})
        obj.setRotation({0, 270, 0})
        -- obj.setScale({1.55, 1.55, 1.55})
        if script then
            familiarStats["player"] = true
            obj.addTag("FamiliarFigure")
            ScriptAdder.call("addHpBarScript",
                             {object = obj, data = familiarStats})
            obj.setColorTint(v.getColorTint())
        end
        index = index + 1
    end
end
