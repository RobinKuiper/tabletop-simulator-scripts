QuickSlots = {red = {}, green = {}, blue = {}, yellow = {}}

function onLoad(saved_data)
    registerShortcuts()
    registerContextMenus()
    loadQuickslots(saved_data)
end

function onSave()
    local saved_data = {}
    for color, v in pairs(QuickSlots) do
        saved_data[color] = {}
        for number, v2 in pairs(v) do saved_data[color][number] = v2 end
    end

    return JSON.encode(saved_data)
end

function onObjectSpawn(object)
    if object.hasTag("Herocard") or object.hasTag("ItemCard") or
        object.hasTag("FamiliarCard") then registerContextMenuItems(object) end
end

function loadQuickslots(saved_data)
    quickslots = JSON.decode(saved_data)
    if quickslots and quickslots.red then
        for color, v in pairs(quickslots) do
            QuickSlots[color] = {}
            for number, v2 in pairs(v) do
                if v2 then
                    local object = getObjectFromGUID(v2)
                    local id = color .. "_" .. number
                    QuickSlots[color][number] = object.getGUID()
                    Global.UI.setAttribute(id, "text", object.getName())
                end
            end
        end
    end
end

function registerContextMenus()
    for _, obj in pairs(getObjects()) do
        if obj.hasTag("Herocard") or obj.hasTag("ItemCard") or
            obj.hasTag("FamiliarCard") then registerContextMenuItems(obj) end
    end
end

function registerContextMenuItems(obj)
    obj.addContextMenuItem("Quickslot 1",
                           function(color, object_position, object)
        addQuickSlotFunction(object, color, 1)
    end, false)

    obj.addContextMenuItem("Quickslot 2",
                           function(color, object_position, object)
        addQuickSlotFunction(object, color, 2)
    end, false)

    obj.addContextMenuItem("Quickslot 3",
                           function(color, object_position, object)
        addQuickSlotFunction(object, color, 3)
    end, false)

    obj.addContextMenuItem("Quickslot 4",
                           function(color, object_position, object)
        addQuickSlotFunction(object, color, 4)
    end, false)
end

function registerShortcuts()
    addHotkey("Quickslot 1", function(color, object, position, isKeyUp)
        if not isKeyUp then return end

        local player = nil
        for _, p in ipairs(Player.getPlayers()) do
            if p.color == color then
                player = p
                break
            end
        end
        doQuickSlotAction(player, 1)
    end, true)

    addHotkey("Quickslot 2", function(color, object, position, isKeyUp)
        if not isKeyUp then return end

        local player = nil
        for _, p in ipairs(Player.getPlayers()) do
            if p.color == color then
                player = p
                break
            end
        end
        doQuickSlotAction(player, 2)
    end, true)

    addHotkey("Quickslot 3", function(color, object, position, isKeyUp)
        if not isKeyUp then return end

        local player = nil
        for _, p in ipairs(Player.getPlayers()) do
            if p.color == color then
                player = p
                break
            end
        end
        doQuickSlotAction(player, 3)
    end, true)

    addHotkey("Quickslot 4", function(color, object, position, isKeyUp)
        if not isKeyUp then return end

        local player = nil
        for _, p in ipairs(Player.getPlayers()) do
            if p.color == color then
                player = p
                break
            end
        end
        doQuickSlotAction(player, 4)
    end, true)
end

function addQuickSlotFunction(object, color, number)
    color = color:lower()
    local id = color .. "_" .. number
    QuickSlots[color][number] = object.getGUID()
    Global.UI.setAttribute(id, 'text', object.getName())
end

function exResync()
    for color, v in pairs(QuickSlots) do
        for number, v2 in pairs(v) do
            if v2 then
                local object = getObjectFromGUID(v2)
                local id = color .. "_" .. number
                Global.UI.setAttribute(id, "text", object.getName())
            end
        end
    end
end

function exQuickSlotAction(params)
    local player = params.player
    local number = params.number

    doQuickSlotAction(player, number)
end

function doQuickSlotAction(player, number)
    local color = player.color:lower()

    if QuickSlots[color] and QuickSlots[color][number] then
        local obj = getObjectFromGUID(QuickSlots[color][number])
        obj.call("doActionEx", {player = player, value = "", id = "attack"})
    else
        broadcastToColor(
            "You haven't configured this quickslot yet. Right click an object and select quickslot " ..
                number .. " to register this quickslot.", player.color, "Orange")
    end
end
