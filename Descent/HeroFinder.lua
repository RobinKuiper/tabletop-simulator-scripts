inputValue = ""

ScriptAdder = nil
StorageFinder = nil
Data = nil

options = {hero = true, class = false}

function onLoad(saved_data)
    local data = JSON.decode(saved_data)
    if data then
        options.hero = data.hero
        options.class = data.class

        if options.hero then
            setCheckboxOn("hero")
            setCheckboxOff("class")
        else
            setCheckboxOn("class")
            setCheckboxOff("hero")
        end
    end

    for i, obj in ipairs(getObjects()) do
        if obj.getName() == "ScriptAdder" then ScriptAdder = obj end
        if obj.getName() == "StorageFinder" then StorageFinder = obj end
        if obj.getName() == "Data" then Data = obj end
    end

    self.createButton({
        click_function = "search",
        function_owner = self,
        label = "Search",
        position = {0, 0.1, 0.35},
        scale = {0.3, 0.3, 0.3},
        width = 3600,
        height = 700,
        font_size = 600,
        color = {0.7573, 0.7573, 0.7573, 0},
        font_color = {0, 0, 0, 0}
    })

    self.createInput({
        input_function = "input",
        function_owner = self,
        label = "Name",
        position = {0, 0.1, -0.25},
        scale = {0.5, 0.5, 0.5},
        width = 4000,
        height = 400,
        font_size = 200,
        alignment = 3,
        font_color = {0, 0, 0, 95},
        color = {0, 0, 0, 0}
    })
end

function onSave() return JSON.encode(options) end

function isempty(s) return s == nil or s == '' end

function input(obj, player_clicker_color, input_value, selected)
    if not selected then inputValue = input_value end
    if input_value:sub(-1) == "\n" then
        inputValue = input_value:sub(1, -2)
        Wait.time(function()
            self.editInput({index = 0, value = inputValue})
        end, 0.2)
        search()
    end
end

function search()
    if isempty(inputValue) then return end

    local getObjFunc = options.hero and "getHero" or "getClass"
    local obj = StorageFinder.call(getObjFunc, {
        name = inputValue,
        pos = self.getPosition()
    })

    if not obj then return end

    if options.hero then
        handleHero(obj)
    else
        handleClass(obj)
    end

    Wait.time(function() self.editInput({index = 0, value = ""}) end, 1)
end

function handleClass(infClassBag)
    local pos = self.getPosition()
    local objPos = {x = pos.x - 10, y = pos.y, z = pos.z}
    local classBag = infClassBag.takeObject({position = objPos, smooth = false})
    infClassBag.destruct()

    local i = 0
    local j = 0
    for _, itemInBag in pairs(classBag.getObjects()) do
        local item = classBag.takeObject({
            position = {objPos.x, objPos.y, objPos.z + (3 * i)},
            rotation = {0, 180, 0},
            smooth = false,
            guid = itemInBag.guid
        })
        item.setRotation({0, 270, 0})
        setScale(item)
        i = i + 1

        local itemStats = Data.call("getClassItem", {name = item.getName()})
        local skill = Data.call("getSkill", {name = item.getName()})
        if itemStats then
            if itemStats.attack or itemStats.defend then
                ScriptAdder.call("addItemCardScript",
                                 {object = item, data = itemStats})
            end
            -- elseif skill then
            --     setScale(item)
            --     item.flip()
        elseif item.hasTag("SkillDeck") then
        elseif item.hasTag("FamiliarCard") then
            local stats = Data.call("getFamiliar", {name = item.getName()})
            ScriptAdder.call("addFamiliarCardScript",
                             {object = item, stats = stats})
        else
            j = j + 1
        end
    end
    classBag.destruct()
end

function handleHero(obj)
    local pos = self.getPosition()
    local objPos = {x = pos.x - 10, y = pos.y, z = pos.z}
    obj.setPosition(objPos)
    obj.setRotation({0, 270, 0})
    obj.addTag("HeroCard")

    local stats = Data.call("getHero", {name = obj.getName()})
    ScriptAdder.call("addHeroCardScript", {object = obj, stats = stats})
end

function setScale(obj)
    local original = obj.getScale()
    local add = original.x < 0.5 and 0.10 or 0.37
    obj.setScale({original.x + add, original.y, original.z + add})
end

function toggleCheckBox(player, value, id)
    if self.UI.getAttribute(id, "value") == "false" then
        setCheckboxOn(id)

        if id == "class" then
            setCheckboxOff("hero")
        elseif id == "hero" then
            setCheckboxOff("class")
        end
    else
        setCheckboxOff(id)

        if id == "class" then
            setCheckboxOn("hero")
        elseif id == "hero" then
            setCheckboxOn("class")
        end
    end
end

function setCheckboxOn(id)
    self.UI.setAttribute(id, "value", "true")
    self.UI.setAttribute(id, "color", "#154c79")
    options[id] = true
end

function setCheckboxOff(id)
    self.UI.setAttribute(id, "value", "false")
    self.UI.setAttribute(id, "color", "white")
    options[id] = false
end
