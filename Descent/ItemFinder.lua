inputValue = ""

shopPanel = nil
StorageFinder = nil
ScriptAdder = nil
Data = nil

function onload()
    for _, v in pairs(getObjects()) do
        if v.getName() == "ShopPanel" then shopPanel = v end
        if v.getName() == "StorageFinder" then StorageFinder = v end
        if v.getName() == "ScriptAdder" then ScriptAdder = v end
        if v.getName() == "Data" then Data = v end
    end

    self.createButton({
        click_function = "search",
        function_owner = self,
        label = "Search",
        position = {0, 0.1, 0.35},
        scale = {0.3, 0.3, 0.3},
        width = 3600,
        height = 700,
        font_size = 600,
        color = {0.7573, 0.7573, 0.7573, 0},
        font_color = {0, 0, 0, 0}
    })

    self.createInput({
        input_function = "input",
        function_owner = self,
        label = "Item Names",
        position = {0, 0.1, -0.25},
        scale = {0.5, 0.5, 0.5},
        width = 8000,
        height = 400,
        font_size = 200,
        alignment = 3,
        font_color = {0, 0, 0, 95},
        color = {0, 0, 0, 0}
    })
end

function isempty(s) return s == nil or s == '' end

function input(obj, player_clicker_color, input_value, selected)
    if not selected then inputValue = input_value end
    if input_value:sub(-1) == "\n" then
        inputValue = input_value:sub(1, -2)
        Wait.time(function()
            self.editInput({index = 0, value = inputValue})
        end, 0.2)
        search()
    end
end

function search()
    local i = 1
    for itemName in inputValue:gmatch("%s*([^,]+),?%s*") do
        doSearch(itemName, i)
        i = i + 1
    end

    Wait.time(function() self.editInput({index = 0, value = ""}) end, 1)
end

function doSearch(name, index)
    local pos = calcPosition(index)
    local obj = StorageFinder.call("getItemCard", {name = name, pos = pos})
    if not obj then return end
    obj.setRotation({0, 270, 0})
    -- obj.flip()
    obj.addTag("ItemCard")

    obj.setName(string.gsub(obj.getName(), "/", "_"))
    name = obj.getName():lower()

    local index = string.find(name, "_") -- find the index of the first occurrence of '/'
    if index then
        name = string.sub(name, 1, index - 1) -- get the substring before the '/'
    end

    local itemStats = Data.call("findItem", {name = name})
    if itemStats then
        if itemStats.defend then
            obj.addTag("Armor")
        elseif itemStats.attack then
            obj.addTag("Weapon")
        end

        -- addScript(obj, itemStats)
        ScriptAdder.call("addItemCardScript", {object = obj, data = itemStats})
    end
end

function calcPosition(index)
    local pos = shopPanel.getPosition()
    local positions = {
        {x = pos.x + 1.06, y = pos.y + 0.5, z = pos.z + 3.33},
        {x = pos.x + 1.06, y = pos.y + 0.5, z = pos.z + 0.11},
        {x = pos.x + 1.06, y = pos.y + 0.5, z = pos.z - 3.31},
        {x = pos.x - 3.65, y = pos.y + 0.5, z = pos.z + 3.33},
        {x = pos.x - 3.65, y = pos.y + 0.5, z = pos.z + 0.11},
        {x = pos.x - 3.65, y = pos.y + 0.5, z = pos.z - 3.31}
    }
    -- local positions = {
    --     {x = pos.x - 3.31, y = pos.y + 0.5, z = pos.z + 1.06},
    --     {x = pos.x, y = pos.y + 0.5, z = pos.z + 1.06},
    --     {x = pos.x + 3.31, y = pos.y + 0.5, z = pos.z + 1.06},
    --     {x = pos.x - 3.31, y = pos.y + 0.5, z = pos.z - 3.61},
    --     {x = pos.x, y = pos.y + 0.5, z = pos.z - 3.61},
    --     {x = pos.x - 3.31, y = pos.y + 0.5, z = pos.z - 3.61}
    -- }
    return positions[index]
end
