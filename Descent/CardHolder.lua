Cards = {{guid = nil}, {guid = nil}, {guid = nil}, {guid = nil}, {guid = nil}}

-- function onLoad()
--     local matPos = self.getPosition()
--     local first = {
--         {position = {x = matPos.x + 3.9, y = 0.75, z = matPos.z + 9.7}},
--         {position = {x = matPos.x + 3.9, y = 0.75, z = matPos.z + 4.93}},
--         {position = {x = matPos.x + 3.9, y = 0.75, z = matPos.z}},
--         {position = {x = matPos.x + 3.9, y = 0.75, z = matPos.z - 5}},
--         {position = {x = matPos.x + 3.9, y = 0.75, z = matPos.z - 9.8}}
--     }
--     local second = {
--         {position = {x = matPos.x - 3.9, y = 0.75, z = matPos.z + 9.7}},
--         {position = {x = matPos.x - 3.9, y = 0.75, z = matPos.z + 4.93}},
--         {position = {x = matPos.x - 3.9, y = 0.75, z = matPos.z}},
--         {position = {x = matPos.x - 3.9, y = 0.75, z = matPos.z - 5}},
--         {position = {x = matPos.x - 3.9, y = 0.75, z = matPos.z - 9.8}}
--     }
--     log(first)
--     self.setSnapPoints(first)
--     self.setSnapPoints(second)
-- end

function ClaimFreePosition(params) Cards[params.index].guid = params.guid end

function GetFreePosition()
    for i, card in ipairs(Cards) do
        if card.guid == nil or not getObjectFromGUID(card.guid) then
            card.index = i
            card.positions = calculatePosition(i)
            return card
        end
    end
end

function calculatePosition(index)
    local matPos = self.getPosition()
    local first = {
        {matPos.x + 3.9, matPos.y + 0.75, matPos.z + 9.7},
        {matPos.x + 3.9, matPos.y + 0.75, matPos.z + 4.93},
        {matPos.x + 3.9, matPos.y + 0.75, matPos.z},
        {matPos.x + 3.9, matPos.y + 0.75, matPos.z - 5},
        {matPos.x + 3.9, matPos.y + 0.75, matPos.z - 9.8}
    }
    local second = {
        {matPos.x - 3.9, matPos.y + 0.75, matPos.z + 9.7},
        {matPos.x - 3.9, matPos.y + 0.75, matPos.z + 4.93},
        {matPos.x - 3.9, matPos.y + 0.75, matPos.z},
        {matPos.x - 3.9, matPos.y + 0.75, matPos.z - 5},
        {matPos.x - 3.9, matPos.y + 0.75, matPos.z - 9.8}
    }
    return {first = first[index], second = second[index]}
end
