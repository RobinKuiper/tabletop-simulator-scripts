function addDie(player, value, id)
    local diceRoller = getClosestDiceRollerToHand(player)
    if not diceRoller then return end
    diceRoller.call("click_roll",
                    {dieIndex = tonumber(id), color = player.color})
end

function roll(player, value, id)
    local diceRoller = getClosestDiceRollerToHand(player)
    if not diceRoller then return end
    diceRoller.call("rollDice")
end

function test(player, value, id)
    local diceRoller = getClosestDiceRollerToHand(player)
    if not diceRoller then return end
    diceRoller.call("click_roll", {dieIndex = tonumber(5), color = player.color})
    diceRoller.call("click_roll", {dieIndex = tonumber(6), color = player.color})
    -- Wait.frames(function() diceRoller.call("rollDice") end, 10)
    function coroutine_rollDice()
        wait(0.5)
        diceRoller.call("rollDice")
        return 1
    end
    startLuaCoroutine(self, "coroutine_rollDice")
end

function clear(player, value, id)
    local diceRoller = getClosestDiceRollerToHand(player)
    if not diceRoller then return end
    diceRoller.call("clearDice")
end

function getClosestDiceRollerToHand(player)
    local hand = player.getHandTransform(1)
    if not hand then
        broadcastToColor("You don't control a hand, pick a different color.",
                         player.color, "White")
        return
    end
    return getClosestObjectWithTag({
        tag = "DiceRoller",
        fromPosition = hand.position
    })
end
