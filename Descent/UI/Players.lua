function modifyHP(player, value, id)
    local guid = value
    lock = true
    local obj = getObjectFromGUID(guid)
    obj.call("onClickEx", {player = player, guid = -1, id = id})
    Wait.frames(function()
        local hpT = obj.UI.getAttribute("hpText", "text")
        local num1, num2 = hpT:match("([^/]+)/([^/]+)")
        local perc = (num1 / num2) * 100
        self.UI.setAttribute(guid .. "_hpText", "text", "♥ " .. hpT)
        self.UI.setAttribute(guid .. "_hpBar", "percentage", perc)

        if perc == 0 then
            self.UI.setAttribute(guid .. "_koImg", "active", true)
        else
            self.UI.setAttribute(guid .. "_koImg", "active", false)
        end

        lock = false
    end, 1)
end

function modifyStamina(player, value, id)
    local guid = value
    lock = true
    local obj = getObjectFromGUID(guid)
    obj.call("onClickEx", {player = player, guid = -1, id = id .. "E"})
    Wait.frames(function()
        local eT = obj.UI.getAttribute("extraText", "text")
        local num1, num2 = eT:match("([^/]+)/([^/]+)")
        local perc = (num1 / num2) * 100
        self.UI.setAttribute(guid .. "_staminaText", "text", "❣ " .. eT)
        self.UI.setAttribute(guid .. "_staminaBar", "percentage", perc)
        lock = false
    end, 1)
end
