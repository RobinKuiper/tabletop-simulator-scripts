function updateQuickSlots()
    local QuickSlots = getObjectWithName("QuickSlots")
    QuickSlots.call("exResync")

    -- for k, v in pairs(QuickSlots) do
    --     for k2, v2 in pairs(v) do
    --         if v2 then
    --             self.UI.setAttribute(k .. "_" .. k2, "text", v2.getName())
    --         end
    --     end
    -- end
end

function click_QuickSlotAction(player, value, id)
    local QuickSlots = getObjectWithName("QuickSlots")
    local number = tonumber(string.match(id, "%d+$"))
    QuickSlots.call("exQuickSlotAction", {player = player, number = number})
end
