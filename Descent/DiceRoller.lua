-- Dice Clicker Strip by MrStump modified by Robin Kuiper
-- You may edit the below variables to change some of the tool's functionality
-- By default, this tool can roll 6 different dice types (d4-d20)
-- If you put a url into the quotes, you can replace a die with a custom_dice
-- If you put a name into the quotes, you can replace the default naming
-- Changing the number of sides will make the tool's images not match (obviously)
-- Only supports custom_dice, not custom models or asset bundles
ref_diceCustom = {
    {
        disabled = false,
        url = "http://cloud-3.steamusercontent.com/ugc/794238861429541623/7E98872087C8C3B2969A506EF71A11F82B601083/",
        name = "Blue",
        sides = 6,
        values = {
            "3♥♥", "6♥↯", "2♥♥↯", "5♥", "4♥♥", "❌❌X❌"
        }
    }, {
        disabled = false,
        url = "http://cloud-3.steamusercontent.com/ugc/794238861429542307/AE3DEC2D611A233F512FB0BE07AF0F86839CD449/",
        name = "Red",
        sides = 6,
        values = {
            "♥♥", "♥♥♥↯", "♥♥", "♥♥♥", "♥♥", "♥"
        }
    }, {
        disabled = false,
        url = "http://cloud-3.steamusercontent.com/ugc/794238861429543704/32CAA869EE2438448869734D5762EA2DAB91C3CF/",
        name = "Yellow",
        sides = 6,
        values = {"2♥", "♥♥↯", "1♥", "♥♥", "♥↯", "1↯"}
    }, {
        disabled = false,
        url = "http://cloud-3.steamusercontent.com/ugc/794238861429544304/77E014CDBCAF5C447938CA8DC24FF6E795641703/",
        name = "Green",
        sides = 6,
        values = {"1↯", "1♥↯", "♥", "♥↯", "1♥", "↯"}
    }, {
        disabled = false,
        url = "http://cloud-3.steamusercontent.com/ugc/794238861429545826/5775F4E97F634E439B58D2369DDE8DC877CD21F8/",
        name = "Grey",
        sides = 6,
        values = {"▼", "▼▼▼", "▼", "▼▼", "▼", "-"}
    }, {
        disabled = false,
        url = "http://cloud-3.steamusercontent.com/ugc/794238861429546515/599C227F2D2EF5B5BFE1E31F435BC13E9426B5FD/",
        name = "Black",
        sides = 6,
        values = {
            "▼▼", "▼▼▼▼", "▼▼", "▼▼▼", "▼▼", "-"
        }
    }, {
        disabled = false,
        url = "http://cloud-3.steamusercontent.com/ugc/794238861429544965/FF30FDF84785BFFB9837B93814680ADE4F16977A/",
        name = "Brown",
        sides = 6,
        values = {"-", "▼▼", "▼", "-", "-", "▼"}
    }
}
-- Note: Names on dice will overrite default die naming "d4, d6, d8, etc"

-- If results print on 1 line or multiple lines (true or false)
announce_singleLine = true
-- If last player to click button before roll happens gets their name announced
announce_playerName = true
-- If last player to click button before roll is used to color name/total/commas
announce_playerColor = true
-- If individual results are displayed (true or false)
announce_each = true
-- Choose what color the print results are Options:
-- "default" = All text is white
-- "player" = All text is color-coded to player that clicked roll last
-- "die" = Results are colored to match the die color
announce_color = "die"

range = 0
heart = 0
surge = 0
shield = 0
miss = false
exColor = "White"
inUse = false

-- END OF VARIABLES TO EDIT WITHOUT SCRIPTING KNOWLEDGE

-- Save to track currently active dice for disposal on load
function onSave()
    local currentDiceGUIDs = {}
    for _, obj in ipairs(currentDice) do
        if obj ~= nil then
            table.insert(currentDiceGUIDs, obj.die.getGUID())
        end
    end

    local saved_data = JSON.encode({
        ["guids"] = currentDiceGUIDs,
        ["saves"] = savedDice
    })
    return saved_data
end

function onLoad(saved_data)
    -- Loads the save of any active dice and deletes them
    savedDice = {}
    if saved_data ~= "" then
        local loaded_data = JSON.decode(saved_data)
        for _, guid in ipairs(loaded_data.guids) do
            local obj = getObjectFromGUID(guid)
            if obj ~= nil then destroyObject(obj) end
        end
        if loaded_data.saves ~= nil then savedDice = loaded_data.saves end
    end
    currentDice = {}

    cleanupDice()

    spawnRollButtons()
    currentDice = {}

    self.createButton({
        click_function = "rollDice",
        function_owner = self,
        label = "Roll",
        position = {-0.5, 0.05, 1.50},
        width = 600,
        height = 400
    })

    self.createButton({
        click_function = "clearDice",
        function_owner = self,
        label = "Clear",
        position = {0.5, 0.05, 1.50},
        width = 600,
        height = 400
    })
end

-- Activated by click
function click_roll(params)
    local color = params.color
    local dieIndex = params.dieIndex
    local altClick = params.altClick

    exColor = color
    inUse = color

    -- Dice spam protection, can be disabled up at top of script
    local diceCount = 0
    for _ in pairs(currentDice) do diceCount = diceCount + 1 end

    -- Check for if click is allowed
    if rollInProgress == nil then
        -- Find dice positions, moving previously spawned dice if needed
        local angleStep = 360 / (#currentDice + 1)
        for i, obj in ipairs(currentDice) do
            obj.die.setPositionSmooth(getPositionInLine(i), false, true)
        end

        -- Determines type of die to spawn (custom or not, number of sides)
        local spawn_type = "Custom_Dice"
        local spawn_sides = ref_diceCustom[dieIndex].sides
        local spawn_scale = 1
        if ref_diceCustom[dieIndex].url == "" then
            spawn_type = ref_defaultDieSides[dieIndex]
        end

        -- Spawns (or remove) that die
        if altClick then
            for i, obj in ipairs(currentDice) do
                local sides = getSidesFromDie(obj.die)
                if sides == spawn_sides then
                    table.remove(currentDice, i)
                    destroyObject(obj.die)
                    break
                end
            end
        else
            local spawn_pos = getPositionInLine(#currentDice + 1)
            local spawnedDie = spawnObject({
                type = spawn_type,
                position = spawn_pos,
                rotation = randomRotation(),
                scale = {spawn_scale, spawn_scale, spawn_scale}
            })
            if spawn_type == "Custom_Dice" then
                spawnedDie.setCustomObject({
                    image = ref_diceCustom[dieIndex].url,
                    type = ref_customDieSides[tostring(spawn_sides)],
                    faces = ref_customDieSides[side]
                })
            end

            local data = {
                ["info"] = ref_diceCustom[dieIndex],
                ["die"] = spawnedDie
            }

            -- After die is spawned, actions to take on it
            table.insert(currentDice, data)
            spawnedDie.setLock(true)
            if ref_diceCustom[dieIndex].name ~= "" then
                spawnedDie.setName(ref_diceCustom[dieIndex].name)
            else
                spawnedDie.setName("d" .. ref_diceCustom[dieIndex].sides)
            end

            -- Timer starting
            Timer.destroy("clickRoller_" .. self.getGUID())
        end
    elseif rollInProgress == false then
        clearDice()
        click_roll(params)
    else
        Player[color].broadcast("Roll in progress.", {0.8, 0.2, 0.2})
    end
end

-- Die rolling

-- Rolls all the dice and then launches monitoring
function rollDice(obj, color)
    rollInProgress = true
    function coroutine_rollDice()
        for _, obj in ipairs(currentDice) do
            obj.die.setLock(false)
            obj.die.randomize()
            wait(0.1)
        end

        monitorDice(color)

        return 1
    end
    startLuaCoroutine(self, "coroutine_rollDice")
end

-- Monitors dice to come to rest
function monitorDice(color)
    function coroutine_monitorDice()
        repeat
            local allRest = true
            for _, obj in ipairs(currentDice) do
                if obj.die ~= nil and obj.die.resting == false then
                    allRest = false
                end
            end
            coroutine.yield(0)
        until allRest == true

        -- Announcement
        if announce_total == true or announce_each == true then
            displayResults(color)
        end

        wait(0.1)
        rollInProgress = false
        inUse = false

        -- Auto die removal
        if 60 ~= -1 then
            -- Timer starting
            Timer.destroy("clickRoller_cleanup_" .. self.getGUID())
            Timer.create({
                identifier = "clickRoller_cleanup_" .. self.getGUID(),
                function_name = "cleanupDice",
                function_owner = self,
                delay = 60
            })
        end

        return 1
    end
    startLuaCoroutine(self, "coroutine_monitorDice")
end

function getInUse() return inUse end

-- After roll broadcasting

function displayResults(color)
    color = color or exColor or "White"

    local resultTable = {}

    range = 0
    heart = 0
    surge = 0
    shield = 0
    miss = false
    misses = 0

    -- Tally result info
    for _, obj in ipairs(currentDice) do
        if obj ~= nil then
            -- Tally color info
            local textColor = {1, 1, 1}
            if announce_color == "player" then
                textColor = stringColorToRGB(color)
            elseif announce_color == "die" then
                textColor = obj.die.getColorTint()
            end
            -- Get die type
            local dSides = getSidesFromDie(obj.die)
            -- Tally value info
            local value = obj.info.values[obj.die.getValue()]
            heart = heart + select(2, string.gsub(value, "♥", ""))
            surge = surge + select(2, string.gsub(value, "↯", ""))
            shield = shield + select(2, string.gsub(value, "▼", ""))
            misses = misses + select(2, string.gsub(value, "❌❌X❌", ""))

            value:gsub("%d+", function(num)
                range = range + tonumber(num)
            end)

            -- Add to table
            table.insert(resultTable,
                         {value = value, color = textColor, sides = dSides})
        end
    end

    if misses > 0 then miss = true end

    -- Sort result table into order
    local sort_func = function(a, b) return a.value < b.value end
    table.sort(resultTable, sort_func)

    -- String assembly

    if announce_singleLine == true then
        -- THIS IF STATEMENT IS FOR SINGLE LINE
        local s = ""
        local s_color = {1, 1, 1}
        if announce_playerColor == true then
            s_color = stringColorToRGB(color)
        end

        -- if announce_each == true then
        --     for i, v in ipairs(resultTable) do
        --         local hex = RGBToHex(v.color)
        --         s = s .. hex
        --         if (v.sides == "-1" and v.value > 0) then
        --             s = s .. "+"
        --         end
        --         s = s .. v.value .. "[-]"
        --         if i ~= #resultTable then s = s .. ", " end
        --     end
        -- end

        if announce_playerName == true then
            local name = Player[color].steam_name or "System"
            s = name .. "     |     " .. s
        end

        if miss then
            s = "MISS"
        else
            if range > 0 then
                s = s .. "◎: " .. tostring(range) .. " | "
            end

            if heart > 0 then
                s = s .. "♥: " .. tostring(heart) .. " | "
            end

            if surge > 0 then
                s = s .. "↯: " .. tostring(surge) .. " | "
            end

            if shield > 0 then
                s = s .. "▼: " .. tostring(shield) .. " | "
            end
        end

        broadcastToAll(s, s_color)
    else
        -- THIS IF STATEMENT IS FOR MULTI LINE
        local s_color = {1, 1, 1}
        if announce_playerColor == true then
            s_color = stringColorToRGB(color)
        end

        if announce_playerName == true then
            broadcastToAll(Player[color].steam_name .. " has rolled:", s_color)
        end

        local s = ""
        local dSides = 0
        if announce_each == true then
            for _, refSides in ipairs(ref_customDieSides_rev) do
                for i, v in ipairs(resultTable) do
                    if v.sides == refSides then
                        local hex = RGBToHex(v.color)
                        if dSides ~= 0 then s = s .. ", " end
                        s = s .. hex .. v.value .. "[-]"
                        dSides = v.sides
                    end
                end
                if s ~= "" then
                    s = "d" .. dSides .. ")  " .. s
                    broadcastToAll(s, s_color)
                    s = ""
                    dSides = 0
                end
            end
        end

        s = "★: " .. tostring(success) .. " | ↯: " .. tostring(surge) ..
                " | +: " .. tostring(plus)
        broadcastToAll(s, s_color)
    end
end

-- Make a descriptive label for a set of dice

function diceToText()
    counts = {}
    types = {}
    for _, obj in ipairs(currentDice) do
        sides = tonumber(string.match(tostring(obj.die), "%d+"))
        if counts[sides] == nil then
            counts[sides] = 0
            table.insert(types, sides)
        end
        counts[sides] = counts[sides] + 1
    end

    table.sort(types)

    ret = {}
    for i = #types, 1, -1 do
        t = types[i]
        c = counts[t]
        table.insert(ret, {count = c, types = t})
    end

    return ret
end

-- Die cleanup

function cleanupDice()
    for _, obj in ipairs(currentDice) do
        if obj.die ~= nil then destroyObject(obj.die) end
    end

    Timer.destroy("clickRoller_cleanup_" .. self.getGUID())
    rollInProgress = nil
    currentDice = {}
end

function clearDice() cleanupDice() end

-- Utility functions

-- Return a position based on relative position on a line
function getPositionInLine(i)
    local totalDice = #currentDice + 3
    local totalWidth = 7
    -- Change total width here maybe
    local widthStep = 7 / (totalDice - 1)
    local x = -widthStep * i + (7 / 2)
    local y = 2
    local z = -2.2
    return self.positionToWorld({x, y, z})
end

-- Gets a random rotation vector
function randomRotation()
    -- Credit for this function goes to Revinor (forums)
    -- Get 3 random numbers
    local u1 = math.random();
    local u2 = math.random();
    local u3 = math.random();
    -- Convert them into quats to avoid gimbal lock
    local u1sqrt = math.sqrt(u1);
    local u1m1sqrt = math.sqrt(1 - u1);
    local qx = u1m1sqrt * math.sin(2 * math.pi * u2);
    local qy = u1m1sqrt * math.cos(2 * math.pi * u2);
    local qz = u1sqrt * math.sin(2 * math.pi * u3);
    local qw = u1sqrt * math.cos(2 * math.pi * u3);
    -- Apply rotation
    local ysqr = qy * qy;
    local t0 = -2.0 * (ysqr + qz * qz) + 1.0;
    local t1 = 2.0 * (qx * qy - qw * qz);
    local t2 = -2.0 * (qx * qz + qw * qy);
    local t3 = 2.0 * (qy * qz - qw * qx);
    local t4 = -2.0 * (qx * qx + ysqr) + 1.0;
    -- Correct
    if t2 > 1.0 then t2 = 1.0 end
    if t2 < -1.0 then ts = -1.0 end
    -- Convert back to X/Y/Z
    local xr = math.asin(t2);
    local yr = math.atan2(t3, t4);
    local zr = math.atan2(t1, t0);
    -- Return result
    return {math.deg(xr), math.deg(yr), math.deg(zr)}
end

-- Coroutine delay, in seconds
function wait(time)
    local start = os.time()
    repeat coroutine.yield(0) until os.time() > start + time
end

-- Turns an RGB table into hex
function RGBToHex(rgb)
    if rgb ~= nil then
        return "[" ..
                   string.format("%02x%02x%02x", rgb[1] * 255, rgb[2] * 255,
                                 rgb[3] * 255) .. "]"
    else
        return ""
    end
end

function getSidesFromDie(die)
    local dSides
    local dieCustomInfo = die.getCustomObject()
    if next(dieCustomInfo) then
        dSides = ref_customDieSides_rev[dieCustomInfo.type + 1]
    else
        dSides = tonumber(string.match(tostring(die), "%d+"))
    end
    return dSides
end
-- Button creation

function spawnRollButtons()
    for i, entry in ipairs(ref_diceCustom) do
        local funcName = "button_" .. i
        local func = function(_, c) click_roll({color = c, dieIndex = i}) end
        self.setVar(funcName, func)
        self.createButton({
            click_function = funcName,
            function_owner = self,
            color = ref_diceCustom[i].name,
            position = {-6.7 + (i - 1) * 2.23, 0.2, -0.04},
            height = 560,
            width = 560
        })
    end
end

-- Data tables

ref_customDieSides = {
    ["4"] = 0,
    ["6"] = 1,
    ["8"] = 2,
    ["10"] = 3,
    ["12"] = 4,
    ["20"] = 5
}

ref_customDieSides_rev = {4, 6, 8, 10, 12, 20}

ref_defaultDieSides = {"Die_4", "Die_6", "Die_8", "Die_10", "Die_12", "Die_20"}
