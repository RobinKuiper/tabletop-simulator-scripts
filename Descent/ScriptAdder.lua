Scripts = {
    HeroCardScript = nil,
    ItemCardScript = nil,
    HpBarScript = nil,
    MonsterCardScript = nil,
    FamiliarCardScript = nil
}

function onload()
    for k, obj in pairs(getObjectsWithTag("Script")) do
        Scripts[obj.getName()] = obj.getLuaScript()
    end
end

function addMonsterCardScript(params)
    local object = params.object
    local data = params.data
    local act = params.act

    local script = Scripts.MonsterCardScript
    local xml = script:sub(script:find("XMLStart") + 8,
                           script:find("XMLStop") - 1)
    local newScript = script:sub(script:find("LUAStart") + 8,
                                 script:find("LUAStop") - 1)

    -- newScript = "--[[StartXML\n" .. xml ..
    --                 "StopXML--xml]]\n" .. newScript

    attackDice = data.attackDice or 1
    defenseDice = data.defenseDice or 1
    attackDice2 = data.attackDice2 or 1
    defenseDice2 = data.defenseDice2 or 1

    newScript = newScript:gsub("attackDice = {1}",
                               "attackDice = {" .. attackDice .. "}")
    newScript = newScript:gsub("defenseDice = {1}",
                               "defenseDice = {" .. defenseDice .. "}")
    newScript = newScript:gsub("attackDice2 = {1}",
                               "attackDice2 = {" .. attackDice2 .. "}")
    newScript = newScript:gsub("defenseDice2 = {1}",
                               "defenseDice2 = {" .. defenseDice2 .. "}")

    if data.lieutenant then
        xml = xml:gsub("id=\"master\"", "id=\"master\" active=\"false\"")
        xml = xml:gsub("id=\"attack2\"", "id=\"attack2\" visibility=\"false\"")
        xml = xml:gsub("id=\"defend2\"", "id=\"defend2\" visibility=\"false\"")
        xml = xml:gsub("FFFAB8", "DD16E0")
        newScript = newScript:gsub("lieutenant = false", "lieutenant = true")
    end

    if act == 2 then newScript = newScript:gsub("act = 1", "act = 2") end

    -- object.UI.setXml(xml)
    newScript = "--[[StartXML\n" .. xml .. "StopXML--xml]]\n" .. newScript
    object.setLuaScript(newScript)
end

function addHeroCardScript(params)
    local object = params.object
    local stats = params.stats

    -- local stats = heroes[object.getName():lower()]
    local heroCardScript = Scripts.HeroCardScript
    local script = heroCardScript:sub(heroCardScript:find("LUAStart") + 8,
                                      heroCardScript:find("LUAStop") - 1)
    local xml = heroCardScript:sub(heroCardScript:find("XMLStart") + 8,
                                   heroCardScript:find("XMLStop") - 1)

    for stat, value in pairs(stats) do
        if stat == "defend" then
            local newValue = "{"
            for i, die in ipairs(value) do
                newValue = newValue .. die .. ", "
            end
            value = newValue .. "}"
            script = script:gsub("defenseDice = {1}", "defenseDice = " .. value)
        elseif stat == "color" then
            script = script:gsub("color = \"Green\"",
                                 "color = \"" .. value .. "\"")
        else
            script = script:gsub(stat .. " = 1", stat .. " = " .. value)
        end
    end

    script = "--[[StartXML\n" .. xml .. "StopXML--xml]]\n" .. script
    object.setLuaScript(script)
end

function addItemCardScript(params)
    local object = params.object
    local data = params.data

    local script = Scripts.ItemCardScript
    local xml = script:sub(script:find("XMLStart") + 8,
                           script:find("XMLStop") - 1)
    local newScript = script:sub(script:find("LUAStart") + 8,
                                 script:find("LUAStop") - 1)

    if data.defend then table.insert(data.dice, 5) end
    local dice = table.concat(data.dice, ",")

    newScript = newScript:gsub("dice = {1}", "dice = {" .. dice .. "}")

    if data.defend then
        newScript = newScript:gsub("attack = true", "attack = false")
        newScript = newScript:gsub("defend = false", "defend = true")
        xml = xml:gsub("text=\"Attack\"", "text=\"Defend\"")
        xml = xml:gsub("ATTACK", "false")
        xml = xml:gsub("DEFENSE", "true")
        -- xml = xml:gsub("FFFAB8", "DD16E0")
    else
        xml = xml:gsub("ATTACK", "true")
        xml = xml:gsub("DEFENSE", "false")
    end

    newScript = "--[[StartXML\n" .. xml .. "StopXML--xml]]\n" .. newScript

    object.setLuaScript(newScript)
end

function addHpBarScript(params)
    local object = params.object
    local data = params.data

    local player = data.player
    local hp = data.hp
    local stamina = data.stamina

    local assets = self.UI.getCustomAssets()
    local script = Scripts.HpBarScript
    local xml = script:sub(script:find("XMLStart") + 8,
                           script:find("XMLStop") - 1)
    local newScript = script:sub(script:find("LUAStart") + 8,
                                 script:find("LUAStop") - 1)
    local stats = "statNames = {"
    local xmlStats = ""
    for j, i in pairs(assets) do
        stats = stats .. i.name .. " = false, "
        xmlStats = xmlStats .. '<Button id="' .. i.name ..
                       '" color="#FFFFFF00" active="false"><Image image="' ..
                       i.name .. '" preserveAspect="true"></Image></Button>\n'
    end
    newScript = "--[[StartXML\n" .. xml:gsub("STATSIMAGE", xmlStats) ..
                    "StopXML--xml]]" .. stats:sub(1, -3) .. "}\n" .. newScript
    xml = xml:gsub("STATSIMAGE", xmlStats)

    object.setDescription(hp .. "/" .. hp)

    newScript = newScript:gsub("health = {value = 10, max = 10}",
                               "health = {value = " .. hp .. ", max = " .. hp ..
                                   "}")

    if stamina then
        newScript = newScript:gsub("extra = {value = 10, max = 10}",
                                   "extra = {value = " .. stamina .. ", max = " ..
                                       stamina .. "}")
        newScript = newScript:gsub("hideExtra = true,", "hideExtra = false,")
    end

    if mana then
        newScript = newScript:gsub("mana = {value = 10, max = 10}",
                                   "mana = {value = " .. stamina .. ", max = " ..
                                       stamina .. "}")
        newScript = newScript:gsub("hideMana = true,", "hideMana = false,")
    end

    if player then
        newScript = newScript:gsub('player = false', 'player = true')
        newScript = newScript:gsub('--SYNCPLACEHOLDER',
                                   'Wait.frames(function() Global.call("sync") end, 1)')
    else
        newScript = newScript:gsub("showBarButtons = false",
                                   "showBarButtons = true")
    end

    newScript = newScript:gsub('<VerticalLayout id="bars" height="200">',
                               '<VerticalLayout id="bars" height="' .. 200 +
                                   (hp == 0 and -100 or 0) +
                                   (0 ~= 0 and 100 or 0) .. '">')

    newScript = newScript:gsub('<Panel id="panel" position="0 0 -220"',
                               '<Panel id="panel" position="0 0 ' ..
                                   object.getBounds().size.y /
                                   object.getScale().y * 110 .. '"')
    object.setLuaScript(newScript)
end

function addFamiliarCardScript(params)
    local object = params.object
    local stats = params.stats

    local script = Scripts.FamiliarCardScript
    local xml = script:sub(script:find("XMLStart") + 8,
                           script:find("XMLStop") - 1)
    local newScript = script:sub(script:find("LUAStart") + 8,
                                 script:find("LUAStop") - 1)

    local dice = ""
    if stats then dice = table.concat(stats.dice, ",") end

    if stats then
        if not stats.attack and not stats.defend then
            xml = xml:gsub("active=\"true\"", "active=\"false\"")
        elseif stats.attack then
            xml = xml:gsub("id=\"attack\" active=\"false\"",
                           "id=\"attack\" active=\"true\"")
            newScript = newScript:gsub("attackDice = nil",
                                       "attackDice = {" .. dice .. "}")
        elseif stats.defend then
            xml = xml:gsub("id=\"defend\" active=\"false\"",
                           "id=\"defend\" active=\"true\"")
            newScript = newScript:gsub("defenseDice = nil",
                                       "defenseDice = {" .. dice .. "}")
        end

        if stats.hp then
            newScript = newScript:gsub("hp = 1", "hp = " .. stats.hp)
        end

        if stats.color then
            newScript = newScript:gsub("color = \"Green\"",
                                       "color = \"" .. stats.color .. "\"")
        end
    end

    newScript = "--[[StartXML\n" .. xml .. "StopXML--xml]]" .. newScript
    object.setLuaScript(newScript)
end
